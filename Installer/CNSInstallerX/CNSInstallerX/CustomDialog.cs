using System;
using System.Diagnostics;
using WixSharp;
using WixSharp.UI.Forms;
using Microsoft.Deployment.WindowsInstaller;

namespace WixSharpSetup
{
    public partial class CustomDialog : ManagedForm, IManagedDialog
    {
        public CustomDialog()
        {
            //NOTE: If this assembly is compiled for v4.0.30319 runtime, it may not be compatible with the MSI hosted CLR.
            //The incompatibility is particularly possible for the Embedded UI scenarios. 
            //The safest way to avoid the problem is to compile the assembly for v3.5 Target Framework.CNSInstallerX
            InitializeComponent();
        }

        void dialog_Load(object sender, EventArgs e)
        {
            //banner.Image = MsiRuntime.Session.GetEmbeddedBitmap("WixUI_Bmp_Banner");
            Text = "[ProductName] Setup";
            //resolve all Control.Text cases with embedded MSI properties (e.g. 'ProductName') and *.wxl file entries
            base.Localize();
        }

        void back_Click(object sender, EventArgs e)
        {
            Shell.GoPrev();
        }

        void next_Click(object sender, EventArgs e)
        {
            if (textBox4.Text.Trim() != "" && textBox2.Text.Trim() != "" && textBox3.Text.Trim() != "" && textBox1.Text.Trim() != "" && textBox5.Text.Trim() != "")
            {
                MsiRuntime.Session["PASSWORD"] = textBox4.Text;
                MsiRuntime.Session["DBNAME"] = textBox2.Text;
                MsiRuntime.Session["HOSTNAME"] = textBox1.Text;
                MsiRuntime.Session["USER"] = textBox3.Text;
                MsiRuntime.Session["DOMAIN"] = textBox5.Text;
                label8.Visible = false;
                Shell.GoNext();
            }
            else {
                label8.Visible = true;
            }
        }

        void cancel_Click(object sender, EventArgs e)
        {
            Shell.Cancel();
        }
        
    }
}