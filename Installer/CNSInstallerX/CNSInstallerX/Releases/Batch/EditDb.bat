echo off
set "host=nome_host"
cd "C:\Program Files (x86)\CNS\Batch"

echo assicurarsi di aver creato il database octoClienti

echo creazione utente e modifica database
start /wait "modifica DataBase" sqlcmd -E -S %host% -i ModificaDb.sql

echo migrate django 
start /wait migrations.bat

echo creazione utente per Octo
start /wait "genera utente" sqlcmd -E -S %host% -i cnsadmin.sql