USE [octoClienti]
GO

if not exists (select * from sysobjects where name='auth_user' and xtype='U' )
INSERT INTO Logs (LOG_description, LOG_date)
VALUES ('lanciare migration di django e script di creazione utente', GETDATE())
else
if ((select count (id) FROM [auth_user] where username='cnsadmin')=0)
INSERT INTO [dbo].[auth_user]
           ([password]
           ,[is_superuser]
           ,[username]
		   ,[first_name]
		   ,[last_name]
		   ,[email]
           ,[is_staff]
           ,[is_active]
           ,[date_joined])
     VALUES
           ('pbkdf2_sha256$36000$hR1VmwSsZLNk$yRgriT75/zyMD/5rYPmwyfrl2gt8rWF2hp5C8R9oEQ0='
           ,1
           ,'cnsadmin'
		   ,'CNS'
		   ,'Administrator'
		   ,''
           ,1
           ,1
           ,GETDATE())