/******		CREAZIONE LOGIN		******/
USE [master]
GO

If not Exists (select * from master.dbo.syslogins where name = 'Admin')
CREATE LOGIN [Admin] WITH PASSWORD=N'656365', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
ALTER SERVER ROLE [dbcreator] ADD MEMBER [Admin]
GO
ALTER SERVER ROLE [securityadmin] ADD MEMBER [Admin]
GO
ALTER SERVER ROLE [serveradmin] ADD MEMBER [Admin]
GO
ALTER SERVER ROLE [setupadmin] ADD MEMBER [Admin]
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [Admin]
GO

/******		CREAZIONE USER		******/
USE [octoClienti]
GO

/****** Object:  User [Admin]    Script Date: 30/07/2018 16:16:53 ******/
if not exists(SELECT * FROM sys.database_principals WHERE name ='Admin')
CREATE USER [Admin] FOR LOGIN [Admin]
GO

ALTER USER [Admin] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [Admin]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [Admin]
GO
ALTER ROLE [db_datareader] ADD MEMBER [Admin]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [Admin]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [Admin]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [Admin]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [Admin]
GO
ALTER ROLE [db_owner] ADD MEMBER [Admin]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [Admin]
GO
use [master]
GO
GRANT AUTHENTICATE SERVER TO [Admin]
GO
GRANT CONNECT SQL TO [Admin]
GO

USE [octoClienti]
GO

/******		GENERA TABELLE		******/

/****** Object:  Table [dbo].[Allarmi]    Script Date: 30/07/2018 16:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Allarmi' and xtype='U' )
CREATE TABLE [dbo].[Allarmi](
	[ALL_date] [datetime] NULL,
	[ALL_numeroAllarme] [varchar](1000) NULL,
	[ALL_messaggioAllarme] [varchar](1000) NULL,
	[ALL_numeroWarning] [varchar](1000) NULL,
	[ALL_messaggioWarning] [varchar](1000) NULL,
	[ALL_vs_MAC_id] [int] NOT NULL,
	[ALL_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Allarmi] PRIMARY KEY CLUSTERED 
(
	[ALL_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Cicloproduzione]    Script Date: 30/07/2018 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Cicloproduzione' and xtype='U' )
CREATE TABLE [dbo].[Cicloproduzione](
	[CPROD_id] [int] IDENTITY(1,1) NOT NULL,
	[CPROD_vs_MAC_id] [int] NOT NULL,
	[CPROD_commessa] [nvarchar](50) NOT NULL,
	[CPROD_importFrom] [nvarchar](50) NOT NULL,
	[CPROD_status] [int] NOT NULL,
	[CPROD_pezziFatti] [int] NOT NULL,
	[CPROD_lotto] [nvarchar](50) NOT NULL,
	[CPROD_programma] [nchar](10) NULL,
	[CPROD_fase] [nchar](10) NULL,
	[CPROD_pallet] [nchar](10) NULL,
	[CPROD_articolo] [nchar](10) NULL,
 CONSTRAINT [PK_Cicloproduzione] PRIMARY KEY CLUSTERED 
(
	[CPROD_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Componenti]    Script Date: 30/07/2018 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Componenti' and xtype='U' )
CREATE TABLE [dbo].[Componenti](
	[COM_id] [int] IDENTITY(1,1) NOT NULL,
	[COM_nome] [nchar](10) NOT NULL,
	[COM_chiave] [varchar](50) NULL,
	[COM_vita] [nchar](10) NULL,
	[COM_codice] [nchar](10) NULL,
	[COM_unita] [nvarchar](10) NULL,
 CONSTRAINT [PK_Componenti] PRIMARY KEY CLUSTERED 
(
	[COM_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
else 
ALTER TABLE Componenti ALTER COLUMN COM_chiave varchar(50); 
GO

/****** Object:  Table [dbo].[Input]    Script Date: 05/10/2018 11.46.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Input' and xtype='U' )
CREATE TABLE [dbo].[Input](
	[IN_id] [int] IDENTITY(1,1) NOT NULL,
	[IN_commessa] [nchar](50) NULL,
	[IN_lotto] [nchar](10) NULL,
	[IN_priorita] [int] NULL,
	[IN_importFrom] [nchar](10) NULL,
	[IN_status] [tinyint] NULL,
	[IN_data] [datetime] NULL,
	[IN_vs_USER_id] [int] NULL,
	[IN_programma] [nchar](10) NULL,
	[IN_vs_CPROD_id] [int] NULL,
	[IN_fase] [nchar](10) NULL,
	[IN_articolo] [nchar](10) NULL,
 CONSTRAINT [PK_Produzione_In] PRIMARY KEY CLUSTERED 
(
	[IN_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Interfaccia]    Script Date: 30/07/2018 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Interfaccia' and xtype='U' )
CREATE TABLE [dbo].[Interfaccia](
	[INT_id] [int] IDENTITY(1,1) NOT NULL,
	[INT_ipAdress] [varchar](20) NULL,
	[INT_status] [varchar](50) NULL,
	[INT_programmaAttivo] [varchar](50) NULL,
	[INT_feedAssi] [int] NULL,
	[INT_feedMandrino] [int] NULL,
	[INT_pezziFatti] [int] NULL,
	[INT_vs_MAC_id] [int] NULL,
	[INT_path] [int] NULL,
	[INT_date] [datetime] NULL,
	[INT_utensileAttivo] [int] NULL,
	[INT_totem] [nvarchar](10) NULL,
	[INT_Attivo] [tinyint] NULL,
	[INT_commessa] [varchar](50) NULL,
 CONSTRAINT [PK_Intefaccia] PRIMARY KEY CLUSTERED 
(
	[INT_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Letmanutenzione]    Script Date: 30/07/2018 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Letmanutenzione' and xtype='U' )
CREATE TABLE [dbo].[Letmanutenzione](
	[LMAN_Id] [int] IDENTITY(1,1) NOT NULL,
	[LMAN_data] [datetime] NOT NULL,
	[LMAN_vs_MAC_id] [int] NOT NULL,
 CONSTRAINT [PK_Letmanutenzione] PRIMARY KEY CLUSTERED 
(
	[LMAN_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Logs]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Logs' and xtype='U' )
CREATE TABLE [dbo].[Logs](
	[LOG_id] [int] IDENTITY(1,1) NOT NULL,
	[LOG_fk_USER_id] [int] NULL,
	[LOG_fk_MAC_id] [int] NULL,
	[LOG_description] [varchar](max) NULL,
	[LOG_status] [int] NULL,
	[LOG_date] [datetime] NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[LOG_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Macchine]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Macchine' and xtype='U' )
CREATE TABLE [dbo].[Macchine](
	[MAC_id] [int] IDENTITY(1,1) NOT NULL,
	[MAC_ipAdress] [varchar](20) NULL,
	[MAC_ipAdress2] [varchar](20) NULL,
	[MAC_nomeMacch] [varchar](20) NULL,
	[MAC_soprannomeMacch] [varchar](20) NULL,
	[MAC_tipoMacchina] [varchar](20) NULL,
	[MAC_controlloNumerico] [varchar](20) NULL,
	[MAC_tipoControllo] [varchar](5) NULL,
	[MAC_packegTipo] [varchar](5) NULL,
	[MAC_reparto] [int] NULL,
	[MAC_imagePath] [varchar](250) NULL,
	[MAC_image] [image] NULL,
	[MAC_serialNumber] [varchar](20) NULL,
	[MAC_boolCamera] [varchar](20) NULL,
	[MAC_ipAdressCamera] [varchar](20) NULL,
	[MAC_portaCamera] [varchar](20) NULL,
	[MAC_comandoCamera] [varchar](20) NULL,
	[MAC_modeCamera] [varchar](20) NULL,
	[MAC_userCamera] [varchar](20) NULL,
	[MAC_passCamera] [varchar](20) NULL,
	[MAC_userUsername] [varchar](50) NULL,
	[MAC_userPassword] [varchar](50) NULL,
	[MAC_vs_SER_id] [int] NULL,
	[MAC_numeroAssi] [int] NULL,
	[MAC_statoConnessione] [varchar](50) NULL,
	[MAC_pathshare] [varchar](50) NULL,
	[MAC_multipallet] [bit] NULL,
	[MAC_attivo] [bit] NOT NULL DEFAULT 1,
	[MAC_isola] [int] NULL,
	[MAC_assi] [varchar](10) NULL DEFAULT 'XYZ'
PRIMARY KEY CLUSTERED 
(
	[MAC_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
else
if COL_LENGTH('dbo.Macchine', 'MAC_assi') IS NULL
ALTER TABLE Macchine ADD MAC_assi varchar(10) NULL DEFAULT 'XYZ' WITH VALUES
GO

/****** Object:  Table [dbo].[Manutenzione2]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Manutenzione2' and xtype='U' )
CREATE TABLE [dbo].[Manutenzione2](
	[MAN2_id] [int] IDENTITY(1,1) NOT NULL,
	[MAN2_chiave] [nvarchar](50) NOT NULL,
	[MAN2_valore] [nvarchar](50) NOT NULL,
	[MAN2_vs_LMAN_id] [int] NOT NULL,
 CONSTRAINT [PK_Manutezione2] PRIMARY KEY CLUSTERED 
(
	[MAN2_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Marcature]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Marcature' and xtype='U' )
CREATE TABLE [dbo].[Marcature](
	[MAR_id] [int] NOT NULL,
	[MAR_vs_OPE_id] [int] NOT NULL,
	[MAR_vs_MAC_id] [int] NOT NULL,
	[MAR_vs_OUT_id] [int] NULL,
	[MAR_date] [datetime] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Operatori]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Operatori' and xtype='U' )
CREATE TABLE [dbo].[Operatori](
	[OPE_id] [int] NOT NULL,
	[OPE_badge] [nchar](10) NULL,
	[OPE_date] [datetime] NULL,
	[OPE_custom1] [nchar](10) NULL,
	[OPE_custom2] [nchar](10) NULL,
 CONSTRAINT [PK_Operatori] PRIMARY KEY CLUSTERED 
(
	[OPE_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Output]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Output' and xtype='U' )
CREATE TABLE [dbo].[Output](
	[OUT_id] [int] IDENTITY(1,1) NOT NULL,
	[OUT_tempoLavoro] [nchar](10) NULL,
	[OUT_tempoPiazzamento] [nchar](10) NULL,
	[OUT_tempoPausa] [nchar](10) NULL,
	[OUT_efficienza] [nchar](10) NULL,
	[OUT_priorita] [nchar](10) NULL,
	[OUT_vs_CPROD_id] [int] NOT NULL,
	[OUT_data] [datetime] NULL,
 CONSTRAINT [PK_Produzione_Out] PRIMARY KEY CLUSTERED 
(
	[OUT_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Produzione]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Produzione' and xtype='U' )
CREATE TABLE [dbo].[Produzione](
	[PROD_id] [int] IDENTITY(1,1) NOT NULL,
	[PROD_lotto] [nchar](10) NULL,
	[PROD_tempoLavoro] [nchar](10) NULL,
	[PROD_tempoPiazzamento] [nchar](10) NULL,
	[PROD_tempoPausa] [nchar](10) NULL,
	[PROD_efficienza] [nchar](10) NULL,
	[PROD_priorita] [int] NULL,
	[PROD_data] [datetime] NULL,
	[PROD_vs_CPROD_id] [int] NOT NULL,
	[PROD_appTempoLavoro] [nchar](10) NULL,
	[PROD_appTempoPiazzamento] [nchar](10) NULL,
	[PROD_appTempoPausa] [nchar](10) NULL,
 CONSTRAINT [PK_Produzione] PRIMARY KEY CLUSTERED 
(
	[PROD_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Reparti]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Reparti' and xtype='U' )
CREATE TABLE [dbo].[Reparti](
	[REP_id] [int] IDENTITY(1,1) NOT NULL,
	[REP_descr] [nvarchar](50) NULL,
	[REP_name] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[REP_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Serie]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Serie' and xtype='U' )
CREATE TABLE [dbo].[Serie](
	[SER_id] [int] IDENTITY(1,1) NOT NULL,
	[SER_nome] [nchar](10) NULL,
	[SER_costruttore] [nchar](10) NULL,
 CONSTRAINT [PK_Serie] PRIMARY KEY CLUSTERED 
(
	[SER_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SerieMacchine_vs_Componenti]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='SerieMacchine_vs_Componenti' and xtype='U' )
CREATE TABLE [dbo].[SerieMacchine_vs_Componenti](
	[SERMAC_id] [int] IDENTITY(1,1) NOT NULL,
	[SERMAC_vs_COM_id] [int] NOT NULL,
	[SERMAC_vs_MAC_id] [int] NULL,
	[SERMAC_vs_SER_id] [int] NULL,
 CONSTRAINT [PK_SerieMacchine_vs_Componenti] PRIMARY KEY CLUSTERED 
(
	[SERMAC_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Setting]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Setting' and xtype='U' )
CREATE TABLE [dbo].[Setting](
	[SET_id] [int] IDENTITY(1,1) NOT NULL,
	[SET_descr] [nvarchar](50) NULL,
	[SET_name] [nvarchar](20) NULL,
	[SET_email] [nvarchar](50) NULL,
	[SET_email_port] [nvarchar](50) NULL,
	[SET_email_ip] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[SET_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 30/07/2018 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Users' and xtype='U' )
CREATE TABLE [dbo].[Users](
	[USER_id] [int] IDENTITY(1,1) NOT NULL,
	[USER_username] [nvarchar](50) NOT NULL,
	[USER_pwd] [nvarchar](50) NOT NULL,
	[USER_mail] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[USER_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*creazione tabella chiavi manutenzione*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists (select * from sysobjects where name='Man_chiavi' and xtype='U' ) 
CREATE TABLE [dbo].[Man_chiavi](
	[KEY_id] [int] IDENTITY(1,1) NOT NULL,
	[KEY_value] [nvarchar](50) NOT NULL,
	[KEY_unita] [nvarchar](50) NULL,
 CONSTRAINT [PK_Man_chiavi] PRIMARY KEY CLUSTERED 
(
	[KEY_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/*creazione tabella tipi macchina*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if not exists (select * from sysobjects where name='TypeMachine' and xtype='U' ) 
CREATE TABLE [dbo].[TypeMachine](
	[TYP_id] [int] IDENTITY(1,1) NOT NULL,
	[TYP_tipoMacchina] [varchar](20) NOT NULL,
	[TYP_controlloNumerico] [varchar](20) NOT NULL,
	[TYP_tipoControllo] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[TYP_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/******			DROP  TABELLE INUTILIZZATE		  ******/
if exists (select * from sysobjects where name='Manutenzione3' and xtype='U' )
DROP TABLE [dbo].[Manutenzione3]
GO

if exists (select * from sysobjects where name='Componenti2' and xtype='U' )
DROP TABLE [dbo].[Componenti2]
GO


/******			POPOLAMENTO TABELLE		  ******/
/*popolamento chiavi manutenzione*/
if ((select count (KEY_id) FROM Man_chiavi)=0)
INSERT INTO [dbo].[Man_chiavi]
           ([KEY_value]
           ,[KEY_unita])
     VALUES
			('MAN_tempMandrino', 'GradiC'),
			('MAN_tempMandrino', NULL),
			('MAN_tempAsseX', NULL),
			('MAN_tempAsseY', NULL),
			('MAN_tempAsseZ', NULL),
			('MAN_tempAsseB', NULL),
			('MAN_tempAsseC', NULL),
			('MAN_tempEncX', NULL),
			('MAN_tempEncY', NULL),
			('MAN_tempEncZ', NULL),
			('MAN_tempEncB', NULL),
			('MAN_tempEncC', NULL),
			('MAN_loadMandrino', NULL),
			('MAN_loadAsseX', NULL),
			('MAN_loadAsseY', NULL),
			('MAN_loadAsseZ', NULL),
			('MAN_loadAsseB', NULL),
			('MAN_loadAsseC', NULL),
			('MAN_rmpMandrino', NULL),
			('MAN_data', NULL),
			('MAN_metriLavoroX', NULL),
			('MAN_metriRapidoX', NULL),
			('MAN_metriLavoroY', NULL),
			('MAN_metriRapidoY', NULL),
			('MAN_metriLavoroZ', NULL),
			('MAN_metriRapidoZ', NULL),
			('MAN_metriLavoroB', NULL),
			('MAN_metriRapidoB', NULL),
			('MAN_metriLavoroC', NULL),
			('MAN_metriRapidoC', NULL),
			('MAN_metriX', NULL),
			('MAN_metriY', NULL),
			('MAN_metriZ', NULL),
			('MAN_metriB', NULL),
			('MAN_metriC', NULL),
			('MAN_sbloccaggiCU', NULL),
			('MAN_cambiEseguiti', NULL),
			('MAN_oreLavoro', NULL),
			('MAN_oreAccensione', NULL),
			('MAN_rpm1', NULL),
			('MAN_rpm2', NULL),
			('MAN_rpm3', NULL),
			('MAN_rpm4', NULL),
			('MAN_rpm5', NULL),
			('MAN_timeOverLimit', NULL),
			('MAN_limitX', NULL),
			('MAN_limitY', NULL),
			('MAN_limitZ', NULL),
			('MAN_limitB', NULL),
			('MAN_limitC', NULL),
			('MAN_opPinza1Grezzo', NULL),
			('MAN_clPinza1Grezzo', NULL),
			('MAN_opPinza1Finiti', NULL),
			('MAN_clPinza1Finiti', NULL),
			('MAN_opPinza2Grezzo', NULL),
			('MAN_clPinza2Grezzo', NULL),
			('MAN_opPinza2Finiti', NULL),
			('MAN_clPinza2Finiti', NULL),
			('MAN_violazioniGialla', NULL),
			('MAN_violazioniRossa', NULL),
			('MAN_opPinzaPallet', NULL),
			('MAN_clPinzaPallet', NULL),
			('MAN_cestelliCaricati', NULL),
			('MAN_pzPrelevati', NULL),
			('MAN_pzDepositati', NULL),
			('MAN_fineLotto', NULL),
			('MAN_accInterventoTermicaNastro', NULL),
			('MAN_accTeleruttoreNastro', NULL),
			('MAN_accLetturaProxy', NULL)
GO

/*popolamento tipi macchina*/
if ((select count (TYP_id) FROM TypeMachine)=0)
INSERT INTO TypeMachine (TYP_tipoMacchina ,TYP_controlloNumerico,TYP_tipoControllo)
VALUES ('Fresa','Fanuc','0i-MF'),
('Fresa',	'Fanuc',	'31iB5'),
('Tornio',	'Fanuc',	'0iTF'),
('Rettifica',	'Mitsubishi',	'FX3U'),
('Rettifica',	'Mitsubishi',	'Q03UE'),
('Robot',	'Fanuc',	'nd'),
('Fresa',	'Fagor',	'nd'),
('Fresa', 'Heidenhain', 'nd')
GO

/******		GENERA FK		******/
IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Allarmi_Macchine' ) 
ALTER TABLE [dbo].[Allarmi]  WITH CHECK ADD  CONSTRAINT [FK_Allarmi_Macchine] FOREIGN KEY([ALL_vs_MAC_id]) REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Allarmi] CHECK CONSTRAINT [FK_Allarmi_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Cicloproduzione_Macchine' ) 
ALTER TABLE [dbo].[Cicloproduzione]  WITH CHECK ADD  CONSTRAINT [FK_Cicloproduzione_Macchine] FOREIGN KEY([CPROD_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Cicloproduzione] CHECK CONSTRAINT [FK_Cicloproduzione_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Input_Cicloproduzione' ) 
ALTER TABLE [dbo].[Input]  WITH CHECK ADD  CONSTRAINT [FK_Input_Cicloproduzione] FOREIGN KEY([IN_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Input] CHECK CONSTRAINT [FK_Input_Cicloproduzione]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Input_Users' ) 
ALTER TABLE [dbo].[Input]  WITH CHECK ADD  CONSTRAINT [FK_Input_Users] FOREIGN KEY([IN_vs_USER_id])
REFERENCES [dbo].[Users] ([USER_id])
GO
ALTER TABLE [dbo].[Input] CHECK CONSTRAINT [FK_Input_Users]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Interfaccia_Macchine' ) 
ALTER TABLE [dbo].[Interfaccia]  WITH CHECK ADD  CONSTRAINT [FK_Interfaccia_Macchine] FOREIGN KEY([INT_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Interfaccia] CHECK CONSTRAINT [FK_Interfaccia_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Letmanutenzione_Macchine' ) 
ALTER TABLE [dbo].[Letmanutenzione]  WITH CHECK ADD  CONSTRAINT [FK_Letmanutenzione_Macchine] FOREIGN KEY([LMAN_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Letmanutenzione] CHECK CONSTRAINT [FK_Letmanutenzione_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Logs_Macchine' ) 
ALTER TABLE [dbo].[Logs]  WITH CHECK ADD  CONSTRAINT [FK_Logs_Macchine] FOREIGN KEY([LOG_fk_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Logs] CHECK CONSTRAINT [FK_Logs_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Macchine_Reparti' ) 
ALTER TABLE [dbo].[Macchine]  WITH CHECK ADD  CONSTRAINT [FK_Macchine_Reparti] FOREIGN KEY([MAC_reparto])
REFERENCES [dbo].[Reparti] ([REP_id])
GO
ALTER TABLE [dbo].[Macchine] CHECK CONSTRAINT [FK_Macchine_Reparti]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Macchine_Serie' ) 
ALTER TABLE [dbo].[Macchine]  WITH CHECK ADD  CONSTRAINT [FK_Macchine_Serie] FOREIGN KEY([MAC_vs_SER_id])
REFERENCES [dbo].[Serie] ([SER_id])
GO
ALTER TABLE [dbo].[Macchine] CHECK CONSTRAINT [FK_Macchine_Serie]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Marcature_Macchine' ) 
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Macchine] FOREIGN KEY([MAR_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Marcature_Operatori' ) 
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Operatori] FOREIGN KEY([MAR_vs_OPE_id])
REFERENCES [dbo].[Operatori] ([OPE_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Operatori]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Marcature_Output' ) 
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Output] FOREIGN KEY([MAR_vs_OUT_id])
REFERENCES [dbo].[Output] ([OUT_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Output]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Cicloproduzione_Produzione' ) 
ALTER TABLE [dbo].[Output]  WITH CHECK ADD  CONSTRAINT [FK_Cicloproduzione_Produzione] FOREIGN KEY([OUT_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Output] CHECK CONSTRAINT [FK_Cicloproduzione_Produzione]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_CPrdouzione_id' ) 
ALTER TABLE [dbo].[Produzione]  WITH CHECK ADD  CONSTRAINT [FK_CPrdouzione_id] FOREIGN KEY([PROD_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Produzione] CHECK CONSTRAINT [FK_CPrdouzione_id]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_SerieMacchine_vs_Componenti_Componenti' ) 
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Componenti] FOREIGN KEY([SERMAC_vs_COM_id])
REFERENCES [dbo].[Componenti] ([COM_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Componenti]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_SerieMacchine_vs_Componenti_Macchine' ) 
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Macchine] FOREIGN KEY([SERMAC_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Macchine]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_SerieMacchine_vs_Componenti_Serie' ) 
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Serie] FOREIGN KEY([SERMAC_vs_SER_id])
REFERENCES [dbo].[Serie] ([SER_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Serie]
GO

IF NOT EXISTS ( SELECT  name FROM    sys.foreign_keys WHERE   name = 'FK_Users_Logs' ) 
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Logs] FOREIGN KEY([USER_id])
REFERENCES [dbo].[Logs] ([LOG_id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Logs]
GO