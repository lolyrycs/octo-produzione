# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Allarmi(models.Model):
    all_date = models.DateTimeField(db_column='ALL_date', blank=True, null=True)  # Field name made lowercase.
    all_numeroallarme = models.CharField(db_column='ALL_numeroAllarme', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    all_messaggioallarme = models.CharField(db_column='ALL_messaggioAllarme', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    all_numerowarning = models.CharField(db_column='ALL_numeroWarning', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    all_messaggiowarning = models.CharField(db_column='ALL_messaggioWarning', max_length=1000, blank=True, null=True)  # Field name made lowercase.
    all_vs_mac = models.ForeignKey('Macchine', models.PROTECT, db_column='ALL_vs_MAC_id')  # Field name made lowercase.
    all_id = models.AutoField(db_column='ALL_id', primary_key=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Allarmi'
        permissions =(('view_allarmi', 'View Allarmi'),)


class Cicloproduzione(models.Model):
    cprod_id = models.AutoField(db_column='CPROD_id', primary_key=True)  # Field name made lowercase.
    cprod_vs_mac = models.ForeignKey('Macchine', models.PROTECT, db_column='CPROD_vs_MAC_id')  # Field name made lowercase.
    cprod_commessa = models.CharField(db_column='CPROD_commessa', max_length=50)  # Field name made lowercase.
    cprod_importfrom = models.CharField(db_column='CPROD_importFrom', max_length=50)  # Field name made lowercase.
    cprod_status = models.IntegerField(db_column='CPROD_status')  # Field name made lowercase.
    cprod_pezzifatti = models.IntegerField(db_column='CPROD_pezziFatti')  # Field name made lowercase.
    cprod_lotto = models.CharField(db_column='CPROD_lotto', max_length=50)  # Field name made lowercase.
    cprod_articolo = models.IntegerField(db_column='CPROD_articolo')  # Field name made lowercase.
    cprod_fase = models.IntegerField(db_column='CPROD_fase')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Cicloproduzione'
        permissions = (('view_cicloproduzione', 'View Cicloproduzione'),)




class Componenti(models.Model):
    com_id = models.AutoField(db_column='COM_id', primary_key=True)  # Field name made lowercase.
    com_nome = models.CharField(db_column='COM_nome', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ### chiave di lettura per le sonde  ###  logica mossa dal programma console
    # com_chiave = models.CharField(db_column='COM_chiave', max_length=10, blank=True, null=True)  # Field name made lowercase.
    com_chiave = models.CharField(db_column='COM_chiave', max_length=50, blank=True, null=True)  # Field name made lowercase.
    com_vita = models.CharField(db_column='COM_vita', max_length=10, blank=True, null=True)  # Field name made lowercase.
    com_codice = models.CharField(db_column='COM_codice', max_length=10, blank=True, null=True)  # Field name made lowercase.
    com_unita = models.CharField(db_column='COM_unita', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Componenti'
        permissions = (('view_componenti', 'View Componenti'),)

'''
class Componenti2(models.Model):
    com2_id = models.AutoField(db_column='COM2_id', primary_key=True)  # Field name made lowercase.
    com2_nome = models.CharField(db_column='COM2_nome', max_length=10)  # Field name made lowercase.
    com2_vs_key = models.ForeignKey('ManChiavi', models.DO_NOTHING, db_column='COM2_vs_KEY_id', blank=True, null=True)  # Field name made lowercase.
    com2_vita = models.CharField(db_column='COM2_vita', max_length=10, blank=True, null=True)  # Field name made lowercase.
    com2_codice = models.CharField(db_column='COM2_codice', max_length=10, blank=True, null=True)  # Field name made lowercase.
    com2_unita = models.CharField(db_column='COM2_unita', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Componenti2'
        permissions = (
            ('view_componenti2', 'View Componenti2'),
        )
'''

class Input(models.Model):
    in_id = models.AutoField(db_column='IN_id', primary_key=True)  # Field name made lowercase.
    in_commessa = models.CharField(db_column='IN_commessa', max_length=50, blank=True, null=True)  # Field name made lowercase.
    in_lotto = models.CharField(db_column='IN_lotto', max_length=10, blank=True, null=True)  # Field name made lowercase.
    in_priorita = models.IntegerField(db_column='IN_priorita', blank=True, null=True)  # Field name made lowercase.
    in_importfrom = models.CharField(db_column='IN_importFrom', max_length=10, blank=True, null=True)  # Field name made lowercase.
    in_status = models.SmallIntegerField(db_column='IN_status', blank=True, null=True)  # Field name made lowercase.
    in_data = models.DateTimeField(db_column='IN_data', auto_now_add=True,  blank=True, null=True)  # Field name made lowercase.
    #in_vs_mac = models.ForeignKey('Macchine', models.DO_NOTHING, db_column='IN_vs_MAC_id')  # Field name made lowercase.
    in_vs_user = models.ForeignKey('Users', models.PROTECT, db_column='IN_vs_USER_id', blank=True, null=True)  # Field name made lowercase.
    in_vs_cprod = models.ForeignKey(Cicloproduzione, models.PROTECT, db_column='IN_vs_CPROD_id', blank=True, null=True)  # Field name made lowercase.
    in_articolo = models.IntegerField(db_column='IN_articolo', blank=True, null=True)  # Field name made lowercase.
    in_fase = models.IntegerField(db_column='IN_fase', blank=True, null=True)  # Field name made lowercase.



    class Meta:
        managed = False
        db_table = 'Input'
        permissions = (
            ('view_input', 'View Input'),
        )


class Interfaccia(models.Model):
    int_id = models.AutoField(db_column='INT_id', primary_key=True)  # Field name made lowercase.
    int_ipadress = models.CharField(db_column='INT_ipAdress', max_length=20, blank=True, null=True)  # Field name made lowercase.
    int_status = models.CharField(db_column='INT_status', max_length=50, blank=True, null=True)  # Field name made lowercase.
    int_programmaattivo = models.CharField(db_column='INT_programmaAttivo', max_length=50, blank=True, null=True)  # Field name made lowercase.
    int_feedassi = models.IntegerField(db_column='INT_feedAssi', blank=True, null=True)  # Field name made lowercase.
    int_feedmandrino = models.IntegerField(db_column='INT_feedMandrino', blank=True, null=True)  # Field name made lowercase.
    int_utensileattivo = models.BinaryField(db_column='INT_utensileAttivo', blank=True, null=True)  # Field name made lowercase.
    int_pezzifatti = models.IntegerField(db_column='INT_pezziFatti', blank=True, null=True)  # Field name made lowercase.
    int_vs_mac = models.ForeignKey('Macchine', models.PROTECT, db_column='INT_vs_MAC_id', blank=True, null=True)  # Field name made lowercase.
    int_path = models.IntegerField(db_column='INT_path', blank=True, null=True)  # Field name made lowercase.
    int_date = models.DateTimeField(db_column='INT_date', blank=True, null=True)  # Field name made lowercase.
    int_totem = models.CharField(db_column='INT_Totem', max_length=10, blank=True, null=True)  # Field name made lowercase.
    int_attivo = models.IntegerField(db_column='INT_Attivo',blank=True, null=True)
    int_commessa = models.CharField(db_column='INT_commessa', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Interfaccia'
        permissions = (
            ('view_interfaccia', 'View Interfaccia'),
        )

class Letmanutenzione(models.Model):
    lman_id = models.AutoField(db_column='LMAN_Id', primary_key=True)  # Field name made lowercase.
    lman_data = models.DateTimeField(db_column='LMAN_data')  # Field name made lowercase.
    lman_vs_mac = models.ForeignKey('Macchine', models.PROTECT, db_column='LMAN_vs_MAC_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Letmanutenzione'
        permissions = (
            ('view_letmanutenzione', 'View Letmanutenzione'),
        )

class Logs(models.Model):
    log_id = models.AutoField(db_column='LOG_id', primary_key=True)  # Field name made lowercase.
    log_fk_user_id = models.IntegerField(db_column='LOG_fk_USER_id', blank=True, null=True)  # Field name made lowercase.
    log_fk_mac = models.ForeignKey('Macchine', models.PROTECT, db_column='LOG_fk_MAC_id', blank=True, null=True)  # Field name made lowercase.
    log_description = models.TextField(db_column='LOG_description', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    log_status = models.IntegerField(db_column='LOG_status', blank=True, null=True)  # Field name made lowercase.
    log_date = models.DateTimeField(db_column='LOG_date', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Logs'
        permissions = (
            ('view_logs', 'View Logs'),
        )

class Setting(models.Model):
    sett_id = models.AutoField(db_column='SET_id', primary_key=True)  # Field name made lowercase.
    sett_descr = models.CharField(db_column='SET_descr', max_length=50, blank=True, null=True)  # Field name made lowercase.
    sett_name = models.CharField(db_column='SET_name', max_length=20, blank=True, null=True)  # Field name made lowercase.
    sett_email = models.CharField(db_column='SET_email', max_length=50, blank=True, null=True)
    sett_email_port = models.CharField(db_column='SET_email_port', max_length=50, blank=True, null=True)
    sett_email_ip = models.CharField(db_column='SET_email_ip', max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Setting'
        permissions = (
            ('view_setting', 'View Setting'),
        )

class Reparti(models.Model):
    rep_id = models.AutoField(db_column='REP_id', primary_key=True)  # Field name made lowercase.
    rep_descr = models.CharField(db_column='REP_descr', max_length=50, blank=True, null=True)  # Field name made lowercase.
    rep_name = models.CharField(db_column='REP_name', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:

        managed = False
        db_table = 'Reparti'
        permissions = (('view_reparto', 'View Reparto'),)

class Pagina(models.Model):
    pag_id = models.AutoField(db_column='PAG_id', primary_key=True)  # Field name made lowercase.
    pag_title = models.CharField(db_column='PAG_title', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pag_path = models.CharField(db_column='PAG_path', max_length=20, blank=True,
                                null=True)  # Field name made lowercase.
    pag_icon = models.CharField(db_column='PAG_icon', max_length=20, blank=True,
                                null=True)  # Field name made lowercase.
    pag_auth = models.CharField(db_column='PAG_auth', max_length=20, blank=True,
                                null=True)  # Field name made lowercase.
    pag_always = models.CharField(db_column='PAG_always', max_length=20, blank=True,
                                  null=True)  # Field name made lowercase.
    pag_position = models.CharField(db_column='PAG_position', max_length=20, blank=True,
                                    null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Pagina'
        permissions = (('view_pagina', 'View Pagina'),)


class Macchine(models.Model):
    mac_id = models.AutoField(db_column='MAC_id', primary_key=True)  # Field name made lowercase.
    mac_ipadress = models.CharField(db_column='MAC_ipAdress', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_ipadress2 = models.CharField(db_column='MAC_ipAdress2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_nomemacch = models.CharField(db_column='MAC_nomeMacch', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_soprannomemacch = models.CharField(db_column='MAC_soprannomeMacch', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_tipomacchina = models.CharField(db_column='MAC_tipoMacchina', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_controllonumerico = models.CharField(db_column='MAC_controlloNumerico', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_tipocontrollo = models.CharField(db_column='MAC_tipoControllo', max_length=5, blank=True, null=True)  # Field name made lowercase.
    mac_packegtipo = models.CharField(db_column='MAC_packegTipo', max_length=5, blank=True, null=True)  # Field name made lowercase.
    #mac_reparto = models.IntegerField(db_column='MAC_reparto', blank=True, null=True)  # Field name made lowercase.
    mac_reparto = models.ForeignKey('Reparti', models.PROTECT, db_column='MAC_reparto', blank=True, null=True)
    mac_imagepath = models.CharField(db_column='MAC_imagePath', max_length=250, blank=True, null=True)  # Field name made lowercase.
    mac_image = models.BinaryField(db_column='MAC_image', blank=True, null=True)  # Field name made lowercase.
    mac_serialnumber = models.CharField(db_column='MAC_serialNumber', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_boolcamera = models.CharField(db_column='MAC_boolCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_ipadresscamera = models.CharField(db_column='MAC_ipAdressCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_portacamera = models.CharField(db_column='MAC_portaCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_comandocamera = models.CharField(db_column='MAC_comandoCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_modecamera = models.CharField(db_column='MAC_modeCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_usercamera = models.CharField(db_column='MAC_userCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_passcamera = models.CharField(db_column='MAC_passCamera', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mac_userusername = models.CharField(db_column='MAC_userUsername', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mac_userpassword = models.CharField(db_column='MAC_userPassword', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mac_pathshare = models.CharField(db_column='MAC_pathshare', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mac_vs_ser = models.ForeignKey('Serie', models.PROTECT, db_column='MAC_vs_SER_id', blank=True,
                                   null=True)  # Field name made lowercase.
    mac_numeroassi = models.IntegerField(db_column='MAC_numeroAssi', blank=True,
                                         null=True)  # Field name made lowercase.
    mac_statoconnessione = models.CharField(db_column='MAC_statoConnessione', max_length=50, blank=True,
                                            null=True)  # Field name made lowercase.
    mac_multipallet = models.NullBooleanField(db_column='MAC_multipallet')  # Field name made lowercase.
    mac_attivo = models.BooleanField(db_column='MAC_attivo')  # Field name made lowercase.
    mac_assi = models.CharField(db_column='MAC_assi', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Macchine'
        permissions = (('view_macchine', 'View Macchine'),)

'''
class Manutenzione(models.Model):
    man_tempmandrino = models.IntegerField(db_column='MAN_tempMandrino', blank=True, null=True)  # Field name made lowercase.
    man_tempassex = models.IntegerField(db_column='MAN_tempAsseX', blank=True, null=True)  # Field name made lowercase.
    man_tempassey = models.IntegerField(db_column='MAN_tempAsseY', blank=True, null=True)  # Field name made lowercase.
    man_tempassez = models.IntegerField(db_column='MAN_tempAsseZ', blank=True, null=True)  # Field name made lowercase.
    man_tempasseb = models.IntegerField(db_column='MAN_tempAsseB', blank=True, null=True)  # Field name made lowercase.
    man_tempassec = models.IntegerField(db_column='MAN_tempAsseC', blank=True, null=True)  # Field name made lowercase.
    man_tempencx = models.IntegerField(db_column='MAN_tempEncX', blank=True, null=True)  # Field name made lowercase.
    man_tempency = models.IntegerField(db_column='MAN_tempEncY', blank=True, null=True)  # Field name made lowercase.
    man_tempencz = models.IntegerField(db_column='MAN_tempEncZ', blank=True, null=True)  # Field name made lowercase.
    man_tempencb = models.IntegerField(db_column='MAN_tempEncB', blank=True, null=True)  # Field name made lowercase.
    man_tempencc = models.IntegerField(db_column='MAN_tempEncC', blank=True, null=True)  # Field name made lowercase.
    man_loadmandrino = models.IntegerField(db_column='MAN_loadMandrino', blank=True, null=True)  # Field name made lowercase.
    man_loadassex = models.IntegerField(db_column='MAN_loadAsseX', blank=True, null=True)  # Field name made lowercase.
    man_loadassey = models.IntegerField(db_column='MAN_loadAsseY', blank=True, null=True)  # Field name made lowercase.
    man_loadassez = models.IntegerField(db_column='MAN_loadAsseZ', blank=True, null=True)  # Field name made lowercase.
    man_loadasseb = models.IntegerField(db_column='MAN_loadAsseB', blank=True, null=True)  # Field name made lowercase.
    man_loadassec = models.IntegerField(db_column='MAN_loadAsseC', blank=True, null=True)  # Field name made lowercase.
    man_rmpmandrino = models.IntegerField(db_column='MAN_rmpMandrino', blank=True, null=True)  # Field name made lowercase.
    man_data = models.DateTimeField(db_column='MAN_data', blank=True, null=True)  # Field name made lowercase.
    man_metrilavorox = models.IntegerField(db_column='MAN_metriLavoroX', blank=True, null=True)  # Field name made lowercase.
    man_metrirapidox = models.IntegerField(db_column='MAN_metriRapidoX', blank=True, null=True)  # Field name made lowercase.
    man_metrilavoroy = models.IntegerField(db_column='MAN_metriLavoroY', blank=True, null=True)  # Field name made lowercase.
    man_metrirapidoy = models.IntegerField(db_column='MAN_metriRapidoY', blank=True, null=True)  # Field name made lowercase.
    man_metrilavoroz = models.IntegerField(db_column='MAN_metriLavoroZ', blank=True, null=True)  # Field name made lowercase.
    man_metrirapidoz = models.IntegerField(db_column='MAN_metriRapidoZ', blank=True, null=True)  # Field name made lowercase.
    man_metrilavorob = models.IntegerField(db_column='MAN_metriLavoroB', blank=True, null=True)  # Field name made lowercase.
    man_metrirapidob = models.IntegerField(db_column='MAN_metriRapidoB', blank=True, null=True)  # Field name made lowercase.
    man_metrilavoroc = models.IntegerField(db_column='MAN_metriLavoroC', blank=True, null=True)  # Field name made lowercase.
    man_metrirapidoc = models.IntegerField(db_column='MAN_metriRapidoC', blank=True, null=True)  # Field name made lowercase.
    man_metrix = models.IntegerField(db_column='MAN_metriX', blank=True, null=True)  # Field name made lowercase.
    man_metriy = models.IntegerField(db_column='MAN_metriY', blank=True, null=True)  # Field name made lowercase.
    man_metriz = models.IntegerField(db_column='MAN_metriZ', blank=True, null=True)  # Field name made lowercase.
    man_metrib = models.IntegerField(db_column='MAN_metriB', blank=True, null=True)  # Field name made lowercase.
    man_metric = models.IntegerField(db_column='MAN_metriC', blank=True, null=True)  # Field name made lowercase.
    man_sbloccaggicu = models.IntegerField(db_column='MAN_sbloccaggiCU', blank=True, null=True)  # Field name made lowercase.
    man_cambieseguiti = models.IntegerField(db_column='MAN_cambiEseguiti', blank=True, null=True)  # Field name made lowercase.
    man_orelavoro = models.IntegerField(db_column='MAN_oreLavoro', blank=True, null=True)  # Field name made lowercase.
    man_oreaccensione = models.IntegerField(db_column='MAN_oreAccensione', blank=True, null=True)  # Field name made lowercase.
    man_rpm1 = models.IntegerField(db_column='MAN_rpm1', blank=True, null=True)  # Field name made lowercase.
    man_rpm2 = models.IntegerField(db_column='MAN_rpm2', blank=True, null=True)  # Field name made lowercase.
    man_rpm3 = models.IntegerField(db_column='MAN_rpm3', blank=True, null=True)  # Field name made lowercase.
    man_rpm4 = models.IntegerField(db_column='MAN_rpm4', blank=True, null=True)  # Field name made lowercase.
    man_rpm5 = models.IntegerField(db_column='MAN_rpm5', blank=True, null=True)  # Field name made lowercase.
    man_timeoverlimit = models.IntegerField(db_column='MAN_timeOverLimit', blank=True, null=True)  # Field name made lowercase.
    man_limitx = models.IntegerField(db_column='MAN_limitX', blank=True, null=True)  # Field name made lowercase.
    man_limity = models.IntegerField(db_column='MAN_limitY', blank=True, null=True)  # Field name made lowercase.
    man_limitz = models.IntegerField(db_column='MAN_limitZ', blank=True, null=True)  # Field name made lowercase.
    man_limitb = models.IntegerField(db_column='MAN_limitB', blank=True, null=True)  # Field name made lowercase.
    man_limitc = models.IntegerField(db_column='MAN_limitC', blank=True, null=True)  # Field name made lowercase.
    man_id = models.AutoField(db_column='MAN_id', primary_key=True)  # Field name made lowercase.
    man_vs_mac = models.ForeignKey(Macchine, models.DO_NOTHING, db_column='MAN_vs_MAC_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Manutenzione'
        permissions = (   ('view_manutenzione', 'View Manutenzione'),)
'''


class ManChiavi(models.Model):
    key_id = models.AutoField(db_column='KEY_id', primary_key=True)  # Field name made lowercase.
    key_value = models.CharField(db_column='KEY_value', max_length=50)  # Field name made lowercase.
    key_unita = models.CharField(db_column='KEY_unita', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Man_chiavi'
        permissions = (('view_manchiavi', 'View ManChiavi'),)

class Serie(models.Model):
    ser_id = models.AutoField(db_column='SER_id', primary_key=True)  # Field name made lowercase.
    ser_nome = models.CharField(db_column='SER_nome', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ser_costruttore = models.CharField(db_column='SER_costruttore', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Serie'
        permissions = (('view_serie', 'View Serie'),)

class Manutenzione2(models.Model):
    man2_id = models.AutoField(db_column='MAN2_id', primary_key=True)  # Field name made lowercase.
    man2_chiave = models.CharField(db_column='MAN2_chiave', max_length=50)  # Field name made lowercase.
    man2_valore = models.CharField(db_column='MAN2_valore', max_length=50)  # Field name made lowercase.
    man2_vs_lman = models.ForeignKey(Letmanutenzione, models.PROTECT, db_column='MAN2_vs_LMAN_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Manutenzione2'
        permissions = (('view_manutenzione2', 'View Manutenzione2'),)

'''
class Manutenzione3(models.Model):
    man3_id = models.IntegerField(db_column='MAN3_id', primary_key=True)  # Field name made lowercase.
    man3_vs_key = models.ForeignKey(ManChiavi, models.DO_NOTHING, db_column='MAN3_vs_KEY_id')  # Field name made lowercase.
    man3_valore = models.CharField(db_column='MAN3_valore', max_length=50)  # Field name made lowercase.
    man3_vs_lman = models.ForeignKey(Letmanutenzione, models.DO_NOTHING, db_column='MAN3_vs_LMAN_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Manutenzione3'
'''

class Marcature(models.Model):
    mar_id = models.AutoField(db_column='MAR_id', primary_key=True)  # Field name made lowercase.
    mar_vs_ope = models.ForeignKey('Operatori', models.PROTECT, db_column='MAR_vs_OPE_id')  # Field name made lowercase.
    mar_vs_mac = models.ForeignKey(Macchine, models.PROTECT, db_column='MAR_vs_MAC_id')  # Field name made lowercase.
    mar_vs_out = models.ForeignKey('Output', models.PROTECT, db_column='MAR_vs_OUT_id', blank=True, null=True)  # Field name made lowercase.
    mar_date = models.DateTimeField(db_column='MAR_date', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Marcature'
        permissions = (('view_marcature', 'View Marcature'),)

class Operatori(models.Model):
    ope_id = models.AutoField(db_column='OPE_id', primary_key=True)  # Field name made lowercase.
    ope_badge = models.CharField(db_column='OPE_badge', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ope_date = models.DateTimeField(db_column='OPE_date', blank=True, null=True)  # Field name made lowercase.
    ope_custom1 = models.CharField(db_column='OPE_custom1', max_length=10, blank=True, null=True)  # Field name made lowercase.
    ope_custom2 = models.CharField(db_column='OPE_custom2', max_length=10, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Operatori'
        permissions = (('view_operatori', 'View Operatori'),)

class Output(models.Model):
    out_id = models.AutoField(db_column='OUT_id', primary_key=True)  # Field name made lowercase.
    out_tempolavoro = models.CharField(db_column='OUT_tempoLavoro', max_length=10, blank=True, null=True)  # Field name made lowercase.
    out_tempopiazzamento = models.CharField(db_column='OUT_tempoPiazzamento', max_length=10, blank=True, null=True)  # Field name made lowercase.
    out_tempopausa = models.CharField(db_column='OUT_tempoPausa', max_length=10, blank=True, null=True)  # Field name made lowercase.
    out_efficienza = models.CharField(db_column='OUT_efficienza', max_length=10, blank=True, null=True)  # Field name made lowercase.
    out_priorita = models.IntegerField(db_column='OUT_priorita', blank=True, null=True)  # Field name made lowercase.
    out_vs_cprod = models.ForeignKey(Cicloproduzione, models.PROTECT,db_column='OUT_vs_CPROD_id')  # Field name made lowercase.
    out_data = models.DateTimeField(db_column='OUT_data', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Output'
        permissions = (('view_output', 'View Output'),)


class Produzione(models.Model):
    prod_id = models.AutoField(db_column='PROD_id', primary_key=True)  # Field name made lowercase.
    prod_lotto = models.CharField(db_column='PROD_lotto', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prod_tempolavoro = models.CharField(db_column='PROD_tempoLavoro', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prod_tempopiazzamento = models.CharField(db_column='PROD_tempoPiazzamento', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prod_tempopausa = models.CharField(db_column='PROD_tempoPausa', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prod_efficienza = models.CharField(db_column='PROD_efficienza', max_length=10, blank=True, null=True)  # Field name made lowercase.
    prod_priorita = models.IntegerField(db_column='PROD_priorita', blank=True, null=True)  # Field name made lowercase.
    prod_data = models.DateTimeField(db_column='PROD_data', blank=True, null=True)  # Field name made lowercase.
    prod_vs_cprod = models.ForeignKey(Cicloproduzione, models.DO_NOTHING,
                                      db_column='PROD_vs_CPROD_id')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Produzione'
        permissions = (('view_produzione', 'View Produzione'),)


class Typemachine(models.Model):
    typ_id = models.AutoField(db_column='TYP_id', primary_key=True)  # Field name made lowercase.
    typ_tipomacchina = models.CharField(db_column='TYP_tipoMacchina', max_length=20)  # Field name made lowercase.
    typ_controllonumerico = models.CharField(db_column='TYP_controlloNumerico', max_length=20)  # Field name made lowercase.
    typ_tipocontrollo = models.CharField(db_column='TYP_tipoControllo', max_length=5, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TypeMachine'


class Users(models.Model):
    user = models.AutoField(db_column='USER_id', primary_key=True)  # Field name made lowercase.
    user_username = models.CharField(db_column='USER_username', max_length=50)  # Field name made lowercase.
    user_pwd = models.CharField(db_column='USER_pwd', max_length=50)  # Field name made lowercase.
    user_mail = models.CharField(db_column='USER_mail', max_length=50, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Users'
        permissions = (('view_users', 'View Users'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class Sysdiagrams(models.Model):
    name = models.CharField(max_length=128)
    principal_id = models.IntegerField()
    diagram_id = models.AutoField(primary_key=True)
    version = models.IntegerField(blank=True, null=True)
    definition = models.BinaryField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sysdiagrams'
        unique_together = (('principal_id', 'name'),)


class SeriemacchineVsComponenti(models.Model):
    sermac_id = models.AutoField(db_column='SERMAC_id', primary_key=True)  # Field name made lowercase.
    sermac_vs_com = models.ForeignKey(Componenti, models.DO_NOTHING, db_column='SERMAC_vs_COM_id')  # Field name made lowercase.
    sermac_vs_mac = models.ForeignKey(Macchine, models.DO_NOTHING, db_column='SERMAC_vs_MAC_id', blank=True, null=True)  # Field name made lowercase.
    sermac_vs_ser = models.ForeignKey(Serie, models.DO_NOTHING, db_column='SERMAC_vs_SER_id', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SerieMacchine_vs_Componenti'
        permissions = (('view_seriemacchinevscomponenti', 'View SeriemacchineVsComponenti'),)