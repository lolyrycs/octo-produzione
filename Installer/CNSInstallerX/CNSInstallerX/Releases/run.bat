@echo off
set arg1=%1
echo %arg1%
if "%arg1%" == "start" ( GOTO STARTDJANGO )
if "%arg1%" == "stop"  ( GOTO KILLALLDJANGOS )
:COMPLETE
GOTO BYE

:STARTDJANGO
pushd %programfiles(x86)%\CNS\nginx\nginx-1.14.0\
start nginx.exe
popd
pushd %programfiles(x86)%\CNS\
Backend\Scripts\activate.bat & python Backend\manage.py runserver 0.0.0.0:8000 --insecure
popd
timeout 5
GOTO COMPLETE

:KILLALLDJANGOS
set f="wmic process where name='python.exe' get ProcessId, CommandLine, Status   /format:csv | findstr /r runserver"

for /f "tokens=3 delims=," %%a in ('%f% ') do taskkill /f /pid %%a
taskkill /F /IM nginx.exe 
timeout 5
GOTO COMPLETE 
 
:BYE