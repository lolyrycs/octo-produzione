#!/usr/bin/python2.7
import re
import subprocess
import os
import time
import sys
import urllib2
import zipfile
import shutil
from distutils.dir_util import copy_tree
import datetime
import zipfile

def replaceText(path, string1, string2 ):
    with open(path, 'r') as filex:
        filedata = filex.read()
    # Replace the target string
    filedata = filedata.replace(string1, string2)
    # Write the file out again
    with open(path, 'w') as filez:
        filez.write(filedata)

exe_path = "C:\\Python27\python.exe"
script_dir = os.path.dirname(os.path.abspath( __file__))
file = open(os.path.join(script_dir,"python.install.log"), "a",buffering=0)
file.write("### INSTALL START ###\n")
file.write("### "+sys.version+" ###\n")

if(len(sys.argv) != 6):
    file.write(str(datetime.datetime.now())+ ": wrong input\n")
    file.write(str(datetime.datetime.now())+ ": I parametri sono "+str(len(sys.argv))+"\n")
    sys.exit()

dbname = sys.argv[1]
hostname = sys.argv[2]
user = sys.argv[3]
password = sys.argv[4]
alldomains = sys.argv[5].split(";")

if(len(alldomains)==0):
    file.write(str(date.today())+ ": I domini non sono validi\n")
    sys.exit()
##unqutoed path
myvirtualenvunq = os.path.join(script_dir,"Backend")
##quoted path
myvirtualenv = "\""+myvirtualenvunq+"\""
system = os.name
file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": CNS verra installato in questa directory")
file.write("\n")
file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Install  Virtualenv")
file.write("\n")
file.write("#" * 85)
file.write("\n")
proc = subprocess.Popen(" virtualenv"+" --python="+exe_path+" "+ myvirtualenv, shell="True")
stdout, stderr = proc.communicate()
proc.wait()
file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Installo i pacchetti di python nel virtualenv v2 Offline")
file.write("\n")

with zipfile.ZipFile(os.path.join(script_dir,"Packages","virtualenv.zip"), 'r') as zip_ref:
    zip_ref.extractall(myvirtualenvunq)

file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Attivo il virtualenv")
file.write("\n")
file.write("#" * 85)
activate_this = os.path.join(myvirtualenvunq,"Scripts","activate_this.py")
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Attivo il virtualenv ----> "+activate_this)
file.write("\n")
file.write("#" * 85)
file.write("\n")
execfile(activate_this, dict(__file__=activate_this))
#proc = subprocess.Popen(" pip install -r \"" + os.path.join(myvirtualenvunq,"requirments.txt") + "\" && deactivate", shell=True)
path = os.environ['PATH']
os.environ['PATH'] = os.path.join(myvirtualenvunq,"Scripts")+";"+path
#subprocess.check_call([sys.executable, '-m', 'pip2', 'install', '-r' , os.path.join(myvirtualenvunq,"requirments.txt")])
#subprocess.check_call(["python.exe", '-m' , 'pip', 'install', os.path.join(script_dir,"pyodbc-4.0.25-cp27-cp27m-win_amd64.whl")])
#file.write(str(datetime.datetime.now())+ ": Evito di ricompilare pyodbc ??")
#file.write("\n")
#subprocess.check_call(["python.exe", '-m', 'pip', 'install', '-r' , os.path.join(myvirtualenvunq,"requirments.txt")])
### imposto i valori di connessione sul backend
file.write(str(datetime.datetime.now())+ ": Modifico setting django e nginx ")
file.write("\n")

myfileSetting = os.path.join(script_dir,"Backend", "Octo", "settings.py")
myNginxConf = os.path.join(script_dir,"Packages","Configs","win_nginx.conf")
myFrontendConf = os.path.join(script_dir,"Frontend","static","config.json")


replaceText(myfileSetting,"'NAME': 'xxxxx'","'NAME': '"+dbname+"'")
replaceText(myfileSetting,"'HOST': 'xxxxx'","'HOST': '"+hostname+"'")
replaceText(myfileSetting,"'USER': 'xxxxx'","'USER': '"+user+"'")
replaceText(myfileSetting,"'PASSWORD': 'xxxxx'","'PASSWORD': '"+password+"'")
### Aggiorno le connessioni cors
myCors = ""
myNginx = "localhost "
firstone = True
for textCors in alldomains:
    if( not firstone):
        myCors += ","
    else:
        replaceText(myFrontendConf, "XXXXXXXXXXXX", textCors)
    myCors += "\"" + textCors  + ":10080\"\n"
    myNginx += " "+textCors
    firstone = False
replaceText(myfileSetting,"XXXXXXXXXXXX",myCors)
replaceText(myNginxConf,"XXXXXXXXXXXX",myNginx)

### imposto il valore di connessione sul servizio
myfileSettingC = os.path.join(script_dir,"Service","cnsservice.exe.config")
replaceText(myfileSettingC,"YYYYYY",dbname)
replaceText(myfileSettingC,"XXXXXX",hostname)
replaceText(myfileSettingC,"ZZZZZZ",user)
replaceText(myfileSettingC,"HHHHHH",password)

## modifica di Lorenzo 03/01/2019 
## myfileSettingC = os.path.join(script_dir,"Frontend","config.json")
## replaceText(myfileSetting,"XXXXXXXXXXXX",alldomains[0]+":8000")
replaceText(myFrontendConf,"YYYYYYYYYY",alldomains[0]+":8000")

## modifica di Lorenzo 19/04/2019  
## inserisco nome host nel batch
myBatFile = os.path.join(script_dir,"Batch","EditDb.bat")
replaceText(myBatFile, "nome_host", hostname)


##file.write("Scarico Nginx")
##file.write("\n")
### endregion
##stdout, stderr = proc.communicate()
##nginxzip = urllib2.urlopen("http://nginx.org/download/nginx-1.14.0.zip")
##with open('nginx.zip', 'wb') as output:
##    output.write(nginxzip.read())

file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Installo Nginx")
file.write("\n")
file.write("#" * 85)
file.write("\n")
zip_ref = zipfile.ZipFile(os.path.join(script_dir,"Packages","nginx.zip"), 'r')
zip_ref.extractall(os.path.join(script_dir,"nginx"))
zip_ref.close()

myfrontend = os.path.join(script_dir,"Frontend")
file.write(os.path.join(script_dir,"nginx", "nginx-1.14.0","frontend"))
file.write(myfrontend)

try:
    os.rename(myfrontend, os.path.join(script_dir,"nginx", "nginx-1.14.0","frontend"))
except:
    file.write(str(datetime.datetime.now())+ ": File or source already moved")

file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Sistemo la Directory del Servizio")
file.write("\n")
file.write("#" * 85)
file.write("\n")
try:
    myservicepath = os.path.join(script_dir, "Service")
    for filename in os.listdir(myservicepath):
        if ( not filename.endswith(".exe")):
            os.rename(os.path.join(script_dir, "Service", filename),os.path.join(script_dir, "ServiceI", filename))
    shutil.rmtree(os.path.join(script_dir, "Service"))
except:
    file.write("File or source already moved")

file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Setting Config Nginx - Backup")
file.write("\n")
file.write("#" * 85)
file.write("\n")
shutil.copy2( os.path.join(script_dir,"nginx", "nginx-1.14.0","conf", "nginx.conf"),  os.path.join(script_dir,"nginx","nginx-1.14.0","conf", "nginx.conf.bk"))
shutil.copy2( os.path.join(script_dir,"Packages","Configs","win_nginx.conf"),  os.path.join(script_dir,"nginx", "nginx-1.14.0","conf", "nginx.conf"))
file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": Setting Config Nginx")
file.write("\n")
file.write("#" * 85)
file.write("\n")
file.write(str(datetime.datetime.now())+ ": That's all FOLFKS -> Lancia da cmd - run.bat start")
file.write(str(datetime.datetime.now())+ ": Per rimuovere il software software run Uninstaller.diagcab")
file.write("\n")
file.write("#" * 85)
file.write("\n")
sys.exit()