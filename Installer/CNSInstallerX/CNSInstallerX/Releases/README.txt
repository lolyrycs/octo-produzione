_________   _______    _________                                                    
\_   ___ \  \      \  /   _____/                                                    
/    \  \/  /   |   \ \_____  \                                                     
\     \____/    |    \/        \                                                    
 \______  /\____|__  /_______  /                                                    
        \/         \/        \/                                                     
.___ _______    _________________________  .____    .____     _____________________ 
|   |\      \  /   _____/\__    ___/  _  \ |    |   |    |    \_   _____/\______   \
|   |/   |   \ \_____  \   |    | /  /_\  \|    |   |    |     |    __)_  |       _/
|   /    |    \/        \  |    |/    |    \    |___|    |___  |        \ |    |   \
|___\____|__  /_______  /  |____|\____|__  /_______ \_______ \/_______  / |____|_  /
            \/        \/                 \/        \/       \/        \/         \/ 

############ Update struttura database
EditDb.bat nome_host_sql

############ Start del programma 
run.bat start (CNS_start.vbs)

############ Stop del programma 
run.bat stop

############ Disinstaller:

0. lancia Uninstaller.diagcab
1. fix disinstaller 
2. seleziona CNS
3. rimuovi CNS
4. cancella i files rimanenti