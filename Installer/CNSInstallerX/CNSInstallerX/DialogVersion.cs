using System;
using System.Diagnostics;
using System.Windows;

using WixSharp;
using WixSharp.UI.Forms;


namespace WixSharpSetup
{
    public partial class DialogVersion : ManagedForm, IManagedDialog
    {
        public DialogVersion()
        {
            //NOTE: If this assembly is compiled for v4.0.30319 runtime, it may not be compatible with the MSI hosted CLR.
            //The incompatibility is particularly possible for the Embedded UI scenarios. 
            //The safest way to avoid the problem is to compile the assembly for v3.5 Target Framework.WixSharp Setup
            InitializeComponent();
        }

        void dialog_Load(object sender, EventArgs e)
        {
            
            Text = "[ProductName] Setup";

            /* 
             * con il metodo select selezioni la parte di stringa che vuoi formattare  
             * 
             * richTextBox1.Select(0, 10);
               richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
             * 
             * */

            richTextBox1.Text = "Versione dell' applicazione 1.3.1\r\n";
            richTextBox1.Text += "\r\n";
            richTextBox1.Text += "Correzione lettura pezzi Fanuc \r\n";
            richTextBox1.Text += "\r\n";
            richTextBox1.Text += "Versione del servizio 1.3.1\r\n";
            richTextBox1.Text += "\t -Correzione lettura pezzi Fanuc\r\n";
            richTextBox1.Text += "\r\n";
            richTextBox1.Text += "Versione dell'applicazione web 1.3\r\n";
            richTextBox1.Text += "\t -correzione visualizzazione Commessa in Home\r\n";
            richTextBox1.Text += "\r\n";

            //resolve all Control.Text cases with embedded MSI properties (e.g. 'ProductName') and *.wxl file entries
            base.Localize();
        }

        void back_Click(object sender, EventArgs e)
        {
            Shell.GoPrev();
        }

        void next_Click(object sender, EventArgs e)
        {
            Shell.GoNext();
        }

        void cancel_Click(object sender, EventArgs e)
        {
            Shell.Cancel();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}