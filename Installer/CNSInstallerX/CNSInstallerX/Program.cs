﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using WixSharp;
using WixSharp.CommonTasks;
using Microsoft.Win32;
using WixSharp.UI;
using WixSharp.Forms;
using WixSharp.Controls;
using WixSharp.Bootstrapper;
using WixSharpSetup;
using Microsoft.Deployment.WindowsInstaller;
using System.IO;

namespace CNSInstallerX
{
    class Program
    {
        
        static void Main()
        {
            string myPath = Environment.GetEnvironmentVariable("Path");
            WixSharp.File service;
            if (myPath != "")
                myPath = myPath + ";";

            myPath = myPath.Replace(@"C:\Python27\", "");
            myPath = myPath.Replace(@"C:\Python27\Scripts", "");
            myPath = myPath.Replace(@";;", ";");
            myPath = myPath + @"C:\Python27\;C:\Python27\Scripts";

            var project = new ManagedProject("CNS",
                  new Dir(@"%ProgramFiles%\CNS\"
                    , new Files("*.*")
                    , new Dir(@"ServiceI", service = new WixSharp.File(@".\Service\cnsservice.exe"))
                    , new ExeFileShortcut("Uninstall CNS", "[System64Folder]msiexec.exe", "/x [ProductCode]"))
                    ,new Property("PASSWORD", "yyyy") { IsDeferred = true }
                    ,new Property("DBNAME", "yyyyy") { IsDeferred = true }
                    ,new Property("HOSTNAME", "yyyy") { IsDeferred = true }
                    ,new Property("USER", "yyyyy") { IsDeferred = true }
                    , new Property("DOMAIN", "yyyyy") { IsDeferred = true }
                    //modifica Lorenzo Lanzoni 04/01/2019
                    //, new RegValue(WixSharp.RegistryHive.LocalMachine, @"Software\Microsoft\Windows\CurrentVersion\Run", "Octo", "\"[INSTALLDIR]run.bat\" \"start\"")
                    ,new RegValue(WixSharp.RegistryHive.LocalMachine, @"Software\Microsoft\Windows\CurrentVersion\Run", "Octo", "\"[INSTALLDIR]Batch\\startvbs.bat\"")
                    //new EnvironmentVariable("Path", myPath),
                    //new ElevatedManagedAction(CustomActions.InstallPython, Return.check, When.After, Step.InstallFiles, Condition.Always) 
                    //new ManagedAction(CustomActions.PromptGetPIP),
                    ,new EnvironmentVariable("PATH", @"C:\Python27\") { Part = EnvVarPart.first }
                    ,new EnvironmentVariable("PATH", @"C:\Python27\Scripts") { Part = EnvVarPart.first }
                    ,new ManagedAction(CustomActions.InstallVirtualEnv)            
                    //,new ElevatedManagedAction( CustomActions.InstallScript) { UsesProperties = "HOSTNAME", Execute = Execute.deferred }
                    //new ManagedAction(CustomActions.InstallVirtualEnv)
                    // new ManagedAction(CustomActions.InstallScript),
                    //TO DO aggiungere la registrazione del servizio
                );

                project.SourceBaseDir = System.IO.Path.Combine(Environment.CurrentDirectory, "Releases");
                project.GUID = new Guid("6fe30b47-2577-43ad-9095-1861ba25889c");
                //project.InstallScope = InstallScope.perMachine;
                project.DefaultDeferredProperties += ",HOSTNAME";
                project.Platform = Platform.x86;

                project.WildCardDedup = Project.UniqueFileNameDedup;
                service.ServiceInstaller = new ServiceInstaller
                {
                    Name = "CNS SERVICE",
                    StartOn = SvcEvent.Install,
                    //StartOn = null,
                    StopOn = null,
                    RemoveOn = SvcEvent.Uninstall_Wait,
                    //StartType = SvcStartType.demand,
                    StartType= SvcStartType.auto,
                };

                //project.ManagedUI = ManagedUI.Empty;    //no standard UI dialogs
                //project.ManagedUI = ManagedUI.Default;  //all standard UI dialogs
                //custom set of standard UI dialogs
                project.PreserveTempFiles = false;
                project.ManagedUI = new ManagedUI();
                project.ManagedUI.InstallDialogs.Add(Dialogs.Welcome)
                                                .Add(Dialogs.Licence)
                                                .Add<DialogVersion>()
                                                .Add<CustomDialog>()
                                                .Add(Dialogs.Progress)
                                                .Add(Dialogs.Exit);

                //project.BannerImage = "CNS.dialog_bmp.jpg";
                /*project.ManagedUI.ModifyDialogs.Add(Dialogs.MaintenanceType)
                                                .Add(Dialogs.Features)
                                                .Add(Dialogs.Progress)
                                                .Add(Dialogs.Exit);*/
                //project.Load += Msi_Load;
                //project.BeforeInstall += Msi_BeforeInstall;
                project.AfterInstall += Msi_AfterInstallReborn;

           
                //project.SourceBaseDir = "<input dir path>";
                //project.OutDir = "<output dir path>";
                ValidateAssemblyCompatibility();
                project.BuildMsi();
        }

        //static void Msi_Load(SetupEventArgs e)
        //{
        //    if (!e.IsUninstalling)
        //        MessageBox.Show(e.ToString(), "Load (Deprecated)");
        //}

        //static void Msi_BeforeInstall(SetupEventArgs e)
        //{
        //    if (!e.IsUninstalling)
        //        MessageBox.Show("Before Install (Deprecated)");
        //e.Session.CustomActionData["HOSTNAME"] = e.Session["HOSTNAME"]; 
        //    MessageBox.Show(e.ToString(), "BeforeInstall");
        //}

        /*static void Msi_AfterInstall(SetupEventArgs e)
        {
            ProcessStartInfo startE = new ProcessStartInfo();
            string programFilesX86 = Environment.ExpandEnvironmentVariables("%programfiles(x86)%");
            System.Environment.SetEnvironmentVariable("path", @"C:\Python27\Scripts;C:\Python27\");
            startE.FileName = "cmd";
            startE.Arguments = string.Format(" /C {0} \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" ", "python", programFilesX86+@"\CNS\install.py", e.Session.Property("DBNAME") + "", e.Session.Property("HOSTNAME") + "", e.Session.Property("USER") + "", e.Session.Property("PASSWORD"));
            startE.UseShellExecute = false;// Do not use OS shell
            startE.CreateNoWindow = false; // We don't need new window
            startE.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
            startE.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (Process process = Process.Start(startE))
            {
                using (System.IO.StreamReader reader = process.StandardOutput)
                {
                    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                    Console.Write(result);
                }
            }
        }*/

        // proxy per il lancio dello script python installer 
        static void Msi_AfterInstallReborn(SetupEventArgs e)
        {
            ProcessStartInfo startE = new ProcessStartInfo();
            string programFilesX86 = Environment.ExpandEnvironmentVariables("%programfiles(x86)%");
            startE.FileName = programFilesX86 + @"\CNS\Launcher.exe";
            startE.Arguments = string.Format(" \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" ", e.Session.Property("DBNAME") + "", e.Session.Property("HOSTNAME") + "", e.Session.Property("USER") + "", e.Session.Property("PASSWORD")+"", e.Session.Property("DOMAIN") + "");
            startE.UseShellExecute = false;// Do not use OS shell
            startE.CreateNoWindow = false; // We don't need new window
            startE.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
            startE.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (Process process = Process.Start(startE))
            {
                using (System.IO.StreamReader reader = process.StandardOutput)
                {
                    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                    Console.Write(result);
                }
            }
        }

        static void ValidateAssemblyCompatibility()
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            if (!assembly.ImageRuntimeVersion.StartsWith("v2."))
            {
                Console.WriteLine("Warning: assembly '{0}' is compiled for {1} runtime, which may not be compatible with the CLR version hosted by MSI. " +
                                  "The incompatibility is particularly possible for the EmbeddedUI scenarios. " +
                                   "The safest way to solve the problem is to compile the assembly for v3.5 Target Framework.",
                                   assembly.GetName().Name, assembly.ImageRuntimeVersion);
            }
        }
    }


    // metodo per forzare l'installazione del python sul server
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult TestEnv(Session session)
        {
            MessageBox.Show(session["HOSTNAME"]);
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult InstallVirtualEnv(Session session)
        {
            if (Directory.Exists(@"C:\Python27\Scripts") && Directory.Exists(@"C:\Python27\"))
            {
                string programFilesX86 = Environment.ExpandEnvironmentVariables("%programfiles(x86)%");
                System.Environment.SetEnvironmentVariable("path", @"C:\Python27\Scripts;C:\Python27\");
                ProcessStartInfo start = new ProcessStartInfo();
                start.FileName = "cmd";
                start.Arguments = string.Format(" /K \"{0}\" {1} {2} ", "pip", "install", " --force-reinstall "+programFilesX86 + @"\CNS\Packages\PythonPackages\pypa-virtualenv-16.1.0-0-g4ad2742.tar.gz & exit");
                start.UseShellExecute = false;// Do not use OS shell
                start.CreateNoWindow = true; // We don't need new window
                start.RedirectStandardOutput = true;// Any output, generated by application will be redirected back
                start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
                using (Process process = Process.Start(start))
                {
                    using (System.IO.StreamReader reader = process.StandardOutput)
                    {
                        string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                        string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                        Console.Write(result);
                    }
                }
                return ActionResult.Success;
            }
            else
            {
                //fix nel caso non fosse presente pip nell'installazione
                MessageBox.Show("Installa Python 2.7 !!!!!!", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return ActionResult.Failure;
            }
        }

        [CustomAction]
        public static ActionResult registerService(Session session)
        {
            Tasks.InstallService(session.Property("INSTALLDIR") + "/Service/cnsservice.exe", true);
            //Tasks.StartService("WixSharp.SimpleService", false);
            return ActionResult.Success;
        }
    }
}