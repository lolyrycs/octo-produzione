﻿using System;
using cns.utils;
using System.Data;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Data.SqlClient;

namespace cns.macchine
{
    public enum ExitCode : int
    {
        Ok = 0,
        ErrorCreateApplication = 0x11,
        ErrorDiscoverEndpoints = 0x12,
        ErrorCreateSession = 0x13,
        ErrorBrowseNamespace = 0x14,
        ErrorCreateSubscription = 0x15,
        ErrorMonitoredItem = 0x16,
        ErrorAddSubscription = 0x17,
        ErrorRunning = 0x18,
        ErrorNoKeepAlive = 0x30,
        ErrorInvalidCommandLine = 0x100
    };

    class OpcCN : ControlloNumerico
    {
        #region variabili
        
        // MAC_ipAdress2
        string port = "4840";
        // MAC_userUsername	MAC_userPassword
        string user = "";
        string pass = "";
        ushort namespaceIndex;
        string prefisso="";

        bool autoAccept = true;
        string endpointURL;
        string ip = ConfigurationManager.AppSettings["ip"] + "";
        int stopTimeout = Timeout.Infinite;
        const int ReconnectPeriod = 10;
        protected Session session;
        SessionReconnectHandler reconnectHandler;
        int clientRunTime = Timeout.Infinite;
        private static ExitCode exitCode;
        Mutex mutexLettura;

        protected ConfigOpc configurations;

        protected SqlConnection connectionOpc = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + "");

        protected int secLavoro =0;
        protected int secPiazz = 0;
        protected int secPausa = 0;

        protected int secLavoroApp = 0;
        protected int secPiazzApp = 0;
        protected int secPausaApp = 0;

        protected int lotto=0;
        protected bool ctrlPrg = true;

        // variabili per custom
        protected DateTime last = DateTime.Now;
        protected DateTime now = DateTime.Now;
        protected int pezziStart = -1;
        protected int secondiStart = 0;
        protected int shiftpezzi = 0;
        protected string pezzicode = "DB140.DBD170";
        #endregion

        //costruttore e letture configurazioni
        public OpcCN(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            ip = ipAddress;
            try
            {
                //read user and password informations
                using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    String sqlQuery = "SELECT MAC_userUsername, MAC_userPassword, MAC_controlloNumerico FROM Macchine WHERE MAC_id="+MAC_id;
                    if (connection.State != ConnectionState.Open) connection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (! reader.IsDBNull(0))
                            {
                                user = reader.GetString(0);
                            }
                            if (!reader.IsDBNull(1))
                            {
                                pass = reader.GetString(1);
                            }

                        }
                        reader.NextResult();
                    }
                    reader.Close();
                    command.Dispose();
                    if (connection.State != ConnectionState.Closed) connection.Close();

                    //Logs.LogDB(connectionOpc, "Macchina Opc Istanziata", MAC_id);
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace);
            }
            configurations = new ConfigOpc();
            configurations.LetturaFileConfig(MAC_nomemacchina);
            string debugconf = "Progamma: ";
            if (configurations.calcola_programma){
                debugconf += "s";
            }
            else
            {
                debugconf += "n";
            }
            debugconf += "/n Pezzi: ";
            if (configurations.calcola_pezzi)
            {
                debugconf += "s";
            }
            else
            {
                debugconf += "n";
            }
            debugconf += "/n Tempi: "+configurations.calcola_tempi;
            Logs.LogDB(connectionOpc, debugconf, MAC_id);
            mutexLettura = new Mutex(false, "threadopc_" + id.ToString());
        }

        public void init()
        {
            // Logs.LogDB(connectionOpc, "inizializzazione dati", MAC_id);
            // Remember to Set namespaceIndex (MAC_ipAdress2), Username and Password
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    string namespaceStr = "";
                    string query = "SELECT MAC_ipAdress2, MAC_userUsername, MAC_userPassword, MAC_pathshare FROM Macchine WHERE MAC_id=" + MAC_id;
                    if (connection.State != ConnectionState.Open) connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    command.Dispose();
                    while (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            // Read the namespaceIndex
                            if (!reader.IsDBNull(0))
                            {
                                namespaceStr = reader.GetString(0);
                                namespaceIndex = Convert.ToUInt16(namespaceStr);
                            }

                            // Read the username and password 
                            if (!reader.IsDBNull(1))
                            {
                                user = reader.GetString(1);
                            }

                            if (!reader.IsDBNull(2))
                            {
                                pass = reader.GetString(2);
                            }

                            //read prefix
                            if (!reader.IsDBNull(3))
                            {
                                prefisso = reader.GetString(3);
                            }
                        }
                        reader.NextResult();
                    }
                    reader.Close();
                    if (connection.State != ConnectionState.Closed) connection.Close();
                    //Logs.LogDB(connectionOpc, "Lettura configurazioni: namespace " + namespaceStr + ", user " + user + ", pass " + pass, MAC_id);
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connectionOpc, "Errore Lettura Dati Configurazione OpcUa: " + ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }

            endpointURL = "opc.tcp://" + ip + ":" + port;

            try
            {
                //await for the loading of application configuration and Session
                LoadConf().Wait();
                istanziato = true;
            }
            catch (Exception ex)
            {
                Utils.Trace("ServiceResultException:" + ex.Message);
                Logs.LogDB(connectionOpc, "Errore durante il caricamento delle configurazioni e della sessione "+ ex.Message+" exitCode: "+ exitCode + ex.StackTrace, MAC_id);
                istanziato = false;
                return;
            }
        }

        //read from machine()
        public string ReadFromMac(string valueToRead)
        {
            mutexLettura.WaitOne();
            valueToRead = RemoveSpecialCharacters(valueToRead);
            DataValue value= new DataValue();
            try
            {
                value = session.ReadValue(new NodeId(prefisso+valueToRead, namespaceIndex));
            }
            catch
            {
                Logs.LogDB(connectionOpc, "Impossibile leggere il valore: " + prefisso+valueToRead, MAC_id);
            }
            mutexLettura.ReleaseMutex();
            return value.ToString();
        }

        //write in machine()
        public bool WriteInMac(string nodeToWrite, string valueToWrite)
        {
            bool ret = false;
            mutexLettura.WaitOne();
            Logs.LogDB(connectionOpc, "occupo mutex per scrittura", MAC_id);
            valueToWrite = RemoveSpecialCharacters(valueToWrite);
            WriteValueCollection nodesToWrite = new WriteValueCollection();
            nodesToWrite.Add(new WriteValue()
            {
                NodeId = new NodeId(nodeToWrite, namespaceIndex),
                AttributeId = Attributes.Value,
                Value = new DataValue() { WrappedValue = valueToWrite }
            });
            
            try
            {
                session.Write(new RequestHeader(), nodesToWrite, out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
                ret = true;
                Logs.LogDB(connectionOpc, "Scritto " + valueToWrite + " in: " + nodeToWrite, MAC_id);
            }
            catch
            {
                Logs.LogDB(connectionOpc, "Impossibile scrivere "+ valueToWrite + " in: " + nodeToWrite, MAC_id);
            }
            Logs.LogDB(connectionOpc, "rilascio mutex da scrittura", MAC_id);
            mutexLettura.ReleaseMutex();
            return ret;
        }

        public string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^0-9a-zA-Z_.]", string.Empty);
        }

        #region internal methods

        private async Task LoadConf()
        {
            try
            { 
                exitCode = ExitCode.ErrorCreateApplication;
                //Logs.LogDB(connectionOpc, "Load the application configuration ", MAC_id);
                ApplicationInstance application = new ApplicationInstance
                {
                    ApplicationName = "Raccolta Dati Opc",
                    ApplicationType = ApplicationType.Client,
                    ConfigSectionName = "Opc.Ua.SampleClient"
                };

                //load the application configuration.
                ApplicationConfiguration config = null;
                string filePath = "C:\\Program Files (x86)\\CNS\\ServiceI\\Opc.Ua.SampleClient.Config.xml";
                try
                {
                    // load the configuration file.
                    config = await ApplicationConfiguration.Load(
                        new FileInfo(filePath),
                        application.ApplicationType,
                        null,
                        true);
                    
                    if (config== null)
                    {
                        Logs.LogDB(connectionOpc, "config null", MAC_id);
                    }
                    else
                    {
                        //Logs.LogDB(connectionOpc, "config lette", MAC_id);
                    }
                }
                catch (Exception e) {
                    Logs.LogDB(connectionOpc, "Errore lettura confuigurazioni " + e.Message +" "+ e.StackTrace, MAC_id);
                }
                
                //Type type = Path.GetExtension(filePath);

                // load the application configuration.
                config = await application.LoadApplicationConfiguration(filePath, false);

                // check the application certificate.
                // bool haveAppCertificate = await application.CheckApplicationInstanceCertificate(false, 0);
                bool haveAppCertificate = true;

                if (!haveAppCertificate)
                {
                    throw new Exception("Application instance certificate invalid!");
                }
                // bool haveAppCertificate = false;

                if (haveAppCertificate)
                {
                    config.ApplicationUri = Utils.GetApplicationUriFromCertificate(config.SecurityConfiguration.ApplicationCertificate.Certificate);
                    if (config.SecurityConfiguration.AutoAcceptUntrustedCertificates)
                    {
                        autoAccept = true;
                    }
                    config.CertificateValidator.CertificateValidation += new CertificateValidationEventHandler(CertificateValidator_CertificateValidation);
                }
                else
                {
                    Logs.LogDB(connectionOpc, "WARN: missing application certificate, using unsecure connection.", MAC_id);
                }

                // Discover endpoints
                // Logs.LogDB(connectionOpc, "Discover endpoints", MAC_id);
                exitCode = ExitCode.ErrorDiscoverEndpoints;
                var selectedEndpoint = CoreClientUtils.SelectEndpoint(endpointURL, haveAppCertificate, 15000);

                // Create a session with OPC UA server
                // Logs.LogDB(connectionOpc, "Create a session with OPC UA server", MAC_id);
                exitCode = ExitCode.ErrorCreateSession;
                var endpointConfiguration = EndpointConfiguration.Create(config);
                var endpoint = new ConfiguredEndpoint(null, selectedEndpoint, endpointConfiguration);

                if (!haveAppCertificate || (user == "" && pass == ""))
                {
                    // session = await Session.Create(config, endpoint, false, false, "OPC UA Console Client", 60000, new UserIdentity(new AnonymousIdentityToken()), null);
                    session = await Session.Create(config, endpoint, false, "OPC UA Console Client", 60000, new UserIdentity(new AnonymousIdentityToken()), null);
                }
                else
                {
                    session = await Session.Create(config, endpoint, false, "OPC UA Console Client", 60000, new UserIdentity(user, pass), null);
                }
                // register keep alive handler
                session.KeepAlive += Client_KeepAlive;
            }
            catch (Exception ex)
            {
                Utils.Trace("ServiceResultException:" + ex.Message);
                Logs.LogDB(connectionOpc, "Errore durante il caricamento delle configurazioni e della sessione " + ex.Message + " exitCode: " + exitCode + ex.StackTrace, MAC_id);
                istanziato = false;
                return;
            }
            
          
        }

        private void CertificateValidator_CertificateValidation(CertificateValidator validator, CertificateValidationEventArgs e)
        {
            if (e.Error.StatusCode == StatusCodes.BadCertificateUntrusted)
            {
                e.Accept = autoAccept;
                if (autoAccept)
                {
                    //Logs.LogDB(connectionOpc, "Accepted Certificate: " + e.Certificate.Subject, MAC_id);
                }
                else
                {
                    //Logs.LogDB(connectionOpc, "Rejected Certificate: " + e.Certificate.Subject, MAC_id);
                }
            }
        }

        private void Client_KeepAlive(Session sender, KeepAliveEventArgs e)
        {
            if (e.Status != null && ServiceResult.IsNotGood(e.Status))
            {
                //Logs.LogDB(connectionOpc, e.Status + " " + sender.OutstandingRequestCount + " / " + sender.DefunctRequestCount, MAC_id);

                if (reconnectHandler == null)
                {
                    Logs.LogDB(connectionOpc, "--- RECONNECTING --- ", MAC_id);
                    reconnectHandler = new SessionReconnectHandler();
                    reconnectHandler.BeginReconnect(sender, ReconnectPeriod * 1000, Client_ReconnectComplete);
                }
            }
        }

        private void Client_ReconnectComplete(object sender, EventArgs e)
        {
            // ignore callbacks from discarded objects.
            if (!Object.ReferenceEquals(sender, reconnectHandler))
            {
                return;
            }

            session = reconnectHandler.Session;
            reconnectHandler.Dispose();
            reconnectHandler = null;
            Logs.LogDB(connectionOpc, "--- RECONNECTED --- ", MAC_id);
        }

        private void OnNotification(MonitoredItem item, MonitoredItemNotificationEventArgs e)
        {
            foreach (var value in item.DequeueValues())
            {
                Logs.LogDB(connectionOpc, item.DisplayName + ": " + value.Value + " , " + value.SourceTimestamp + " , " + value.StatusCode, MAC_id);
            }
        }
        #endregion
    }
}
