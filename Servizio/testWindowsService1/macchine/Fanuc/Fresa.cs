﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cns.utils;
using System.Threading;
using System.Data.SqlClient;
using System.Data;

namespace cns.macchine.Fanuc
{
    public class Fresa : FanucCN
    {
        public Fresa(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {

        }

        #region gestione Interfaccia
        public override void gestioneInterfaccia(object myId = null)
        {
            while (Service1.alive)
            {
                if (connesso)
                {
                    if (hndl == 0)
                    {
                        ret = Focas1.cnc_allclibhndl3(this.MAC_ipAdress, 8193, 9, out hndl);
                        //ret = Focas1.cnc_allclibhndl2(9, out hndl);
                        if (ret != Focas1.EW_OK)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE GENERAZIONE HANDLE, RET = " + ret.ToString(), MAC_id);
                            Focas1.cnc_freelibhndl(hndl);
                            hndl = 0;
                        }
                    }
                    else
                    {
                        #region Lettura Numero Path attivi
                        ret = Focas1.cnc_diagnoss(hndl, 1111, 0, 5, Path);
                        if (ret == Focas1.EW_OK)
                            pathMacchina = Path.u.ldata.ToString();
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA NUMERO PATH MACCHINA, RET =  " + ret.ToString(), MAC_id);
                        }
                        #endregion
                        ret = 0;
                        checkStatus();
                        ret = 0;
                        letturaProgrammaAttivo();
                        ret = 0;
                        #region Lettura feedRate Assi
                        //Modifica per KAFO STM
                        ret = Focas1.cnc_rdparam(hndl, 3002, 0, 5, IOV);
                        if (ret == Focas1.EW_OK)
                        {
                            ret = Focas1.cnc_rdparam(hndl, 3002, 0, 5, IOV);
                            if (IOV.u.cdata > 15 && IOV.u.cdata < 32 ||
                                IOV.u.cdata > 47 && IOV.u.cdata < 64 ||
                                IOV.u.cdata > 79 && IOV.u.cdata < 96 ||
                                IOV.u.cdata > 111 && IOV.u.cdata < 128 ||
                                IOV.u.cdata > 143 && IOV.u.cdata < 160 ||
                                IOV.u.cdata > 175 && IOV.u.cdata < 192 ||
                                IOV.u.cdata > 207 && IOV.u.cdata < 224 ||
                                IOV.u.cdata > 239
                                )
                            {
                                feedDritto();
                            }
                            else
                                feedRovesciati();
                        }
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA PARAMETRO IOV, RET = " + ret.ToString(), MAC_id);
                        }
                        #endregion
                        ret = 0;
                        feedRateMandrino();
                        ret = 0;
                        #region Lettura Utensile in mandrino

                        if (MAC_tipoControllo == "31iB5")
                        {
                            ret = Focas1.pmc_rdpmcrng(hndl, 9, 0, 20, 20, 9, D20);
                            if (ret == Focas1.EW_OK)
                            {
                                if (Convert.ToInt32(D20.ldata[0]) >= 1 && Convert.ToInt32(D20.ldata[0]) < 153) //Lettura utensile da 1 a 9
                                {
                                    utensileAttivo = Convert.ToInt32(D20.ldata[0]);
                                }
                            }
                            else
                            {
                                Logs.LogDB(connection_Fanuc, "ERRORE ERRORE UTENSILE IN MANDRINO [D20], RET = " + ret.ToString(), MAC_id);
                            }
                        }
                        else if (MAC_tipoControllo == "0i-MF")
                        {
                            ret = Focas1.pmc_rdpmcrng(hndl, 9, 0, 10, 10, 9, D10);
                            if (ret == Focas1.EW_OK)
                            {
                                int ldata32 = Convert.ToInt32(D10.ldata[0]);
                                if (ldata32 >= 1 && ldata32 <= 153)
                                {
                                    utensileAttivo = calcoloUtensileAttivo(ldata32, 16, 6);
                                }
                            }
                            else
                            {
                                Logs.LogDB(connection_Fanuc, "ERRORE ERRORE UTENSILE IN MANDRINO [D10], RET = " + ret.ToString(), MAC_id);
                            }
                        }
                        else
                        {
                            ret = Focas1.pmc_rdpmcrng(hndl, 9, 0, 10, 10, 9, D10);
                            if (ret == Focas1.EW_OK)
                            {
                                int ldata32 = Convert.ToInt32(D10.ldata[0]);
                                if (ldata32 >= 1 && ldata32 <= 153)
                                {
                                    utensileAttivo = calcoloUtensileAttivo(ldata32, 16, 6);
                                }
                            }
                            else
                            {
                                Logs.LogDB(connection_Fanuc, "ERRORE ERRORE UTENSILE IN MANDRINO [D10], RET = " + ret.ToString(), MAC_id);
                            }
                        }
                        #endregion
                        ret = 0;
                        #region Lettura Numero Pezzi fatti
                        //ret = Focas1.cnc_rdmacro(hndl, 3901, 10, PZ);
                        //if (ret == Focas1.EW_OK)
                        //{
                        //    pezziFatti = Convert.ToInt32((PZ.mcr_val / Math.Pow(10, PZ.dec_val)));
                        //}
                        //else
                        //{
                        //    Logs.LogDB(connection_Fanuc, "ERRORE LETTURA NUMERO PEZZI FATTI, RET = " + ret.ToString());
                        //}
                        #endregion
                        ret = 0;
                        //Scrittura dati in Database
                        InterfacciDB(connection_Fanuc);
                    }
                }
                else
                {
                    if (hndl != 0)
                    {
                        Focas1.cnc_freelibhndl(hndl);
                        hndl = 0;
                        Logs.LogDB(connection_Fanuc, "Fresa Fanuc Disconnessa");
                    }
                }
                Thread.Sleep(3000);
            }
        }
        #endregion

        #region gestione Diagnostica
        public override void gestioneDiagnostica(object myId = null)
        {
            short ret = 0;
           // int MAC_numeroAssi = 0;

            while (Service1.alive)
            {
                Thread.Sleep(1000);

                //--- Se handle = 0 istanzio nuova sessione
                if (hndlDiagnostica == 0 && connesso)
                {
                    ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlDiagnostica);
                    //ret = Focas1.cnc_allclibhndl2(9, out hndlDiagnostica);
                    if (ret != Focas1.EW_OK)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE GENERAZIONE HANDLE, RET = " + ret.ToString(), MAC_id);
                        Focas1.cnc_freelibhndl(hndlDiagnostica);
                        hndlDiagnostica = 0;
                    }
                }

                if (connesso && hndlDiagnostica!=0)
                {
                    letturaDatiMacchineUtComuni();

                    string sqlQueryDiagnostica = "";
                    bool val_num_assi = false;
                    if (MAC_tipoControllo == "31iB5")
                    {
                        //string aaa = (D5076.ldata != null) ? D5076.ldata[0].ToString() : null;
                        //string aaa = (DNG309_1.u.rdata != null) ? DNG309_1.u.rdata.dgn_val.ToString() : null;
                        
                        #region impostazioni registro B1iB5
                        //Asse X
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5000, 5003, 12, D5000);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE X [D5000], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5004, 5007, 12, D5004);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE X [D5004], RET = " + ret.ToString(), MAC_id);
                        }

                        //Asse Y
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5008, 5011, 12, D5008);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE Y [D5008], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5012, 5015, 12, D5012);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE Y [D5012], RET = " + ret.ToString(), MAC_id);
                        }

                        //Asse Z
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5016, 5019, 12, D5016);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE Z [D5016], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5020, 5023, 12, D5020);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE Z [D5020], RET =  " + ret.ToString(), MAC_id);
                        }

                        if (MAC_numeroAssi > 3)
                        {
                            //Asse B
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5024, 5027, 12, D5024);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE B [D5024], RET =  " + ret.ToString(), MAC_id);
                            }

                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5028, 5031, 12, D5028);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE B [D5028], RET =  " + ret.ToString(), MAC_id);
                            }
                        }

                        if (MAC_numeroAssi > 4)
                        {
                            //Asse C
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5032, 5035, 12, D5032);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE C [D5032], RET =  " + ret.ToString(), MAC_id);

                            }

                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5036, 5039, 12, D5036);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE C [D5036], RET =  " + ret.ToString(), MAC_id);
                            }
                        }

                        //Metri TOT X
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5040, 5043, 12, D5040);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE X [D5040], RET =  " + ret.ToString(), MAC_id);
                        }

                        //Metri TOT Y
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5044, 5047, 12, D5044);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE Y [D5044], RET =  " + ret.ToString(), MAC_id);
                        }

                        //Metri TOT Z
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5048, 5051, 12, D5048);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE Z [D5048], RET =  " + ret.ToString(), MAC_id);
                        }


                        if (MAC_numeroAssi > 3)
                        {
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5052, 5055, 12, D5052);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE B [D5052], RET =  " + ret.ToString(), MAC_id);
                            }
                        }

                        if (MAC_numeroAssi > 4)
                        {
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5056, 5059, 12, D5056);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE C [D5056], RET =  " + ret.ToString(), MAC_id);
                            }
                        }

                        ////---------------------------/////////
                        ////--------CAMBIO UTENSILE----/////////
                        ////---------------------------/////////

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5120, 5123, 12, D5120);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D5120], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5124, 5127, 12, D5124);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D5124], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5128, 5131, 12, D5128);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D5128], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5132, 5135, 12, D5132);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D5132], RET =  " + ret.ToString(), MAC_id);
                        }

                        ////---------------------------/////////
                        ////--------DIAGNOSI MANDRINO--/////////
                        ////---------------------------/////////

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5060, 5063, 12, D5060);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5060], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5064, 5067, 12, D5064);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5064], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5068, 5071, 12, D5068);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5068], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5072, 5075, 12, D5072);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5072], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5076, 5079, 12, D5076);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5076], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5224, 5227, 12, D5224);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D5224], RET = " + ret.ToString(), MAC_id);
                        }

                        //esempio condizionale

                        #endregion
                        if (MAC_numeroAssi >= 3)
                        {
                            sqlQueryDiagnostica = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_tempMandrino', '" + DNG403.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseX', '" + DNG308_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseY', '" + DNG308_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseZ', '" + DNG308_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    //"(MAN_tempAsseB, 0, " + man_id + "), " +
                                    //"(MAN_tempAsseC, 0, " + man_id + "), " +
                                    "('MAN_tempEncX', '" + DNG309_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncY', '" + DNG309_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncZ', '" + DNG309_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    //"(MAN_tempEncB, 0, " + man_id + "), " +
                                    //"(MAN_tempEncC, 0, " + man_id + "), " +
                                    "('MAN_loadMandrino', '" + DNG410.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseX', '" + LOAD.svload1.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseY', '" + LOAD.svload2.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseZ', '" + LOAD.svload3.data.ToString() + "', " + man_id + "), " +
                                    //"(MAN_loadAsseB, " + substringsTimer100[106] + ", " + man_id + "), " +
                                    //"(MAN_loadAsseC, " + substringsTimer100[107] + ", " + man_id + "), " +
                                    "('MAN_rmpMandrino', '" + SPDSPEED.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroX', '" + D5004.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroY', '" + D5012.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroZ', '" + D5020.ldata[0].ToString() + "', " + man_id + "), " +
                                    //"(MAN_metriLavoroB, " + substringsTimer100[31] + ", " + man_id + "), " +
                                    //"(MAN_metriLavoroC, " + substringsTimer100[34] + ", " + man_id + "), " +
                                    "('MAN_metriRapidoX', '" + D5000.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoY', '" + D5008.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoZ', '" + D5016.ldata[0].ToString() + "', " + man_id + "), " +
                                    //"(MAN_metriRapidoB, " + substringsTimer100[30] + ", " + man_id + "), " +
                                    //"(MAN_metriRapidoC, " + substringsTimer100[33] + ", " + man_id + "), " +
                                    "('MAN_metriX', '" + D5040.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriY', '" + D5044.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriZ', '" + D5048.ldata[0].ToString() + "', " + man_id + "), " +
                                    //"(MAN_metriB, " + substringsTimer100[32] + ", " + man_id + "), " +
                                    //"(MAN_metriC, " + substringsTimer100[35] + ", " + man_id + "), " +
                                    "('MAN_sbloccaggiCU', '" + D5120.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_cambiEseguiti', '" + D5124.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreLavoro', '" + D5128.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreAccensione', '" + D5132.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm1', '" + D5060.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm2', '" + D5064.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm3', '" + D5068.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm4', '" + D5072.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm5', '" + D5076.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_timeOverLimit', '" + D5224.ldata[0].ToString() + "', " + man_id + ") ";

                            if (MAC_numeroAssi == 3) val_num_assi = true;
                        }

                        if (MAC_numeroAssi >= 4)
                        {
                            sqlQueryDiagnostica += ", ('MAN_tempAsseB', '" + DNG308_4.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                                   "('MAN_tempEncB', '" + DNG309_4.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                                   "('MAN_loadAsseB', '" + LOAD.svload4.data.ToString() + "', " + man_id + "), " +
                                                   "('MAN_metriLavoroB','" + D5028.ldata[0].ToString() + "', " + man_id + "), " +
                                                   "('MAN_metriRapidoB', '" + D5024.ldata[0].ToString() + "', " + man_id + "), "+
                                                   "('MAN_metriB', '" + D5052.ldata[0].ToString() + "', " + man_id + ") " ;
                            if (MAC_numeroAssi == 4) val_num_assi = true;
                            
                        }

                        if (MAC_numeroAssi == 5)
                        {
                            sqlQueryDiagnostica += ", ('MAN_tempAsseC', '" + DNG308_5.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                                   "('MAN_tempEncC', '" + DNG309_4.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                                   "('MAN_loadAsseC', '" + LOAD.svload5.data.ToString() + "', " + man_id + "), " +
                                                   "('MAN_metriLavoroC','" + D5036.ldata[0].ToString() + "', " + man_id + "), " +
                                                   "('MAN_metriRapidoC', '" + D5032.ldata[0].ToString() + "', " + man_id + "), " +
                                                   "('MAN_metriC', '" + D5056.ldata[0].ToString() + "', " + man_id + ") ";
                            val_num_assi = true;
                        }
                    }
                    else if (MAC_tipoControllo == "0i-MF")
                    {
                        #region impostazione registro 0i-MF

                        ////---------------------------/////////
                        ////--------METRI ASSE---------/////////
                        ////---------------------------/////////
                        //Asse X

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2000, 2003, 12, D2000);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE X [D2000], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2004, 2007, 12, D2004);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE X [D2004], RET = " + ret.ToString(), MAC_id);
                        }


                        //Asse Y
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2008, 2011, 12, D2008);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE Y [D2008], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2012, 2015, 12, D2012);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE Y [D2012], RET = " + ret.ToString(), MAC_id);
                        }

                        //Asse Z
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2016, 2019, 12, D2016);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE Z [D2016], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2020, 2023, 12, D2020);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE Z [D2020], RET = " + ret.ToString(), MAC_id);
                        }

                        //Numero Assi maggiore di 3, Asse B
                        if (MAC_numeroAssi > 3)
                        {
                            //Asse B
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2024, 2027, 12, D2024);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE B [D2024], RET = " + ret.ToString(), MAC_id);
                            }

                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2028, 2031, 12, D2028);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE B [D2028], RET = " + ret.ToString(), MAC_id);
                            }
                        }

                        //Numero Assi maggiore di 4, Asse C
                        if (MAC_numeroAssi > 4)
                        {
                            //Asse C
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2032, 2035, 12, D2032);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE C [D2032], RET = " + ret.ToString(), MAC_id);
                            }

                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2036, 2039, 12, D2036);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE C [D2036], RET = " + ret.ToString(), MAC_id);
                            }
                        }

                        //Metri TOT X
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2040, 2043, 12, D2040);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE X [D2040], RET = " + ret.ToString(), MAC_id);
                        }

                        //Metri TOT Y
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2044, 2047, 12, D2044);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE Y [D2044], RET = " + ret.ToString(), MAC_id);
                        }

                        //Metri TOT Z
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2048, 2051, 12, D2048);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE Z [D2048], RET = " + ret.ToString(), MAC_id);
                        }

                        //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                        if (MAC_numeroAssi > 3)
                        {
                            //Metri TOT B
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2052, 2055, 12, D2052);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE B [D2052], RET = " + ret.ToString(), MAC_id);
                            }
                        }

                        //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                        if (MAC_numeroAssi > 4)
                        {
                            //Metri TOT C
                            ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2056, 2059, 12, D2056);
                            if (ret != 0)
                            {
                                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE C [D2056], RET = " + ret.ToString(), MAC_id);
                            }
                        }

                        //LEGGO LIMITE IMPOSTA IN MACCHINA
                        //Asse X
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2200, 2203, 12, D2200);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA LIMITE X [D2200], RET = " + ret.ToString(), MAC_id);
                        }

                        //Asse Y
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2204, 2207, 12, D2204);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA LIMITE Y [D2204], RET = " + ret.ToString(), MAC_id);
                        }

                        //Asse Z
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2208, 2211, 12, D2208);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA LIMITE Z [D2208], RET = " + ret.ToString(), MAC_id);
                        }

                        ////---------------------------/////////
                        ////--------CAMBIO UTENSILE----/////////
                        ////---------------------------/////////

                        //Ciclo blocco sblocco mandrino
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2120, 2123, 12, D2120);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D2120], RET = " + ret.ToString(), MAC_id);
                        }

                        //Numero ciclo di cambio utensile effettuati
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2124, 2127, 12, D2124);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D2124], RET = " + ret.ToString(), MAC_id);
                        }

                        //Numero di ore di lavoro
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2128, 2131, 12, D2128);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ORE LAV. [D2128], RET = " + ret.ToString(), MAC_id);
                        }

                        //Numero di ore di accensione CNC
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2132, 2135, 12, D2132);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ORE ACCENSIONE [D2132], RET = " + ret.ToString(), MAC_id);
                        }

                        ////---------------------------/////////
                        ////--------DIAGNOSI MANDRINO--/////////
                        ////---------------------------/////////

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2060, 2063, 12, D2060);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2060], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2064, 2067, 12, D2064);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2064], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2068, 2071, 12, D2068);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2068], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2072, 2075, 12, D2072);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2072], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2076, 2079, 12, D2076);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2076], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2224, 2227, 12, D2224);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2224], RET = " + ret.ToString(), MAC_id);
                        }


                        ////--------------------------------/////////
                        ////--------MOTORI TRIFASE---------/////////
                        ////------------------------------/////////

                        //--- Ore totali Frigo Olio
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2420, 2423, 12, D2420);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. FRIGO OLIO [D2420], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2416, 2419, 12, D2416);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. FRIGO OLIO [D2416], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2412, 2415, 12, D2412);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. FRIGO OLIO [D2412], RET = " + ret.ToString(), MAC_id);
                        }

                        //--- Ore Acqua da esterno
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2148, 2151, 12, D2148);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQUA DA ESTERNO [D2148], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2144, 2147, 12, D2144);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQUA DA ESTERNO [D2144], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2140, 2143, 12, D2140);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQAUA DA ESTERNO [D2140], RET =  " + ret.ToString(), MAC_id);

                        }

                        //--- Ore Acqua da interno
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2156, 2159, 12, D2156);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQUA DA INTERNO [D2156], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2160, 2163, 12, D2160);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQUA DA INTERNO [D2160], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2152, 2155, 12, D2152);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ACQAUA DA INTERNO [D2152], RET =  " + ret.ToString(), MAC_id);
                        }

                        //--- Ore Lavaggio bancale
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2168, 2171, 12, D2168);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. LAV. BANCALE [D2168], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2172, 2175, 12, D2172);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. LAV. BANCALE [D2172], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2164, 2167, 12, D2164);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. LAV. BANCALE [D2164], RET =  " + ret.ToString(), MAC_id);
                        }

                        //--- Ore Magazzino utensili
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2180, 2183, 12, D2180);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MAGAZZINO C.U. [D2180], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2184, 2187, 12, D2184);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MAGAZZINO C.U. [D2184], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2176, 2179, 12, D2176);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MAGAZZINO C.U. [D2176], RET =  " + ret.ToString(), MAC_id);
                        }

                        //--- Ore Braccio utensili
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2192, 2195, 12, D2192);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BRACCIO C.U. [D2192], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2196, 2199, 12, D2196);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BRACCIO C.U. [DI2196], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2188, 2191, 12, D2188);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BRACCIO C.U. [D2176], RET =  " + ret.ToString(), MAC_id);
                        }

                        ////--------------------------------/////////
                        ////------------ACCESSORI----------/////////
                        ////------------------------------/////////

                        //--- Pinza Sblocco utensile
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2400, 2403, 12, D2400);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. PINZA UT. [D2400], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2404, 2407, 12, D2404);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. PINZA UT. [2404], RET =  " + ret.ToString(), MAC_id);
                        }

                        //--- Batterie CNC
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2436, 2439, 12, D2436);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BATTERIA CNC [2436], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2440, 2443, 12, D2440);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BATTERIA CNC [D2440], RET =  " + ret.ToString(), MAC_id);
                        }

                        //--- Batterie DRIVE
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2448, 2451, 12, D2448);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BATTERIA DRIVE [D2192], RET =  " + ret.ToString(), MAC_id);

                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2452, 2455, 12, D2452);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BATTERIA DRIVE [D2452], RET =  " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2456, 2459, 12, D2456);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. BATTERIA DRIVE [D2456], RET =  " + ret.ToString(), MAC_id);
                        }

                        #endregion
                        //DiagnosticaDBFresa(connection_Diagnosi, );

                        sqlQueryDiagnostica = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_tempMandrino', '" + DNG403.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseX', '" + DNG308_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseY', '" + DNG308_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseZ', '" + DNG308_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncX', '" + DNG309_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncY', '" + DNG309_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncZ', '" + DNG309_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadMandrino', '" + DNG410.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseX', '" + LOAD.svload1.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseY', '" + LOAD.svload2.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseZ', '" + LOAD.svload3.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_rmpMandrino', '" + SPDSPEED.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroX', '" + D2004.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroY', '" + D2012.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroZ', '" + D2020.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoX', '" + D2000.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoY', '" + D2008.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoZ', '" + D2016.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriX', '" + D2040.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriY', '" + D2044.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriZ', '" + D2048.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_sbloccaggiCU', '" + D2120.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_cambiEseguiti', '" + D2124.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreLavoro', '" + D2128.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreAccensione', '" + D2132.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm1', '" + D2060.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm2', '" + D2064.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm3', '" + D2068.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm4', '" + D2072.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm5', '" + D2076.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_timeOverLimit', '" + D2224.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_limitX', '" + D2200.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_limitY', '" + D2204.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_limitZ', '" + D2208.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_frigoOlio_attuali', '" + D2420.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_frigoOlio_tot', '" + D2416.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_frigoOlio_limite', '" + D2412.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaEsterno_attuali', '" + D2144.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaEsterno_tot', '" + D2148.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaEsterno_limite', '" + D2140.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaInterno_attuali', '" + D2156.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaInterno_tot', '" + D2160.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_acquaInterno_limite', '" + D2152.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_lavBancale_attuale', '" + D2168.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_lavBancale_tot', '" + D2172.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_lavBancale_limite', '" + D2164.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_magUt_attuale', '" + D2180.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_magUt_tot', '" + D2184.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_magUt_limite', '" + D2176.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_braccioUt_attuale', '" + D2192.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_braccioUt_tot', '" + D2196.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_braccioUt_limite', '" + D2188.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_pinzaUt_attuale', '" + D2404.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_pinzaUt_limite', '" + D2400.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_batCNC_attuale', '" + D2440.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_batCNC_limite', '" + D2436.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_batDrive_attuale', '" + D2452.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_batDrive_tot', '" + D2456.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_batDrive_limite', '" + D2448.ldata[0].ToString() + "', " + man_id + ") " ;
                        val_num_assi = true;
                    }
                    if (val_num_assi) DiagnosticaDB(connection_Diagnosi, sqlQueryDiagnostica);
                }
                if (!connesso && hndlDiagnostica != 0)
                {
                    Focas1.cnc_freelibhndl(hndlDiagnostica);
                    hndlDiagnostica = 0;
                }
            }
        }
        
        /*public int DiagnosticaDBFresa(SqlConnection mysqlCon, string MAN_tempMandrino = null, string MAN_tempAsseX = null, string MAN_tempAsseY = null, string MAN_tempAsseZ = null, string MAN_tempAsseB = null, string MAN_tempAsseC = null, string MAN_tempEncX = null, string MAN_tempEncY = null, string MAN_tempEncZ = null, string MAN_tempEncB = null, string MAN_tempEncC = null, string MAN_loadMandrino = null, string MAN_loadAsseX = null, string MAN_loadAsseY = null, string MAN_loadAsseZ = null, string MAN_loadAsseB = null, string MAN_loadAsseC = null, string MAN_rmpMandrino = null,  string MAN_metriLavoroX = null, string MAN_metriRapidoX = null, string MAN_metriLavoroY = null, string MAN_metriRapidoY = null, string MAN_metriLavoroZ = null, string MAN_metriRapidoZ = null, string MAN_metriLavoroB = null, string MAN_metriRapidoB = null, string MAN_metriLavoroC = null, string MAN_metriRapidoC = null, string MAN_metriX = null, string MAN_metriY = null, string MAN_metriZ = null, string MAN_metriB = null, string MAN_metriC = null, string MAN_sbloccaggiCU = null, string MAN_cambiEseguiti = null, string MAN_oreLavoro = null, string MAN_oreAccensione = null, string MAN_rpm1 = null, string MAN_rpm2 = null, string MAN_rpm3 = null, string MAN_rpm4 = null, string MAN_rpm5 = null, string MAN_timeOverLimit = null, string MAN_limitX = null, string MAN_limitY = null, string MAN_limitZ = null, string MAN_limitB = null, string MAN_limitC = null,  string MAN_frigoOlio_attuali = null, string MAN_frigoOlio_tot = null, string MAN_frigoOlio_limite = null, string MAN_acquaEsterno_attuali = null, string MAN_acquaEsterno_tot = null, string MAN_acquaEsterno_limite = null, string MAN_acquaInterno_attuali = null, string MAN_acquaInterno_tot = null, string MAN_acquaInterno_limite = null, string MAN_lavBancale_attuale = null, string MAN_lavBancale_tot = null, string MAN_lavBancale_limite = null, string MAN_magUt_attuale = null, string MAN_magUt_tot = null, string MAN_magUt_limite = null, string MAN_braccioUt_attuale = null, string MAN_braccioUt_tot = null, string MAN_braccioUt_limite = null, string MAN_pinzaUt_attuale = null, string MAN_pinzaUt_limite = null, string MAN_batCNC_attuale = null, string MAN_batCNC_limite = null, string MAN_batDrive_attuale = null, string MAN_batDrive_tot = null, string MAN_batDrive_limite = null)
        {
            return base.DiagnosticaDB(mysqlCon, MAN_tempMandrino, MAN_tempAsseX, MAN_tempAsseY, MAN_tempAsseZ, MAN_tempAsseB, MAN_tempAsseC, MAN_tempEncX, MAN_tempEncY, MAN_tempEncZ, MAN_tempEncB, MAN_tempEncC, MAN_loadMandrino, MAN_loadAsseX, MAN_loadAsseY, MAN_loadAsseZ, MAN_loadAsseB, MAN_loadAsseC, MAN_rmpMandrino, MAN_metriLavoroX, MAN_metriRapidoX, MAN_metriLavoroY, MAN_metriRapidoY, MAN_metriLavoroZ, MAN_metriRapidoZ, MAN_metriLavoroB, MAN_metriRapidoB, MAN_metriLavoroC, MAN_metriRapidoC, MAN_metriX, MAN_metriY, MAN_metriZ, MAN_metriB, MAN_metriC, MAN_sbloccaggiCU, MAN_cambiEseguiti, MAN_oreLavoro, MAN_oreAccensione, MAN_rpm1, MAN_rpm2, MAN_rpm3, MAN_rpm4, MAN_rpm5, MAN_timeOverLimit, MAN_limitX, MAN_limitY, MAN_limitZ, MAN_limitB, MAN_limitC, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, MAN_frigoOlio_attuali, MAN_frigoOlio_tot, MAN_frigoOlio_limite, MAN_acquaEsterno_attuali, MAN_acquaEsterno_tot, MAN_acquaEsterno_limite, MAN_acquaInterno_attuali, MAN_acquaInterno_tot, MAN_acquaInterno_limite, MAN_lavBancale_attuale, MAN_lavBancale_tot, MAN_lavBancale_limite, MAN_magUt_attuale, MAN_magUt_tot, MAN_magUt_limite, MAN_braccioUt_attuale, MAN_braccioUt_tot, MAN_braccioUt_limite, MAN_pinzaUt_attuale, MAN_pinzaUt_limite, MAN_batCNC_attuale, MAN_batCNC_limite, MAN_batDrive_attuale, MAN_batDrive_tot, MAN_batDrive_limite);
        }
        */
        #endregion

        #region gestione Produzione
        public override void gestioneProduzione(object myId = null)
        {
            while (Service1.alive)
            {
                Thread.Sleep(5000);
                gestioneProduzioneMacchineUtComune();
                if (connesso && hndlProduzione!=0)
                {
                    if (MAC_tipoControllo == "31iB5_old")
                    {
                        calcoloTempi(5252, 5275);
                        aggiungiCommessa(5240, 5251);
                    }
                    else if (MAC_tipoControllo == "0i-MF" || MAC_tipoControllo == "31iB5")
                    {
                        calcoloTempi(2252, 2275);
                        aggiungiCommessa(2240, 2251);
                    }
                    else
                    {
                        Logs.LogDB(connection_Produzione, "Tipo controllo errato, Fresa", MAC_id);
                    }
                    spostaCommesse();
                }
                
            }
        }
        #endregion
                
    }
}
