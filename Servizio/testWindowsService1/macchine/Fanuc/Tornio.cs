﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cns.utils;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace cns.macchine.Fanuc
{
    class Tornio : FanucCN
    {
        public Tornio(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {


        }

        #region gestione Interfaccia
        public override void gestioneInterfaccia(object myId = null)
        {
            while (Service1.alive)
            {
                if (connesso)
                {
                    //non istanziato
                    if (hndl == 0)
                    {
                        //istanzio connessione
                        ret = Focas1.cnc_allclibhndl3(this.MAC_ipAdress, 8193, 9, out hndl);
                        if (ret != Focas1.EW_OK)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE GENERAZIONE HANDLE, RET = " + ret.ToString(), MAC_id);
                            Focas1.cnc_freelibhndl(hndl);
                            hndl = 0;
                        }
                        //ret = Focas1.cnc_allclibhndl2(9, out hndl);
                    }
                    else
                    {
                        #region Lettura Numero Path attivi
                        ret = Focas1.cnc_diagnoss(hndl, 1111, 0, 5, Path);
                        if (ret == Focas1.EW_OK)
                            pathMacchina = Path.u.ldata.ToString();
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA NUMERO PATH MACCHINA, RET =  " + ret.ToString(), MAC_id);
                        }
                        #endregion
                        ret = 0;
                        checkStatus();
                        ret = 0;
                        letturaProgrammaAttivo();
                        ret = 0;
                        #region Lettura feedRate Assi
                        //Modifica per KAFO STM
                        ret = Focas1.cnc_rdparam(hndl, 3002, 0, 5, IOV);
                        if (ret == Focas1.EW_OK)
                        {
                            ret = Focas1.cnc_rdparam(hndl, 3002, 0, 5, IOV);
                            if (IOV.u.cdata > 15 && IOV.u.cdata < 32 ||
                                IOV.u.cdata > 47 && IOV.u.cdata < 64 ||
                                IOV.u.cdata > 79 && IOV.u.cdata < 96 ||
                                IOV.u.cdata > 111 && IOV.u.cdata < 128 ||
                                IOV.u.cdata > 143 && IOV.u.cdata < 160 ||
                                IOV.u.cdata > 175 && IOV.u.cdata < 192 ||
                                IOV.u.cdata > 207 && IOV.u.cdata < 224 ||
                                IOV.u.cdata > 239
                                )
                            {
                                feedDritto();
                            }
                            else
                                feedRovesciati();
                        }
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA PARAMETRO IOV, RET = " + ret.ToString(), MAC_id);
                        }
                        #endregion
                        ret = 0;
                        feedRateMandrino();
                        ret = 0;
                        #region Lettura Utensile in mandrino

                        if (MAC_tipoControllo == "0iTF")
                        {
                            ret = Focas1.pmc_rdpmcrng(hndl, 9, 0, 400, 400, 9, D400);
                            if (ret == Focas1.EW_OK)
                            {
                                utensileAttivo = Convert.ToInt32(D400.ldata[0]);
                            }
                            else
                            {
                                Logs.LogDB(connection_Fanuc, "ERRORE ERRORE UTENSILE IN MANDRINO [D400], RET = " + ret.ToString(), MAC_id);
                            }
                        }

                        #endregion
                        //ret = 0;
                        #region Lettura Numero Pezzi fatti
                        /*ret = Focas1.cnc_rdmacro(hndl, 3901, 10, PZ);
                        if (ret == Focas1.EW_OK)
                        {
                            pezziFatti = Convert.ToInt32((PZ.mcr_val / Math.Pow(10, PZ.dec_val)));
                        }
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA NUMERO PEZZI FATTI, RET = " + ret.ToString());
                        }*/
                        #endregion
                        ret = 0;
                        //Scrittura dati in Database
                        InterfacciDB(connection_Fanuc);
                    }

                }
                else
                {
                    if (hndl != 0)
                    {
                        Focas1.cnc_freelibhndl(hndl);
                        hndl = 0;
                        Logs.LogDB(connection_Fanuc, "Tornio Fanuc Disconnesso");
                    }
                }
                Thread.Sleep(3000);
            }
        }
        #endregion

        #region gestione Diagnostica
        public override void gestioneDiagnostica(object myId = null)
        {
            short ret = 0;
            bool handle = false;
            int MAC_numeroAssi = 0;

            while (Service1.alive)
            {
                Thread.Sleep(1000);

                if (hndlDiagnostica == 0 && connesso)
                {
                    //ret = Focas1.cnc_allclibhndl2(9, out hndlDiagnostica);
                    ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlDiagnostica);
                    if (ret != Focas1.EW_OK)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE GENERAZIONE HANDLE, RET = " + ret.ToString(), MAC_id);
                        Focas1.cnc_freelibhndl(hndlDiagnostica);
                        hndlDiagnostica = 0;
                    }
                    else
                        handle = true;
                }

                if (connesso && hndlDiagnostica!=0)
                {



                    letturaDatiMacchineUtComuni();
                    string sqlQueryDiagnostica = "";

                    ////---------------------------/////////
                    ////--------METRI ASSE---------/////////
                    ////---------------------------/////////

                    //Asse X
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2000, 2003, 12, D2000);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE X [D2000], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2004, 2007, 12, D2004);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE X [D2004], RET = " + ret.ToString(), MAC_id);
                    }

                    //Asse Z
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2008, 2011, 12, D2008);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE Z [D2008], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2012, 2015, 12, D2012);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE Z [D2012], RET = " + ret.ToString(), MAC_id);
                    }

                    //Asse C
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2016, 2019, 12, D2016);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE C [D2016], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2020, 2023, 12, D2020);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE C [D2020], RET = " + ret.ToString(), MAC_id);
                    }

                    //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                    if (MAC_numeroAssi > 3)
                    {
                        //Asse B
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5024, 5027, 12, D5024);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE B [D5024], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5028, 5031, 12, D5028);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE B [D5028], RET = " + ret.ToString(), MAC_id);
                        }
                    }

                    //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                    if (MAC_numeroAssi > 4)
                    {
                        //Asse C
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5032, 5035, 12, D5032);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI R ASSE C [D5032], RET = " + ret.ToString(), MAC_id);
                        }

                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5036, 5039, 12, D5036);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI ASSE C [D5036], RET = " + ret.ToString(), MAC_id);
                        }
                    }

                    //Metri TOT X
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2040, 2043, 12, D2040);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE X [D2040], RET = " + ret.ToString(), MAC_id);
                    }

                    //Metri TOT Z
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2044, 2047, 12, D2044);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE Z [D2044], RET = " + ret.ToString(), MAC_id);
                    }

                    //Metri TOT C
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2048, 2051, 12, D2048);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE C [D2048], RET = " + ret.ToString(), MAC_id);
                    }

                    //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                    if (MAC_numeroAssi > 3)
                    {
                        //Metri TOT B
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5052, 5055, 12, D5052);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE B [D5052], RET = " + ret.ToString(), MAC_id);
                        }
                    }

                    //CONTROLLARE I VALORI PER IL TORNIO CAMBIARE DA D5XXX A D2XXX
                    if (MAC_numeroAssi > 4)
                    {
                        //Metri TOT C
                        ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 5056, 5059, 12, D5056);
                        if (ret != 0)
                        {
                            Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA METRI T ASSE C [D2056], RET = " + ret.ToString(), MAC_id);
                        }
                    }

                    ////---------------------------/////////
                    ////--------CAMBIO UTENSILE----/////////
                    ////---------------------------/////////

                    //Ciclo blocco sblocco mandrino
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2120, 2123, 12, D2120);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D2120], RET = " + ret.ToString(), MAC_id);
                    }

                    //Numero ciclo di cambio utensile da torretta effettuati
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2124, 2127, 12, D2124);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. C.U. [D2124], RET = " + ret.ToString(), MAC_id);
                    }

                    //Numero di ore di lavoro
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2128, 2131, 12, D2128);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ORE LAV. [D2128], RET = " + ret.ToString(), MAC_id);
                    }

                    //Numero di ore di accensione CNC
                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2132, 2135, 12, D2132);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. ORE ACCENSIONE [D2132], RET = " + ret.ToString(), MAC_id);
                    }

                    ////---------------------------/////////
                    ////--------DIAGNOSI MANDRINO--/////////
                    ////---------------------------/////////

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2060, 2063, 12, D2060);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2060], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2064, 2067, 12, D2064);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2064], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2068, 2071, 12, D2068);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2068], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2072, 2075, 12, D2072);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2072], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2076, 2079, 12, D2076);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2076], RET = " + ret.ToString(), MAC_id);
                    }

                    ret = Focas1.pmc_rdpmcrng(hndlDiagnostica, 9, 2, 2224, 2227, 12, D2224);
                    if (ret != 0)
                    {
                        Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA DIAGN. MANDRINO [D2224], RET = " + ret.ToString(), MAC_id);
                    }

                    /////---------- da controllare i codici 5000/2000 ----------------/////////
                    bool val_num_assi = false;
                    if (MAC_numeroAssi >= 3)
                    {
                        sqlQueryDiagnostica = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                "('MAN_tempMandrino', '" + DNG403.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempAsseX', '" + DNG308_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempAsseZ', '" + DNG308_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempAsseC', '" + DNG308_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempEncX', '" + DNG309_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempEncZ', '" + DNG309_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_tempEncC', '" + DNG309_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_loadMandrino', '" + DNG410.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                "('MAN_loadAsseX', '" + LOAD.svload1.data.ToString() + "', " + man_id + "), " +
                                "('MAN_loadAsseZ', '" + LOAD.svload2.data.ToString() + "', " + man_id + "), " +
                                "('MAN_loadAsseC', '" + LOAD.svload3.data.ToString() + "', " + man_id + "), " +
                                "('MAN_rmpMandrino', '" + SPDSPEED.data.ToString() + "', " + man_id + "), " +
                                "('MAN_metriLavoroX', '" + D2004.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriLavoroZ', '" + D2012.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriLavoroC', '" + D2020.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriRapidoX', '" + D2000.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriRapidoZ', '" + D2008.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriRapidoC', '" + D2016.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriX', '" + D2040.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriZ', '" + D2044.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_metriC', '" + D2048.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_sbloccaggiCU', '" + D2120.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_cambiEseguiti', '" + D2124.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_oreLavoro', '" + D2128.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_oreAccensione', '" + D2132.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_rpm1', '" + D2060.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_rpm2', '" + D2064.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_rpm3', '" + D2068.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_rpm4', '" + D2072.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_rpm5', '" + D2076.ldata[0].ToString() + "', " + man_id + "), " +
                                "('MAN_timeOverLimit', '" + D2224.ldata[0].ToString() + "', " + man_id + ") ";
                        if (MAC_numeroAssi == 3) val_num_assi = true;
                    }
                    if (MAC_numeroAssi == 4)
                    {
                        sqlQueryDiagnostica = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_tempMandrino', '" + DNG403.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseX', '" + DNG308_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseY', '" + DNG308_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseZ', '" + DNG308_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempAsseB', '" + DNG308_4.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncX', '" + DNG309_1.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncY', '" + DNG309_2.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncZ', '" + DNG309_3.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_tempEncB', '" + DNG309_4.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadMandrino', '" + DNG410.u.rdata.dgn_val.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseX', '" + LOAD.svload1.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseY', '" + LOAD.svload2.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseZ', '" + LOAD.svload3.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_loadAsseB', '" + LOAD.svload4.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_rmpMandrino', '" + SPDSPEED.data.ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroX', '" + D5004.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroY', '" + D5012.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroZ', '" + D5020.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriLavoroB','" + D5028.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoX', '" + D5000.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoY', '" + D5008.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoZ', '" + D5016.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriRapidoB', '" + D5024.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriX', '" + D5040.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriY', '" + D5044.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriZ', '" + D5048.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_metriB', '" + D5052.ldata[0].ToString() + "', " + man_id + ") " +
                                    "('MAN_sbloccaggiCU', '" + D5120.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_cambiEseguiti', '" + D5124.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreLavoro', '" + D5128.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_oreAccensione', '" + D5132.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm1', '" + D5060.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm2', '" + D5064.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm3', '" + D5068.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm4', '" + D5072.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_rpm5', '" + D5076.ldata[0].ToString() + "', " + man_id + "), " +
                                    "('MAN_timeOverLimit', '" + D5224.ldata[0].ToString() + "', " + man_id + ") ";
                        val_num_assi = true;

                    }
                    if (val_num_assi) DiagnosticaDB(connection_Diagnosi, sqlQueryDiagnostica);
                }

                if (!connesso && hndlDiagnostica!=0)
                {
                    Focas1.cnc_freelibhndl(hndlDiagnostica);
                    hndlDiagnostica = 0;
                    handle = false;
                }
            }
        }
        #endregion
        

        #region gestione Produzione
        public override void gestioneProduzione(object myId = null)
        {
            while (Service1.alive)
            {
                Thread.Sleep(5000);
                gestioneProduzioneMacchineUtComune();
                if (connesso && hndlProduzione != 0)
                {
                    if (MAC_tipoControllo == "0iTF")
                    {
                        calcoloTempi(2252, 2275);
                        aggiungiCommessa(2240, 2251);
                    }
                    else
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Tipo controllo errato, Tornio", MAC_id);
                    }
                    spostaCommesse();
                }
            }
        }
        #endregion
        

    }
}
