﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Data;
using cns.utils;

using System.Threading;
using System.Configuration;

namespace cns.macchine.Fanuc
{
    class Robot : FanucCN
    {

        #region DEFINIZIONI PER FANUC ROBOT

        private volatile bool istanziato = false;
        //public string HostName = "";
        private FRRJIf.Core mobjCore=null;
        private FRRJIf.DataTable mobjDataTable;
        private FRRJIf.DataTable mobjDataTable2;
        private FRRJIf.DataString mobjStrReg;
        private FRRJIf.DataString mobjStrReg2;
        private FRRJIf.DataString mobjStrRegComment;
        private FRRJIf.DataSysVar mobjSysVarInt;
        private FRRJIf.DataNumReg mobjNumReg;
        private FRRJIf.DataNumReg mobjNumReg2;
        private FRRJIf.DataNumReg mobjNumReg3;
        private FRRJIf.DataAlarm mobjAlarm;
        private FRRJIf.DataAlarm mobjAlarmCurrent;
        //public bool connesso = false;

        private Array intSDO = new short[100];
        private Array intSI = new short[2];
        private Array intSO = new short[10];
        private Array intUO = new short[10];
        private Array intUI = new short[10];
        private Array intRDO = new short[3];
        private object vntValue = null;
        private object vntValue2 = null;
        private object vntValue3 = null;
        private bool blnDT = false;
        //bool blnSDO = false;
        private bool blnSI = false;
        private bool blnSO = false;
        private bool blnUO = false;
        private new int? commessaAttiva=null;

        private int secondiLavoro=0;
        private int secondiPiazzamento=0;
        /*public static string strTmp = null;
        //
        public int[] rispostaScrittura;
        public int[] rispostaScrittura2;
        public int[] rispostaDiagnostici;
        public int[] rispostaDiagnostici2;
        public int[] rispostaRichieste = new int[10];*/
        #endregion
        #region funzioni ausiliarie
        //Connessione a Robot
        private bool msubInit()
        {
            if (this.mobjCore != null)
            {
                //Console.WriteLine(MAC_id+mobjCore.ToString());
                return true;
            }

            try
            {
                int shift = Convert.ToInt32(ConfigurationSettings.AppSettings["shiftrobot"]);
                this.mobjCore = new FRRJIf.Core();
                //connect
                // You need to set data table before connecting.
                this.mobjDataTable = this.mobjCore.DataTable;

                {
                    //mobjAlarm = mobjDataTable.AddAlarm(FRRJIf.FRIF_DATA_TYPE.ALARM_LIST, 5, 0);
                    mobjAlarmCurrent = mobjDataTable.AddAlarm(FRRJIf.FRIF_DATA_TYPE.ALARM_CURRENT, 11, 0);
                    mobjStrReg = mobjDataTable.AddString(FRRJIf.FRIF_DATA_TYPE.STRREG, 1, 3);
                    mobjStrReg2 = mobjDataTable.AddString(FRRJIf.FRIF_DATA_TYPE.STRREG, 11, 15);
                    mobjStrRegComment = mobjDataTable.AddString(FRRJIf.FRIF_DATA_TYPE.STRREG_COMMENT, 1, 3);
                    mobjSysVarInt = mobjDataTable.AddSysVar(FRRJIf.FRIF_DATA_TYPE.SYSVAR_INT, "$MCR.$GENOVERRIDE");
                    mobjNumReg = mobjDataTable.AddNumReg(FRRJIf.FRIF_DATA_TYPE.NUMREG_INT, 601+shift, 630+shift);       
                    mobjNumReg2 = mobjDataTable.AddNumReg(FRRJIf.FRIF_DATA_TYPE.NUMREG_REAL, 631 + shift, 640 + shift);     
                    Debug.Assert(mobjStrRegComment != null);
                }

                mobjDataTable2 = mobjCore.DataTable;
                {
                    mobjNumReg3 = mobjDataTable.AddNumReg(FRRJIf.FRIF_DATA_TYPE.NUMREG_INT, 641 + shift, 670 + shift);      
                }

                if (!this.istanziato)
                    this.istanziato = this.mobjCore.Connect(MAC_ipAdress);

                if (istanziato == false)
                {
                    msubClearVars();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                
                using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    Logs.LogDB(connection, ex.Message + " " + ex.StackTrace, MAC_id);
                mobjCore = null;
                return false;
            }
        }

        //Robot Disconnesso
        private void msubDisconnected()
        {
            //MessageBox.Show("Connect error");
            SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
            Logs.LogDB(connection, "Robot non disponibile per connessione tramite libreria", MAC_id);

            //Console.WriteLine("ROBOT DISCONNESSO" + MAC_id);
            msubClearVars();
        }

        //Pulitura Variabili
        private void msubClearVars()
        {
            mobjCore.Disconnect();
            istanziato = false;
            mobjCore = null;
            mobjDataTable = null;
            mobjDataTable2 = null;
            //mobjAlarm[id_Robot] = null;
            mobjAlarmCurrent = null;
            mobjSysVarInt = null;
            mobjNumReg = null;
            mobjNumReg2 = null;
            mobjNumReg3 = null;
            mobjStrReg = null;
            mobjStrReg2 = null;
            mobjStrRegComment = null;
        }

        //Funzione di lettura allarmi attivi sul Robot
        private string mstrAlarm(ref FRRJIf.DataAlarm objAlarm, int Count)
        {
            string tmp = null;
            short intID = 0;
            short intNumber = 0;
            short intCID = 0;
            short intCNumber = 0;
            short intSeverity = 0;
            short intY = 0;
            short intM = 0;
            short intD = 0;
            short intH = 0;
            short intMn = 0;
            short intS = 0;
            string strM1 = "";
            string strM2 = "";
            string strM3 = "";
            bool blnRes = false;

            blnRes = objAlarm.GetValue(Count, ref intID, ref intNumber, ref intCID, ref intCNumber, ref intSeverity, ref intY, ref intM, ref intD, ref intH,
            ref intMn, ref intS, ref strM1, ref strM2, ref strM3);

            if (blnRes == false)
            {
                tmp = tmp + "Error\r\n";
                return tmp;
            }

            if (!string.IsNullOrEmpty(strM1))
                tmp = tmp + strM1;
            else
                tmp = tmp + "Error\r\n";

            return tmp;
        }

        #endregion

        //costruttore
        public Robot(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {


        }

        //istanziato in diagnostica

        #region gestione Interfaccia 
        public override void gestioneInterfaccia(object myId = null)
        {
            while (Service1.alive)
            {
                //msubInit();
                Thread.Sleep(10000);
                if (this.istanziato && connesso)
                {
                    try
                    {
                        try
                        {
                            //Console.WriteLine("refresh table in interfaccia " + MAC_id);
                            blnDT = mobjDataTable.Refresh();
                        }
                        catch (Exception ex)
                        {
                            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                Logs.LogDB(connection, ex.Message + " " + ex.StackTrace, MAC_id);
                        }

                        if (blnDT == false)
                        {
                            msubDisconnected();
                            return;
                        }

                        status = null;
                        feedAssi = null;

                        #region Lettura SI
                        blnSI = mobjCore.ReadSI(8, ref intSI, 2);
                        if (blnSI == false)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA SI", MAC_id);
                        }

                        if (Convert.ToInt16(intSI.GetValue(0)) == 1 && Convert.ToInt16(intSI.GetValue(1)) == 1)
                            status += "AUTO ";
                        else if (Convert.ToInt16(intSI.GetValue(0)) == 1 && Convert.ToInt16(intSI.GetValue(1)) == 0)
                            status += "T1 ";
                        else if (Convert.ToInt16(intSI.GetValue(0)) == 0 && Convert.ToInt16(intSI.GetValue(1)) == 0)
                            status += "T2 ";
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ANOMALIA CONTROLLO NUMERICO", MAC_id);
                        }

                        status += " ";
                        #endregion

                        //read UO
                        #region Letturo UO
                        blnUO = mobjCore.ReadUO(2, ref intUO, 6);
                        if (blnUO == false)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA UO, RET = " + ret.ToString(), MAC_id);
                        }

                        //Lettura modi                            
                        if (Convert.ToInt16(intUO.GetValue(1)) == 1) //UO3
                            status += "RUN ";
                        else if (Convert.ToInt16(intUO.GetValue(1)) == 0)
                            status += "STP ";
                        else if (Convert.ToInt16(intUO.GetValue(2)) == 1) //UO4
                            status += "BRK "; ;

                        status += " ";

                        //Lettura stato di Home
                        if (Convert.ToInt16(intUO.GetValue(6)) == 1) //UO7
                            status = "HME ";
                        else
                            status += "***";
                        #endregion
                        status += " ";

                        //read SO
                        #region Lettura SO
                        blnSO = mobjCore.ReadSO(3, ref intSO, 5);
                        if (blnSO == false)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA SO", MAC_id);
                        }

                        status += " ";

                        //Lettura stato di EMERGENZA
                        blnSI = mobjCore.ReadSI(0, ref intSI, 1);
                        if (blnSI == false)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA STATO EMERGENZA", MAC_id);
                        }

                        if (Convert.ToInt16(intSI.GetValue(0)) == 0)
                            status += "EMG ";
                        else
                            status += "***";

                        status += " ";

                        //Lettura stato di allarme                            
                        if (Convert.ToInt16(intSO.GetValue(0)) == 1)
                            status += "ALM";
                        else
                            status += "***";

                        #endregion

                        //read Sys Var                         
                        #region lettura Feed Robot
                        if (mobjSysVarInt.GetValue(ref vntValue3) == true)
                            feedAssi = Convert.ToInt32(vntValue3);
                        else
                            feedAssi = 00;
                        #endregion

                        #region Lettura programma Attivo
                        //for (int i = 1; i <= 2; i++)
                        //{
                        //    mobjStrReg.GetValue(i, ref tmpComment);
                        //    if (mobjStrReg.GetValue(i, ref tmpValue) == true)
                        //        tmpString = tmpString + "/" + String.Format(tmpComment);
                        //}
                        //execProg = tmpString.TrimStart('/');
                        #endregion

                        //read Num Reg
                        #region lettura Contapezzi
                        for (int n = mobjNumReg.StartIndex; n <= mobjNumReg.EndIndex; n++)
                        {
                            if (mobjNumReg.GetValue(n, ref vntValue2) == true)
                                pezziFatti = Convert.ToInt32(vntValue2);
                            else
                                pezziFatti = 00;
                        }
                        #endregion
                        for (int ii = 1; ii < 12; ii++)
                        {
                            strTmp = null;
                            strTmp = mstrAlarm(ref mobjAlarmCurrent, ii);

                            if (strTmp != "Error\r\n")
                            {
                                String[] substrings = strTmp.Split(' ');
                                int lunghezza = substrings.Length;
                                string All_message = null;
                                for (int app = 1; app < lunghezza; app++)
                                    All_message += substrings[app] + " ";

                                Service1.AlarmDB(connection_Fanuc, substrings[0], All_message, macId:MAC_id);
                            }
                        }
                        InterfacciDB(connection_Fanuc);
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""),"interfaccia robot " +ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
            }
        }
        #endregion
        
        #region gestione Diagnostica
        public override void gestioneDiagnostica(object myId = null)
        {
            //short ret = 0;
            //string sqlQuery = null;
            //int N = 0;
            //bool handle = false;
            //int MAC_numeroAssi = 0;
            while (Service1.alive)
            {
                Thread.Sleep(2000);

                //if (mobjCore != null)
                //{
                if (connesso)
                {
                    if (!istanziato) msubInit();
                    else
                    {
                        try
                        {
                            if (istanziato)
                            {
                                #region lettura variabili diagnostica
                                int jj = 0;
                                //Pulisco appoggio per Diagnostici da Robot
                                for (int zz = mobjNumReg3.StartIndex; zz <= mobjNumReg3.EndIndex; zz++)
                                {
                                    rispostaDiagnostici[jj] = 0;
                                    jj++;
                                }

                                //Lettura registro numerico Robot
                                jj = 0;

                                for (int xx = mobjNumReg3.StartIndex; xx <= mobjNumReg3.EndIndex; xx++)
                                {
                                    if (mobjNumReg3.GetValue(xx, ref vntValue) == true)
                                        rispostaDiagnostici[jj] = Convert.ToInt32(vntValue);
                                    else
                                    {
                                        rispostaDiagnostici[jj] = 0;
                                    }
                                    jj++;
                                }
                                #endregion

                                Letman();

                                string sqlQueryDiagnostica = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_opPinza1Grezzo', '" + rispostaDiagnostici[17] + "', " + man_id + "), " +
                                    "('MAN_clPinza1Grezzo', '" + rispostaDiagnostici[18] + "', " + man_id + "), " +
                                    "('MAN_opPinza1Finiti', '" + rispostaDiagnostici[0] + "', " + man_id + "), " +
                                    "('MAN_clPinza1Finiti', '" + rispostaDiagnostici[1] + "', " + man_id + "), " +
                                    "('MAN_opPinza2Grezzo', '" + rispostaDiagnostici[2] + "', " + man_id + "), " +
                                    "('MAN_clPinza2Grezzo', '" + rispostaDiagnostici[3] + "', " + man_id + "), " +
                                    "('MAN_opPinza2Finiti', '" + rispostaDiagnostici[4] + "', " + man_id + "), " +
                                    "('MAN_clPinza2Finiti', '" + rispostaDiagnostici[5] + "', " + man_id + "), " +
                                    "('MAN_violazioniGialla', '" + rispostaDiagnostici[9] + "', " + man_id + "), " +
                                    "('MAN_violazioniRossa', '" + rispostaDiagnostici[10] + "', " + man_id + "), " +
                                    "('MAN_opPinzaPallet', '" + rispostaDiagnostici[11] + "', " + man_id + "), " +
                                    "('MAN_clPinzaPallet',  '" + rispostaDiagnostici[12] + "'," + man_id + "), " +
                                    "('MAN_cestelliCaricati', '" + rispostaDiagnostici[13] + "', " + man_id + "), " +
                                    "('MAN_pzPrelevati', '" + rispostaDiagnostici[14] + "', " + man_id + "), " +
                                    "('MAN_pzDepositati', '" + rispostaDiagnostici[15] + "', " + man_id + "), " +
                                    "('MAN_fineLotto', '" + rispostaDiagnostici[16] + "', " + man_id + "), " +
                                    "('MAN_accInterventoTermicaNastro', '" + rispostaDiagnostici[6] + "', " + man_id + "), " +
                                    "('MAN_accTeleruttoreNastro', '" + rispostaDiagnostici[7] + "', " + man_id + "), " +
                                    "('MAN_accLetturaProxy', '" + rispostaDiagnostici[8] + "', " + man_id + ") ";

                                DiagnosticaDB(connection_Diagnosi, sqlQueryDiagnostica);

                                #region Aggiornamento Interfaccia 2^ parte


                                int www = 0;
                                //Pulisco appoggio per Richieste da Robot
                                for (int cc = mobjNumReg.StartIndex; cc <= mobjNumReg.EndIndex; cc++)
                                {
                                    rispostaScrittura2[www] = 0;
                                    www++;
                                }

                                //Lettura registro numerico Robot
                                www = 0;
                                for (int vv = mobjNumReg.StartIndex; vv <= mobjNumReg.EndIndex; vv++)
                                {
                                    if (mobjNumReg.GetValue(vv, ref vntValue) == true)
                                        rispostaScrittura2[www] = Convert.ToInt32(vntValue);
                                    else
                                    {
                                        rispostaScrittura2[www] = 0;
                                        //MessageBox.Show("Errore0");
                                    }
                                    www++;
                                }
                                www = 0;


                                // variabili di appoggio
                                //string tmpCommessaAttiva = null;
                                string tmpRicettaAttiva = null;
                                int? tmpPinzaAttiva = null;
                                //int? tmpPezziFatti = null;
                                string tmpTotem = null;

                                switch (rispostaDiagnostici[19])
                                {
                                    case 1:
                                        commessaAttiva = rispostaScrittura2[5];
                                        tmpRicettaAttiva = rispostaScrittura2[0].ToString();
                                        tmpPinzaAttiva = rispostaDiagnostici[24];
                                        pezziFatti = rispostaScrittura2[15];
                                        break;

                                    case 2:
                                        commessaAttiva = rispostaScrittura2[6];
                                        tmpRicettaAttiva = rispostaScrittura2[1].ToString();
                                        tmpPinzaAttiva = rispostaDiagnostici[24];
                                        pezziFatti = rispostaScrittura2[16];
                                        break;

                                    case 3:
                                        commessaAttiva = rispostaScrittura2[7];
                                        tmpRicettaAttiva = rispostaScrittura2[2].ToString();
                                        tmpPinzaAttiva = rispostaDiagnostici[24];
                                        pezziFatti = rispostaScrittura2[17];
                                        break;

                                    case 4:
                                        commessaAttiva = rispostaScrittura2[8];
                                        tmpRicettaAttiva = rispostaScrittura2[3].ToString();
                                        tmpPinzaAttiva = rispostaDiagnostici[24];
                                        pezziFatti = rispostaScrittura2[18];
                                        break;

                                    default:
                                        tmpPinzaAttiva = rispostaDiagnostici[24];
                                        break;
                                }

                                if (rispostaDiagnostici[26] == 1)
                                    tmpTotem = "verde";
                                else if (rispostaDiagnostici[27] == 1)
                                    tmpTotem = "rosso";
                                else if (rispostaDiagnostici[28] == 1)
                                    tmpTotem = "blu";


                                sqlQueryDiagnostica = "UPDATE Interfaccia SET INT_programmaAttivo = " + DbHelper.cleanNullValue(tmpRicettaAttiva) + ", INT_utensileAttivo = " + DbHelper.cleanNullValue(tmpPinzaAttiva) + ", " +
                                                        "INT_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + ", INT_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + ", INT_totem = " + DbHelper.cleanNullValue(tmpTotem) +
                                                        " WHERE INT_id = (SELECT MAX(INT_id) FROM Interfaccia WHERE INT_vs_MAC_id = '" + MAC_id + "')";

                                DiagnosticaDB(connection_Diagnosi, sqlQueryDiagnostica);

                                #endregion

                            }

                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "diagnostica robot " + ex.Message + " " + ex.StackTrace, MAC_id);
                        }
                    }
                    //}
                    //else
                    //{
                    //msubInit();
                    //}
                }
                else
                {
                    if (istanziato)
                    {
                        SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
                        Logs.LogDB(connection, "Robot disconnesso", MAC_id);
                        msubClearVars();
                    }

                }
            }
        }
        #endregion

        #region gestione Produzone

        //CONTROLLO PRODUZIONE
        //ROBOT

        //prende le commesse da db (cprod status 1)
        void scaricaDatiR()
        {
            List<int?> index = new List<int?>();
            int? rows_countProd = null;
            int tt = 0;
            int N = 0;
            try
            {
                /* codice non raggiungibile da errore sql CPROD_id = '1' STATUS
                //Estraggo indici da tabella Input
                string sqlQuery = "SELECT IN_id FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id WHERE CPROD_id = '1' AND CPROD_vs_MAC_id = '" + MAC_id + "'";
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                index.Add(reader.GetInt32(0));
                                tt++;
                                Console.WriteLine(MAC_id);
                            }
                        }
                        reader.Close();
                        command.Dispose();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
                if (index.Count != 0)
                {
                    //Trasferisco da Input a Produzione
                    //Status = 2 --> Valore letto da Input e copiato in Produzione
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        foreach (int indice in index)
                        {
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                            try
                            {
                                //Update tabella Input
                                sqlQuery = "UPDATE Input SET IN_status = '2' WHERE IN_id = " + indice;
                                command.Connection = connection_Produzione;
                                command.CommandText = sqlQuery;
                                N = command.ExecuteNonQuery();

                                //inserisce il record in produzione
                                sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                                " SELECT IN_vs_CPROD_id, IN_lotto, IN_priorita, IN_data FROM Input WHERE IN_id= " + indice;
                                command.Connection = connection_Produzione;
                                command.CommandText = sqlQuery;
                                N = command.ExecuteNonQuery();

                            }
                            catch (Exception ex)
                            {
                                Logs.LogDB(connection_Produzione, "errore caricamento in produzione " + ex.Message, MAC_id);
                            }

                            command.Dispose();
                        }
                    }
                }
                */

                string sqlQuery = "select count(IN_id) FROM Input INNER JOIN Cicloproduzione on IN_vs_CPROD_id=CPROD_id WHERE CPROD_status = '1' AND CPROD_vs_MAC_id = " + MAC_id + " ";
                int prod = 0;
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        prod = Convert.ToInt32(command.ExecuteScalar());
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
                if (prod > 0) caricoRigheRobot();
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
        }

        //Carico Record da tabella Produzione a Robot
        void caricoRigheRobot()
        {
            try
            {
                int ww = 0;

                //Azzero Array
                for (int ll = 0; ll < 10; ll++)
                {
                    vuote[ll] = null;
                    finite[ll] = null;
                }

                //Leggo tabella in macchina per contare Record finiti e vuoti
                for (int qq = 20; qq < 25; qq++)
                {
                    if (rispostaScrittura[qq] == 0)
                    {
                        commesseVuote = true;
                        vuote[ww] = ww;
                        ww++;
                    }
                    else if (rispostaScrittura[qq] < 0)
                    {
                        commesseFinite = true;
                        finite[ww] = ww;
                        ww++;
                    }
                    else
                    {
                        ww++;
                    }
                }
                try
                {
                    //Rendo non disponibile la riga delle commessa attiva
                    finite[rispostaDiagnostici2[19]] = null;
                }
                catch (Exception e)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "COMMESSA ATTIVA NULLA, risp diagn 19", MAC_id);
                }
                int rr = 0;
                //Estraggo solo i primi 5 Record in Produzione CC
                //for (int pp = 0; pp < index.Count(); pp++)
                //{
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        string sqlQuery = "SELECT TOP(4) IN_id, CPROD_commessa, CPROD_lotto, IN_priorita, CPROD_programma, CPROD_pezziFatti " +
                            "FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id " +
                            "WHERE CPROD_status = '1' AND CPROD_vs_MAC_id = " + MAC_id + " ORDER BY IN_priorita ASC";

                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (tmpCommesse[rr, 0] == null)
                                {
                                    tmpCommesse[rr, 0] = (reader.GetInt32(0)).ToString();
                                    if (!reader.IsDBNull(1))
                                        tmpCommesse[rr, 1] = reader.GetString(1);
                                    if (!reader.IsDBNull(2))
                                        tmpCommesse[rr, 2] = reader.GetString(2);
                                    if (!reader.IsDBNull(3))
                                        tmpCommesse[rr, 3] = (reader.GetInt32(3)).ToString();
                                    else
                                    {
                                        tmpCommesse[rr, 3] = "0";
                                    }
                                    if (!reader.IsDBNull(4))
                                        tmpCommesse[rr, 4] = (reader.GetInt32(4)).ToString();
                                    else
                                    {
                                        tmpCommesse[rr, 4] = "0";
                                    }

                                    if (!reader.IsDBNull(5))
                                        tmpCommesse[rr, 5] = (reader.GetInt32(5)).ToString();
                                    rr++;
                                }
                            }
                            reader.Close();
                        }
                        else
                        {
                            try
                            {
                                reader.Close();
                                sqlQuery = "INSERT INTO Logs (LOG_date, LOG_status, LOG_description, LOG_fk_MAC_id) VALUES (GETDATE(), '1', 'CC: hasRows = False', '" + MAC_id + "')";
                                command.CommandType = CommandType.Text;
                                command.CommandText = sqlQuery;
                                command.Connection = connection_Produzione;
                                int M = command.ExecuteNonQuery();
                                if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                command.Dispose();
                            }
                            catch (Exception ex)
                            {
                                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                            }
                        }
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
                int[] tmpScrittura = new int[30];

                for (int yy = 0; yy < 4; yy++)
                {
                    if (commesseVuote == true || commesseFinite == true)
                    {
                        for (int uu = 0; uu < 5; uu++)
                        {
                            if (vuote[uu] != null || finite[uu] != null)
                            {
                                int? mycurrentCommessa = null;
                                if (vuote[uu] != null)
                                {
                                    mycurrentCommessa = vuote[uu];
                                }
                                else
                                {
                                    mycurrentCommessa = finite[uu];
                                }
                                if (mycurrentCommessa >= 0 && mycurrentCommessa < 5)
                                {
                                    if (tmpCommesse[yy, 2] != null)
                                        rispostaScrittura[(int)mycurrentCommessa] = Convert.ToInt32(tmpCommesse[yy, 4]);
                                    else
                                        rispostaScrittura[(int)mycurrentCommessa] = 0;

                                    if (tmpCommesse[yy, 1] != null)
                                        rispostaScrittura[(int)mycurrentCommessa + 5] = Convert.ToInt32(tmpCommesse[yy, 1]);
                                    else
                                        rispostaScrittura[(int)mycurrentCommessa + 5] = 0;


                                    if (tmpCommesse[yy, 2] != null)
                                        rispostaScrittura[(int)mycurrentCommessa + 10] = Convert.ToInt32(tmpCommesse[yy, 2]);
                                    else
                                        rispostaScrittura[(int)mycurrentCommessa + 10] = 0;

                                    if (tmpCommesse[yy, 5] != null)
                                        rispostaScrittura[(int)mycurrentCommessa + 15] = Convert.ToInt32(tmpCommesse[yy, 5]);
                                    else
                                        rispostaScrittura[(int)mycurrentCommessa + 15] = 0;

                                    if (tmpCommesse[yy, 3] != null)
                                        rispostaScrittura[(int)mycurrentCommessa + 20] = Convert.ToInt32(tmpCommesse[yy, 3]);
                                    else
                                        rispostaScrittura[(int)mycurrentCommessa + 20] = 0;
                                    try
                                    {
                                        if (mobjNumReg.SetValues(mobjNumReg.StartIndex, rispostaScrittura, mobjNumReg.EndIndex - mobjNumReg.StartIndex + 1) == false)
                                        {
                                            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                            {

                                                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                                string sqlQuery = "INSERT INTO Logs (LOG_date, LOG_status, LOG_description, LOG_fk_MAC_id) " +
                                                    "VALUES (GETDATE(), '1', 'Errore scrittura Produzione su Robot', " + DbHelper.cleanNullValue(MAC_id) + ")";
                                                SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                                                int M = command.ExecuteNonQuery();
                                                command.Dispose();
                                                if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                            }
                                        }
                                        else
                                        {
                                            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                            {
                                                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                                //inserisce il record in produzione
                                                SqlCommand command=new SqlCommand();
                                               string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                                                " SELECT IN_vs_CPROD_id, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + DbHelper.cleanNullValue(tmpCommesse[yy, 0]);
                                                command.Connection = connection_Produzione;
                                                command.CommandText = sqlQuery;
                                                int N = command.ExecuteNonQuery();

                                                //Update tabella Input
                                                sqlQuery = "UPDATE Input SET IN_status = '2' WHERE IN_id = "+DbHelper.cleanNullValue(tmpCommesse[yy, 0]);
                                                command.Connection = connection_Produzione;
                                                command.CommandText = sqlQuery;
                                                N = command.ExecuteNonQuery();

                                                //Marco record in Produzione come inviato a Robot
                                                //Status = 2 --> Record scritto su Robot
                                            
                                                try
                                                {
                                                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                                    command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '2' " +
                                                        "FROM Input INNER JOIN Cicloproduzione ON CPROD_id= IN_vs_CPROD_id WHERE IN_id= " + DbHelper.cleanNullValue(tmpCommesse[yy, 0]), connection_Produzione);
                                                    int m = command.ExecuteNonQuery();
                                                    command.Dispose();
                                                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                                }
                                            }

                                            //Trasferisco da Produzione a Output
                                            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                            {
                                                try
                                                {
                                                    
                                                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                                                    string sqlQuery = "INSERT INTO Output (OUT_priorita, OUT_data, OUT_vs_CPROD_id) " +
                                                        "SELECT PROD_priorita , GETDATE(), PROD_vs_CPROD_id " +
                                                        "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE CPROD_status = '2' AND CPROD_vs_MAC_id=" + MAC_id+ " AND IN_id= " + DbHelper.cleanNullValue(tmpCommesse[yy, 0]);

                                                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                                                    command.Connection = connection_Produzione;
                                                    command.CommandText = sqlQuery;
                                                    int N = command.ExecuteNonQuery();
                                                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                                    command.Dispose();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                                }
                                            }

                                            //Aggiorno status in Tabella Produzione
                                            //Status = 3 --> Valore letto da Produzionee copiato in Output
                                            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                            {
                                                try
                                                {
                                                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                                    string sqlQuery = "UPDATE Cicloproduzione SET CPROD_status ='3' FROM Cicloproduzione inner join Input ON CPROD_id=IN_vs_CPROD_id" +
                                                        " WHERE CPROD_status = '2'  AND IN_id= " + DbHelper.cleanNullValue(tmpCommesse[yy, 0]);
                                                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                                                    int N = command.ExecuteNonQuery();
                                                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                                    command.Dispose();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                                }
                                            }
                                            //---------------------------------------------------------
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                    }
                                    //Refresh data table
                                    //Console.WriteLine("refresh table in produzione");
                                    blnDT = mobjDataTable.Refresh();

                                    if (blnDT == false)
                                    {
                                        msubDisconnected();
                                        return;
                                    }

                                    for (int i=0; i<6; i++)
                                        tmpCommesse[yy, i] = null;
                                }
                                else { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore gestione commesse in robot", MAC_id); }

                                if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                                //Ricontrollo su Robot la presenza di righe vuote
                                //Pulisco Array 
                                for (int pp = 0; pp < 5; pp++)
                                {
                                    vuote[pp] = null;
                                    finite[pp] = null;
                                }
                                ww = 0;
                                //Controllo stato righe vuote/Commesse finite
                                for (int qq = 20; qq < 25; qq++)
                                {
                                    if (rispostaScrittura[qq] == 0)
                                    {
                                        commesseVuote = true;
                                        vuote[ww] = ww;
                                        ww++;
                                    }
                                    else if (rispostaScrittura[qq] < 0)
                                    {
                                        commesseFinite = true;
                                        finite[ww] = ww;
                                        ww++;
                                    }
                                    else
                                    {
                                        ww++;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""),"inserimento commessa"+ ex.Message + " " + ex.StackTrace, MAC_id);
            }
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------//
        
        void TempiTotali(int commessaAttiva)
        {
            bool blnResult = false;
            string statoFromSql = null;
            string tempoFromSql = null;
            string sqlQuery="";
            string appTempoFromSql = null;
            string piazzamentoDB = null;
            string appPiazzamentoDB = null;
            //Se ho una commessa attiva, cerco in DB se commessa Finita o Sospesa
            if (commessaAttiva != null)        //Modifica Simone Benericetti 04/12/218 cambiato da 0 a null
            //Cerco la commessa attiva su Db per vedere se ho già tempo salvato
            {
                //Controllo lo stato della commessa attiva in DB e se avevo già dei tempi salvati
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    sqlQuery = "SELECT CPROD_status, PROD_tempoLavoro, PROD_appTempoLavoro, PROD_tempoPiazzamento, PROD_appTempoPiazzamento " +
                        "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id " +
                        "WHERE CPROD_commessa = '" + commessaAttiva + "' AND CPROD_vs_MAC_id ='"+MAC_id+"'";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != System.Data.ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!(reader.IsDBNull(0)))
                                statoFromSql = Convert.ToString((int)reader[0]);    //Modifica di Simone Benericetti 05/12/2018 tolto (byte) dopo (int)
                            if (!(reader.IsDBNull(1)))
                                tempoFromSql = reader.GetString(1);
                            if (!(reader.IsDBNull(2)))
                                appTempoFromSql = reader.GetString(2);
                            if (!(reader.IsDBNull(3)))
                                piazzamentoDB = reader.GetString(3);
                            if (!(reader.IsDBNull(4)))
                                appPiazzamentoDB = reader.GetString(4);

                        }
                        reader.Close();
                    }
                    command.Dispose();
                }
                if (statoFromSql != null)
                    statoFromSql = statoFromSql.Trim();

                //Se tempo in Db presente e commessa sospesa (status = 4) sommo i tempi
                if (tempoFromSql != null && statoFromSql == "4" && appTempoFromSql != null)
                {
                    secondiLavoro = secondiLavoro + Convert.ToInt32(appTempoFromSql);
                }
                if (piazzamentoDB != null && statoFromSql == "4" && appPiazzamentoDB != null)
                {
                    secondiPiazzamento = secondiPiazzamento + Convert.ToInt32(appPiazzamentoDB);
                }

                //Aggiorno String Register su Robot
                if (!(blnResult = mobjStrReg2.SetValue(11, secondiLavoro.ToString())))
                { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore scrittura String Register per tempo Lavoro", MAC_id); }
                else
                        if (!(blnResult = mobjStrReg2.Update()))
                { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore Aggiornamento string register", MAC_id); }

                //Aggiorno String Register su Robot
                if (!(blnResult = mobjStrReg2.SetValue(12, secondiPiazzamento.ToString())))
                { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore scrittura String Register per tempo Lavoro", MAC_id); }
                else
                        if (!(blnResult = mobjStrReg2.Update()))
                { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore Aggiornamento string register", MAC_id); }

                //Aggiorno il tempo anche su Db se tempo è attivo
                if (rispostaDiagnostici2[19] != 0)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            sqlQuery = "UPDATE Produzione " +
                                "SET PROD_tempoLavoro = '" + secondiLavoro.ToString() + "', PROD_tempoPiazzamento = '" + secondiPiazzamento.ToString() + "' " +
                                "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id " +
                                "WHERE CPROD_commessa = '" + commessaAttiva + "' AND CPROD_status != '2' AND CPROD_vs_MAC_id = " + MAC_id;
                            SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                            if (connection_Produzione.State != System.Data.ConnectionState.Open) connection_Produzione.Open();
                            int m = command.ExecuteNonQuery();
                            command.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
            }
        }

        public override void gestioneProduzione(object myId = null)
        {
            #region Definizioni Variabili
            //---------------------//
            bool blnResult;
            //--------------------//

            int rows_countIn = 0;
            int rows_countProd = 0;
            int rows_countProdForOutput = 0;
            string tempoLavoro = null;
            string appTempoLavoro = null;
            string tempoPiazzamento = null;
            string appTempoPiazzamento = null;
            int minutiLavoro = 0;
            int oreLavoro = 0;
            int minutiPiazzamento = 0;
            int orePiazzamento = 0;

            int appSecondiLavoro = 0;
            int appMinutiLavoro = 0;
            int appOreLavoro = 0;
            int appSecondiPiazzamento = 0;
            int appMinutiPiazzamento = 0;
            int appOrePiazzamento = 0;

            string strgSecondiLavoro = null;
            string strgMinutiLavoro = null;
            string strgOreLavoro = null;

            string strgSecondiPiazzamento = null;
            string strgMinutiPiazzamento = null;
            string strgOrePiazzamneto = null;

            string appStrgSecondiLavoro = null;
            string appStrgMinutiLavoro = null;
            string appStrgOreLavoro = null;

            string appStrgSecondiPiazzamento = null;
            string appStrgMinutiPiazzamento = null;
            string appStrgOrePiazzamneto = null;
            string sqlQuery = "";
            #endregion

            while (Service1.alive)
            {
                Thread.Sleep(5000);
                if (this.istanziato && connesso )
                {
                    try
                    {
                        //Refresh 
                        try
                        {
                            //Console.WriteLine("refresh table in prod "+ MAC_id);
                            blnDT = this.mobjDataTable.Refresh();

                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                        }
                        if (blnDT == false)
                        {
                            msubDisconnected();
                        }
                        else
                        {
                            #region lettura variabili in scrittura
                            int ww = 0;
                            int qq = 0;
                            //Pulisco appoggio per Richieste da Robot
                            for (qq = mobjNumReg.StartIndex; qq <= mobjNumReg.EndIndex; qq++)
                            {
                                rispostaScrittura[ww] = 0;
                                ww++;
                            }

                            //Lettura registro numerico Robot
                            ww = 0;
                            for (qq = mobjNumReg.StartIndex; qq <= mobjNumReg.EndIndex; qq++)
                            {
                                if (mobjNumReg.GetValue(qq, ref vntValue) == true)
                                    rispostaScrittura[ww] = Convert.ToInt32(vntValue);
                                else
                                {
                                    rispostaScrittura[ww] = 0;
                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore lettura NumReg variabili in scrittura", MAC_id);
                                }
                                ww++;
                            }
                            #endregion
                            //-----------------------------------------------------------------------//
                            #region lettura variabili per richieste da robot
                            ww = 0;
                            //Pulisco appoggio per Richieste da Robot
                            for (qq = mobjNumReg2.StartIndex; qq <= mobjNumReg2.EndIndex; qq++)
                            {
                                rispostaRichieste[ww] = 0;
                                ww++;
                            }

                            //Lettura registro numerico Robot
                            ww = 0;
                            for (qq = mobjNumReg2.StartIndex; qq <= mobjNumReg2.EndIndex; qq++)
                            {
                                if (mobjNumReg2.GetValue(qq, ref vntValue) == true)
                                    rispostaRichieste[ww] = Convert.ToInt32(vntValue);
                                else
                                {
                                    rispostaRichieste[ww] = 0;
                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore lettura NumReg variabili in scrittura", MAC_id);
                                }
                                ww++;
                            }
                            #endregion
                            //-----------------------------------------------------------------------//
                            #region lettura variabili diagnostica
                            ww = 0;
                            //Pulisco appoggio per Richieste da Robot
                            for (qq = mobjNumReg3.StartIndex; qq <= mobjNumReg3.EndIndex; qq++)
                            {
                                rispostaDiagnostici2[ww] = 0;
                                ww++;
                            }

                            //Lettura registro numerico Robot
                            ww = 0;
                            for (qq = mobjNumReg3.StartIndex; qq <= mobjNumReg3.EndIndex; qq++)
                            {
                                if (mobjNumReg3.GetValue(qq, ref vntValue) == true)
                                    rispostaDiagnostici2[ww] = Convert.ToInt32(vntValue);
                                else
                                {
                                    rispostaDiagnostici2[ww] = 0;
                                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "Errore lettura NumReg variabili in scrittura", MAC_id);
                                }
                                ww++;
                            }
                            #endregion
                            //-----------------------------------------------------------------------//
                            #region lettura tempi in secondi 


                            int efficienza = 0;
                            int totSecLavoro = 0;
                            int totSecPiazzamento = 0;
                            //Tempo di lavoro
                            if (rispostaDiagnostici2[19] == 0)
                                secondiLavoro = 0;
                            else
                            {
                                //Estraggo tempi di lavoro
                                secondiLavoro = rispostaDiagnostici2[20];

                                //Leggo la commessa attiva su robot

                                switch (rispostaDiagnostici2[19])
                                {
                                    case 1:
                                        commessaAttiva = rispostaScrittura[5];
                                        break;

                                    case 2:
                                        commessaAttiva = rispostaScrittura[6];
                                        break;

                                    case 3:
                                        commessaAttiva = rispostaScrittura[7];
                                        break;

                                    case 4:
                                        commessaAttiva = rispostaScrittura[8];
                                        break;

                                    case 5:
                                        break;

                                    default:
                                        break;
                                }
                                //Console.WriteLine("La commessa attiva è " + commessaAttiva);
                            }
                            //TempiTotali(commessaAttiva);

                            //--------------------------------------------------------------------------//

                            //Tempo di Piazzamento
                            if (rispostaDiagnostici2[21] == 0)
                                secondiPiazzamento = 0;
                            else
                            {
                                //Esteraggo tempi di piazzmanto
                                secondiPiazzamento = rispostaDiagnostici2[21];
                            }
                            if (commessaAttiva!=null)
                            TempiTotali((int)commessaAttiva);
                            #endregion
                            //------------------------------------------------------------//
                        }
                        //Carico Record da tabella Produzione a Robot
                        scaricaDatiR();

                        int commessa = 0;
                        //int pezziFatti = -1;
                        int lotto = -1;
                        switch (rispostaDiagnostici2[19])
                        {
                            case 1:
                                commessa = rispostaScrittura[5];
                                lotto = rispostaScrittura[10];
                                pezziFatti = rispostaScrittura[15];
                                break;
                            case 2:
                                commessa = rispostaScrittura[6];
                                lotto = rispostaScrittura[11];
                                pezziFatti = rispostaScrittura[16];
                                break;
                            case 3:
                                commessa = rispostaScrittura[7];
                                lotto = rispostaScrittura[12];
                                pezziFatti = rispostaScrittura[17];
                                break;
                            case 4:
                                commessa = rispostaScrittura[8];
                                lotto = rispostaScrittura[13];
                                pezziFatti = rispostaScrittura[18];
                                break;
                            case 5:
                                break;
                            default:
                                break;
                        }
                        //-- Aggiorno in DB Pezzi Fatti da Robot
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            try
                            {
                                //--- Aggiorno DB Produzione
                                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                if (commessa != null)
                                {
                                    sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezziFatti = '" + pezziFatti + "' WHERE CPROD_commessa = '" + commessa + "' AND CPROD_vs_MAC_id=" + MAC_id;
                                    SqlCommand command1 = new SqlCommand(sqlQuery, connection_Produzione);
                                    int ss = command1.ExecuteNonQuery();
                                    command1.Dispose();
                                    //--- Lettura Lotto salvato in DB
                                    int tmpLottoFromDB1 = 0;
                                    sqlQuery = "SELECT CPROD_lotto FROM Cicloproduzione WHERE CPROD_commessa = '" + commessa + "' AND CPROD_vs_MAC_id = " + MAC_id;    //Modificato da Simone Benericetti 05/12/2018 tolta virgoletta ' 
                                    command1.Connection = connection_Produzione;
                                    command1.CommandText = sqlQuery;
                                    SqlDataReader reader1 = command1.ExecuteReader();
                                    if (reader1.HasRows)
                                    {
                                        while (reader1.Read())
                                        {
                                            tmpLottoFromDB1 = Convert.ToInt32(reader1.GetString(0));
                                        }
                                        reader1.Close();
                                    }
                                    reader1.Close();
                                    command1.Dispose();
                                    //--- Controllo se lotto in Robot diverso da Lotto in DB
                                    if (tmpLottoFromDB1 != lotto)
                                    {
                                        sqlQuery = "UPDATE Cicloproduzione " +
                                            "SET CPROD_lotto = '" + lotto + "' WHERE CPROD_commessa = '" + commessa + "' AND CPROD_vs_MAC_id = " + MAC_id;
                                        command1.Connection = connection_Produzione;
                                        command1.CommandText = sqlQuery;
                                        int hh = command1.ExecuteNonQuery();
                                        command1.Dispose();
                                    }
                                }
                                if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                            }
                            catch (Exception ex)
                            {
                                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                            }
                        }
                        //Attivazione commessa da Robot
                        if (rispostaRichieste[7] == 1)
                        {
                            //estraggoTempi();

                            if (commessaAttiva != null)        //Modifica Simone Benericetti 04/12/218 cambiato da 0 a null
                            {
                                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                {
                                    //--- Aggiorno DB Produzione
                                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                    if (pezziFatti < lotto) //Commessa sospesa e non finita
                                    {
                                        try
                                        {
                                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                            SqlCommand command = new SqlCommand();
                                            sqlQuery = "UPDATE Produzione " +
                                                "SET PROD_appTempoLavoro = PROD_tempoLavoro, PROD_appTempoPausa = PROD_tempoPausa, PROD_appTempoPiazzamento = PROD_tempoPiazzamento " +
                                                "FROM Produzione INNNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id " +
                                                "WHERE CPROD_vs_MAC_id = '" + MAC_id + "' AND CPROD_commessa = " + commessaAttiva;    //Modificato da Simone Benericetti tolto riferimento a PROD_commessa
                                                                                                                                      //"WHERE PROD_commessa = '" + commessa + "' AND CPROD_vs_MAC_id = '" + MAC_id + "' AND CPROD_commessa = " + commessa;    //Modificato da Simone Benericetti tolto riferimento a PROD_commessa

                                            command.Connection = connection_Produzione;
                                            command.CommandText = sqlQuery;
                                            int kk = command.ExecuteNonQuery();
                                            command.Dispose();
                                            int daAttivare = rispostaRichieste[6];

                                            sqlQuery = "UPDATE Cicloproduzione " +
                                                "SET CPROD_status=4 WHERE CPROD_vs_MAC_id = '" + MAC_id + "' AND CPROD_commessa = " + commessaAttiva;    //Modificato da Simone Benericetti tolto riferimento a PROD_commessa
                                                                                                                                                         //"WHERE PROD_commessa = '" + commessa + "' AND CPROD_vs_MAC_id = '" + MAC_id + "' AND CPROD_commessa = " + commessa;    //Modificato da Simone Benericetti tolto riferimento a PROD_commessa

                                            command.Connection = connection_Produzione;
                                            command.CommandText = sqlQuery;
                                            kk = command.ExecuteNonQuery();
                                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                        }

                                    }
                                    //commessa finita
                                    else {
                                        try {
                                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                            SqlCommand command = new SqlCommand();
                                            sqlQuery = "UPDATE Cicloproduzione SET CPROD_status=5 WHERE CPROD_vs_MAC_id = '" + MAC_id + "' AND CPROD_commessa = " + commessaAttiva;
                                            command.Connection = connection_Produzione;
                                            command.CommandText = sqlQuery;
                                            int kk = command.ExecuteNonQuery();
                                            command.Dispose();
                                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                                        }
                                    }
                                }
                            }

                            int[] intValues = new int[60];
                            float[] sngValues = new float[10];

                            rispostaRichieste[7] = 0;   //Ack Richiesta del Robot
                                                        //rispostaRichieste[6] = 0;

                            for (int rr = 0; rr < 10; rr++)
                                sngValues[rr] = rispostaRichieste[rr];

                            if (mobjNumReg2.SetValues(mobjNumReg2.StartIndex, sngValues, mobjNumReg2.EndIndex - mobjNumReg2.StartIndex + 1) == false)
                            { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "SetNumReg Real Error", MAC_id); }    //MessageBox.Show("SetNumReg Real Error");

                            rispostaScrittura[28] = 1;           //Risposta a Robot

                            if (mobjNumReg.SetValues(mobjNumReg.StartIndex, rispostaScrittura, mobjNumReg.EndIndex - mobjNumReg.StartIndex + 1) == false)
                            { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "SetNumReg Real Error", MAC_id); }     //MessageBox.Show("SetNumReg Real Error");

                            //Refresh data table
                            //Console.WriteLine("refresh table2 in produzione");
                            blnDT = mobjDataTable2.Refresh();
                            if (blnDT == false)
                            {
                                msubDisconnected();
                                return;
                            }
                        }

                        //Leggo Richiesta Aggiunta Commessa
                        if (rispostaRichieste[4] == 1)
                        {
                            float[] sngValues = new float[10];
                            //Carico i valori nuova commessa - lotto - priorità - ricetta da Robot a Tabella Input Database 
                            carica_da_macchina(rispostaRichieste[1].ToString(), rispostaRichieste[2].ToString(), rispostaRichieste[3].ToString(), "ROBOT", rispostaRichieste[0].ToString());
                            
                            //Elimino i valori appena salvati
                            rispostaRichieste[4] = 0;   //Ack Richista del Robot
                            rispostaRichieste[1] = 0;   //Commessa
                            rispostaRichieste[2] = 0;   //Lotto
                            rispostaRichieste[3] = 0;   //Priorità
                            rispostaRichieste[0] = 0;   //Programma
                            for (int rr = 0; rr < 10; rr++)
                                sngValues[rr] = rispostaRichieste[rr];

                            if (mobjNumReg2.SetValues(mobjNumReg2.StartIndex, sngValues, mobjNumReg2.EndIndex - mobjNumReg2.StartIndex + 1) == false)
                            { Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), "SetNumReg Real Error", MAC_id); }

                            //Refresh data table
                            //Console.WriteLine("refresh table2 in produzione");
                            blnDT = mobjDataTable2.Refresh();
                            if (blnDT == false)
                            {
                                msubDisconnected();
                                return;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }

                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            //--- JOIN TRA TABELLA Produzione e Produzione_Out ---//
                            sqlQuery = "UPDATE a SET a.OUT_tempoLavoro = b.PROD_tempoLavoro, a.OUT_tempoPiazzamento = b.PROD_tempoPiazzamento, a.OUT_efficienza = b.PROD_efficienza " +
                                    "FROM Output AS a INNER JOIN Produzione AS b ON a.OUT_vs_CPROD_id = b.PROD_vs_CPROD_id";
                            SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            int N = command.ExecuteNonQuery();
                            command.Dispose();
                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }

            }
        }
        #endregion
    }
}