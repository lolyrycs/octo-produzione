﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cns.utils;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Net;

namespace cns.macchine.Fagor
{
    class Fresa : FagorCN
    {
        int indexmessage;

        //costruttore
        public Fresa(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            //implementare lo shift dei messaggi una volta ricevuti le letture corrette
            indexmessage = 105 + (numeroAssi == null ? 0 : (int)numeroAssi);
        }

        public void init()
        {
            byte[] bytes = new byte[2018];
            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sender.ReceiveTimeout = (5000);
            sender.SendTimeout = (5000);
            IPAddress ipAddress = IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            try
            {
                sender.Connect(remoteEP);
                //Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());
                byte[] msg = Encoding.ASCII.GetBytes("SYNC;");
                int bytesSent = sender.Send(msg);
                int bytesRec = sender.Receive(bytes);
                rexFagor = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                sender.Disconnect(true);
                //sender.Shutdown(SocketShutdown.Both);
                //sender.Close();
                istanziato = true;
            }
            catch (ArgumentNullException ane)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC[573], ANE = " + ane.Message.Replace("'", "''"), MAC_id);
                istanziato = false;
            }
            catch (SocketException se)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC[612], SE = " + se.Message.Replace("'", "''") + se.StackTrace.Replace("'", "''"), MAC_id);
                istanziato = false;
            }
            catch (Exception e)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC[651], E = " + e.Message.Replace("'", "''"), MAC_id);
                istanziato = false;
            }
        }

        #region gestione Interfaccia
        public override void gestioneInterfaccia(object myId)
        {
            // gestione del  blocco critico per la gestione
            // dell'apertura di un solo socket per volta sulla macchina
            while (Service1.alive)
            {
               

                if (connesso)
                {
                    if (!istanziato) init();
                    //plc connesso e disponibile, richiamato il sync
                    else{
                        mutex.WaitOne();
                        try
                        {
                            String[] substrings = rexFagor.Split(';');

                            #region Lettura stato Fagor
                            int startIndex = 0;
                            int length = 2;

                            string tmpMan = (Convert.ToInt32(substrings[3])).ToString("X");
                            string tmpAut = (Convert.ToInt32(substrings[2])).ToString("X");
                            startIndex = 2;
                            string tmp = (Convert.ToInt32(substrings[1]).ToString("X"));
                            if (tmp.Length < 4)
                                tmp = "0" + tmp;
                            string tmpSubMan = tmp.Substring(startIndex, length);

                            status = "";

                            //Leggo se sono in stato di MANUALE/AUTOMATICO/SIMULAZIONE
                            if (tmpMan != "0")
                            {
                                INT_totem = "rosso";
                                if (tmpMan == "1")
                                    status += "JOG ";
                                else if (tmpMan == "10000001")
                                    status += "MDI ";
                                else
                                    status += "MAN ";

                            }
                            else if (tmpAut != "0")
                            {
                                if (tmpSubMan == "02")
                                {
                                    status += "MDI ";
                                    INT_totem = "rosso";
                                }
                                else
                                    status += "MEM ";
                            }
                            else
                            {
                                if (tmpSubMan == "D")
                                    status += "EDT ";
                                else
                                    status += "*** ";
                            }

                            //Leggo lo stato del programma ESECUZIOEN/STOP/PAUSA
                            startIndex = 0;
                            string tmpSubStateHigh = tmp.Substring(startIndex, length);
                            startIndex = 2;
                            string tmpSubStateLow = tmp.Substring(startIndex, length);

                            if (tmpSubStateHigh == "02")
                            {
                                status += "STRT ";
                                INT_totem = "verde";
                            }
                            else if (tmpSubStateHigh == "04")
                            {
                                status += "HLD ";
                                INT_totem = "rosso";
                            }
                            else if (tmpSubStateLow == "05")
                                status += "STP ";
                            else
                                status += "*** ";

                            //Leggo se CNC in errore
                            if (tmpSubStateHigh == "08")
                            {
                                status += "ALM ";
                                INT_totem = "giallo";
                            }
                            else
                                status += "*** ";
                            #endregion

                            //substrings[108] = COMMENTO PROGRAMMA (COMMESSA) PER MV205
                            //substrings[106] = COMMENTO PROGRAMMA (COMMESSA) PER JAZZ 3 ASSI

                            #region Lettura programma Attivo Fagor
                            if (substrings[9] != "")
                                //    //if (substrings[10] != null)
                                //    //execProg[aa] = substrings[9] + "/" + substrings[10];
                                //    //else
                                execProg = substrings[9];
                            //else
                            //    //execProg[aa] = "***" + "/" + "***";
                            //    execProg[aa] = "***";

                            if (substrings.Length > indexmessage)   //Modificato da Simone Benericetti 08/12/2018 -correzione Lorenzo 11/04/2019
                            {
                                if (substrings[indexmessage] != "")
                                    commento = substrings[indexmessage];
                                else
                                    commento = "***";
                            }
                            #endregion

                            #region Lettura commessa attiva
                            switch (Convert.ToInt32(substrings[52]))
                            {
                                case (2):
                                    commessaAttiva = talk("RVAR;2;PLC.R[110]", 100);
                                    /*
                                    if (substrings[54] != null)
                                        commessaAttiva = substrings[54];

                                    else
                                        commessaAttiva = "***";*/
                                    break;

                                case (4):
                                    commessaAttiva = talk("RVAR;2;PLC.R[111]", 100);
                                    break;

                                case (8):
                                    commessaAttiva = talk("RVAR;2;PLC.R[112]", 100);
                                    break;

                                case (16):
                                    commessaAttiva = talk("RVAR;2;PLC.R[113]", 100);
                                    break;

                                case (32):
                                    commessaAttiva = talk("RVAR;2;PLC.R[114]", 100);
                                    break;

                                case (64):
                                    commessaAttiva = talk("RVAR;2;PLC.R[115]", 100);
                                    break;

                                case (128):
                                    commessaAttiva = talk("RVAR;2;PLC.R[116]", 100);
                                    break;

                                case (256):
                                    commessaAttiva = talk("RVAR;2;PLC.R[117]", 100);
                                    break;

                                case (512):
                                    commessaAttiva = talk("RVAR;2;PLC.R[118]", 100);
                                    break;

                                case (1024):
                                    commessaAttiva = talk("RVAR;2;PLC.R[119]", 100);
                                    break;

                                default:
                                    commessaAttiva = "***";
                                    break;
                            }
                            #endregion

                            if (commessaAttiva == null) commessaAttiva = "***";

                            #region Lettura Feed Assi Fagor
                            if (substrings[4] != null)
                                feedAssi = Convert.ToInt32(substrings[4]);
                            else
                                feedAssi = 00;
                            #endregion

                            #region Lettura Feed Mandrino Fagor
                            if (substrings[5] != null)
                                feedMandrino = Convert.ToInt32(substrings[5]);
                            else
                                feedMandrino = 00;
                            #endregion

                            #region Lettura utensile il mandrino Fagor
                            if (substrings[6] != null)
                                utensileAttivo = Convert.ToInt32(substrings[6]);
                            else
                                utensileAttivo = 00;
                            #endregion

                            #region Lettura Numero Pezzi Fatti Fagor
                            switch (Convert.ToInt32(substrings[52]))
                            {
                                case (2):
                                    if (substrings[56] != null)
                                        pezziFatti = Convert.ToInt32(substrings[56]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (4):
                                    if (substrings[61] != null)
                                        pezziFatti = Convert.ToInt32(substrings[61]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (8):
                                    if (substrings[66] != null)
                                        pezziFatti = Convert.ToInt32(substrings[66]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (16):
                                    if (substrings[71] != null)
                                        pezziFatti = Convert.ToInt32(substrings[71]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (32):
                                    if (substrings[76] != null)
                                        pezziFatti = Convert.ToInt32(substrings[76]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (64):
                                    if (substrings[81] != null)
                                        pezziFatti = Convert.ToInt32(substrings[81]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (128):
                                    if (substrings[86] != null)
                                        pezziFatti = Convert.ToInt32(substrings[86]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (256):
                                    if (substrings[91] != null)
                                        pezziFatti = Convert.ToInt32(substrings[91]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (512):
                                    if (substrings[96] != null)
                                        pezziFatti = Convert.ToInt32(substrings[96]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                case (1024):
                                    if (substrings[101] != null)
                                        pezziFatti = Convert.ToInt32(substrings[101]);
                                    else
                                        pezziFatti = 0;
                                    break;

                                default:
                                    pezziFatti = 0;
                                    break;
                            }
                            #endregion


                            if (substrings.Length > indexmessage)   //Modificato da Simone Benericetti 08/12/2018 
                            {
                                if (substrings[indexmessage] != "0")
                                {
                                    int num_mexs = 0;
                                    try { num_mexs = Convert.ToInt32(substrings[indexmessage]); }
                                    catch (Exception ex) { Logs.LogDB(connection_Fagor, "Errore numero allarmi \n" + ex.Message + "\n" + ex.StackTrace, MAC_id); }

                                    for (int n = 0; n < num_mexs; n++)
                                    {
                                        int i = substrings[indexmessage + n + 1].IndexOf(']');
                                        AlarmNumber[n] = substrings[indexmessage + n + 1].Substring(0, (i + 1));
                                        int lengthAlm = substrings[indexmessage + n + 1].Length - (i + 2);
                                        AlarmMessage[n] = substrings[indexmessage + n + 1].Substring((i + 2));
                                    }

                                    for (int m = 0; m < num_mexs; m++)
                                    {
                                        Service1.AlarmDB(connection_Fagor, AlarmNumber[m], AlarmMessage[m], null, null, MAC_id);
                                    }
                                }
                            }
                            //Scrittura dati in Database
                            using (connection_Fagor = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                            {
                                InterfacciDB(connection_Fagor);
                            }
                            gestioneDiagnostica();
                        }
                        catch (Exception e)
                        {
                            Logs.LogDB(connection_Fagor, "ERRORE interfaccia \n" + e.Message + e.StackTrace, MAC_id);
                        }

                        mutex.ReleaseMutex();
                    }
                }
                else
                {

                    //plc disconnesso
                    if (istanziato){
                        istanziato = false;
                        Logs.LogDB(connection_Fagor, "Fresa Fagor Disconnessa");
                    }
                    Thread.Sleep(5000);
                }

                
                //Thread.Sleep(3000);
            }
        }
        #endregion

        #region gestione Diagnostica
        public override void gestioneDiagnostica(object myId = null)
        {
            //   int id_Fanuc = Convert.ToInt32(id) - 1;
            //short ret = 0;
            string sqlQuery = null;
            int N = 0;
            //bool connessione = false;
            //bool handle = false;
            substringsTimer100 = rexFagor.Split(';');
            /////--------------------------/////////
            //--- Scrittura dei dati diagnostici in Database

            Letman();
            try
            {
                using (connection_Diagnosi = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    if (MAC_numeroAssi >= 3)
                    {
                        sqlQuery = "insert into Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_tempMandrino', 0, " + man_id + "), " +
                                    "('MAN_tempAsseX', 0, " + man_id + "), " +
                                    "('MAN_tempAsseY', 0, " + man_id + "), " +
                                    "('MAN_tempAsseZ', 0, " + man_id + "), " +
                                    "('MAN_tempEncX', 0, " + man_id + "), " +
                                    "('MAN_tempEncY', 0, " + man_id + "), " +
                                    "('MAN_tempEncZ', 0, " + man_id + "), " +
                                    "('MAN_tempEncB', 0, " + man_id + "), " +
                                    "('MAN_tempEncC', 0, " + man_id + "), " +
                                    "('MAN_loadMandrino', " + DbHelper.cleanNullValue(substringsTimer100[indexmessage]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseX', " + DbHelper.cleanNullValue(substringsTimer100[103]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseY', " + DbHelper.cleanNullValue(substringsTimer100[104]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseZ', " + DbHelper.cleanNullValue(substringsTimer100[105]) + ", " + man_id + "), " +
                                    "('MAN_rmpMandrino', 0, " + man_id + "), " +
                                    "('MAN_metriLavoroX', " + DbHelper.cleanNullValue(substringsTimer100[22]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroY', " + DbHelper.cleanNullValue(substringsTimer100[25]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroZ', " + DbHelper.cleanNullValue(substringsTimer100[28]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoX', " + DbHelper.cleanNullValue(substringsTimer100[21]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoY', " + DbHelper.cleanNullValue(substringsTimer100[24]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoZ', " + DbHelper.cleanNullValue(substringsTimer100[27]) + ", " + man_id + "), " +
                                    "('MAN_metriX', " + DbHelper.cleanNullValue(substringsTimer100[23]) + ", " + man_id + "), " +
                                    "('MAN_metriY', " + DbHelper.cleanNullValue(substringsTimer100[26]) + ", " + man_id + "), " +
                                    "('MAN_metriZ', " + DbHelper.cleanNullValue(substringsTimer100[29]) + ", " + man_id + "), " +
                                    "('MAN_sbloccaggiCU', " + DbHelper.cleanNullValue(substringsTimer100[38]) + ", " + man_id + "), " +
                                    "('MAN_cambiEseguiti', " + DbHelper.cleanNullValue(substringsTimer100[39]) + ", " + man_id + "), " +
                                    "('MAN_oreLavoro', " + DbHelper.cleanNullValue(substringsTimer100[36]) + ", " + man_id + "), " +
                                    "('MAN_oreAccensione', " + DbHelper.cleanNullValue(substringsTimer100[37]) + ", " + man_id + "), " +
                                    "('MAN_rpm1', " + DbHelper.cleanNullValue(substringsTimer100[14]) + ", " + man_id + "), " +
                                    "('MAN_rpm2', " + DbHelper.cleanNullValue(substringsTimer100[15]) + ", " + man_id + "), " +
                                    "('MAN_rpm3', " + DbHelper.cleanNullValue(substringsTimer100[16]) + ", " + man_id + "), " +
                                    "('MAN_rpm4', " + DbHelper.cleanNullValue(substringsTimer100[17]) + ", " + man_id + "), " +
                                    "('MAN_rpm5', " + DbHelper.cleanNullValue(substringsTimer100[18]) + ", " + man_id + "), " +
                                    "('MAN_timeOverLimit', " + DbHelper.cleanNullValue(substringsTimer100[45]) + ", " + man_id + ")";
                    }
                    else if (MAC_numeroAssi >= 4)
                    {
                        sqlQuery += "('MAN_tempAsseB', 0, " + man_id + "), " +
                                    "('MAN_loadAsseB', " + DbHelper.cleanNullValue(substringsTimer100[106]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroB', " + DbHelper.cleanNullValue(substringsTimer100[31]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoB', " + DbHelper.cleanNullValue(substringsTimer100[30])+ ", " + man_id + "), ";
                    }
                    else if (MAC_numeroAssi == 5)
                    {
                        sqlQuery = "insert into Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                    "('MAN_tempMandrino', 0, " + man_id + "), " +
                                    "('MAN_tempAsseX', 0, " + man_id + "), " +
                                    "('MAN_tempAsseY', 0, " + man_id + "), " +
                                    "('MAN_tempAsseZ', 0, " + man_id + "), " +
                                    "('MAN_tempAsseB', 0, " + man_id + "), " +
                                    "('MAN_tempAsseC', 0, " + man_id + "), " +
                                    "('MAN_tempEncX', 0, " + man_id + "), " +
                                    "('MAN_tempEncY', 0, " + man_id + "), " +
                                    "('MAN_tempEncZ', 0, " + man_id + "), " +
                                    "('MAN_tempEncB', 0, " + man_id + "), " +
                                    "('MAN_tempEncC', 0, " + man_id + "), " +
                                    "('MAN_loadMandrino', " + DbHelper.cleanNullValue(substringsTimer100[108]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseX', " + DbHelper.cleanNullValue(substringsTimer100[103]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseY', " + DbHelper.cleanNullValue(substringsTimer100[104]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseZ', " + DbHelper.cleanNullValue(substringsTimer100[105]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseB', " + DbHelper.cleanNullValue(substringsTimer100[106]) + ", " + man_id + "), " +
                                    "('MAN_loadAsseC', " + DbHelper.cleanNullValue(substringsTimer100[107]) + ", " + man_id + "), " +
                                    "('MAN_rmpMandrino', 0, " + man_id + "), " +
                                    "('MAN_metriLavoroX', " + DbHelper.cleanNullValue(substringsTimer100[22]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroY', " + DbHelper.cleanNullValue(substringsTimer100[25]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroZ', " + DbHelper.cleanNullValue(substringsTimer100[28]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroB', " + DbHelper.cleanNullValue(substringsTimer100[31]) + ", " + man_id + "), " +
                                    "('MAN_metriLavoroC', " + DbHelper.cleanNullValue(substringsTimer100[34]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoX', " + DbHelper.cleanNullValue(substringsTimer100[21]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoY', " + DbHelper.cleanNullValue(substringsTimer100[24]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoZ', " + DbHelper.cleanNullValue(substringsTimer100[27]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoB', " + DbHelper.cleanNullValue(substringsTimer100[30]) + ", " + man_id + "), " +
                                    "('MAN_metriRapidoC', " + DbHelper.cleanNullValue(substringsTimer100[33]) + ", " + man_id + "), " +
                                    "('MAN_metriX', " + DbHelper.cleanNullValue(substringsTimer100[23]) + ", " + man_id + "), " +
                                    "('MAN_metriY', " + DbHelper.cleanNullValue(substringsTimer100[26]) + ", " + man_id + "), " +
                                    "('MAN_metriZ', " + DbHelper.cleanNullValue(substringsTimer100[29]) + ", " + man_id + "), " +
                                    "('MAN_metriB', " + DbHelper.cleanNullValue(substringsTimer100[32]) + ", " + man_id + "), " +
                                    "('MAN_metriC', " + DbHelper.cleanNullValue(substringsTimer100[35]) + ", " + man_id + "), " +
                                    "('MAN_sbloccaggiCU', " + DbHelper.cleanNullValue(substringsTimer100[38]) + ", " + man_id + "), " +
                                    "('MAN_cambiEseguiti', " + DbHelper.cleanNullValue(substringsTimer100[39]) + ", " + man_id + "), " +
                                    "('MAN_oreLavoro', " + DbHelper.cleanNullValue(substringsTimer100[36]) + ", " + man_id + "), " +
                                    "('MAN_oreAccensione', " + DbHelper.cleanNullValue(substringsTimer100[37]) + ", " + man_id + "), " +
                                    "('MAN_rpm1', " + DbHelper.cleanNullValue(substringsTimer100[14]) + ", " + man_id + "), " +
                                    "('MAN_rpm2', " + DbHelper.cleanNullValue(substringsTimer100[15]) + ", " + man_id + "), " +
                                    "('MAN_rpm3', " + DbHelper.cleanNullValue(substringsTimer100[16]) + ", " + man_id + "), " +
                                    "('MAN_rpm4', " + DbHelper.cleanNullValue(substringsTimer100[17]) + ", " + man_id + "), " +
                                    "('MAN_rpm5', " + DbHelper.cleanNullValue(substringsTimer100[18]) + ", " + man_id + "), " +
                                    "('MAN_timeOverLimit', " + DbHelper.cleanNullValue(substringsTimer100[45]) + ", " + man_id + ")";

                    }
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Diagnosi);
                    if (connection_Diagnosi.State != ConnectionState.Open) connection_Diagnosi.Open();
                    N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (connection_Diagnosi.State != ConnectionState.Closed) connection_Diagnosi.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
        }
        #endregion

        #region gestione Produzione
        public override void gestioneProduzione(object myId = null)
        {
            //SqlDataReader dataReader;
            byte[] bytes = new byte[1024];
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            string sqlQuery = "";
            List<List<string>> tmpCommesse;
            List<List<string>> CommesseCNC;

            while (Service1.alive)
            {
                //bool createdNew;
                //string threadId = "threadfagor_p" + myId + "";
                //Mutex mutex = new Mutex(false, threadId, out createdNew);
                
                if (connesso  && istanziato)
                {
                    
                    try
                    {
                        mutex.WaitOne();

                        tmpCommesse = new List<List<string>>();
                        CommesseCNC = new List<List<string>>();
                        // Thread.Sleep(1000);
                        int rows_countProd = 0;

                        produzioneComune();
                        //--- Conto il numero dei record presenti in tabella Produzione
                        try
                        {
                            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                            {
                                //aggiunto id macchina
                                sqlQuery = "SELECT COUNT (CPROD_id) FROM Cicloproduzione WHERE CPROD_status = 2 AND CPROD_vs_MAC_id = " + MAC_id + "; ";
                                SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                rows_countProd = Convert.ToInt32(command.ExecuteScalar());
                                if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                command.Dispose();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                        }

                        if (rows_countProd > 0)
                        {
                            if (!impegnatoCNC)
                            {
                                impegnatoCNC = true;
                                caricoRigheFagor();
                                impegnatoCNC = false;
                            }
                        }
                        string risposta = null;

                        if (!impegnatoCNC)
                        {
                            impegnatoCNC = true;
                            //--- Controllo M162=1
                            risposta = talk("RVAR;2;PLC.M[162]", 100);

                            //--- Se ho richiesta di attivare commessa estraggo i tempi della commessa precedente
                            if (risposta == "1")
                            {
                                reqAttivazione = true;
                                estraggoTempi();

                                //--- Elimino i tempi appena salvati
                                //--- Elimino tempi Piazzamento e Lavoro
                                for (int ll = 0; ll < 4; ll++)
                                {
                                    string indiceCanc = (44 + ll).ToString();
                                    talk("WVAR;2;PLC.R[" + indiceCanc + "];0", 100);
                                }
                                //--- Elimino tempi di Pausa
                                for (int ll = 0; ll < 2; ll++)
                                {
                                    string indiceCanc = (100 + ll).ToString();
                                    talk("WVAR;2;PLC.R[" + indiceCanc + "];0", 100);
                                }

                                //--- Dopo aver salvato i tempi M162=0
                                talk("WVAR;2;PLC.M[162];0", 100);
                                //--- Dopo aver salvato i tempi M163=1
                                talk("WVAR;2;PLC.M[163];1", 100);

                                //controolo r49
                                if (R49 == 0) reqAttivazione = false;
                            }
                            impegnatoCNC = false;
                        }
                        risposta = null;

                        //--- Controllo M166=1
                        if (!impegnatoCNC)
                        {
                            //Socket senderTimer1 = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            risposta = talk("RVAR;2;PLC.M[166]", 100);

                            //--- Se ho richiesta di aggiungere commesse inserisco in Produzione_In
                            if (risposta == "1")
                            {
                                leggoInputCnc();

                                //--- Elimino i valori appena salvati
                                for (int ll = 0; ll < 5; ll++)
                                {
                                    string indiceCanc = (105 + ll).ToString();
                                    talk("WVAR;2;PLC.R[" + indiceCanc + "];0", 100);
                                }
                                //--- Dopo aver salvato i valori M166=0
                                talk("WVAR;2;PLC.M[166];0", 100);

                                //Thread.Sleep(100);
                                //--- Dopo aver salvato i tempi M167=1
                                talk("WVAR;2;PLC.M[167];1", 100);
                                risposta = "0";
                            }
                        }
                        if (!impegnatoCNC)
                        {
                            impegnatoCNC = true;
                            estraggoTempi();
                            impegnatoCNC = false;
                        }

                        mutex.ReleaseMutex();
                        //Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    {

                        Logs.LogDB(connection_Fagor, "ERRORE produzione" + e.Message + e.StackTrace, MAC_id);
                    }
                    
                }
                else
                {
                    Thread.Sleep(5000);
                }


            }
        }

        //--- Leggo valori nuovi per inserimento
        void leggoInputCnc()
        {
            SqlCommand cmd;
            byte[] bytes = new byte[1024];
            //Socket senderTimer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            string sqlQuery = "";

            List<string> commessaNuova = new List<string>();

            //--- Leggo NUOVI: commessa - lotto - priorita 
            for (int gg = 0; gg < 5; gg++)
            {
                string indice = (105 + gg).ToString();
                commessaNuova.Add(talk("RVAR;2;PLC.R[" + indice + "]", 100));
            }
            
            //Carico i valori nuova commessa - lotto - priorità - ricetta da CNC a Tabella Input Database 
            carica_da_macchina(commessaNuova[0], commessaNuova[1], commessaNuova[2], "CNC", codiceArticolo: commessaNuova[3], fase: commessaNuova[4]);
        }

        public override List<string> invioCommessa(List<string> commessa, int indice)
        {
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            byte[] bytes = new byte[1024];
            byte[] msg;
            //string ret = null;

            bool val = true;
            List<string> messaggi = new List<string>();
            switch (indice)
            {
                case (0):
                    messaggi.Add("WVAR;2;PLC.R[51];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[52];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[53]; 0");
                    messaggi.Add("WVAR;2;PLC.R[54];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[110];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[141];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[151];" + commessa[6].ToString() + "");
                    break;
                case (1):
                    messaggi.Add("WVAR;2;PLC.R[56];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[57];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[58]; 0");
                    messaggi.Add("WVAR;2;PLC.R[59];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[111];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[142];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[152];" + commessa[6].ToString() + "");
                    break;
                case (2):
                    messaggi.Add("WVAR;2;PLC.R[61];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[62];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[63]; 0");
                    messaggi.Add("WVAR;2;PLC.R[64];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[112];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[143];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[153];" + commessa[6].ToString() + "");
                    break;
                case (3):
                    messaggi.Add("WVAR;2;PLC.R[66];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[67];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[68]; 0");
                    messaggi.Add("WVAR;2;PLC.R[69];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[113];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[144];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[154];" + commessa[6].ToString() + "");
                    break;
                case (4):
                    messaggi.Add("WVAR;2;PLC.R[71];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[72];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[73]; 0");
                    messaggi.Add("WVAR;2;PLC.R[74];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[114];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[145];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[155];" + commessa[6].ToString() + "");
                    break;
                case (5):
                    messaggi.Add("WVAR;2;PLC.R[76];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[77];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[78]; 0");
                    messaggi.Add("WVAR;2;PLC.R[79];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[115];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[146];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[156];" + commessa[6].ToString() + "");
                    break;
                case (6):
                    messaggi.Add("WVAR;2;PLC.R[81];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[82];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[83]; 0");
                    messaggi.Add("WVAR;2;PLC.R[84];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[116];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[147];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[157];" + commessa[6].ToString() + "");
                    break;
                case (7):
                    messaggi.Add("WVAR;2;PLC.R[86];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[87];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[88]; 0");
                    messaggi.Add("WVAR;2;PLC.R[89];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[117];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[148];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[158];" + commessa[6].ToString() + "");
                    break;
                case (8):
                    messaggi.Add("WVAR;2;PLC.R[91];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[92];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[93]; 0");
                    messaggi.Add("WVAR;2;PLC.R[94];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[118];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[149];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[159];" + commessa[6].ToString() + "");
                    break;
                case (9):
                    messaggi.Add("WVAR;2;PLC.R[96];" + commessa[1].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[97];" + commessa[2].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[98]; 0");
                    messaggi.Add("WVAR;2;PLC.R[99];" + commessa[3].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[119];" + commessa[0].ToString() + "");

                    messaggi.Add("WVAR;2;PLC.R[150];" + commessa[5].ToString() + "");
                    messaggi.Add("WVAR;2;PLC.R[160];" + commessa[6].ToString() + "");
                    break;
                default:
                    val = false;
                    break;
            }
            if (val)
            {
                //--- Scrivo valori su CNC
                foreach(string messaggio in messaggi)
                {
                    talk(messaggio, 100);
                    //Thread.Sleep(100);
                }

                //--- Elimino Valori da Tabella Produzione_Octo      
                int M = 0;

                try
                {
                    //INSERIMENTO IN PRODUZIONE E OUTPUT
                    SqlCommand comando;
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        comando = new SqlCommand("INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + commessa[4] + "", connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        M = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();


                        comando = new SqlCommand("INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + commessa[4] + "", connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        M = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                        comando = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = 3 FROM Cicloproduzione WHERE CPROD_id = '" + commessa[0] + "'", connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        M = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                        commessa[0] = null;
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
            return commessa;
        }

        //leggo valori commesse in macchina
        public override void estraggoValoriCNC()
        {
            byte[] bytes = new byte[1024];
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            commesseCnc = new List<string>();
            //--- Estraggo i valori del campo priorita
            for (int aa = 54; aa < 100; aa = aa + 5)
            {
                commesseCnc.Add(talk("RVAR;2;PLC.R[" + aa.ToString() + "]", 100));
            }
            //--- Controllo se commessa è attiva
            R49 = Convert.ToInt32(talk("RVAR;2;PLC.R[49]", 100));

            //--- Controllo righe estratte
            controlloRigheCnc(R49);
        }

        //--- Estraggo tempi lavoro e piazzamento
        void estraggoTempi()
        {
            #region def tempi
            int? R49T = null;
            //int hh = 0;
            int[] tempi = new int[6];
            int[] tmpTempi = new int[3];
            int secLavoro = 0;
            int secPiazzamento = 0;
            int secPausa = 0;
            int secLavoroDB = 0;
            int secPiazzamentoDB = 0;
            int secPausaDB = 0;
            int efficienza = 0;

            int? fkProd = null;
            int? tmpLotto = null;
            int? tmpStatus = null;
            int? pezziFatti = null;

            bool reqSalvataggioAppoggio = false;

            //SqlCommand cmd;
            //SqlCommand cmd2;
            string sqlQuery;
            SqlDataReader dataReader;
            byte[] bytes = new byte[1024];
            //Socket senderTimer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            //IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            #endregion

            //Thread.Sleep(100);
            //--- Leggo quale commessa è attiva
            R49T = Convert.ToInt32(talk("RVAR;2;PLC.R[49]", 100));
            //--- Leggo numero pezzi fatti
            switch (R49T)
            {
                case (2):                   //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[110]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[53]", 100));
                    break;
                case (4):                   //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[111]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[58]", 100));
                    break;
                case (8):                   //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[112]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[63]", 100));
                    break;
                case (16):                  //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[113]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[68]", 100));
                    break;
                case (32):                  //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[114]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[73]", 100));
                    break;
                case (64):                  //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[115]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[78]", 100));
                    break;
                case (128):                 //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[116]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[83]", 100));
                    break;
                case (256):                 //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[117]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[88]", 100));
                    break;
                case (512):                 //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[118]", 100));
                    // Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[93]", 100));
                    break;
                case (1024):                 //
                    fkProd = Convert.ToInt32(talk("RVAR;2;PLC.R[119]", 100));
                    //Thread.Sleep(200);
                    pezziFatti = Convert.ToInt32(talk("RVAR;2;PLC.R[98]", 100));
                    break;
                default:
                    //--- Se nessuna commessa era attiva al t = t-1 resetto contatori dei tempo R44,45,46,47 - R100,101
                    for (int ii = 0; ii < 4; ii++)
                    {
                        string indice = (44 + ii).ToString();
                        talk("WVAR;2;PLC.R[" + indice + "];0", 100);
                    }
                    //Thread.Sleep(100);
                    for (int ii = 0; ii < 2; ii++)
                    {
                        string indice = (100 + ii).ToString();
                        talk("WVAR;2;PLC.R[" + indice + "];0", 100);
                    }
                    break;
            }
            //

            if (R49T != 0)
            {
                //--- Leggo minuti setup - ore setup - minuti lavoro - ore lavoro
                for (int gg = 0; gg < 4; gg++)
                {
                    string indice = (44 + gg).ToString();
                    tempi[gg] = Convert.ToInt32(talk("RVAR;2;PLC.R[" + indice + "]", 100));
                }

                //--- Leggo minuti e ore di Pausa
                tempi[4] = Convert.ToInt32(talk("RVAR;2;PLC.R[" + 100 + "]", 100));
                tempi[5] = Convert.ToInt32(talk("RVAR;2;PLC.R[" + 101 + "]", 100));

                secPiazzamento = TimeHelper.calcolaSecondiDaRegistro(tempi[1], tempi[0]);

                secLavoro = TimeHelper.calcolaSecondiDaRegistro(tempi[3], tempi[2]);
                secPausa = TimeHelper.calcolaSecondiDaRegistro(tempi[5], tempi[4]);

                //--- ESTRAGGO VALORI LOTTO e STATUS
                int cprod_id = -1;

                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        /*
                        sqlQuery = "SELECT top(1) IN_vs_CPROD_id  FROM Input WHERE IN_id = '" + fkProd + "';";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        cprod_id = Convert.ToInt32(command.ExecuteScalar());
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                        */
                        if (fkProd!=null)
                            cprod_id = (int)fkProd;


                        SqlCommand comando = new SqlCommand("SELECT PROD_lotto, CPROD_status FROM (Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id) WHERE CPROD_id = '" + cprod_id + "'", connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        dataReader = comando.ExecuteReader();
                        while (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                tmpLotto = Convert.ToInt32(dataReader.GetString(0));
                                tmpStatus = dataReader.GetInt32(1);
                            }
                            dataReader.NextResult();
                        }
                        dataReader.Close();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }

                //Controllo se status della commessa = 2 
                //Se uguale a 2 lo imposto a 3 
                /*if (tmpStatus == 2)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            //imposto status a 3 
                            SqlCommand comando = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '3' WHERE CPROD_id = '" + cprod_id + "'", connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            int Q = comando.ExecuteNonQuery();
                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                            comando.Dispose();
                            tmpStatus = 3;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }*/

                //Se ho richiesta di attivazione di una commessa controllo se commessa precedente era finito
                //se non finita appoggio i tempi precedenti su DB
                if (reqAttivazione)
                {
                    if (pezziFatti < tmpLotto)
                    {
                        reqSalvataggioAppoggio = true;
                    }
                }


                //--- ESTRAGGO TEMPI DA DB --- // --- CALCOLO NUOVI TEMPI --- // 
                if (tmpStatus == 4)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            SqlCommand comando = new SqlCommand("SELECT PROD_appTempoLavoro, PROD_appTempoPiazzamento, PROD_appTempoPausa FROM Produzione INNNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = '" + cprod_id + "'", connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            dataReader = comando.ExecuteReader();
                            while (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    if (!dataReader.IsDBNull(0))
                                        secLavoroDB = Convert.ToInt32(dataReader.GetString(0));
                                    else
                                        secLavoroDB = 0;
                                    if (!dataReader.IsDBNull(1))
                                        secPiazzamentoDB = Convert.ToInt32(dataReader.GetString(1));
                                    else
                                        secPiazzamentoDB = 0;
                                    if (!dataReader.IsDBNull(2))
                                        secPausaDB = Convert.ToInt32(dataReader.GetString(2));
                                    else
                                        secPausaDB = 0;
                                }
                                dataReader.NextResult();
                            }
                            dataReader.Close();

                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                            comando.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                    secLavoro = secLavoro + secLavoroDB;
                    secPausa = secPausa + secPausaDB;
                    secPiazzamento = secPiazzamento + secPiazzamentoDB;
                }

                //--- CALCOLO EFFICIENZA ---//
                efficienza = 0;
                if (secLavoro + secPiazzamento + secPausa > 0)
                {
                    efficienza = secLavoro * 100 / (secLavoro + secPiazzamento + secPausa);
                }
                int StatusProd = 0;
                int StatusOut = 0;
                if (tmpStatus != null)
                {
                    StatusProd = (int)tmpStatus;
                    StatusOut = (int)tmpStatus;
                    if (pezziFatti >= tmpLotto)
                    {
                        StatusProd = 5;
                        StatusOut = 5;
                    }
                    else
                    {
                        if (reqSalvataggioAppoggio)
                        {
                            StatusProd = 4;
                            StatusOut = 4;
                        }

                    }
                }
                //--- Aggiorno Tempi in db Produzione
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "if (ISNULL((SELECT distinct(PROD_tempoLavoro) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(cprod_id) + "),0)<=" + DbHelper.cleanNullValue(secLavoro) + ")" +
                                "AND(ISNULL((SELECT distinct(PROD_tempoPiazzamento) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(cprod_id) + "),0) <= " + DbHelper.cleanNullValue(secPiazzamento) + ")" +
                                "AND(ISNULL((SELECT distinct(PROD_tempoPausa) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(cprod_id) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                                "UPDATE Produzione SET PROD_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + ", PROD_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazzamento) + ",  PROD_tempoPausa= " + DbHelper.cleanNullValue(secPausa) + ", PROD_efficienza= " + DbHelper.cleanNullValue(efficienza) +
                               " FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(cprod_id) + " ";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }
                if (reqSalvataggioAppoggio)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            sqlQuery = "UPDATE Produzione SET PROD_appTempoLavoro = PROD_tempoLavoro, PROD_appTempoPiazzamento = PROD_tempoPiazzamento , PROD_appTempoPausa = PROD_tempoPausa WHERE PROD_vs_CPROD_id = '" + cprod_id + "'";
                            SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            int N = comando.ExecuteNonQuery();
                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                            comando.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }

                //AGGIORNO PEZZI FATTI IN CICLOPRODUZIOPNE
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + ", CPROD_status=" + StatusProd + " WHERE CPROD_id="+cprod_id;
                        SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace);
                }

                
                //--- INSERT DATI SU TABELLA Produzione_Out 
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        //Aggiorno anche tabella output
                        sqlQuery = "UPDATE Output SET OUT_tempoLavoro = " + secLavoro + " , OUT_tempoPiazzamento = " + secPiazzamento + " " +
                        ", OUT_tempoPausa = " + secPausa + " , OUT_efficienza = " + efficienza + " WHERE OUT_vs_CPROD_id = " + cprod_id + ";";
                        SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace);
                }

                //--- JOIN TRA TABELLA Produzione e Produzione_Out ---//
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "UPDATE a SET a.OUT_priorita = c.PROD_priorita FROM Output AS a INNER JOIN Cicloproduzione AS b ON a.OUT_vs_CPROD_id = b.CPROD_id INNER JOIN Produzione AS c ON c.PROD_vs_CPROD_id = b.CPROD_id ";
                        SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = comando.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace);
                }

                reqSalvataggioAppoggio = false;
                reqAttivazione = false;
            }
        }

        #endregion
    }
}
