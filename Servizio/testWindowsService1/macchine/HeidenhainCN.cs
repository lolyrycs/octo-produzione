﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using cns.utils;
using System.Threading;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace cns.macchine
{
    class HeidenhainCN : ControlloNumerico
    {
        #region definizione variabili
        public SqlConnection connection_Heidenhain = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");

        public string password = ConfigurationSettings.AppSettings["Heiden_pass"];
        public string memory_path= "\\PLC\\program\\symbol\\global\\";

        public HeidenhainDNCLib.JHMachine m_machine=null;
        public HeidenhainDNCLib.JHAutomatic m_automatic = null;
        public HeidenhainDNCLib.DNC_STATE m_connectionState = HeidenhainDNCLib.DNC_STATE.DNC_STATE_NOT_INITIALIZED;
        public HeidenhainDNCLib.DNC_STATE m_serverState = HeidenhainDNCLib.DNC_STATE.DNC_STATE_NOT_INITIALIZED;
        public HeidenhainDNCLib.DNC_STS_PROGRAM m_programState = HeidenhainDNCLib.DNC_STS_PROGRAM.DNC_PRG_STS_IDLE;
        public HeidenhainDNCLib.IJHDataEntry mainDataEntry = null;
        private HeidenhainDNCLib.JHDataAccess dataAccess = null;

        public bool istanziato= false;
        public bool isNck;

        public bool MG_OCTO_0901, MG_OCTO_0907, MG_OCTO_0913, MG_OCTO_0919, MG_OCTO_0925, MG_OCTO_0931, MG_OCTO_0937, MG_OCTO_0943, MG_OCTO_0949, MG_OCTO_0955;

        //BOOL RICHIESTE CAMBIO COMMESSA E AGGIUNTA NUOVA COMMESSA DA CNC -> OCTO
        public bool MG_ATTIVA_COMMESSA_AD_OCTO, MG_RICH_COMMESSA_AD_OCTO;

        //BOOL RICHIESTE CAMBIO COMMESSA E AGGIUNTA NUOVA COMMESSA DA OCTO -> CNC
        public bool MG_ATTIVA_COMMESSA_OK_DA_OCTO, MG_RICH_COMMESSA_OK_DA_OCTO;

        public int  DG_OCTO_0902, DG_OCTO_0903, DG_OCTO_0904, DG_OCTO_0905, DG_OCTO_0906,
                    DG_OCTO_0908, DG_OCTO_0909, DG_OCTO_0910, DG_OCTO_0911, DG_OCTO_0912,
                    DG_OCTO_0914, DG_OCTO_0915, DG_OCTO_0916, DG_OCTO_0917, DG_OCTO_0918,
                    DG_OCTO_0920, DG_OCTO_0921, DG_OCTO_0922, DG_OCTO_0923, DG_OCTO_0924,
                    DG_OCTO_0926, DG_OCTO_0927, DG_OCTO_0928, DG_OCTO_0929, DG_OCTO_0930,
                    DG_OCTO_0932, DG_OCTO_0933, DG_OCTO_0934, DG_OCTO_0935, DG_OCTO_0936,
                    DG_OCTO_0938, DG_OCTO_0939, DG_OCTO_0940, DG_OCTO_0941, DG_OCTO_0942,
                    DG_OCTO_0944, DG_OCTO_0945, DG_OCTO_0946, DG_OCTO_0947, DG_OCTO_0948,
                    DG_OCTO_0950, DG_OCTO_0951, DG_OCTO_0952, DG_OCTO_0953, DG_OCTO_0954,
                    DG_OCTO_0956, DG_OCTO_0957, DG_OCTO_0958, DG_OCTO_0959, DG_OCTO_0960;

        public string ctrl_code = null;

        // Delegate definitions

        // IJHMachine
        public delegate void OnStateChangedHandler(HeidenhainDNCLib.DNC_EVT_STATE stateEvent);
        // IJHAutomatic
        public delegate void OnProgramStateChangedHandler(HeidenhainDNCLib.DNC_EVT_PROGRAM progStatEvent);
        public delegate void OnDncModeChangedHandler(HeidenhainDNCLib.DNC_MODE dncMode);
        // IJHAutomatic2
        public delegate void OnExecutionMessageHandler(int a_lChannel, object a_varNumericValue, string a_strValue);
        public delegate void OnProgramChangedHandler(int a_lChannel, System.DateTime a_dTimeStamp, string a_strNewProgram);
        public delegate void OnToolChangedHandler(int a_lChannel, HeidenhainDNCLib.IJHToolId a_pidToolOut, HeidenhainDNCLib.IJHToolId a_pidToolIn, System.DateTime a_dTimeStamp);
        // IJHAutomatic3
        public delegate void OnExecutionModeChangedHandler(int a_lChannel, HeidenhainDNCLib.DNC_EXEC_MODE executionMode);

        // IJHError
        public delegate void OnErrorHandler(HeidenhainDNCLib.DNC_ERROR_GROUP errorGroup, int lErrorNumber, HeidenhainDNCLib.DNC_ERROR_CLASS errorClass, string bstrError, int lChannel);
        public delegate void OnErrorClearedHandler(int lErrorNumber, int lChannel);
        // IJHError2
        public delegate void OnError2Handler(HeidenhainDNCLib.JHErrorEntry a_pErrorEntry);
        public delegate void OnErrorCleared2Handler(HeidenhainDNCLib.JHErrorEntry a_pErrorEntry);

        public Mutex mutex = new Mutex();
        #endregion

        public HeidenhainCN(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            bool createdNew;
            string threadId = "threadfagor_i" + id + "";
            mutex = new Mutex(false, threadId, out createdNew);

            switch (tipoControllo)
            {
                case "640":
                    //ctrl_code = "TNC 6xx";
                    ctrl_code = nomemacchina;
                    break;
                default:
                    break;
            }
        }

        // Update the controls on the Dialog
        private void UpdateControls()
        {
            // Set enable state of the controls
            if (ctrl_code != null)
            {
                // active upon not connected
                if (m_serverState == HeidenhainDNCLib.DNC_STATE.DNC_STATE_NOT_INITIALIZED)
                {
                    Console.WriteLine("non connesso");
                }
                // active upon connected
                else{
                    Console.WriteLine("non connesso");
                }
                Console.WriteLine(m_serverState == HeidenhainDNCLib.DNC_STATE.DNC_STATE_MACHINE_IS_AVAILABLE ? m_programState.ToString() : "???");
            }
        }

        // Server state change handler
        public void OnStateChangedImpl(HeidenhainDNCLib.DNC_EVT_STATE stateEvent)
        {
            switch (stateEvent)
            {
                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_MACHINE_AVAILABLE:
                    {
                        // CNC is available, so make the actual connect
                        Console.WriteLine("connesso");
                    }
                    break;
                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_HOST_NOT_AVAILABLE:
                    {
                        Console.WriteLine( "VC# Example - " + ctrl_code + " cannot be reached");
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_HOST_AVAILABLE:
                    {
                        Console.WriteLine( "VC# Example - " + ctrl_code + " is powered up");
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_WAIT_PERMISSION:
                    {
                        Console.WriteLine( "VC# Example - Waiting permission from " + ctrl_code);
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_DNC_AVAILABLE:
                    {
                        Console.WriteLine( "VC# Example - Server running on " + ctrl_code);
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_MACHINE_BOOTED:
                    {
                        Console.WriteLine( "VC# Example - CNC has booted on " + ctrl_code);
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_MACHINE_INITIALIZING:
                    {
                        Console.WriteLine( "VC# Example - CNC is initializing on " + ctrl_code);
                    }
                    break;
                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_MACHINE_SHUTTING_DOWN:
                    {
                        Console.WriteLine( "VC# Example - " + ctrl_code + " is shutting down");
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_DNC_STOPPED:
                    {
                        Console.WriteLine( "VC# Example - Server stopped on " + ctrl_code);
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_CONNECTION_LOST:
                    {
                        Console.WriteLine( "VC# Example - Connection closed by " + ctrl_code);
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_HOST_STOPPED:
                    {
                        // CNC is shutting down or connection lost
                        Console.WriteLine( "VC# Example - " + ctrl_code + " was powered down");
                    }
                    break;

                case HeidenhainDNCLib.DNC_EVT_STATE.DNC_EVT_STATE_PERMISSION_DENIED:
                    {
                        // DNC access permission has been denied by CNC operator
                        Console.WriteLine( "VC# Example - Access denied by " + ctrl_code);
                    }
                    break;
            }

            m_serverState = m_machine.GetState();
            UpdateControls();
        }

        /*// Server state changed COM event handler
        private void OnCOMStateChanged(HeidenhainDNCLib.DNC_EVT_STATE eventValue)
        {
            // Thread-safe invocation of the event handler
            BeginInvoke(new OnStateChangedHandler(OnStateChangedImpl), eventValue);
        }

        // Program state changed COM event handler
        private void OnCOMProgramStateChanged(HeidenhainDNCLib.DNC_EVT_PROGRAM eventValue)
        {
            // Thread-safe invocation of the event handler
            BeginInvoke(new OnProgramStateChangedHandler(OnProgramStateChangedImpl), eventValue);
        }*/
        // Program state change handler
        public void OnProgramStateChangedImpl(HeidenhainDNCLib.DNC_EVT_PROGRAM progStatEvent)
        {
            m_programState = m_automatic.GetProgramStatus();
            UpdateControls();
        }

        // Make the DNC connection
        private void Connect()
        {
            // JHAutomatic
            try
            {
                // Create the Automatic object
                m_automatic = (HeidenhainDNCLib.JHAutomatic)m_machine.GetInterface(HeidenhainDNCLib.DNC_INTERFACE_OBJECT.DNC_INTERFACE_JHAUTOMATIC);

                dataAccess = m_machine.GetInterface(HeidenhainDNCLib.DNC_INTERFACE_OBJECT.DNC_INTERFACE_JHDATAACCESS);
                if (isNck)
                {
                    dataAccess.SetAccessMode(HeidenhainDNCLib.DNC_ACCESS_MODE.DNC_ACCESS_MODE_TABLEDATAACCESS, password);
                    dataAccess.SetAccessMode(HeidenhainDNCLib.DNC_ACCESS_MODE.DNC_ACCESS_MODE_PLCDATAACCESS, password);
                    dataAccess.SetAccessMode(HeidenhainDNCLib.DNC_ACCESS_MODE.DNC_ACCESS_MODE_CFGDATAACCESS, password);
                }

                /*  Hook up the event handlers to each required COM event, using the C# event abstraction approach.
                 *  (As opposed to the raw approach (as in ErrorListener))
                 *  IMPORTANT:
                 *      A COM event connection is created for each event handler that is added.
                 *      As a result, each COM event will be fired <n>-times, but only one will actually call the handler method.
                 *
                {
                    /*  Typecasting to <vtable interface>_Event advises the COM VTable interface.
                     *  VTable (as opposed to IDispatch) is more efficient.
                     *
                    ((HeidenhainDNCLib._IJHAutomaticEvents3_Event)m_automatic).OnProgramStatusChanged +=
                        new HeidenhainDNCLib._IJHAutomaticEvents_OnProgramStatusChangedEventHandler(OnCOMProgramStateChanged);
                }*/

                // Get the initial program status.
                m_programState = m_automatic.GetProgramStatus();
                Logs.LogDB(connection_Produzione, "PLC connesso", MAC_id);
                istanziato = true;

            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Heidenhain, "Errore connessione macchina " + ex.Message + ex.StackTrace, MAC_id);
            }

        }

        /// <summary>
        /// Disconnect from control if connected.
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (this.m_machine != null)
                {
                    // 2. Unsubscribe all events 
#if RAW_COM_EVENTS
                    if (this.machineStateListener != null)
                    {
                        this.machineStateListener.Stop();
                        this.machineStateListener = null;
                    }
#else
                    this.m_machine.OnStateChanged -= new HeidenhainDNCLib._DJHMachineEvents2_OnStateChangedEventHandler(this.OnStateChangedImpl);
#endif
                    this.m_serverState = HeidenhainDNCLib.DNC_STATE.DNC_STATE_NOT_INITIALIZED;

                    // 3. Disconect from control
                    try
                    {
                        this.m_machine.Disconnect();
                    }
                    catch (COMException cex)
                    {
                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! VERY IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        // If Disconnect() returns HRESULT = 0x80040266 / DNC_HRESULT.DNC_E_NOT_POS_NOW,
                        // you probably forgot to release some HeidenhainDNC resources!
                        // In most cases of the DNC_E_NOT_POS_NOW HRESULT, the Disconnect() is not possible,
                        // because there are still HeidenhainDNC resources like the "interfaces", 
                        // "helper objects" or "events" in use.
                        // --> Please release them before calling the Disconnect() method to avoid memory leaks!
                        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!! VERY IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        if (cex.ErrorCode == Convert.ToInt32(HeidenhainDNCLib.DNC_HRESULT.DNC_E_NOT_POS_NOW))
                        {
                            // This (2nd) Disconnect() forces to Disconnect() from control, even if there
                            // are some HeidenhainDNC resources in use. This should never happen.
                            // Please ensure to release all requested HeidenhainDNC resources!
                            this.m_machine.Disconnect();
                            Logs.LogDB(connection_Heidenhain, "errore rilascio risorse Heidenhain");
                        }
                        else
                        {
                            Logs.LogDB(connection_Heidenhain, "errore COM Heidenhain in disconnection "+cex.StackTrace, MAC_id);
                        }
                    }

                    // 4. Release machine object
                    Marshal.ReleaseComObject(this.m_machine);
                    this.m_machine = null;
                }
                
            }
            catch (COMException cex)
            {
                Logs.LogDB(connection_Heidenhain, "errore COM disconnessione Heidenhain in " +cex.Message+" \n"+ cex.StackTrace, MAC_id);
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Heidenhain, "errore disconnessione Heidenhain in disconnection " + ex.StackTrace, MAC_id);
            }
        }


        //inizializzo la macchina, verifico la connessione
        public void init()
        {
            string connectionName = ctrl_code;
            // Copy the connectionName string to an object.
            object var = (object)connectionName;

            HeidenhainDNCLib.JHMachineInProcess machine = new HeidenhainDNCLib.JHMachineInProcess();


            //fa apparire la finestra per definire i parametri di connessione
            //machine.ConfigureConnection(HeidenhainDNCLib.DNC_CONFIGURE_MODE.DNC_CONFIGURE_MODE_ALL, ref var);
            //connectionName = (string)var;

            // release COM object
            System.Runtime.InteropServices.Marshal.ReleaseComObject(machine);

            try
            {
                // Create the machine object
#if INPROC
                m_machine = new HeidenhainDNCLib.JHMachineInProcess();
#else
                m_machine = new HeidenhainDNCLib.JHMachine();
#endif

#if CONNECTREQUEST3
                HeidenhainDNCLib.IJHConnection2 newConnection = (HeidenhainDNCLib.IJHConnection2)m_machine;
                newConnection.Configure(HeidenhainDNCLib.DNC_CNC_TYPE.DNC_CNC_TYPE_TNC6xx_NCK, HeidenhainDNCLib.DNC_PROTOCOL.DNC_PROT_RPC);

                newConnection.set_propertyValue(HeidenhainDNCLib.DNC_CONNECTION_PROPERTY.DNC_CP_HOST, (object)"NL02PC537VM_TNC640_M9");
                m_machine.ConnectRequest3(newConnection);
#else
                // (Try to) connect to the given CNC.
                // The CNC may be offline.
                //Logs.LogDB(connection_Heidenhain, "connection name: " + ctrl_code);
                if (ctrl_code!=null) m_machine.ConnectRequest(ctrl_code);
#endif
                // Attach the JHMachine event OnServerStateChanged to our handler
#if VTABLE_EVENTS

    #if RAW_COM_EVENTS
                /*  Hook up the event handlers to the COM interface, using the more efficient raw approach.
                    *  Pro:
                    *      - each COM event is fired only once
                    *  Con:
                    *      - a handler must be defined for each COM event of the given COM interface
                    */
                m_machineListener = new MachineListener(this, m_machine);
    #else
                /*  Hook up the event handlers to each required COM event, using the C# event abstraction approach.
                    *  (As opposed to the raw approach (as in ErrorListener))
                    *  IMPORTANT:
                    *      A COM event connection is created for each event handler that is added.
                    *      As a result, each COM event will be fired <n>-times, but only one will actually call the handler method.
                    *
                    *      Hooking up all 4 events, will result in an interface leak as signalled by the JHMachine::Disconnect method.
                    *      This is caused by the use of the thread-safe BeginInvoke method inside the event handler.
                    */
               ((HeidenhainDNCLib._IJHMachineEvents2_Event)m_machine).OnStateChanged +=
                    new HeidenhainDNCLib._IJHMachineEvents2_OnStateChangedEventHandler(OnCOMStateChanged);
    #endif
#else
                //m_machine.OnStateChanged += new HeidenhainDNCLib._DJHMachineEvents2_OnStateChangedEventHandler(OnCOMStateChanged);
                // .NET4: Event invocation for COM objects requires event to be attributed with DispIdAttribute""-> set "Embed Interop Types" property on HeidenhainDNCLib Reference to false.
#endif

                // Get the initial (actual) server state. This also starts the state events coming.
                m_serverState = m_machine.GetState();
                if (m_serverState == HeidenhainDNCLib.DNC_STATE.DNC_STATE_MACHINE_IS_AVAILABLE )
                {
                    // Machine is available, so complete the connection now
                    isNck = true;
                    Connect();
                }
                else
                {
                    isNck = false;
                    //Console.WriteLine("non connesso");
                    // Machine is still offline or not completely booted yet.
                    // The Connect() is executed by the OnStateChanged event handler: OnStateChangedImpl(),
                    // as soon as the required state is reached.


                    if (m_serverState == HeidenhainDNCLib.DNC_STATE.DNC_STATE_NOT_INITIALIZED)
                    {
                        Console.WriteLine("non connesso");
                    }
                    // active upon connected
                    else
                    {
                        Console.WriteLine("stato connessione: " + m_serverState.ToString() );
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Heidenhain, "Errore connessione macchina " +ex.Message+ ex.StackTrace, MAC_id);
                Marshal.ReleaseComObject(m_machine);
            }
        }

        #region I/O

        //leggi dati da macchina passando indirizzo di memoria in mutua esclusione alla scrittura o altre letture
        public string Leggi_da_mac(string address)
        {
            mutex.WaitOne();
            string ret = leggi(address);
            mutex.ReleaseMutex();
            return ret;
        }

        //lettura
        private string leggi(string address)
        {
            string ret = "";
            HeidenhainDNCLib.IJHDataEntryList dataEntryList = null;
            HeidenhainDNCLib.IJHDataEntry innerDataEntry = null;
            HeidenhainDNCLib.IJHDataEntryPropertyList dataEntryPropertyList = null;
            HeidenhainDNCLib.IJHDataEntryProperty dataEntryProperty = null;
            HeidenhainDNCLib.IJHDataEntryProperty dataEntryPropertyType = null;

            //aggiunge al codice \PLC\program\symbol\global\
            address = memory_path + address;

            try
            {
                if (this.mainDataEntry != null)
                {
                    Marshal.ReleaseComObject(this.mainDataEntry);
                }

                mainDataEntry = this.dataAccess.GetDataEntry(address);

                if (this.mainDataEntry.bIsNode)
                {
                    dataEntryList = this.mainDataEntry.childList;
                    List<object> data = new List<object>();
                    for (int i = 0; i < dataEntryList.Count; i++)
                    {
                        innerDataEntry = dataEntryList[i];
                        data.Add(innerDataEntry.bstrFullName);
                        Marshal.ReleaseComObject(innerDataEntry);
                    }
                }
                else
                {
                    dataEntryPropertyList = this.mainDataEntry.propertyList;

                    // --- show all available DNC_DATAENTRY_PROPKIND for selected property ------------------
                    for (int i = 0; i < dataEntryPropertyList.Count; i++)
                    {
                        dataEntryProperty = dataEntryPropertyList[i];
                        //dataEntryProperty.kind==HeidenhainDNCLib.DNC_DATAENTRY_PROPKIND.DNC_DATAENTRY_PROPKIND_DATA 
                        string propertyKind = dataEntryProperty.kind.ToString();
                        object varValue = dataEntryProperty.varValue;
                        //varValue.ToString()

                        if (varValue != null)
                        {
                            // Show also the enumeration text of DNC_TRIG_MODE for property kinds of type DNC_DATAENTRY_PROPKIND_SUBSCRIBE_CAPABILITY
                            if (dataEntryProperty.kind == HeidenhainDNCLib.DNC_DATAENTRY_PROPKIND.DNC_DATAENTRY_PROPKIND_DATA)
                            {
                                ret = varValue.ToString();
                            }
                        }

                        Marshal.ReleaseComObject(dataEntryProperty);
                    }
                }
            }
            catch (COMException cex)
            {
                Logs.LogDB(connection_Heidenhain, "" + cex.Message + " " + cex.StackTrace, MAC_id);
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Heidenhain, "" + ex.Message + " " + ex.StackTrace, MAC_id);
            }
            finally
            {

                if (dataEntryList != null)
                {
                    Marshal.ReleaseComObject(dataEntryList);
                }

                if (innerDataEntry != null)
                {
                    Marshal.ReleaseComObject(innerDataEntry);
                }

                if (dataEntryPropertyList != null)
                {
                    Marshal.ReleaseComObject(dataEntryPropertyList);
                }

                if (dataEntryProperty != null)
                {
                    Marshal.ReleaseComObject(dataEntryProperty);
                }

                if (dataEntryPropertyType != null)
                {
                    Marshal.ReleaseComObject(dataEntryPropertyType);
                }
            }
            return ret;
        }

        //scrive il datio value in macchina nell'indirizzo di memoria in mutua esclusione alla lettura o altre scritture
        public bool Scrivi_in_mac(string value, string address)
        {
            
            mutex.WaitOne();
            bool ret = false;

            leggi(address);

            HeidenhainDNCLib.IJHDataEntryList dataEntryList = null;
            HeidenhainDNCLib.IJHDataEntryPropertyList dataEntryPropertyList = null;
            HeidenhainDNCLib.IJHDataEntryProperty dataEntryProperty = null;
            HeidenhainDNCLib.IJHDataEntryProperty dataEntryPropertyType = null;

            try
            {
                if (!this.mainDataEntry.bIsNode)
                {
                    dataEntryPropertyList = this.mainDataEntry.propertyList;
                    dataEntryProperty = dataEntryPropertyList.get_property(HeidenhainDNCLib.DNC_DATAENTRY_PROPKIND.DNC_DATAENTRY_PROPKIND_DATA);
                    if (!dataEntryProperty.bIsReadOnly)
                    {
                        Type varValueType = dataEntryProperty.varValue.GetType();

                        
                        if (varValueType == typeof(System.DBNull))
                        {
                            dataEntryPropertyType = dataEntryPropertyList.get_property(HeidenhainDNCLib.DNC_DATAENTRY_PROPKIND.DNC_DATAENTRY_PROPKIND_UPPER_BOUND);
                            varValueType = dataEntryPropertyType.varValue.GetType();
                        }

                        dataEntryProperty.varValue = TypeDescriptor.GetConverter(varValueType).ConvertFromString(value);
                        ret = true;
                    }
                }
            }
            catch (COMException cex)
            {
                ret = false;
                Logs.LogDB(connection_Heidenhain, "" + cex.ErrorCode + ", errore scrittura Heidenhain \n "+address, MAC_id);
            }
            catch (Exception ex)
            {
                ret = false;
                Logs.LogDB(connection_Heidenhain, address+" \n " + ex.Message + " " + ex.StackTrace, MAC_id);
            }
            finally
            {
                if (dataEntryList != null)
                {
                    Marshal.ReleaseComObject(dataEntryList);
                }

                if (dataEntryPropertyList != null)
                {
                    Marshal.ReleaseComObject(dataEntryPropertyList);
                }

                if (dataEntryProperty != null)
                {
                    Marshal.ReleaseComObject(dataEntryProperty);
                }

                if (dataEntryPropertyType != null)
                {
                    Marshal.ReleaseComObject(dataEntryPropertyType);
                }
            }
            mutex.ReleaseMutex();
            return ret;
        }

        #endregion
    }
}
