using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cns.utils;
using System.Threading;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Net;
using System.Net.Sockets;


namespace cns.macchine
{
    public class FagorCN : ControlloNumerico
    {
        #region DEFINIZIONI PER FAGOR
        public string rexFagor;
        public string[] substringsTimer100;
        /*
        public string status = "";
        public string commessaAttiva = "";
        public string execProg = "";
        public int? feedAssi;
        public int? feedMandrino;
        public int? utensileAttivo;
        public int? pezziFatti;
        public string INT_totem;*/
        
        public string commento = "";

        public string[] AlarmMessage = new string[17];
        public string[] AlarmNumber = new string[17];
        public string[] OperatorMessage = new string[17];
        public string[] OperatorNumber = new string[17];

        public int R49 = 0;
        public List<string> commesseCnc = new List<string>();
        public bool commesseFinite = false;
        public bool commesseVuote = false;
        //public List<int?> vuote = new List<int?>();
        //public List<int?> finite = new List<int?>();
        public int?[] vuote = new int?[10];
        public int?[] finite = new int?[10];
        public bool impegnatoCNC = false;
        public bool reqAttivazione = false;
        public Mutex mutex = new Mutex();
        //public Socket sender;
        #endregion

        public SqlConnection connection_Fagor = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");

        public FagorCN(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            bool createdNew;
            string threadId = "threadfagor_i" + id + "";
            mutex = new Mutex(false, threadId, out createdNew);
        }

        #region produzione

        public string talk(string messaggio, int wait)
        {
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            byte[] bytes = new byte[1024];
            string ret = null;
            try
            {
                Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sender.ReceiveTimeout=(5000);
                sender.SendTimeout = (5000);
                sender.Connect(remoteEP);
                byte[] msg = Encoding.ASCII.GetBytes(messaggio);
                int bytesSent = sender.Send(msg);
                

                
                if (wait != 0)
                {
                    Thread.Sleep(wait);

                    int bytesRec = sender.Receive(bytes);
                    string tmpRisposta = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    String[] substrings = tmpRisposta.Split(';');
                    ret = substrings[3];
                }
                else
                {
                    int bytesRec = sender.Receive(bytes);
                    string tmpRisposta = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                }
                //sender.Disconnect(true);
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch (ArgumentNullException ane)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC, ANE = " + ane.Message.Replace("'", "''"), MAC_id);
            }
            catch (SocketException se)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC, SE = " + se.Message.Replace("'", "''"), MAC_id);
            }
            catch (Exception e)
            {
                Logs.LogDB(connection_Fagor, "ERRORE LETTURA CNC, E = " + e.Message.Replace("'", "''"), MAC_id);
            }
            return ret;
        }
        
        public void produzioneComune()
        {
            bool connessione = false;
            string sqlQuery = "";
            int rows_countIn = 0;
            connessione = false;
            //controllose la Macchina è connessa
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    sqlQuery = "SELECT MAC_statoConnessione FROM Macchine WHERE MAC_id = '" + MAC_id + "' ";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            connessione = reader.GetString(0) == "CONNESSO";
                        }
                        reader.Close();
                    }
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
            }

            if (connessione)
            {
                rows_countIn = 0;
                //conto gli input di quella macchina
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "SELECT COUNT (IN_id) FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id WHERE IN_Status = '1' AND CPROD_vs_MAC_id = '" + MAC_id + "' "; 
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        rows_countIn = Convert.ToInt32(command.ExecuteScalar());
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }

                //Se numoro di Record nuovi in Input (status = 1) maggiore di zero li copio in Produzione
                if (rows_countIn > 0)
                    scaricaDati();
            }
        }

        //funzioni virtuali
        public virtual void estraggoValoriCNC() { }
        public virtual List<string> invioCommessa(List<string> commessa, int indice)
        {
            return new List<string>();
        }

        //--- Carico righe da Tabella Produzione_Octo a CNC
        public void caricoRigheFagor()
        {
            List<List<string>> tmpCommesse = new List<List<string>>();
            List<List<string>> CommesseCNC = new List<List<string>>();
            SqlDataReader dataReader;
            byte[] bytes = new byte[1024];
            IPAddress ipAddress = System.Net.IPAddress.Parse(MAC_ipAdress);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 5001);
            string sqlQuery = "";

            //--- Estraggo Priorita da CNC + Controllo
            this.estraggoValoriCNC();

            //--- Estraggo i primi 10 indici degli articoli in Tabella Produzione 
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                   
                    sqlQuery = "SELECT TOP(10) CPROD_id, CPROD_commessa, CPROD_lotto, IN_priorita, IN_id,  CPROD_articolo, CPROD_fase FROM Cicloproduzione  INNER JOIN Input ON CPROD_id=IN_vs_CPROD_id  WHERE CPROD_status = 2 AND CPROD_vs_MAC_id= " + MAC_id + "; ";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    dataReader = command.ExecuteReader();
                    //--- ESTRAGGO VALORI E LI APPOGGIO IN UN ARRAY ---//
                    while (dataReader.HasRows)
                    {
                        while (dataReader.Read())
                        {
                            List<string> temp = new List<string>();
                            temp.Add((dataReader.GetInt32(0)).ToString());
                            temp.Add(dataReader.GetString(1));
                            temp.Add(dataReader.GetString(2));
                            temp.Add((dataReader.GetInt32(3)).ToString());
                            temp.Add((dataReader.GetInt32(4)).ToString());
                            //articolo e fase

                            if (dataReader.IsDBNull(5)) { temp.Add("0"); }
                            else { temp.Add(dataReader.GetString(5)); }

                            if (dataReader.IsDBNull(6)) { temp.Add("0"); }
                            else { temp.Add(dataReader.GetString(6)); }
                            tmpCommesse.Add(temp);
                        }
                        dataReader.NextResult();
                    }
                    dataReader.Close();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    command.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
            }


            //controllo commessa attiva
            for (int ff = 0; ff < tmpCommesse.Count; ff++)
            {
                if (commesseVuote == true || commesseFinite == true)
                {
                    for (int uu = 0; uu < 10; uu++)
                    {
                        if (vuote[uu] != null || finite[uu] != null)
                        {
                            int mycurrentCommessa;
                            if (vuote[uu] != null)
                            {
                                mycurrentCommessa = (int)vuote[uu];
                            }
                            else
                            {
                                mycurrentCommessa = (int)finite[uu];
                            }
                            tmpCommesse[ff]= invioCommessa(tmpCommesse[ff], mycurrentCommessa);
                            estraggoValoriCNC();
                            break;
                        }
                    }
                }
            }
        }
        
        //--- Controllo le righe estratte finite o vuote
        public void controlloRigheCnc(int? R49)
        {
            //--- Pulitura Array
            vuote = new int?[10];
            finite = new int?[10];

            //--- Conto le righe occupate
            for (int cc = 0; cc < 10; cc++)
            {
                if (commesseCnc[cc] != "")
                {
                    //--- Se valore minore di zero, commessa finita
                    if (Convert.ToInt32(commesseCnc[cc]) == 0)
                    {
                        commesseVuote = true;
                        vuote[cc] = cc;
                    }
                    else if (Convert.ToInt32(commesseCnc[cc]) < 0)
                    {
                        finite[cc] = cc;
                        commesseFinite = true;
                    }/*
                    else
                    {
                        finite.Add(null);
                        vuote.Add(null);
                    }*/
                }
               
            }

            //--- Rendo non disponibile la riga della commessa attiva
            switch (R49)
            {
                case (2):                   //10
                    finite[0] = null;
                    vuote[0] = null;
                    break;
                case (4):                   //100
                    finite[1] = null;
                    vuote[1] = null;
                    break;
                case (8):                   //1000
                    finite[2] = null;
                    vuote[2] = null;
                    break;
                case (16):                  //10000
                    finite[3] = null;
                    vuote[3] = null;
                    break;
                case (32):                  //100000
                    finite[4] = null;
                    vuote[4] = null;
                    break;
                case (64):                  //1000000
                    finite[5] = null;
                    vuote[5] = null;
                    break;
                case (128):                 //10000000
                    finite[6] = null;
                    vuote[6] = null;
                    break;
                case (256):                 //100000000
                    finite[7] = null;
                    vuote[7] = null;
                    break;
                case (512):                 //1000000000
                    finite[8] = null;
                    vuote[8] = null;
                    break;
                case (1024):                 //10000000000
                    finite[9] = null;
                    vuote[9] = null;
                    break;
                default:
                    break;
            }
        }
        
        #endregion
    }
}

