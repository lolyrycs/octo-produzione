﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Net.NetworkInformation;
using System.Data.SqlClient;
using System.Threading;
using System.Configuration;
using cns.utils;
using static cns.Service1;


namespace cns.macchine
{
    public class Macchina
    {
        #region definizioni generali
        public SqlConnection connection_Diagnosi = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
        public SqlConnection connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");

        public int MAC_id = -1;
        public string MAC_tipoMacchina = "";
        public string MAC_controlloNumerico = "";
        public string MAC_ipAdress = "";
        public string MAC_tipoControllo = "";
        public int? MAC_numeroAssi;
        public string MAC_nomemacchina;
        public string MAC_pathshare;
        public int? CPROD_attivo;
        public int? IN_attivo;

        public string status = "";
        public volatile string commessaAttiva="";
        public int efficienza = 0;
        public int id_commessa = 0;

        public string execProg = "";
        public int? feedAssi = null;
        public int? feedMandrino = null;
        public int? utensileAttivo = null;
        public int? pezziFatti = null;
        public string INT_totem = "";
        public bool multipallet = false;
        public int man_id = -1;
        public List<int> cp_ids = new List<int>();
        public int? programma = null;
        public string JSON_path = "";

        public volatile bool connesso = false;
        public volatile bool istanziato = false;


        public ushort hndl = 0;
        public ushort hndlDiagnostica = 0;
        public ushort hndlProduzione = 0;
        public ushort hndlSocket = 0;

        public int[] articolo_list = new int[10];
        public int[] fase_list = new int[10];
        public int[] cpid_list = new int[10];

        public int[] tmpFase = new int[10];
        public int[] tmpArticolo = new int[10];

        public Macchina()
        {

        }

        public Macchina(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina=null, string pathshare=null) {
            this.MAC_id = id;
            this.MAC_tipoMacchina = tipoMacchina;
            this.MAC_controlloNumerico = controlloNumerico;
            this.MAC_ipAdress = ipAddress;
            this.MAC_tipoControllo = tipoControllo;
            this.MAC_numeroAssi = numeroAssi;
            this.MAC_nomemacchina = nomemacchina;
            this.MAC_pathshare = pathshare;
        }

        SqlConnection connectionPing = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region funzioni ausiliarie
        // funzione per la gestione degli allarmi macchina
        public int AlarmDB(SqlConnection mysqlCon, string numeroallarme = null, string messaggioallarme = null, string numerooperatore = null, string messaggiooperatore = null)
        {
            try
            {
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    string sqlQuery = "INSERT INTO Allarmi (ALL_date, ALL_numeroAllarme, ALL_messaggioAllarme, ALL_numeroWarning, ALL_messaggioWarning, ALL_vs_MAC_id)" +
                                                " values (GETDATE(), " + DbHelper.cleanNullValue(DbHelper.cleanMessSql(numeroallarme)) + ", " + DbHelper.cleanNullValue(DbHelper.cleanMessSql(messaggioallarme)) + "," + DbHelper.cleanNullValue(DbHelper.cleanMessSql(numerooperatore)) + "," + DbHelper.cleanNullValue(DbHelper.cleanMessSql(messaggiooperatore)) + ", " + this.MAC_id + ")";
                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                return -1;
            }
        }

        // funzioni per la gestione degll'interfaccia
        public int InterfacciDB(SqlConnection mysqlCon)
        {
            string sqlQuery;
            try
            {
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    sqlQuery = "UPDATE Interfaccia SET INT_Attivo=null WHERE INT_vs_MAC_ID= " +MAC_id+ " AND INT_Attivo=1;";
                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();

                    sqlQuery = "INSERT INTO Interfaccia (INT_date, INT_status, INT_programmaAttivo, INT_vs_MAC_id, INT_feedAssi," +
                                " INT_pezziFatti, INT_feedMandrino, INT_utensileAttivo, INT_totem, INT_Attivo, INT_commessa)" +
                                " VALUES (GETDATE(), " + DbHelper.cleanNullValue(status) + ", " + DbHelper.cleanNullValue(execProg) + "," + 
                                DbHelper.cleanNullValue(MAC_id) + "," + DbHelper.cleanNullValue(feedAssi) + ", " + DbHelper.cleanNullValue(pezziFatti) + ", " + 
                                DbHelper.cleanNullValue(feedMandrino) + " , " + DbHelper.cleanNullValue(utensileAttivo) + " , " + 
                                DbHelper.cleanNullValue(INT_totem) + ", 1, "+ DbHelper.cleanNullValue(commessaAttiva) + " )";
                    command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(mysqlCon ,ex.Message + " " + ex.StackTrace, MAC_id);
                return -1;
            }
        }

        //modifica pezzi fatti e commessa in db inetrfaccia
        public int ModificaInterfaccia()
        {
            string sqlQuery;
            SqlConnection mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
            try
            {
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    sqlQuery = "UPDATE Interfaccia SET INT_status = " + DbHelper.cleanNullValue(status) + ", INT_programmaAttivo = " + DbHelper.cleanNullValue(execProg) + ", INT_feedAssi = " + DbHelper.cleanNullValue(feedAssi) +
                        ", INT_feedMandrino = " + DbHelper.cleanNullValue(feedMandrino) + ", INT_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + ", INT_date = GETDATE(), INT_utensileAttivo = " + DbHelper.cleanNullValue(utensileAttivo) +
                        ", INT_totem = " + DbHelper.cleanNullValue(INT_totem) + ", INT_commessa = " + DbHelper.cleanNullValue(commessaAttiva) +
                        " WHERE INT_Attivo = 1 AND INT_vs_MAC_id = " + MAC_id;
                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(mysqlCon, ex.Message + " " + ex.StackTrace, MAC_id);
                return -1;
            }
        }
        #endregion

        #region controllo periodico connessione macchina
        //MODIFICA EFFETTUATA DA MONTESI 10/04/2018
        //---------------------------------------------------------------------//
        //----PROCESSO 3 controllo periodico connessione macchina--------------//
        //---------------------------------------------------------------------//
        public void ping()
        {
            string ipAdress = null;
            while (true)
            {
                try
                {
                    string sqlQuery = "SELECT MAC_ipAdress FROM Macchine WHERE MAC_id=" + this.MAC_id;
                    using (connectionPing = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        SqlCommand command = new SqlCommand(sqlQuery, connectionPing);
                        if (connectionPing.State != ConnectionState.Open) connectionPing.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ipAdress = reader.GetString(0);
                            }
                            reader.Close();
                        }
                        else
                        {
                            break;
                        }
                        command.Dispose();
                        if (connectionPing.State != ConnectionState.Closed) connectionPing.Close();
                    }
                    Ping p1 = new Ping();
                    PingReply PR = p1.Send(ipAdress);
                    // check when the ping is not success
                    while (!PR.Status.ToString().Equals("Success"))
                    {
                        sqlQuery = "UPDATE Macchine SET MAC_statoConnessione = 'DISCONNESSO' WHERE MAC_id=" + this.MAC_id;
                        using (connectionPing = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            SqlCommand command = new SqlCommand(sqlQuery, connectionPing);
                            if (connectionPing.State != ConnectionState.Open) connectionPing.Open();
                            int N = command.ExecuteNonQuery();
                            command.Dispose();
                            if (connectionPing.State != ConnectionState.Closed) connectionPing.Close();
                        }

                        this.connesso = false;
                        //cambio totem in db
                        try
                        {
                            SqlConnection mysqlCon = new SqlConnection();
                            using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                            {
                                sqlQuery = "UPDATE Interfaccia SET INT_totem = 'grigio' WHERE INT_Attivo = 1 AND INT_vs_MAC_id = " + MAC_id;
                                SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                                if (mysqlCon.State != ConnectionState.Open)
                                    mysqlCon.Open();
                                int N = command.ExecuteNonQuery();
                                command.Dispose();
                                if (mysqlCon.State != ConnectionState.Closed)
                                    mysqlCon.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                        }
                        //azzero gli handle per richiamarli alla connessione della macchina 
                        //Focas1.cnc_freelibhndl(hndl);
                        //Focas1.cnc_freelibhndl(hndlDiagnostica);
                        //Focas1.cnc_freelibhndl(hndlProduzione);
                        hndl = 0;
                        hndlDiagnostica = 0;
                        hndlProduzione = 0;
                        hndlSocket = 0;

                        //if (mobjCore != null)
                        //    mobjCore[0].Disconnect();
                        //label4.Text = PR.Status.ToString();
                        Thread.Sleep(10000);
                        PR = p1.Send(ipAdress);
                    }
                    // check after the ping is success
                    while (PR.Status.ToString().Equals("Success"))
                    {
                        this.connesso = true;
                        sqlQuery = "UPDATE Macchine SET MAC_statoConnessione = 'CONNESSO' WHERE MAC_id=" + this.MAC_id;
                        using (connectionPing = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            SqlCommand command = new SqlCommand(sqlQuery, connectionPing);
                            if (connectionPing.State != ConnectionState.Open) connectionPing.Open();
                            int N = command.ExecuteNonQuery();
                            command.Dispose();
                            if (connectionPing.State != ConnectionState.Closed) connectionPing.Close();
                        }
                        //label4.Text = PR.Status.ToString();
                        Thread.Sleep(10000);
                        PR = p1.Send(ipAdress);
                       
                    }
                }
                catch (Exception ex)
                {
                    this.connesso = false;
                    hndl = 0;
                    hndlDiagnostica = 0;
                    hndlProduzione = 0;
                    hndlSocket = 0;
                    try
                    {
                        string sqlQuery = "UPDATE Macchine SET MAC_statoConnessione = 'DISCONNESSO' WHERE MAC_id=" + this.MAC_id;
                        using (connectionPing = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            SqlCommand command = new SqlCommand(sqlQuery, connectionPing);
                            if (connectionPing.State != ConnectionState.Open) connectionPing.Open();
                            int N = command.ExecuteNonQuery();
                            command.Dispose();
                            if (connectionPing.State != ConnectionState.Closed) connectionPing.Close();
                        }
                        //cambio totem in db
                        try
                        {
                            SqlConnection mysqlCon = new SqlConnection();
                            using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                            {
                                sqlQuery = "UPDATE Interfaccia SET INT_totem = 'grigio' WHERE INT_Attivo = 1 AND INT_vs_MAC_id = " + MAC_id;
                                SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                                if (mysqlCon.State != ConnectionState.Open)
                                    mysqlCon.Open();
                                int N = command.ExecuteNonQuery();
                                command.Dispose();
                                if (mysqlCon.State != ConnectionState.Closed)
                                    mysqlCon.Close();
                            }
                        }
                        catch (Exception ex2)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex2.Message + " " + ex2.StackTrace, MAC_id);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex2.Message + " " + ex2.StackTrace, MAC_id);
                    }
                    //firstLap = false;
                }
            }
        }
        #endregion

        #region gestione Interfaccia
        virtual public void gestioneInterfaccia(object myId = null) {
            ;
        }
        #endregion

        #region gestione Diagnostica
        virtual public void gestioneDiagnostica(object myId = null)
        {
            ;
        }

        #region scrittura diagnostica parametrizzata
        // Metodo Generale parametrizzato
        public int DiagnosticaDB(SqlConnection mysqlCon, string MAN_tempMandrino = null,
            string MAN_tempAsseX = null, string MAN_tempAsseY = null, string MAN_tempAsseZ = null, string MAN_tempAsseB = null, string MAN_tempAsseC = null, string MAN_tempEncX = null, string MAN_tempEncY = null, string MAN_tempEncZ = null, string MAN_tempEncB = null,
            string MAN_tempEncC = null, string MAN_loadMandrino = null, string MAN_loadAsseX = null, string MAN_loadAsseY = null, string MAN_loadAsseZ = null, string MAN_loadAsseB = null, string MAN_loadAsseC = null, string MAN_rmpMandrino = null,
            string MAN_metriLavoroX = null, string MAN_metriRapidoX = null, string MAN_metriLavoroY = null, string MAN_metriRapidoY = null, string MAN_metriLavoroZ = null, string MAN_metriRapidoZ = null, string MAN_metriLavoroB = null, string MAN_metriRapidoB = null,
            string MAN_metriLavoroC = null, string MAN_metriRapidoC = null, string MAN_metriX = null, string MAN_metriY = null, string MAN_metriZ = null, string MAN_metriB = null, string MAN_metriC = null, string MAN_sbloccaggiCU = null, string MAN_cambiEseguiti = null,
            string MAN_oreLavoro = null, string MAN_oreAccensione = null, string MAN_rpm1 = null, string MAN_rpm2 = null, string MAN_rpm3 = null, string MAN_rpm4 = null, string MAN_rpm5 = null, string MAN_timeOverLimit = null, string MAN_limitX = null,
            string MAN_limitY = null, string MAN_limitZ = null, string MAN_limitB = null, string MAN_limitC = null, string MAN_opPinza1Grezzo = null, string MAN_clPinza1Grezzo = null, string MAN_opPinza1Finiti = null, string MAN_clPinza1Finiti = null,
            string MAN_opPinza2Grezzo = null, string MAN_clPinza2Grezzo = null, string MAN_opPinza2Finiti = null, string MAN_clPinza2Finiti = null, string MAN_violazioniGialla = null, string MAN_violazioniRossa = null, string MAN_opPinzaPallet = null, string MAN_clPinzaPallet = null, string MAN_cestelliCaricati = null,
            string MAN_pzPrelevati = null, string MAN_pzDepositati = null, string MAN_fineLotto = null, string MAN_accInterventoTermicaNastro = null, string MAN_accTeleruttoreNastro = null, string MAN_accLetturaProxy = null, string MAN_frigoOlio_attuali = null, string MAN_frigoOlio_tot = null, string MAN_frigoOlio_limite = null,
            string MAN_acquaEsterno_attuali = null, string MAN_acquaEsterno_tot = null, string MAN_acquaEsterno_limite = null, string MAN_acquaInterno_attuali = null, string MAN_acquaInterno_tot = null, string MAN_acquaInterno_limite = null, string MAN_lavBancale_attuale = null, string MAN_lavBancale_tot = null, string MAN_lavBancale_limite = null,
            string MAN_magUt_attuale = null, string MAN_magUt_tot = null, string MAN_magUt_limite = null, string MAN_braccioUt_attuale = null, string MAN_braccioUt_tot = null, string MAN_braccioUt_limite = null, string MAN_pinzaUt_attuale = null, string MAN_pinzaUt_limite = null, string MAN_batCNC_attuale = null,
            string MAN_batCNC_limite = null, string MAN_batDrive_attuale = null, string MAN_batDrive_tot = null, string MAN_batDrive_limite = null
            )
        {
            try
            {
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    string sqlQuery = "insert into Manutenzione (" +
                                                "MAN_vs_MAC_id, MAN_tempMandrino, MAN_tempAsseX, MAN_tempAsseY, MAN_tempAsseZ, " +
                                                "MAN_tempAsseB, MAN_tempAsseC, MAN_tempEncX, MAN_tempEncY, MAN_tempEncZ, MAN_tempEncB, MAN_tempEncC," +
                                                "MAN_loadMandrino, MAN_loadAsseX, MAN_loadAsseY, MAN_loadAsseZ, MAN_loadAsseB, MAN_loadAsseC, MAN_rmpMandrino, MAN_data, " +
                                                "MAN_metriLavoroX, MAN_metriLavoroY, MAN_metriLavoroZ, MAN_metriLavoroB, MAN_metriLavoro," +
                                                "MAN_metriRapidoX, MAN_metriRapidoY, MAN_metriRapidoZ, MAN_metriRapidoB, MAN_metriRapidoC," +
                                                "MAN_metriX, MAN_metriY, MAN_metriZ, MAN_metriB, MAN_metriC, MAN_sbloccaggiCU, MAN_cambiEseguiti, " +
                                                "MAN_oreLavoro, MAN_oreAccensione, MAN_rpm1, MAN_rpm2, MAN_rpm3, MAN_rpm4, MAN_rpm5, MAN_timeOverLimit, " +
                                                "MAN_limitX, MAN_limitY, MAN_limitZ, MAN_limitB, MAN_limitC," +

                                                "MAN_opPinza1Grezzo, MAN_clPinza1Grezzo, MAN_opPinza1Finiti, MAN_clPinza1Finiti, MAN_opPinza2Grezzo," +
                                                "MAN_clPinza2Grezzo, MAN_opPinza2Finiti, MAN_clPinza2Finiti, MAN_violazioniGialla, MAN_violazioniRossa," +
                                                "MAN_opPinzaPallet, MAN_clPinzaPallet, MAN_cestelliCaricati, MAN_pzPrelevati, MAN_pzDepositati," +
                                                "MAN_fineLotto, MAN_accInterventoTermicaNastro, MAN_accTeleruttoreNastro, MAN_accLetturaProxy," +

                                                "MAN_frigoOlio_attuali, MAN_frigoOlio_tot, MAN_frigoOlio_limite, " +
                                                "MAN_acquaEsterno_attuali, MAN_acquaEsterno_tot, MAN_acquaEsterno_limite," +
                                                "MAN_acquaInterno_attuali, MAN_acquaInterno_tot, MAN_acquaInterno_limite," +
                                                "MAN_lavBancale_attuale, MAN_lavBancale_tot, MAN_lavBancale_limite," +
                                                "MAN_magUt_attuale, MAN_magUt_tot, MAN_magUt_limite," +
                                                "MAN_braccioUt_attuale, MAN_braccioUt_tot, MAN_braccioUt_limite," +
                                                "MAN_pinzaUt_attuale, MAN_pinzaUt_limite, " +
                                                "MAN_batCNC_attuale, MAN_batCNC_limite, " +
                                                "MAN_batDrive_attuale, MAN_batDrive_tot, MAN_batDrive_limite) " +
                                                "values (" + MAC_id + "," + DbHelper.cleanNullValue(MAN_tempMandrino) + "," + DbHelper.cleanNullValue(MAN_tempAsseX) + "," + DbHelper.cleanNullValue(MAN_tempAsseY) + "," + DbHelper.cleanNullValue(MAN_tempAsseZ) + "," + DbHelper.cleanNullValue(MAN_tempAsseB) + "," + DbHelper.cleanNullValue(MAN_tempAsseC) +
                                                "," + DbHelper.cleanNullValue(MAN_tempEncX) + "," + DbHelper.cleanNullValue(MAN_tempEncY) + "," + DbHelper.cleanNullValue(MAN_tempEncZ) + "," + DbHelper.cleanNullValue(MAN_tempEncB) + "," + DbHelper.cleanNullValue(MAN_tempEncC) + "," + DbHelper.cleanNullValue(MAN_loadMandrino) +
                                                "," + DbHelper.cleanNullValue(MAN_loadAsseY) + "," + DbHelper.cleanNullValue(MAN_loadAsseY) + "," + DbHelper.cleanNullValue(MAN_loadAsseZ) + "," + DbHelper.cleanNullValue(MAN_loadAsseB) + "," + DbHelper.cleanNullValue(MAN_loadAsseC) + "," + DbHelper.cleanNullValue(MAN_rmpMandrino) +
                                                ", GETDATE() , " + DbHelper.cleanNullValue(MAN_metriLavoroX) + "," + DbHelper.cleanNullValue(MAN_metriLavoroY) + "," + DbHelper.cleanNullValue(MAN_metriLavoroZ) + "," + DbHelper.cleanNullValue(MAN_metriLavoroB) + "," + DbHelper.cleanNullValue(MAN_metriLavoroC) +
                                                "," + DbHelper.cleanNullValue(MAN_metriRapidoX) + "," + DbHelper.cleanNullValue(MAN_metriRapidoY) + "," + DbHelper.cleanNullValue(MAN_metriRapidoZ) + "," + DbHelper.cleanNullValue(MAN_metriRapidoB) + "," + DbHelper.cleanNullValue(MAN_metriRapidoC) + "," + DbHelper.cleanNullValue(MAN_metriX) +
                                                "," + DbHelper.cleanNullValue(MAN_metriY) + "," + DbHelper.cleanNullValue(MAN_metriZ) + "," + DbHelper.cleanNullValue(MAN_metriB) + "," + DbHelper.cleanNullValue(MAN_metriC) + "," + DbHelper.cleanNullValue(MAN_sbloccaggiCU) + "," + DbHelper.cleanNullValue(MAN_cambiEseguiti) +
                                                "," + DbHelper.cleanNullValue(MAN_oreLavoro) + "," + DbHelper.cleanNullValue(MAN_oreAccensione) + "," + DbHelper.cleanNullValue(MAN_rpm1) + "," + DbHelper.cleanNullValue(MAN_rpm2) + "," + DbHelper.cleanNullValue(MAN_rpm3) + "," + DbHelper.cleanNullValue(MAN_rpm4) +
                                                "," + DbHelper.cleanNullValue(MAN_rpm5) + "," + DbHelper.cleanNullValue(MAN_timeOverLimit) + "," + DbHelper.cleanNullValue(MAN_limitX) + "," + DbHelper.cleanNullValue(MAN_limitY) + "," + DbHelper.cleanNullValue(MAN_limitZ) + "," + DbHelper.cleanNullValue(MAN_limitB) +
                                                "," + DbHelper.cleanNullValue(MAN_limitC) + "," + DbHelper.cleanNullValue(MAN_opPinza1Grezzo) + "," + DbHelper.cleanNullValue(MAN_clPinza1Grezzo) + "," + DbHelper.cleanNullValue(MAN_opPinza1Finiti) + "," + DbHelper.cleanNullValue(MAN_clPinza1Finiti) + "," + DbHelper.cleanNullValue(MAN_opPinza2Grezzo) +
                                                "," + DbHelper.cleanNullValue(MAN_clPinza2Grezzo) + "," + DbHelper.cleanNullValue(MAN_opPinza2Finiti) + "," + DbHelper.cleanNullValue(MAN_clPinza2Finiti) + "," + DbHelper.cleanNullValue(MAN_violazioniGialla) + "," + DbHelper.cleanNullValue(MAN_violazioniRossa) + "," + DbHelper.cleanNullValue(MAN_opPinzaPallet) +
                                                "," + DbHelper.cleanNullValue(MAN_clPinzaPallet) + "," + DbHelper.cleanNullValue(MAN_cestelliCaricati) + "," + DbHelper.cleanNullValue(MAN_pzPrelevati) + "," + DbHelper.cleanNullValue(MAN_pzDepositati) + "," + DbHelper.cleanNullValue(MAN_fineLotto) + "," + DbHelper.cleanNullValue(MAN_accInterventoTermicaNastro) +
                                                "," + DbHelper.cleanNullValue(MAN_accTeleruttoreNastro) + "," + DbHelper.cleanNullValue(MAN_accLetturaProxy) + "," + DbHelper.cleanNullValue(MAN_frigoOlio_attuali) + "," + DbHelper.cleanNullValue(MAN_frigoOlio_tot) + "," + DbHelper.cleanNullValue(MAN_frigoOlio_limite) + "," + DbHelper.cleanNullValue(MAN_acquaEsterno_attuali) +
                                                "," + DbHelper.cleanNullValue(MAN_acquaEsterno_tot) + "," + DbHelper.cleanNullValue(MAN_acquaEsterno_limite) + "," + DbHelper.cleanNullValue(MAN_acquaInterno_attuali) + "," + DbHelper.cleanNullValue(MAN_acquaInterno_tot) + "," + DbHelper.cleanNullValue(MAN_acquaInterno_limite) + "," + DbHelper.cleanNullValue(MAN_lavBancale_attuale) +
                                                "," + DbHelper.cleanNullValue(MAN_lavBancale_tot) + "," + DbHelper.cleanNullValue(MAN_lavBancale_limite) + "," + DbHelper.cleanNullValue(MAN_magUt_attuale) + "," + DbHelper.cleanNullValue(MAN_magUt_tot) + "," + DbHelper.cleanNullValue(MAN_magUt_limite) + "," + DbHelper.cleanNullValue(MAN_braccioUt_attuale) +
                                                "," + DbHelper.cleanNullValue(MAN_braccioUt_tot) + "," + DbHelper.cleanNullValue(MAN_braccioUt_limite) + "," + DbHelper.cleanNullValue(MAN_pinzaUt_attuale) + "," + DbHelper.cleanNullValue(MAN_pinzaUt_limite) + "," + DbHelper.cleanNullValue(MAN_batCNC_attuale) + "," + DbHelper.cleanNullValue(MAN_batCNC_limite) +
                                                "," + DbHelper.cleanNullValue(MAN_batDrive_attuale) + "," + DbHelper.cleanNullValue(MAN_batDrive_tot) + "," + DbHelper.cleanNullValue(MAN_batDrive_limite) +
                                                " )";

                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                return -1;
            }
        }

        #endregion

        public void Letman()
        {
            try
            {
                using (SqlConnection connection_Diagnosi = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //crea il record in letmanutenzione, salva l'id 
                    string sqlQuery = "INSERT INTO Letmanutenzione (LMAN_data, LMAN_vs_MAC_id) VALUES " +
                    "(GETDATE(), " + MAC_id + "); " +
                    "SELECT CAST(SCOPE_IDENTITY() AS int)";
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection_Diagnosi;
                    if (connection_Diagnosi.State != ConnectionState.Open) connection_Diagnosi.Open();
                    command.CommandText = sqlQuery;
                    man_id = (Int32)command.ExecuteScalar();
                    command.Dispose();
                    if (connection_Diagnosi.State != ConnectionState.Closed)
                        connection_Diagnosi.Close();
                }
            }
            catch (Exception e)
            {
                Logs.LogDB(connection_Diagnosi, "errore nella scrittura in manutenzione", MAC_id);
            }
        }

        public int DiagnosticaDB(SqlConnection mysqlCon, string querydiagnostica)
        {
            try
            {
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command = new SqlCommand(querydiagnostica, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(mysqlCon, ex.Message + " " + ex.StackTrace, MAC_id);
                return -1;
            }
        }

        #endregion

        #region gestione Produzione
        virtual public void gestioneProduzione(object myId = null)
        {
            ;
        }

        public int contaCommesseAttive(int status)
        {
            int rows_countProd = 0;
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    string sqlQuery = "SELECT COUNT (CPROD_id) " +
                        "FROM Cicloproduzione WHERE CPROD_status = '" + status.ToString() + "' AND CPROD_vs_MAC_id='" + MAC_id + "'";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    rows_countProd = Convert.ToInt32(command.ExecuteScalar());
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
            return rows_countProd;
        }

        //CONTROLLO PRODUZIONE
        //CNC
        //Scarico Dati da tabella Input e importo in Produzione


            //DEPRECATED
        public int? scaricaDati()
        {
            List<int?> index = new List<int?>();
            int? rows_countProd = null;
            int tt = 0;

            //Estraggo indici da tabella Cicloproduzione
            string sqlQuery = "SELECT CPROD_id FROM Cicloproduzione INNER JOIN Input on IN_vs_CPROD_id=CPROD_id WHERE CPROD_status = '1' AND IN_status='1' AND CPROD_vs_MAC_id = '" + MAC_id + "'";
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    command.Dispose();
                    while (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            index.Add(reader.GetInt32(0));
                            tt++;
                        }
                        reader.NextResult();
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Produzione ,ex.Message + " " + ex.StackTrace, MAC_id);
            }

            if (index.Count() == 0)
            {
                return null;
            }
            
            //Trasferisco da Input a Produzione 
            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                int N;
                SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                foreach (int indice in index)
                {
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    //Console.WriteLine("dentro il foreach");
                    try
                    {
                        int in_id = 0;
                        //seleziono l'input da cui è venuta la richiesta
                        sqlQuery = "SELECT IN_id FROM Input WHERE IN_vs_CPROD_id = " + indice+ " AND IN_status=1";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        in_id = (Int32)command.ExecuteScalar();
                        /*SqlDataReader reader = command.ExecuteReader();
                        while (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                in_id=reader.GetInt32(0);
                                if (reader.IsDBNull(1))
                                {
                                    in_status=null;
                                }
                                else
                                {
                                    in_status = reader.GetString(1);
                                }
                                
                            }
                            reader.NextResult();
                        }
                        reader.Close();*/
                        if (in_id != 0)
                        {
                            //Update tabella Input
                            sqlQuery = "UPDATE Input SET IN_status = '2'  WHERE IN_id = " + in_id;
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();


                            //Update tabella Cicloproduzione
                            sqlQuery = "UPDATE Cicloproduzione SET CPROD_status = '2'  WHERE CPROD_id = " + indice;
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();

                            /*/inserisce il record in produzione 
                            sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, IN_data FROM Input WHERE IN_id= " + in_id + "";
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();*/
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, "errore caricamento in produzione " + DbHelper.cleanMessSql(ex.Message), MAC_id);
                    }
                    
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    command.Dispose();
                }
            }

            //Leggo numero record non trasferiti in macchina
            //Status = 1 --> Record appena inserito da Tabella Precedente
            //Da ricontrollare non funziona se le macchine sono più di una in DB
            rows_countProd = contaCommesseAttive(1);

            return (rows_countProd != null) ? rows_countProd : 0;
            //if (rows_countProd > 0)
            //TO DO  aggiornare l'oggetto  in caso di Fanuc o con Fagor
            //    caricoRigheFanuc(rows_countProd, id_Macchina);
            //    caricoRigheFagor(rows_countProd, id_Macchina);
        }

        //ritorna l'indice identificativo di cicloproduzione
        public void carica_da_macchina(string commessa, string lotto, string priorita, string importFrom, string programma = "NULL" , string fase="NULL", string codiceArticolo="NULL")
        {
            int cp_id = 0;
            string sqlQuery = "";
            //Carico i valori nuova commessa - lotto - priorità - ricetta da Robot a Tabella Input Database 
            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                {
                    try
                    {
                        //crea il record in cicloproduzione, salva l'id 
                        sqlQuery = "INSERT INTO Cicloproduzione (CPROD_vs_MAC_id, CPROD_commessa, CPROD_importFrom, CPROD_status, CPROD_lotto, CPROD_pezziFatti, CPROD_fase, CPROD_articolo) VALUES " +
                            "(" + MAC_id + ", " + DbHelper.cleanNullValue(commessa) + ", " + DbHelper.cleanNullValue(importFrom) + ", '1', " + DbHelper.cleanNullValue(lotto) + ", 0, " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(codiceArticolo) + " ); " +
                            "SELECT CAST(SCOPE_IDENTITY() AS int)";
                        SqlCommand command = new SqlCommand();
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        cp_id = (Int32)command.ExecuteScalar();
                        int N;
                        if (cp_id != 0)
                        {
                            //Update tabella Input
                            sqlQuery = "INSERT INTO Input (IN_commessa, IN_lotto, IN_importFrom,  IN_status,  IN_vs_CPROD_id, IN_priorita, IN_data, IN_programma, IN_fase, IN_articolo) " +
                                "SELECT CPROD_commessa, CPROD_lotto, CPROD_importFrom, '1' , " + cp_id + ", " + DbHelper.cleanNullValue(priorita) + ", GETDATE(), " + DbHelper.cleanNullValue(programma) + ", " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(codiceArticolo) + " " +
                                "FROM Cicloproduzione WHERE CPROD_id=" + cp_id + "";
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();
                        }

                        //ToDo inserire il valore cp_id in macchina come codice commessa *Lorenzo*//
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, "errore caricamento in produzione " + ex.Message+ MAC_id);
                    }
                }
            }
        }

        #endregion

        #region Files
        virtual public bool getPath()
        {
            return false;
        }
        virtual public bool uploadfile(string path, string content)
        {
            return false;
        }
        virtual public string DownloadFile(string path)
        {
            return "";
        }
    }
        #endregion
    
}
