using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using cns.utils;
using System.Data;
using System.Threading;

namespace cns.macchine.Heidenhain
{
    class Fresa : HeidenhainCN
    {

        //costruttore
        public Fresa(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            produzione = new Produzione(MAC_id);
            produzioneMacchina = new ProduzioneMacchina(MAC_id);
        }

        #region definizioni variabili

        //codice globale
        string codice = "DG_OCTO_";

        //codici produzione
        int start= 901, fase_code= 962, articolo_code= 961, id_code=981;
        public SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");

        ProduzioneMacchina produzioneMacchina = new ProduzioneMacchina();
        Produzione produzione = new Produzione();

        #endregion

        public override void gestioneInterfaccia(object myId = null)
        {
            while (Service1.alive)
            {
                if (connesso)
                {
                    if (!istanziato)
                    {
                        init();
                    }
                    else
                    {
                        produzioneMacchina.checkProd();
                        pezziFatti = produzioneMacchina.Attiva.pezzifatti;
                        commessaAttiva = produzioneMacchina.Attiva.cprod_id.ToString();
                        //aggiornamento interfaccia su pezzifatti e commessaAttiva
                        InterfacciDB(connection);
                    }
                }
                else
                {
                    //chiudi connessione
                    if (istanziato)
                    {
                        istanziato = false;
                        Disconnect();
                        Logs.LogDB(connection_Produzione, "PLC non connesso", MAC_id);
                    }
                }

                Thread.Sleep(3000);
            }
        }

        public override void gestioneDiagnostica(object myId = null)
        {
            int oreAccensione = 0;
            int cicliCambiUt = 0;
            int cicliPinzaBloccoUt = 0;
            int templettura = 0;
            
            //mandrino
            List<int> Mandrino = new List<int>();
            int service_mandrino = 0;
            int service_mandrino_ore = 0;

            //ASSI:rapido/lavoro/totale/carico
            List<int> AsseX = new List<int>();
            List<int> AsseY = new List<int>();
            List<int> AsseZ = new List<int>();
            List<int> AsseB = new List<int>();
            List<int> AsseC = new List<int>();

            while (Service1.alive)
            {
                Thread.Sleep(2000);
                if(connesso && istanziato)
                {
                    //azzeramento array
                    Mandrino = new List<int>();
                    AsseX = new List<int>();
                    AsseY = new List<int>();
                    AsseZ = new List<int>();
                    AsseB = new List<int>();
                    AsseC = new List<int>();

                    try
                    {
                        //acquisizione dati
                        oreAccensione = Convert.ToInt32(Leggi_da_mac(codice + "0201"));
                        cicliCambiUt = Convert.ToInt32(Leggi_da_mac(codice + "0202"));
                        cicliPinzaBloccoUt = Convert.ToInt32(Leggi_da_mac(codice + "0203"));

                        //mandrino
                        for (int i = 1; i < 6; i++)
                        {
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "030" + i));
                            Mandrino.Add(templettura);
                        }
                        service_mandrino = Convert.ToInt32(Leggi_da_mac(codice + "0307"));
                        service_mandrino_ore = Convert.ToInt32(Leggi_da_mac(codice + "0308"));

                        //ASSI:rapido/lavoro/totale/carico
                        int startassi = 400;
                        for (int i = 1; i < 5; i++)
                        {
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "0" + (startassi + i).ToString()));
                            AsseX.Add(templettura);
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "0" + (startassi + 4 + i).ToString()));
                            AsseY.Add(templettura);
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "0" + (startassi + 8 + i).ToString()));
                            AsseZ.Add(templettura);
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "0" + (startassi + 12 + i).ToString()));
                            AsseB.Add(templettura);
                            templettura = Convert.ToInt32(Leggi_da_mac(codice + "0" + (startassi + 16 + i).ToString()));
                            AsseC.Add(templettura);
                        }

                        //Scrittura Database
                        //try
                        //{
                            Letman();
                            string sqlQuery = "";
                            using (connection_Diagnosi = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                            {
                                if (MAC_numeroAssi >= 3)
                                {
                                    sqlQuery = "insert into Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES " +
                                                "('MAN_oreAccensione', " + oreAccensione + ", " + man_id + "), " +
                                                "('MAN_cambiEseguiti', " + cicliCambiUt + ", " + man_id + "), " +
                                                "('MAN_sbloccaggiCU', " + cicliPinzaBloccoUt + ", " + man_id + "), " +
                                                "('MAN_loadMandrino', " + service_mandrino + ", " + man_id + "), " +
                                                "('MAN_tempMandrino', " + service_mandrino_ore + ", " + man_id + "), " +
                                                "('MAN_rpm1', " + Mandrino[0] + ", " + man_id + "), " +
                                                "('MAN_rpm2', " + Mandrino[1] + ", " + man_id + "), " +
                                                "('MAN_rpm3', " + Mandrino[2] + ", " + man_id + "), " +
                                                "('MAN_rpm4', " + Mandrino[3] + ", " + man_id + "), " +
                                                "('MAN_rpm5', " + Mandrino[4] + ", " + man_id + "), " +
                                                "('MAN_metriRapidoX', " + AsseX[0] + ", " + man_id + "), " +
                                                "('MAN_metriRapidoY', " + AsseY[0] + ", " + man_id + "), " +
                                                "('MAN_metriRapidoZ', " + AsseZ[0] + ", " + man_id + "), " +
                                                "('MAN_metriLavoroX', " + AsseX[1] + ", " + man_id + "), " +
                                                "('MAN_metriLavoroY', " + AsseY[1] + ", " + man_id + "), " +
                                                "('MAN_metriLavoroZ', " + AsseZ[1] + ", " + man_id + "), " +
                                                "('MAN_metriX', " + AsseX[2] + ", " + man_id + "), " +
                                                "('MAN_metriY', " + AsseY[2] + ", " + man_id + "), " +
                                                "('MAN_metriZ', " + AsseZ[2] + ", " + man_id + "), " +
                                                "('MAN_loadAsseX', " + AsseX[3] + ", " + man_id + "), " +
                                                "('MAN_loadAsseY', " + AsseY[3] + ", " + man_id + "), " +
                                                "('MAN_loadAsseZ', " + AsseZ[3] + ", " + man_id + ") ";
                                }
                                else if (MAC_numeroAssi >= 4)
                                {
                                    sqlQuery += ", ('MAN_metriRapidoB', " + AsseB[0] + ", " + man_id + "), " +
                                                "('MAN_metriLavoroB', " + AsseB[1] + ", " + man_id + "), " +
                                                "('MAN_metriB', " + AsseB[2] + ", " + man_id + "), " +
                                                "('MAN_loadAsseB', " + AsseB[3] + ", " + man_id + ") ";
                                }
                                else if (MAC_numeroAssi == 5)
                                {
                                    sqlQuery += ", ('MAN_metriRapidoC', " + AsseC[0] + ", " + man_id + "), " +
                                                "('MAN_metriLavoroC', " + AsseC[1] + ", " + man_id + "), " +
                                                "('MAN_metriC', " + AsseC[2] + ", " + man_id + "), " +
                                                "('MAN_loadAsseC', " + AsseC[3] + ", " + man_id + ") ";

                                }
                                SqlCommand command = new SqlCommand(sqlQuery, connection_Diagnosi);
                                if (connection_Diagnosi.State != ConnectionState.Open) connection_Diagnosi.Open();
                                command.ExecuteNonQuery();
                                command.Dispose();
                                if (connection_Diagnosi.State != ConnectionState.Closed) connection_Diagnosi.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                        }
                    /*}
                    catch
                    {

                    }*/
                }
            }
        }

        public override void gestioneProduzione(object myId = null)
        {
            #region variabili produzione
            bool reqAttivazione = false;

            #endregion

            while (Service1.alive)
            {
                Thread.Sleep(4000);
                if (connesso && istanziato)
                {
                    //richiesta aggiunta
                    aggiungiCommessaOcto();
                    //leggiamo le commesse in macchina
                    Leggi_tab_Prod();
                    // Leggi_tab_Prod();
                    AggiuntaNuoveCommesse();

                    //estraggo tempi, aggiungo appoggio, calcolo efficienza aggiorno tempi
                    letturaTempiMacchina();
                    produzioneMacchina.AggiornamentoTempi();

                    //controllo se ci sono richieste di attivazione
                    reqAttivazione = Leggi_da_mac("MG_ATTIVA_COMMESSA_AD_OCTO") == "True";

                    //salvo in appoggio, pulizia tempi in macchina, -> Risposta
                    if (reqAttivazione)
                    {
                        Queries.SalvataggioAppoggio(MAC_id, produzioneMacchina.Attiva.cprod_id);
                        if (produzioneMacchina.Attiva.pezzifatti >= produzioneMacchina.Attiva.lotto) produzioneMacchina.Attiva.status = 5;
                        else produzioneMacchina.Attiva.status = 4;
                        azzeraTempi();

                        //Risposta per attivazione commessa
                        Scrivi_in_mac("True", "MG_ATTIVA_COMMESSA_OK_DA_OCTO");
                    }

                    //aggiorno i nuovi dati in database
                    Queries.AggiornaDati(produzioneMacchina.Attiva.cprod_id, produzioneMacchina.Attiva.status, produzioneMacchina.Attiva.pezzifatti, MAC_id);
                }
            }
        }

        #region funzioni produzione

        //aggiunta commessa da macchina -> Database
        public void aggiungiCommessaOcto()
        {
            bool reqAggiunta = Leggi_da_mac("MG_RICH_COMMESSA_AD_OCTO") == "True";
            string nuovaCommessa = null;
            string nuovaLotto = null;
            string nuovaPriorita = null;
            string nuovaFase = null;
            string nuovaArticolo = null;

            //leggi reqAggiunta da macchina
            if (reqAggiunta)
            {
                //leggi dati da macchina
                nuovaCommessa = Leggi_da_mac("DG_OCTO_0991");
                nuovaLotto = Leggi_da_mac("DG_OCTO_0992");
                nuovaPriorita = Leggi_da_mac("DG_OCTO_0993");
                nuovaFase = Leggi_da_mac("DG_OCTO_0994");
                nuovaArticolo = Leggi_da_mac("DG_OCTO_0995");

                //scrivi dati in db
                carica_da_macchina(nuovaCommessa, nuovaLotto, nuovaPriorita, "CNC", fase: nuovaFase, codiceArticolo: nuovaArticolo);

                //rispondi alla macchina
                Scrivi_in_mac("True", "MG_RICH_COMMESSA_OK_DA_OCTO");

                //azzera i valori in macchina
                Scrivi_in_mac("0","DG_OCTO_0991");
                Scrivi_in_mac("0", "DG_OCTO_0992");
                Scrivi_in_mac("0", "DG_OCTO_0993");
                Scrivi_in_mac("0", "DG_OCTO_0994");
                Scrivi_in_mac("0", "DG_OCTO_0995");
            }
        }

        //lettura della tabella produzione
        public bool Leggi_tab_Prod()
        {
            string address;
            bool res = false;
            int indice;

            bool attiva=false;
            int programma=0, commessa=0, lotto=0, fase=0, articolo=0, pezzifatti=0, priorita=0, id=0;
            produzioneMacchina = new ProduzioneMacchina(MAC_id);
            CommessaMacchina CM = new CommessaMacchina();
            for (int riga = 0; riga <= 9; riga++)
            {
                for (int col = 0; col <= 5; col++)
                {
                    indice = riga * 6 + col;
                    address = (col == 0 ? "M" : "D") + "G_OCTO_0" + (indice + start);

                    switch (col)
                    {
                        case 0:
                            attiva = Leggi_da_mac(address) == "True";
                            break;
                        case 1:
                            programma = Convert.ToInt32(Leggi_da_mac(address));
                            break;
                        case 2:
                            commessa = Convert.ToInt32(Leggi_da_mac(address));
                            break;
                        case 3:
                            lotto = Convert.ToInt32(Leggi_da_mac(address));
                            break;
                        case 4:
                            pezzifatti = Convert.ToInt32(Leggi_da_mac(address));
                            break;
                        case 5:
                            priorita = Convert.ToInt32(Leggi_da_mac(address));
                            break;
                        default:
                            break;
                    }
                }

                articolo = Convert.ToInt32(Leggi_da_mac("DG_OCTO_0" + (articolo_code + 2 * riga).ToString()));
                fase = Convert.ToInt32(Leggi_da_mac("DG_OCTO_0" + (fase_code + 2 * riga).ToString()));
                id = Convert.ToInt32(Leggi_da_mac("DG_OCTO_0" + (id_code + riga).ToString()));
                    
                CM = new CommessaMacchina(MAC_id, id, riga+1, commessa, lotto, priorita, fase, articolo, attiva, pezzifatti);
                produzioneMacchina.commesse.Add(CM);
                res = true;
            }
            return res;
        }

        //aggiunta commessa in macchina parte da 0
        public void aggiungiCommessaMacchina(int indice, Commessa commessa)
        {
            string codice = "DG_OCTO_0";
            int codestart = start+1+(indice*6);

            Scrivi_in_mac(commessa.programma.ToString(), codice + codestart);       //programma
            Scrivi_in_mac(commessa.commessa.ToString(), codice + (codestart + 1));  //commessa
            Scrivi_in_mac(commessa.lotto.ToString(), codice + (codestart + 2));     //lotto
            Scrivi_in_mac(commessa.pezzifatti.ToString(), codice + (codestart + 3));//commessa
            Scrivi_in_mac(commessa.priorita.ToString(), codice + (codestart + 4));  //priorita

            Scrivi_in_mac(commessa.cprod_id.ToString(), codice + (id_code + indice));           //id
            Scrivi_in_mac(commessa.articolo.ToString(), codice + (articolo_code + indice * 2)); //articolo
            Scrivi_in_mac(commessa.fase.ToString(), codice + (fase_code + indice * 2));         //fase
        }

        //estraggo tempi, aggiungo appoggio, calcolo efficienza
        public void letturaTempiMacchina()
        {
            int oreSet = Convert.ToInt32(Leggi_da_mac(codice + 1101));
            int minSet = Convert.ToInt32(Leggi_da_mac(codice + 1102));
            int secSet = Convert.ToInt32(Leggi_da_mac(codice + 1103));

            int oreLav = Convert.ToInt32(Leggi_da_mac(codice + 1104));
            int minLav = Convert.ToInt32(Leggi_da_mac(codice + 1105));
            int secLav = Convert.ToInt32(Leggi_da_mac(codice + 1106));

            int orePas = Convert.ToInt32(Leggi_da_mac(codice + 1107));
            int minPas = Convert.ToInt32(Leggi_da_mac(codice + 1108));
            int secPas = Convert.ToInt32(Leggi_da_mac(codice + 1109));

            produzioneMacchina.SecondiLavoro = TimeHelper.calcolaSecondiDaRegistro(oreLav, minLav, secLav);
            produzioneMacchina.SecondiPausa = TimeHelper.calcolaSecondiDaRegistro(orePas, minPas, secPas);
            produzioneMacchina.SecondiPiazzamento = TimeHelper.calcolaSecondiDaRegistro(oreSet, minSet, secSet);
            produzioneMacchina.Tempi();
        }

        public void azzeraTempi()
        {
            for (int i=0; i<9; i++)
            {
                Scrivi_in_mac("0", codice + (1101+i));
            }
        }

        //se ci sono commesse in db e posti liberi in macchina carico commesse
        public bool AggiuntaNuoveCommesse()
        {
            bool ret = false;
            if (produzione.LeggiDb() & produzioneMacchina.checkProd())
            {
                //ToDo Controllare quanti giri fa l'inserimento in macchina
                foreach (Commessa commessa in produzione.commesse)
                {
                    for (int i = 0; i < produzioneMacchina.commesse.Count; i++)
                    {
                        CommessaMacchina cm = produzioneMacchina.commesse[i];
                        //riga della commessa disponibile
                        if ((cm.finita || cm.vuota) & !cm.attiva)
                        {
                            //Inserisci commessa in macchina e in Produzione/Output
                            aggiungiCommessaMacchina(i, commessa);
                            Queries.InserimentoProduzione(commessa.cprod_id, MAC_id);
                            break;
                        }
                    }
                }
            }
            return ret;
        }

        //tool di azzeramento commesse e tempi
        //ToDo: da non lasciare in Produzione!!
        public bool Tool_ClearValues()
        {
            bool ret = false;
            try
            {
                azzeraTempi();

                //azzera commesse 
                for (int riga = 0; riga <= 9; riga++)
                {
                    aggiungiCommessaMacchina(riga, new Commessa());
                }

                ret = true;
            }
            catch
            {
                Console.WriteLine("Errore pulizia macchina");
            }
            return ret;
        }

        /*eliminazione commesse in macchina : 
         * elenco STATUS
         * 6 da eliminare
         * 7 eliminata
         * 8 impossibile da eliminare
         */
        public void EliminaCommesse()
        {
            List<int> ids_to_remove = Queries.leggi_canc_comm(MAC_id);
            foreach (int id in ids_to_remove)
            {
                // get indice della commessa in macchina dall'id, ignora la commessa attiva
                int index = produzioneMacchina.get_index_by_id(id);
                if (index >= 0)
                {
                    aggiungiCommessaMacchina(index, new Commessa());
                    Queries.cambia_status_comm(id, 7, MAC_id);
                }
                else
                {
                    Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "commessa " + id + " inesistente o attiva, impossibile eliminare");
                    Queries.cambia_status_comm(id, 8, MAC_id);
                }
            }
        }

        #endregion

    }
}
