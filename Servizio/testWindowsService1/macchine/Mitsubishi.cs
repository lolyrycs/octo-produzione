﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using cns.utils;
using System.Data;
using System.Threading;

namespace cns.macchine
{
    class Mitsubishi : ControlloNumerico
    {
        public SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
        public int iLogicalStationNumber;
        public int iReturnCode;
        private ActUtlTypeLib.ActUtlTypeClass lpcom_ReferencesUtlType = new ActUtlTypeLib.ActUtlTypeClass();
        private ActProgTypeLib.ActProgTypeClass lpcom_ReferencesProgType = new ActProgTypeLib.ActProgTypeClass();

        public string toshift = "Q03UE";

        #region variabili octo
        public int programmaAttivo = 0;
        public bool commesseVuote = false;
        public bool commesseFinite = false;
        public string[,] tmpCommesse = new string[10, 9];
        public int?[] finite = new int?[10];
        public int?[] vuote = new int?[10];
        //Per tabella produzione
        public int[] tabProduzione = new int[60];
        public int[] tmpTempiLetti = new int[6];
        //lettura tempi
        volatile bool reqAttivazione = false;
        volatile bool boolCommessaFinita = false;
        volatile bool istanziato = false;

        public List<int> cp_ids = new List<int>();
        #endregion

        public Mitsubishi(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {

        }

        #region util

        public string tolen(string num, int len)
        {
            while (num.Length < len)
            {
                num = '0' + num;
            }

            return num;
        }

        public int leggi_double(int posizione)
        {
            int ret = 0;
            string szDeviceName = "D" + posizione;
            int[] arrDeviceValue = new int[2];
            try
            {

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom(szDeviceName, 1,
                                                                   out arrDeviceValue[0]);
                szDeviceName = "D" + (posizione + 1);
                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom(szDeviceName, 1,
                                                                      out arrDeviceValue[1]);
                string primo, secondo = "";
                primo = tolen(Convert.ToString(arrDeviceValue[1], 2), 16);
                secondo = tolen(Convert.ToString(arrDeviceValue[0], 2), 16);
                ret = Convert.ToInt32(primo + secondo, 2);
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection, "ERRORE LETTURA DOUBLE, " + ex.Message + " " + ex.StackTrace, MAC_id);
            }

            return ret;
        }

        public bool scrivi_double(int posizione, int num)
        {
            bool ret = false;
            string szDeviceName = "D" + posizione;
            int[] arrDeviceValue = new int[2];
            try
            {
                string tot = tolen(Convert.ToString(num, 2), 32);
                arrDeviceValue[1] = Convert.ToInt32(tot.Substring(0, 16), 2);
                arrDeviceValue[0] = Convert.ToInt32(tot.Substring(16, 16), 2);

                iReturnCode = lpcom_ReferencesUtlType.WriteDeviceRandom(szDeviceName, 1,
                                                                   ref arrDeviceValue[0]);
                szDeviceName = "D" + (posizione + 1);
                iReturnCode = lpcom_ReferencesUtlType.WriteDeviceRandom(szDeviceName, 1,
                                                                      ref arrDeviceValue[1]);


                ret = true;
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection, "ERRORE LETTURA DOUBLE, " + ex.Message + " " + ex.StackTrace, MAC_id);
                ret = false;
            }

            return ret;
        }
    
        public bool open_connection()
        {
            if (!istanziato)
            {
                try
                {
                    try
                    {
                        //When ActUtlType is selected by the radio button,
                        using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            if (connection.State != ConnectionState.Open) connection.Open();
                            SqlCommand command = new SqlCommand();
                            string sqlQuery = "SELECT (MAC_ipAdress2) FROM Macchine WHERE MAC_controlloNumerico= '" + MAC_controlloNumerico + "' AND MAC_attivo = 1 AND MAC_id= " + MAC_id + "";
                            command.CommandText = sqlQuery;
                            command.Connection = connection;
                            var n = command.ExecuteScalar();
                            if (n != null)
                            {
                                iLogicalStationNumber = Convert.ToInt32(n);
                            }
                            command.Dispose();
                            if (connection.State != ConnectionState.Closed) connection.Close();
                        }
                    }
                    catch (Exception exception)
                    {
                        Logs.LogDB(connection, "ERRORE LETTURA STATION NUMBER (MAC_ipAdress2), MSG: " + exception.Message + " " + exception.StackTrace, MAC_id);
                    }

                    //Set the value of 'LogicalStationNumber' to the property.
                    lpcom_ReferencesUtlType.ActLogicalStationNumber = iLogicalStationNumber;

                    //Set the value of 'Password'.
                    lpcom_ReferencesUtlType.ActPassword = "";

                    //The Open method is executed.
                    iReturnCode = lpcom_ReferencesUtlType.Open();
                    //When the Open method is succeeded, disable the TextBox of 'LogocalStationNumber'.
                    //When the Open method is succeeded, make the EventHandler of ActUtlType Controle.
                    if (iReturnCode != 0)
                    {
                        Logs.LogDB(connection_Produzione, "ERRORE CONNESSIONE PLC, CODE: " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                        istanziato = false;
                    }
                    else {

                        istanziato = true;
                        Logs.LogDB(connection_Produzione, "PLC CONESSO", MAC_id);
                    }
                }
                //Exception processing
                catch (Exception exception)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE CONNESSIONE PLC, MSG: " + exception.Message + " " + exception.StackTrace, MAC_id);
                    istanziato = false;
                }
            }
            return istanziato;
        }

        #endregion

        #region interfaccia

        public override void gestioneInterfaccia(object myId = null)
        {
            while (Service1.alive)
            {
                if (connesso)
                {
                    if (!istanziato) open_connection();
                    else
                    {
                        checkStatus();
                        InterfacciDB(connection);
                    }
                }
                else {
                    //chiudi connessione
                    if (istanziato)
                    {
                        istanziato = false;
                        Logs.LogDB(connection_Produzione, "PLC non connesso", MAC_id);
                        try
                        {
                            iReturnCode = lpcom_ReferencesUtlType.Close();
                            if (iReturnCode != 0)
                            {
                                Logs.LogDB(connection_Produzione, "ERRORE DISCONNESSIONE PLC, CODE: " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                            }
                        }
                        catch
                        {
                            Logs.LogDB(connection_Produzione, "ERRORE DISCONNESSIONE PLC, CODE: " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                        }
                    }
                }
                
                Thread.Sleep(3000);
            }
        }

        public void checkStatus()
        {
            int iReturnCode;				//Return code
            String szDeviceName = "";		//List data for 'DeviceName'
            int iNumberOfData = 1;			//Data for 'DeviceSize'
            short[] arrDeviceValue = new short[iNumberOfData];		    //Data for 'DeviceValue
            int iNumber;					//Loop counter
            System.String[] arrData;        //Array for 'Data'

            status = "";
            try
            {
                // Lettura MAN - AUTO
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        szDeviceName = "M" + 1115;      //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        szDeviceName = "M" + 115;      //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        break;
                }

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue[0]);

                if (iReturnCode == 0)
                {
                    if (arrDeviceValue[0] == 1)
                        status += "MAN ";
                    else
                        status += "AUT ";
                }
                else
                {
                    Logs.LogDB(connection, "ERRORE LETTURA STATO, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }

                status += " ";

                // Lettura EMG
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        szDeviceName = "M" + 3838;      //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        szDeviceName = "M" + 3838;      //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        break;
                }

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue[0]);

                if (iReturnCode == 0)
                {
                    if (arrDeviceValue[0] == 1)
                    {
                        status += "EMG ";
                        INT_totem = "viola";
                    }
                    else
                        status += "****";
                }
                else
                {
                    Logs.LogDB(connection, "ERRORE LETTURA STATO, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }

                //lettura totem
                // Lettura ALLARME
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        szDeviceName = "M" + 3466;      //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        szDeviceName = "M" + 3466;      //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        break;
                }

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue[0]);

                if (iReturnCode == 0)
                {
                    if (arrDeviceValue[0] == 1)
                    {
                        INT_totem = "rosso";
                    }
                }
                else
                {
                    Logs.LogDB(connection, "ERRORE LETTURA STATO, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }

                // Lettura CORSA
                int shift = 0;
                if (MAC_tipoControllo == toshift) shift = 1000;
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        szDeviceName = "M" + (3467-shift);      //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        szDeviceName = "M" + (3467-shift);      //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        break;
                }

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue[0]);

                if (iReturnCode == 0)
                {
                    if (arrDeviceValue[0] != 1) //Modifica Simone 28/12 per simulazione LACO
                    {
                        INT_totem = "verde";
                    }
                }
                else
                {
                    Logs.LogDB(connection, "ERRORE LETTURA STATO, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }

                // Lettura EMG
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        szDeviceName = "M" + 3468;      //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        szDeviceName = "M" + 3468;      //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        break;
                }

                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue[0]);

                if (iReturnCode == 0)
                {
                    if (arrDeviceValue[0] == 1)
                    {
                        INT_totem = "giallo";
                    }
                }
                else
                {
                    Logs.LogDB(connection, "ERRORE LETTURA STATO, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(connection, "ERRORE LETTURA STATO, " + ex.Message + " " + ex.StackTrace, MAC_id);
            }
        }

        #endregion

        #region produzione

        public override void gestioneProduzione(object myId = null)
        {
            while (Service1.alive)
            {
                Thread.Sleep(5000);
                if (connesso && istanziato)
                {
                    gestioneProduzioneMacchineUtComune();
                    int shift = 0;
                    if (MAC_tipoControllo == toshift) shift = 1000;
                    if (MAC_tipoControllo == "FX3U")
                    {
                        calcoloTempi(4160, 4171);
                        aggiungiCommessa(4150, 4154);
                    }
                    else if (MAC_tipoControllo == "Q03UE")
                    {
                        calcoloTempi(4160, 4171);
                        aggiungiCommessa(4150, 4154);
                    }
                    else
                    {
                        Logs.LogDB(connection_Produzione, "Tipo controllo errato, Rettifica", MAC_id);
                    }
                    spostaCommesse();
                }
            }
        }

        public void gestioneProduzioneMacchineUtComune()
        {
            if (connesso)
            {
                #region Var. lettura produzione
                String szDeviceName = "";		//List data for 'DeviceName'
                int iNumberOfData_prod = 60;          //Data for 'DeviceValue'
                int iNumberOfData_attiv = 10;          //Data for 'DeviceValue'
                int[] arrDeviceValue;         //Data for 'DeviceValue'
                int iNumber;                    //Loop counter
                System.String[] arrData;        //Array for 'Data'
                #endregion

                ushort inizio;
                ushort fine;

                //Controllo il tipo di PLC montato in macchina
                switch (MAC_tipoControllo)
                {
                    case ("FX3U"):
                        inizio = 4210;      //Da controllare in macchina Simone 12/12
                        fine = 4259;        //Da controllare in macchina Simone 12/12
                        break;
                    case ("Q03UE"):
                        inizio = 4210;      //Da controllare in macchina Simone 12/12
                        fine = 4259;        //Da controllare in macchina Simone 12/12
                        break;
                    default:
                        inizio = 0;      //Da controllare in macchina Simone 12/12
                        fine = 0;        //Da controllare in macchina Simone 12/12
                        break;
                }

                //Lettura programma attivo
                #region Lettura programmaAttivo
                programmaAttivo = 0;
                int iNumberOfData = 1;
                arrDeviceValue = new int[iNumberOfData_attiv];
                int shift = 1000;
                if (MAC_tipoControllo == toshift) shift = 2000;
                int appInizio = inizio - shift;

                //Lettura arrey da macchina (leggo stato leed che indica il prog. attivo)
                for (int rr = 0; rr < iNumberOfData_attiv; rr++)
                {
                    szDeviceName = "M" + (appInizio - 10 + rr).ToString();
                    try
                    {
                        iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom(szDeviceName, iNumberOfData, out arrDeviceValue[rr]);
                    }
                    catch (Exception exception)
                    {
                        Logs.LogDB(connection_Produzione, exception.Message + exception.StackTrace, MAC_id);
                    }
                }

                //--- Lettura programma attivo  
                if (iReturnCode == 0)
                {
                    for (int qq = 0; qq < iNumberOfData_attiv; qq++)
                    {
                        if (arrDeviceValue[qq] == 1)
                        {
                            programmaAttivo = qq + 1;
                            break;
                        }
                    }
                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE TABELLA PRODUZIONE, CODE: "+ String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }
                #endregion

                //--- Lettura tabella di produzione                    
                #region Lettura tabellaProduzione
                arrDeviceValue = new int[iNumberOfData_prod];
                int posizione = 0;
                for (int aa = 0; aa < iNumberOfData_prod; aa++)
                {
                    //lettura valori della tabella produzione doble
                    posizione = inizio + 2 * aa;
                    arrDeviceValue[aa] = leggi_double(posizione);
                }

                /* deprecated
                 * 
                 * szDeviceName = "D" + inizio.ToString();
                try
                {
                    iReturnCode = lpcom_ReferencesUtlType.ReadDeviceBlock(szDeviceName, iNumberOfData_prod, out arrDeviceValue[0]);
                }
                catch (Exception exception)
                {
                    Logs.LogDB(connection_Produzione, exception.Message + Name, MAC_id);
                }*/

                //--- Lettura tabella di produzione                    
                if (iReturnCode == 0)
                {
                    /*for (int qq = 0; qq < iNumberOfData_prod; qq++)
                        tabProduzione[qq] = arrDeviceValue[qq];*/

                    short[] temp = new short[10];

                    int riga = 0;
                    int col = 0;
                    for (int aa = 0; aa < 6; aa++)
                    {
                        for (int qq = 0; qq < 10; qq++)
                        {
                            tabProduzione[qq * 6 + aa] = arrDeviceValue[aa * 10 + qq];
                        }
                    }
                    /*for (int i=0; i<6; i++)
                    {
                        for (int j = 0; j < 10; j++)
                            temp[j]=( arrDeviceValue[i * 10 + j]);
                        tabProduzione

                    }*/


                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE TABELLA PRODUZIONE, CODE: " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
                }
                #endregion

                //--- Lettura pezzi fatti su commessa attiva
                #region Commessa Attiva
                switch (programmaAttivo)
                {
                    case 1:                                                 //Controllare tutti gli indici dell'array
                        commessaAttiva = tabProduzione[0].ToString();
                        pezziFatti = tabProduzione[2];
                        break;

                    case 2:
                        commessaAttiva = tabProduzione[6].ToString();
                        pezziFatti = tabProduzione[8];
                        break;

                    case 3:
                        commessaAttiva = tabProduzione[12].ToString();
                        pezziFatti = tabProduzione[14];
                        break;

                    case 4:
                        commessaAttiva = tabProduzione[18].ToString();
                        pezziFatti = tabProduzione[20];
                        break;

                    case 5:
                        commessaAttiva = tabProduzione[24].ToString();
                        pezziFatti = tabProduzione[26];
                        break;

                    case 6:
                        commessaAttiva = tabProduzione[30].ToString();
                        pezziFatti = tabProduzione[32];
                        break;

                    case 7:
                        commessaAttiva = tabProduzione[36].ToString();
                        pezziFatti = tabProduzione[38];
                        break;

                    case 8:
                        commessaAttiva = tabProduzione[42].ToString();
                        pezziFatti = tabProduzione[38];
                        break;

                    case 9:
                        commessaAttiva = tabProduzione[48].ToString();
                        pezziFatti = tabProduzione[50];
                        break;

                    case 10:
                        commessaAttiva = tabProduzione[54].ToString();
                        pezziFatti = tabProduzione[56];
                        break;

                    default:
                        break;
                }
                #endregion

                //--- Aggiorno dati interfaccia 
                ModificaInterfaccia();

                //Update dati letti in Database (pezzi fatti e commessa attiva)
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        string sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezzifatti = '" + pezziFatti + "' " +
                                   "WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                }
                catch (Exception e)
                {
                    Logs.LogDB(connection_Produzione, "errore aggiornamento pezzi e commessa produzione", MAC_id);
                }
            }
        }


        #region Scrivi Job
        public void aggiornaStatoLavori(short startIndex, short stopIndex, int indextt, int status)
        {
            //Marco record in Produzione come inviato a CNC
            //Status = 2 --> Record scritto su CNC

            ushort length = 28;
            int[] arrDeviceValue = new int[6];
            int iNumberOfData = 1;
            string szDeviceName = "";
            for (int aa = 2; aa < 8; aa++)
            {
                try
                {
                    arrDeviceValue[aa - 2]
                        = Convert.ToInt32(tmpCommesse[indextt, aa]);
                }

                //Exception processing
                catch (Exception ex)
                {
                    Logs.LogDB(connection, ex.Message + ex.StackTrace, MAC_id);
                    return;
                }
            }
            int pos = 0;
            try
            {
                for (int i = 0; i < 6; i++)
                {
                    pos = (startIndex + i * 20);

                    scrivi_double(pos, arrDeviceValue[i]);
                    //iReturnCode = lpcom_ReferencesUtlType.WriteDeviceBlock(szDeviceName, iNumberOfData, ref arrDeviceValue[0]);
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection, ex.Message + ex.StackTrace, MAC_id);
                return;
            }

            if (iReturnCode != 0)
            {
                Logs.LogDB(connection_Produzione, "ERRORE " + indextt + " RIGA TAB. PROD. CNC, RET = " + iReturnCode.ToString(), MAC_id);
            }
            else
            {
                try
                {
                    int in_id = Convert.ToInt32(tmpCommesse[indextt, 0]);
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                        SqlCommand command = new SqlCommand("UPDATE Input SET IN_status = '2' WHERE IN_id = '" + in_id + "'", connection_Produzione);
                        int m = command.ExecuteNonQuery();

                        command.Dispose();

                        string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                                " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + in_id + "";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        m = command.ExecuteNonQuery();

                        //cicloproduzione status 2
                        command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '2' " +
                           "FROM Cicloproduzione INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE IN_id = '" + in_id + "' AND CPROD_vs_MAC_id= " + MAC_id, connection_Produzione);
                        m = command.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                    }

                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
                }
                tmpCommesse[indextt, 0] = null;
                Console.WriteLine("inserite in macchina");
                //Logs.LogDB(connection, "commesse inserite in macchina", MAC_id);
            }
        }

        public void scriviLavori()
        {
            int ww = 0;

            for (int ll = 0; ll < 10; ll++)
            {
                finite[ll] = null;
                vuote[ll] = null;
            }

            //Leggo tabella in macchina per contare Record finiti e vuoti
            for (int qq = 5; qq < 60; qq = qq + 6)
            {
                if (tabProduzione[qq] == 0)
                {
                    commesseVuote = true;
                    vuote[ww] = ww;
                }
                else if (tabProduzione[qq] < 0)
                {
                    commesseFinite = true;
                    finite[ww] = ww;
                }
                ww++;
            }

            //Rendo non disponibile la riga delle commessa attiva
            if (programmaAttivo != 0)
            {
                finite[programmaAttivo - 1] = null;
                vuote[programmaAttivo - 1] = null;
            }

            int rr = 0;

            //Pulisco tmpCommesse
            for (int i = 0; i < 10; i++)
            {
                for (int o = 0; o < 9; o++)
                    tmpCommesse[i, o] = null;
            }

            //Controllo se i Record inseriti in tabella Produzione sono maggiori di 10
            //Estraggo solo i primi 10 Record in Produzione CC
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command = new SqlCommand("SELECT TOP (10) IN_id, CPROD_programma, CPROD_commessa, CPROD_lotto, CPROD_pezziFatti, IN_priorita, CPROD_id, IN_fase, IN_articolo " +
                       "FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id  " +
                       "WHERE CPROD_status = '1' AND CPROD_vs_MAC_id='" + MAC_id + "' ORDER BY IN_priorita ASC", connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (tmpCommesse[rr, 0] == null)
                            {
                                if (!reader.IsDBNull(0))
                                    tmpCommesse[rr, 0] = (reader.GetInt32(0)).ToString();           //Indice database
                                else
                                    tmpCommesse[rr, 0] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(1))                                            //Programma
                                    tmpCommesse[rr, 1] = reader.GetString(1);
                                else
                                    tmpCommesse[rr, 1] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(2))                                            //Commessa
                                    tmpCommesse[rr, 2] = reader.GetString(2);
                                else
                                    tmpCommesse[rr, 2] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(3))                                            //Lotto
                                    tmpCommesse[rr, 3] = reader.GetString(3);
                                else
                                    tmpCommesse[rr, 3] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(4))                                            //Pezzi Fatti
                                    tmpCommesse[rr, 4] = (reader.GetInt32(4)).ToString();
                                else
                                    tmpCommesse[rr, 4] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(5))                                            //Priorità
                                    tmpCommesse[rr, 7] = (reader.GetInt32(5)).ToString();
                                else
                                    tmpCommesse[rr, 7] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(7))                                            //Fase
                                    tmpCommesse[rr, 5] = reader.GetString(7);
                                else
                                    tmpCommesse[rr, 5] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(8))                                            //Articolo
                                    tmpCommesse[rr, 6] = reader.GetString(8);
                                else
                                    tmpCommesse[rr, 6] = "0";
                                //-------------------------------------------------------------------------------------
                                if (!reader.IsDBNull(6))                                            //CPROD_id
                                    cp_ids.Add(reader.GetInt32(6));
                                else
                                    cp_ids.Add(0);
                                //-------------------------------------------------------------------------------------
                                rr++;
                            }
                        }
                    }
                    reader.Close();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }

            for (int yy = 0; yy < rr; yy++)
            {
                if (commesseVuote == true || commesseFinite == true)
                {
                    for (int uu = 0; uu < 10; uu++)
                    {
                        if (vuote[uu] != null || finite[uu] != null)
                        {
                            int? mycurrentCommessa = null;
                            if (vuote[uu] != null)
                            {
                                mycurrentCommessa = vuote[uu];
                            }
                            else
                            {
                                mycurrentCommessa = finite[uu];
                            }
                            switch (MAC_tipoControllo)
                            {
                                case ("FX3U"):
                                    switch (mycurrentCommessa)
                                    {
                                        case (0):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4210, 0, yy, 2);
                                            break;
                                        case (1):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4212, 0, yy, 2);
                                            break;
                                        case (2):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4214, 0, yy, 2);
                                            break;
                                        case (3):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4216, 0, yy, 2);
                                            break;
                                        case (4):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4218, 0, yy, 2);
                                            break;
                                        case (5):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4220, 0, yy, 2);
                                            break;
                                        case (6):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4222, 0, yy, 2);
                                            break;
                                        case (7):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4224, 0, yy, 2);
                                            break;
                                        case (8):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4226, 0, yy, 2);
                                            break;
                                        case (9):
                                            //Passso info da locale a PLC
                                            aggiornaStatoLavori(4228, 0, yy, 2);
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                default:
                                    break;
                                case ("Q03UE"):
                                    switch (mycurrentCommessa)
                                    {
                                        case (0):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4210, 0, yy, 2);
                                            break;
                                        case (1):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4212, 0, yy, 2);
                                            break;
                                        case (2):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4214, 0, yy, 2);
                                            break;
                                        case (3):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4216, 0, yy, 2);
                                            break;
                                        case (4):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4218, 0, yy, 2);
                                            break;
                                        case (5):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4220, 0, yy, 2);
                                            break;
                                        case (6):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4222, 0, yy, 2);
                                            break;
                                        case (7):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4224, 0, yy, 2);
                                            break;
                                        case (8):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4226, 0, yy, 2);
                                            break;
                                        case (9):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(4228, 0, yy, 2);
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                            }

                            //Ricontrollo su CNC la presenza di righe vuote
                            //Pulisco Array 
                            for (int pp = 0; pp < 10; pp++)
                            {
                                vuote[pp] = null;
                                finite[pp] = null;
                            }
                            ww = 0;
                            int[] temp_data = new int[60];
                            ushort inizio = 2500;
                            ushort fine = 2699;
                            int iData = 0;
                            if (MAC_tipoControllo == "FX3U" || MAC_tipoControllo == "Q03UE")
                            {
                                inizio = 4210;
                                fine = 4269;
                            }
                            //--- Lettura tabella di produzione       
                            //
                            //Processing of ReadDeviceBlock2 method
                            //

                            int iNumberOfData = (int)(inizio - fine);
                            for (int i = 0; i < 60; i++)
                            {
                                temp_data[i] = leggi_double(inizio + 2 * i);
                            }


                            if (iReturnCode == 0)
                            {
                                int riga = 0;
                                int col = 0;
                                for (int aa = 0; aa < 6; aa++)
                                {
                                    for (int qq = 0; qq < 10; qq++)
                                    {
                                        tabProduzione[qq * 6 + aa] = temp_data[aa * 10 + qq];
                                    }
                                }
                            }
                            else
                            {
                                Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO D2500, RET = " + iReturnCode.ToString(), MAC_id);
                            }
                            //Leggo tabella in macchina per contare Record finiti e vuoti


                            for (int qq = 4; qq < 50; qq = qq + 5)
                            {
                                if (tabProduzione[qq] == 0)
                                {
                                    commesseVuote = true;
                                    vuote[ww] = ww;
                                }
                                else if (tabProduzione[qq] < 0)
                                {
                                    commesseFinite = true;
                                    finite[ww] = ww;
                                }
                                ww++;
                            }
                            //Rendo non disponibile la riga delle commessa attiva
                            if (programmaAttivo != 0)
                            {
                                finite[programmaAttivo - 1] = null;
                                vuote[programmaAttivo - 1] = null;
                            }


                            break;
                        }
                    }
                }
            }
        }
        #endregion

        #region calcoloTempi 
        public void calcoloTempi(ushort indicestart, ushort indicestop)
        {
            #region Var. Tempi PLC
            String szDeviceName = "";       //List data for 'DeviceName'
            String appSzDeviceName = "";    //List data for 'DeviceName'
            int iNumberOfData_tmp = 6;      //Data for 'DeviceValue'            
            int[] arrDeviceValue = new int[6];         //Data for 'DeviceValue'
            int iNumber;                    //Loop counter
            System.String[] arrData;        //Array for 'Data'
            #endregion

            #region Def. Variabili
            int tmpStatus = 0;
            string PROD_appTempoLavoro = null;
            string PROD_appTempoPiazzamento = null;
            string PROD_appTempoPausa = null;
            string PROD_efficienza = null;

            int secLavoro = 0;
            int secPiazzamento = 0;
            int secPausa = 0;
            int secLavoroDB = 0;
            int secPiazzamentoDB = 0;
            int secPausaDB = 0;

            int EFF_Totale = 0;
            int EFF_Lavoro = 0;
            #endregion

            string sqlQuery = "";
            SqlCommand command = null;
            SqlDataReader reader = null;
            //Aggiorno tempi

            // Leggo lo stato della commessa attiva
            StatoLavoriComune();

            // Leggo status commessa attiva
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //Leggo status commessa attiva
                    sqlQuery = "SELECT (CPROD_status) FROM Cicloproduzione WHERE CPROD_commessa = '" + commessaAttiva + "' AND CPROD_vs_MAC_id= " + MAC_id + " order by cprod_id desc";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    var st = command.ExecuteScalar();
                    if (st != null)
                    {
                        tmpStatus = Convert.ToInt32(st);
                    }
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }

            // leggo i tempi dalla macchina
            for (int i = 0; i < 6; i++)
            {
                arrDeviceValue[i] = leggi_double(indicestart + 2 * i);
            }



            /*szDeviceName = "D" + indicestart.ToString();
            try
            {
                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceBlock2(szDeviceName, iNumberOfData_tmp, out arrDeviceValue[0]);
            }
            catch (Exception exception)
            {
                Logs.LogDB(connection_Produzione, exception.Message + Name, MAC_id);
            }*/


            //allineamento tempi Piazzamento - Lavoro -Pausa
            if (iReturnCode == 0)
            {
                tmpTempiLetti[0] = arrDeviceValue[2];
                tmpTempiLetti[1] = arrDeviceValue[5];
                tmpTempiLetti[2] = arrDeviceValue[1];
                tmpTempiLetti[3] = arrDeviceValue[4];
                tmpTempiLetti[4] = arrDeviceValue[0];
                tmpTempiLetti[5] = arrDeviceValue[3];
            }
            else
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA TEMPI, RET = " + String.Format("0x{0:x8} [HEX]", iReturnCode), MAC_id);
            }

            // Controllo lo status della commessa attiva
            if (tmpStatus > 3)
            {
                if (tmpStatus == 4)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            sqlQuery = "SELECT PROD_appTempoLavoro, PROD_appTempoPiazzamento, PROD_appTempoPausa " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = '" + commessaAttiva + "' order by CPROD_id desc";
                            command = new SqlCommand(sqlQuery, connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(0))
                                    {
                                        PROD_appTempoLavoro = reader.GetString(0);
                                        secLavoroDB = Convert.ToInt32(PROD_appTempoLavoro);
                                    }
                                    else secLavoroDB = 0;
                                    if (!reader.IsDBNull(1))
                                    {
                                        PROD_appTempoPiazzamento = reader.GetString(1);
                                        secPiazzamentoDB = Convert.ToInt32(PROD_appTempoPiazzamento);
                                    }
                                    else secPiazzamentoDB = 0;
                                    if (!reader.IsDBNull(2))
                                    {
                                        PROD_appTempoPausa = reader.GetString(2);
                                        secPausaDB = Convert.ToInt32(PROD_appTempoPausa);
                                    }
                                    else secPausaDB = 0;
                                }
                                reader.Close();
                            }
                            reader.Close();
                            command.Dispose();
                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
            }
            secPiazzamento = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[0], tmpTempiLetti[1]) + secPiazzamentoDB;
            secLavoro = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[2], tmpTempiLetti[3]) + secLavoroDB;
            secPausa = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[4], tmpTempiLetti[5]) + secPausaDB;
            EFF_Totale = secLavoro + secPausa + secPiazzamento;
            EFF_Lavoro = secLavoro;
            if (EFF_Totale != 0)
                efficienza = (EFF_Lavoro * 100) / EFF_Totale;
            PROD_efficienza = efficienza.ToString();


            // aggiorno i tempi su DB
            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    sqlQuery = "if (ISNULL((SELECT distinct(PROD_tempoLavoro) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa =" + DbHelper.cleanNullValue(commessaAttiva) + "),0)<=" + DbHelper.cleanNullValue(secLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPiazzamento) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazzamento) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPausa) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                            "UPDATE Produzione SET PROD_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + ", PROD_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazzamento) + ",  PROD_tempoPausa= " + DbHelper.cleanNullValue(secPausa) + ", PROD_efficienza= " + DbHelper.cleanNullValue(efficienza) +
                           " FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + " ";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }
                //i pezzi fatti
                //Leggo status commessa attiva


                //if (tmpStatus==4)
                //{
                //    sqlQuery = "UPDATE Produzione " +
                //            "SET PROD_appTempoLavoro = PROD_tempoLavoro,  PROD_appTempoPiazzamento = PROD_tempoPiazzamento,  " +
                //            "PROD_appTempoPausa = PROD_tempoPausa " +
                //            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id  WHERE CPROD_commessa = '" + commessaAttiva + "'";
                //    command = new SqlCommand(sqlQuery, connection_Produzione);
                //    command.ExecuteNonQuery();
                //    command.Dispose();
                //}
                //if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
            }

            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //Aggiorno anche tabella output
                    sqlQuery = "if (ISNULL((SELECT distinct(OUT_tempoLavoro) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPiazzamento) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazzamento) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPausa) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                        "UPDATE Output SET OUT_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + " , OUT_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazzamento) + " " +
                    ", OUT_tempoPausa = " + DbHelper.cleanNullValue(secPausa) + " , OUT_efficienza = " + DbHelper.cleanNullValue(efficienza) + " FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva);
                    SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    int N = comando.ExecuteNonQuery();
                    sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + " WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva);
                    comando = new SqlCommand(sqlQuery, connection_Produzione);
                    N = comando.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();


                    comando.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
            }

            //in macchina azzera tempi e attiva commessa
            if (reqAttivazione)
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        sqlQuery = "UPDATE Produzione " +
                                "SET PROD_appTempoLavoro = PROD_tempoLavoro,  PROD_appTempoPiazzamento = PROD_tempoPiazzamento,  " +
                                "PROD_appTempoPausa = PROD_tempoPausa " +
                                "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id  WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();

                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }

                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        sqlQuery = "UPDATE Output " +
                                "SET OUT_TempoLavoro = PROD_tempoLavoro,  OUT_TempoPiazzamento = PROD_tempoPiazzamento,  " +
                                "OUT_TempoPausa = PROD_tempoPausa " +
                                "FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id=CPROD_id INNER JOIN Produzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();

                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }

                //originale = R972.ldata[0] - 64;
                //rispostaAttivazione = originale + 128;

                //Azzero i tempi su PLC
                short IpsData = 0;
                int index = 4160;
                for (int bb = 0; bb < 6; bb++)
                {
                    /*appSzDeviceName = (Convert.ToInt32(szDeviceName) + bb).ToString();
                    try
                    {
                        //The WriteDeviceRandom2 method is executed.
                        iReturnCode = lpcom_ReferencesUtlType.WriteDeviceRandom2(appSzDeviceName, 1, ref IpsData);
                    }
                    //Exception processing		*/
                    if (!scrivi_double(index + 2 * bb, 0))
                    {
                        Logs.LogDB(connection_Produzione, "ERRORE AZZERAMENTO TEMPI", MAC_id);
                    }
                }

                //Risposta per attivazione commessa a PLC
                szDeviceName = "M3463";
                if(MAC_tipoControllo==toshift) szDeviceName = "M2463";
                IpsData = 1;

                try
                {
                    //The WriteDeviceRandom2 method is executed.
                    iReturnCode = lpcom_ReferencesUtlType.WriteDeviceRandom2(szDeviceName, 1, ref IpsData);
                    reqAttivazione = false;
                }
                //Exception processing			
                catch (Exception exception)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE RISPOSTA PER ATTIVAZIONE COMMESSA" + exception.Message + exception.StackTrace, MAC_id);
                }
            }
        }

        public void StatoLavoriComune()
        {

            #region Var. Attivaz. Commessa
            String szDeviceName = "M3461";  //List data for 'DeviceName'
            if (MAC_tipoControllo == toshift) szDeviceName = "M2461";
            int iNumberOfData = 1;			//Data for 'DeviceSize'
            short[] arrDeviceValue_Req_At;	//Data for 'DeviceValue
            int iNumber;					//Loop counter
            System.String[] arrData;        //Array for 'Data'
            //Assign the array for 'DeviceValue'.
            arrDeviceValue_Req_At = new short[iNumberOfData];
            #endregion

            //Attivazione commessa da PLC - M3461
            try
            {
                //When ActUtlType is selected by the radio button,
                //The ReadDeviceRandom2 method is executed.
                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom2(szDeviceName, iNumberOfData, out arrDeviceValue_Req_At[0]);
            }
            //Exception processing
            catch (Exception exception)
            {
                Logs.LogDB(connection_Produzione, "ERRORE TABELLA PRODUZIONE", MAC_id);
            }

            if (iReturnCode == 0)
            {
                if (arrDeviceValue_Req_At[0].ToString() == "1")
                {
                    switch (programmaAttivo)
                    {
                        case 1:
                            if (tabProduzione[2] >= tabProduzione[1] || tabProduzione[5] < 0)
                            {
                                commessaAttiva = tabProduzione[0].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 2:
                            if (tabProduzione[8] >= tabProduzione[7] || tabProduzione[11] < 0)
                            {
                                commessaAttiva = tabProduzione[6].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 3:
                            if (tabProduzione[14] >= tabProduzione[13] || tabProduzione[17] < 0)
                            {
                                commessaAttiva = tabProduzione[12].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 4:
                            if (tabProduzione[20] >= tabProduzione[19] || tabProduzione[23] < 0)
                            {
                                commessaAttiva = tabProduzione[18].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 5:
                            if (tabProduzione[26] >= tabProduzione[25] || tabProduzione[29] < 0)
                            {
                                commessaAttiva = tabProduzione[24].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 6:
                            if (tabProduzione[32] >= tabProduzione[31] || tabProduzione[35] < 0)
                            {
                                commessaAttiva = tabProduzione[30].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 7:
                            if (tabProduzione[38] >= tabProduzione[37] || tabProduzione[41] < 0)
                            {
                                commessaAttiva = tabProduzione[36].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 8:
                            if (tabProduzione[44] >= tabProduzione[43] || tabProduzione[47] < 0)
                            {
                                commessaAttiva = tabProduzione[42].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 9:
                            if (tabProduzione[50] >= tabProduzione[49] || tabProduzione[53] < 0)
                            {
                                commessaAttiva = tabProduzione[48].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 10:
                            if (tabProduzione[56] >= tabProduzione[55] || tabProduzione[59] < 0)
                            {
                                commessaAttiva = tabProduzione[54].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        default:
                            reqAttivazione = true;
                            break;
                    }
                    //Console.WriteLine("La commessa attiva è " + commessaAttiva);
                }
            }

            string sqlQuery;
            if (boolCommessaFinita && reqAttivazione && programmaAttivo != 0)
            {
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        boolCommessaFinita = false;
                        sqlQuery = "UPDATE Cicloproduzione SET CPROD_status = '5' WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = command.ExecuteNonQuery();
                        command.Dispose();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
            else if (reqAttivazione && programmaAttivo != 0)
            {
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "UPDATE Cicloproduzione " +
                            "SET CPROD_status = '4'" +
                            "WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = command.ExecuteNonQuery();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
        }
        #endregion

        #region Lettura richiesta aggiunta commessa
        public void aggiungiCommessa(ushort indicestart, ushort indicestop)
        {
            #region Var. Req. Agg. Commessa
            String szDeviceName = "M3465";	//List data for 'DeviceName'
            if (MAC_tipoControllo==toshift) szDeviceName = "M2465";
            int iNumberOfData = 1;			//Data for 'DeviceSize'
            int[] arrDeviceValue_Req_Ag;	//Data for 'DeviceValue
            System.String[] arrData;        //Array for 'Data'
            //Assign the array for 'DeviceValue'.
            arrDeviceValue_Req_Ag = new int[iNumberOfData];
            #endregion

            #region Var. Agg. Commessa
            int[] arrDeviceValue_Comm_Ag; //Data for 'DeviceValue
            //Assign the array for 'DeviceValue'.
            arrDeviceValue_Comm_Ag = new int[6];
            #endregion

            #region Def. Var.
            bool reqAggiunta = false;
            string nuovaCommessa = null;
            string nuovaLotto = null;
            string nuovaPriorita = null;
            string nuovaCodice = null;
            string nuovaFase = null;
            int AGG_originale = 0;
            int AGG_risposta = 0;
            string sqlQuery = "";
            #endregion

            //Richiesta aggiunta commessa
            //arrDeviceValue_Req_Ag[0] = leggi_double(4165);


            try
            {
                iReturnCode = lpcom_ReferencesUtlType.ReadDeviceRandom(szDeviceName, iNumberOfData, out arrDeviceValue_Req_Ag[0]);
            }
            catch (Exception exception)
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA REQ. AGGIUNTA COMMESSA", MAC_id);
            }


            if (iReturnCode == 0)
            {
                if (arrDeviceValue_Req_Ag[0].ToString() == "1")
                {
                    reqAggiunta = true;
                    szDeviceName = "D" + indicestart.ToString();
                    iNumberOfData = 3;
                    //--- commessa - pezzi - priorita da aggiungere 

                    arrDeviceValue_Comm_Ag = new int[iNumberOfData];
                    int posizione = 0;
                    for (int aa = 0; aa < iNumberOfData; aa++)
                    {
                        //lettura valori delle commesse richieste  doble
                        posizione = indicestart + 2 * aa;
                        arrDeviceValue_Comm_Ag[aa] = leggi_double(posizione);
                    }

                    //leggidouble
                    /*try
                    {
                        iReturnCode = lpcom_ReferencesUtlType.ReadDeviceBlock2(szDeviceName, iNumberOfData, out arrDeviceValue_Comm_Ag[0]);
                    }
                    catch (Exception exception)
                    {
                        Logs.LogDB(connection_Produzione, "ERRORE LETTURA COMMESSA AGGIUNTIVA", MAC_id);
                    }*/

                    if (iReturnCode == 0)
                    {
                        nuovaCommessa = arrDeviceValue_Comm_Ag[0].ToString();
                        nuovaLotto = arrDeviceValue_Comm_Ag[1].ToString();
                        //nuovaFase = arrDeviceValue_Comm_Ag[2].ToString();
                        //nuovaCodice = arrDeviceValue_Comm_Ag[3].ToString();
                        nuovaPriorita = arrDeviceValue_Comm_Ag[2].ToString();
                        //Carico i valori nuova commessa - lotto - priorità - ricetta da CNC a Tabella Input Database 
                        carica_da_macchina(nuovaCommessa, nuovaLotto, nuovaPriorita, "CNC", fase:"0",codiceArticolo: "0");
                    }
                    else
                    {
                        Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO ", MAC_id);
                    }
                }
            }

            //Risposta per aggiunta commessa a PLC
            if (reqAggiunta)
            {
                szDeviceName = "M3459";
                if(MAC_tipoControllo==toshift) szDeviceName = "M2459";
                short IpsData = 1;

                try
                {
                    iReturnCode = lpcom_ReferencesUtlType.WriteDeviceRandom2(szDeviceName, 1, ref IpsData);
                }
                catch (Exception exception)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE RISPOSTA PER ATTIVAZIONE COMMESSA" + exception.Message + exception.StackTrace, MAC_id);
                }

                indicestart = 4150;
                for (int i = 0; i < 3; i++)
                {
                    scrivi_double(indicestart + 2 * i, 0);
                }
            }
        }
        #endregion

        #region SpostaCommesse
        public void spostaCommesse()
        {
            if (contaCommesseAttive(1) > 0)
            {
                //scrive lavori in macchina
                scriviLavori();
            }
            //seleziona indici
            List<int> index = new List<int>();
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //inserisce il record in produzione
                    SqlCommand command = new SqlCommand();
                    string sqlQuery = "SELECT PROD_id FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_status=2 and CPROD_vs_MAC_id=" + MAC_id;
                    command.CommandText = sqlQuery;
                    command.Connection = connection_Produzione;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            index.Add(reader.GetInt32(0));
                        }
                    }
                    reader.Close();
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }

            //aggiorno tabelle DB
            if (index.Count > 0)
            {
                string sqlQuery = "";
                int N = 0;
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        foreach (int ind in index)
                        {
                            //Trasferisco da Produzione a Output
                            sqlQuery = "INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data, OUT_tempoLavoro, OUT_tempoPiazzamento,  OUT_tempoPausa, OUT_efficienza) " +
                                    "SELECT PROD_vs_CPROD_id, PROD_priorita , GETDATE(), PROD_tempoLavoro, PROD_tempoPiazzamento, PROD_tempoPausa, PROD_efficienza " +
                                    "FROM Produzione WHERE PROD_id = '" + ind + "'";
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();

                            //Aggiorno status in Tabella Produzione
                            //Status = 3 --> Valore letto da Produzionee copiato in Output
                            sqlQuery = "UPDATE Cicloproduzione SET CPROD_status = '3' WHERE CPROD_id= (select PROD_vs_CPROD_id from Produzione where PROD_id= '" + ind + "')";
                            command = new SqlCommand(sqlQuery, connection_Produzione);
                            N = command.ExecuteNonQuery();
                        }
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
            //--- Aggiorno tabella di Output 
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //--- JOIN TRA TABELLA Produzione e Produzione_Out ---//
                    string sqlQuery = "UPDATE a SET a.OUT_tempoLavoro = b.PROD_tempoLavoro, " +
                        "a.OUT_tempoPiazzamento = b.PROD_tempoPiazzamento, a.OUT_tempoPausa = b.PROD_tempoPausa, a.OUT_efficienza = b.PROD_efficienza " +
                            "FROM Output AS a INNER JOIN Produzione AS b ON a.OUT_vs_CPROD_id = b.PROD_vs_CPROD_id";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }

        }
        #endregion

        #endregion
    }
}
