﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using cns.utils;
using System.Data;
using System.Threading;


using BOOL = System.Boolean;
using DWORD = System.UInt32;
using LPWSTR = System.String;
using NET_API_STATUS = System.UInt32;
using System.Security.Principal;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;
using System.IO;

namespace cns.macchine
{
    public class FanucCN : ControlloNumerico
    {
        public SqlConnection connection_Fanuc = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");

        #region DEFINIZIONI PER FANUC CNC
        //PER FOCAS
        public Focas1.ODBDGN Path = new Focas1.ODBDGN();
        public Focas1.ODBST odbst = new Focas1.ODBST();
        public Focas1.ODBEXEPRG PRG = new Focas1.ODBEXEPRG();
        public Focas1.IODBPMC2 G10 = new Focas1.IODBPMC2();
        public Focas1.ODBALMMSG2_data_custom[] alm = new Focas1.ODBALMMSG2_data_custom[17];
        public Focas1.OPMSG3_data_custom[] msg = new Focas1.OPMSG3_data_custom[17];
        //Modifica per STM
        public Focas1.IODBPMC2 G12 = new Focas1.IODBPMC2();
        public Focas1.IODBPSD IOV = new Focas1.IODBPSD();
        //Diagnostici:
        //Motori
        public Focas1.ODBDGN DNG403 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG410 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG308_1 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG308_2 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG308_3 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG308_4 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG308_5 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG309_1 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG309_2 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG309_3 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG309_4 = new Focas1.ODBDGN();
        public Focas1.ODBDGN DNG309_5 = new Focas1.ODBDGN();
        public Focas1.ODBACT SPDSPEED = new Focas1.ODBACT();
        public Focas1.ODBSVLOAD LOAD = new Focas1.ODBSVLOAD();
        //Movimenti Assi
        public Focas1.IODBPMC2 D5000 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5004 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5008 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5012 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5016 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5020 = new Focas1.IODBPMC2();
        // Opzione Tornio / 0i-MF
        public Focas1.IODBPMC2 D2000 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2004 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2008 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2012 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2016 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2020 = new Focas1.IODBPMC2();
        // Fresa 0i-MF
        public Focas1.IODBPMC2 D2024 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2028 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2032 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2036 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2052 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2056 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D5024 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5120 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5124 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5128 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5132 = new Focas1.IODBPMC2();
        //Opzione per tornio
        public Focas1.IODBPMC2 D2120 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2124 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2128 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2132 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2136 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D5060 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5064 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5068 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5072 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5076 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5220 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5224 = new Focas1.IODBPMC2();
        //Opzioni per tornio
        public Focas1.IODBPMC2 D2060 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2064 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2068 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2072 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2076 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2220 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2224 = new Focas1.IODBPMC2();
        // Ore motori trifase
        public Focas1.IODBPMC2 D2420 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2416 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2412 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D2424 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2432 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2428 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2436 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D2140 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2148 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2144 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D2152 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2156 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2160 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D2164 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2168 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2172 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D2180 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2184 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2176 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2188 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2192 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2196 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2400 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2404 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2440 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2448 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2452 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2456 = new Focas1.IODBPMC2();

        //
        public Focas1.IODBPMC2 D5028 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5032 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5036 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5040 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5044 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5048 = new Focas1.IODBPMC2();
        //Opzione per tornio
        public Focas1.IODBPMC2 D2040 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2044 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2048 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2200 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2204 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D2208 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 D5052 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D5056 = new Focas1.IODBPMC2();
        //
        public Focas1.IODBPMC2 G30 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D0 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D10 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D20 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 D400 = new Focas1.IODBPMC2();
        public Focas1.ODBM PZ = new Focas1.ODBM();
        //Per tabella produzione
        public Focas1.IODBPMC2 R970 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 R971 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 R972 = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 buff = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 buff_s = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 buff_l = new Focas1.IODBPMC2();

        public Focas1.IODBPMC2 fase_from_mac= new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 art_from_mac = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 cpids_from_mac = new Focas1.IODBPMC2();
        public Focas1.IODBPMC2 c_from_mac = new Focas1.IODBPMC2();




        //Per tabella produzione
        int[] tabProduzione = new int[50];
        int[] tmpTempiLetti = new int[6];
        int programmaAttivo = 0;
        //GENERALI PER FANUC
        public string pathMacchina = null;
        public string commento = "";
        /*
        public static string INT_totem = null;
        public static string execProg;
        

        public static int? feedAssi;
        public static int? feedMandrino;
        public static int? utensileAttivo;
        public static int? pezziFatti;
        public static int? commessaAttiva;*/

       
        
        public  short ret;
        public short conf;

        public string[] AlarmMessage = new string[17];
        public string[] AlarmNumber = new string[17];
        public string[] OperatorMessage = new string[17];
        public string[] OperatorNumber = new string[17];
        short num_alm = 17;
        short num_opmsg = 17;
        bool handle = false;
        #endregion

        #region DEFINIZIONI PER FANUC ROBOT
        public string HostName;
        /*
        private FRRJIf.Core[] mobjCore;
        private FRRJIf.DataTable[] mobjDataTable;
        private FRRJIf.DataTable[] mobjDataTable2;
        private FRRJIf.DataString[] mobjStrReg;
        private FRRJIf.DataString[] mobjStrReg2;
        private FRRJIf.DataString[] mobjStrRegComment;
        private FRRJIf.DataSysVar[] mobjSysVarInt;
        private FRRJIf.DataNumReg[] mobjNumReg;
        private FRRJIf.DataNumReg[] mobjNumReg2;
        private FRRJIf.DataNumReg[] mobjNumReg3;
        private FRRJIf.DataAlarm[] mobjAlarm;*/
        
        //public static FRRJIf.DataAlarm[] mobjAlarmCurrent;
        
        Array intSDO = new short[100];
        Array intSI = new short[2];
        Array intSO = new short[10];
        Array intUO = new short[10];
        Array intUI = new short[10];
        Array intRDO = new short[3];
        /*
        object vntValue = null;
        object vntValue2 = null;
        object vntValue3 = null;
        bool blnDT = false;
        bool blnSDO = false;
        bool blnSI = false;
        bool blnSO = false;
        bool blnUO = false;*/
        public string strTmp = null;
        //
        
        
        public int[] rispostaRichieste = new int[10];
        public int[] rispostaScrittura = new int[30];
        public int[] rispostaScrittura2 = new int[30];
        public int[] rispostaDiagnostici = new int[30];
        public int[] rispostaDiagnostici2 = new int[30];

        #endregion

        // commesse in elaborazione in scrittura
        public int?[] finite = new int?[10];
        public int?[] vuote = new int?[10];
        public string[,] tmpCommesse = new string[10, 6];
        public bool commesseVuote = false;
        public bool commesseFinite = false;

        public string[] commesseString = new string[10];
        public bool commesseStringBool = false;

        //lettura tempi
        volatile bool reqAttivazione = false;
        volatile bool boolCommessaFinita = false;

        public FanucCN(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {
            commesseStringBool = ConfigurationSettings.AppSettings["commesseString"] == "S";
        }

        #region Util 
        public  int calcoloUtensileAttivo(int number ,  int offset ,  int divider) {
            int utensileAttivo = number - (number / offset * divider);
            return utensileAttivo;
        }
        #endregion

        #region Gestione Feed
        //Feed BIT dritti
        public void feedDritto()
        {
            short ret;
            ret = Focas1.pmc_rdpmcrng(hndl, 0, 0, 12, 12, 9, G12);
            if (ret == Focas1.EW_OK)
                feedAssi = G12.ldata[0];
            else
            {
                Logs.LogDB(connection_Fanuc, "ERRORE LETTURA PARAMETRO G12 DRITTI, RET = " + ret.ToString(), MAC_id);
            }
        }

        //Feed BIT rovesciati
        public void feedRovesciati()
        {
            short ret;
            //ret = Focas1.pmc_rdpmcrng(hndl, 0, 1, 10, 11, 12, G10);
            ret = Focas1.pmc_rdpmcrng(hndl, 0, 0, 12, 12, 9, G12);
            if (ret == Focas1.EW_OK)
            {
                int giri = 0;
                int decremento = -145;
                for (int a = 55; a < 256; a = a + 10)
                {
                    if (G12.ldata[0] == a)
                    {
                        int decremento2 = decremento + 20 * giri;
                        feedAssi = Convert.ToInt32(G12.ldata[0]) - (decremento + 20 * giri);
                        break;
                    }
                    giri++;
                }
            }
            else
            {
                Logs.LogDB(connection_Fanuc, "ERRORE LETTURA PARAMETRO G12 ROVESCIATI, RET = " + ret.ToString(), MAC_id);
            }
        }
        #endregion

        #region Lettura feedRate Mandrino
        public void feedRateMandrino()
        {
            ret = Focas1.pmc_rdpmcrng(hndl, 0, 0, 30, 30, 9, G30);
            if (ret == Focas1.EW_OK)
                feedMandrino = G30.ldata[0];
            else
            {
                Logs.LogDB(connection_Fanuc, "ERRORE LETTURA FEED MANDRINO, RET = " + ret.ToString(), MAC_id);
            }
        }
        #endregion

        #region Lettura programma attivo
        public void letturaProgrammaAttivo()
        {
            execProg = null;
            ret = Focas1.cnc_exeprgname(hndl, PRG);
            if (ret == Focas1.EW_OK)
            {
                if (PRG.o_num != 0)
                    execProg = "O" + PRG.o_num.ToString();
                else
                {
                    for (int I = 0; I < 36; I++)
                    {
                        try
                        {
                            if (Convert.ToInt32(PRG.name[I]) != 0)
                                execProg = execProg + Convert.ToChar(PRG.name[I]);
                            else
                                break;
                        }
                        catch (Exception ex)
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE CONVERSIONE PROGRAMMA ATTIVO: " + execProg + "\n" + ex.Message + "\n" + ex.StackTrace, MAC_id);
                        }
                    }
                }
            }
            else
            {
                Logs.LogDB(connection_Fanuc, "ERRORE LETTURA PROGRAMMA ATTIVO, RET = " + ret.ToString(), MAC_id);
            }
            /*
            string tmpProg = null;
            if (execProg != tmpProg)
            {
                commento = null;
                short top = 0;
                try
                {
                    if (PRG.o_num != 0)
                        top = Convert.ToInt16(execProg.Trim('O'));
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Fanuc, "ERRORE CONVERSIONE PROGRAMMA ATTIVO: " + execProg+"\n" + ex.Message+"\n" +ex.StackTrace, MAC_id);
                }

                short num = 1;
                if (top != 0 && num != 0)
                {
                    try
                    {
                        Focas1.PRGDIR2 PRGDIR2 = new Focas1.PRGDIR2();
                        ret = Focas1.cnc_rdprogdir2(hndl, 1, ref top, ref num, PRGDIR2);

                        for (int m = 0; m < PRGDIR2.dir1.comment.Length; m++)
                            commento += PRGDIR2.dir1.comment[m].ToString();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Fanuc, "ERRORE LETTURA COMMENTO SU PROG. ATTIVO, RET = " + ex.Message, MAC_id);
                    }

                    tmpProg = execProg;
                }
            }
            if (commento  != null)
                commento = commento.Trim('(', ')');
                */
        }
        #endregion

        #region letturaStatus
        public void checkStatus()
        {
            ret = Focas1.cnc_statinfo(hndl, odbst);
            if (ret == Focas1.EW_OK)
            {
                status = "";

                switch (odbst.aut)
                {
                    case 0:
                        status += "MDI ";
                        break;

                    case 1:
                        status += "MEM ";
                        break;

                    case 3:
                        status += "EDIT ";
                        INT_totem = "rosso";
                        break;

                    case 4:
                        status += "HNDL ";
                        break;

                    case 5:
                        status += "JOG ";
                        INT_totem = "rosso";
                        break;

                    default:
                        status += "****";
                        break;
                }

                status += " ";

                switch (odbst.run)
                {
                    case 0:
                        status += "STP";
                        INT_totem = "rosso";
                        break;

                    case 2:
                        status += "HLD";
                        INT_totem = "rosso";
                        break;

                    case 3:
                        status += "STRT";
                        INT_totem = "verde";
                        break;

                    default:
                        status += "****";
                        break;
                }

                status += " ";

                switch (odbst.mstb)
                {
                    case 1:
                        status += "FIN";
                        break;

                    default:
                        status += "***";
                        break;
                }

                status += " ";

                switch (odbst.emergency)
                {
                    case 1:
                        status += "EMG";
                        INT_totem = "viola";
                        break;

                    default:
                        status += "***";
                        break;
                }

                status += " ";

                switch (odbst.alarm)
                {
                    case 1:
                        status += "ALM";
                        if (INT_totem != "viola")
                            INT_totem = "giallo";
                        ret = 0;
                        int countALM = 0;
                        int countMSG = 0;

                        ret = Focas1.cnc_rdalmmsg2_custom(hndl, -1, ref num_alm, alm);
                        if (ret == Focas1.EW_OK)
                        {
                            for (int i = 0; i < num_alm; i++)
                            {
                                AlarmMessage[i] = "";
                                AlarmNumber[i] = "";
                                if (alm[i].msg_len > 0)
                                {
                                    AlarmMessage[i] = alm[i].alm_msg.ToString();
                                    AlarmNumber[i] = alm[i].alm_no.ToString();
                                    countALM++;
                                }
                                else
                                {
                                    AlarmMessage[i] = "";
                                    AlarmNumber[i] = "";
                                }
                            }
                        }
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA ALLARMI SU CN, RET =  " + ret.ToString(), MAC_id);
                        }

                        // Operators Messages
                        ret = Focas1.cnc_rdopmsg3_custom(hndl, -1, ref num_opmsg, msg);
                        if (ret == Focas1.EW_OK)
                        {
                            for (int i = 0; i < num_opmsg; i++)
                            {
                                OperatorMessage[i] = "";
                                OperatorNumber[i] = "";
                                if (msg[i].datano != -1)
                                {
                                    OperatorMessage[i] = msg[i].data.ToString();
                                    OperatorNumber[i] = msg[i].datano.ToString();       //modifica simone benericetti 28/11/2018
                                    countMSG++;
                                }
                            }
                        }
                        else
                        {
                            Logs.LogDB(connection_Fanuc, "ERRORE LETTURA WARNING SU CN, RET = " + ret.ToString(), MAC_id);
                        }
                        int numeromax = countALM;
                        if (countMSG > countALM) numeromax = countMSG;
                        //unisce messaggi e allarmi
                        for (int a = 0; a < numeromax; a++)
                        {
                            if (OperatorMessage != null && AlarmMessage != null)
                            {
                                Service1.AlarmDB(connection_Fanuc, AlarmNumber[a], AlarmMessage[a], OperatorNumber[a], OperatorMessage[a], MAC_id);
                            }
                        }

                        break;
                    default:
                        status += "***";
                        break;
                }
            }
            else
            {
                Logs.LogDB(connection_Fanuc, "ERRORE LETTURA STATO, RET = " + ret.ToString(), MAC_id);
            }

        }
        #endregion

        #region LetturaDiagnostici
        public void letturaDatiMacchineUtComuni() {


            Letman();

            //Temperature Motori
            //Lettura temperatura Mandrino
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 403, 1, 5, DNG403);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA MANDRINO [DNG403], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura temperatura Motore Asse X
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 308, 1, 5, DNG308_1);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ASSE X [DNG308#1], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura temperatura Motore Asse Y
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 308, 2, 5, DNG308_2);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ASSE Y [DNG308#2], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura temperatura Motore Asse Z
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 308, 3, 5, DNG308_3);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ASSE Z [DNG308#3], RET = " + ret.ToString(), MAC_id);
            }

            if (MAC_numeroAssi > 3)
            {
                //Lettura temperatura Motore Asse B
                ret = Focas1.cnc_diagnoss(hndlDiagnostica, 308, 4, 5, DNG308_4);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ASSE B [DNG308#4], RET = " + ret.ToString(), MAC_id);
                }
            }

            if (MAC_numeroAssi > 4)
            {
                //Lettura temperatura Motore Asse C
                ret = Focas1.cnc_diagnoss(hndlDiagnostica, 308, 5, 5, DNG308_5);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ASSE C [DNG308#5], RET = " + ret.ToString(), MAC_id);
                }
            }
            ///-----/////

            //Lettura temperatura Encoder Assi X
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 309, 1, 5, DNG309_1);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ENC. X [DNG309#1], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura temperatura Encoder Assi Y
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 309, 2, 5, DNG309_2);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ENC. Y [DNG309#2], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura temperatura Encoder Assi Z
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 309, 3, 5, DNG309_3);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ENC. Z [DNG309#3], RET = " + ret.ToString(), MAC_id);
            }

            if (MAC_numeroAssi > 3)
            {
                //Lettura temperatura Encoder Assi B
                ret = Focas1.cnc_diagnoss(hndlDiagnostica, 309, 4, 5, DNG309_4);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ENC. B [DNG309#4], RET = " + ret.ToString(), MAC_id);
                }
            }

            if (MAC_numeroAssi > 4)
            {
                //Lettura temperatura Encoder Assi C
                ret = Focas1.cnc_diagnoss(hndlDiagnostica, 309, 5, 5, DNG309_5);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA TEMPERATURA ENC. C [DNG309#5], RET = " + ret.ToString(), MAC_id);
                }
            }
            ////-------///////

            //Lettura sforzo Mandrino
            ret = Focas1.cnc_diagnoss(hndlDiagnostica, 410, 1, 6, DNG410);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA SFORZO MANDRINO [DNG410], RET = " + ret.ToString(), MAC_id);
            }

            //Lettura sforzo Assi X - Y - Z - B - C
            short num = Convert.ToInt16(MAC_numeroAssi);
            ret = Focas1.cnc_rdsvmeter(hndlDiagnostica, ref num, LOAD);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA SFORZO ASSI, RET = " + ret.ToString(), MAC_id);
            }

            //////-----////////

            //Velocita madrino
            ret = Focas1.cnc_acts(hndlDiagnostica, SPDSPEED);
            if (ret != 0)
            {
                Logs.LogDB(connection_Diagnosi, "ERRORE LETTURA VELOCITA MANDRINO, RET = " + ret.ToString(), MAC_id);
            }

            //Letman();

        }
        #endregion

        #region controllo Produzione
        public void gestioneProduzioneMacchineUtComune()
        {
            if (hndlProduzione == 0 && connesso)
            {
                //ret = Focas1.cnc_allclibhndl2(9, out hndlProduzione);
                ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlProduzione);
                if (ret != Focas1.EW_OK)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE GENERAZIONE HANDLE, RET = " + ret.ToString(), MAC_id);
                    Focas1.cnc_freelibhndl(hndlProduzione);
                    hndlProduzione = 0;
                }
                else
                    handle = true;
            }

            if (connesso && hndlProduzione!=0)
            {
                int rows_countIn = 0;
                int rows_countProd = 0;

                #region Lettura programmaAttivo
                programmaAttivo = 0;
                // Programma 1 - 7
                ret = Focas1.pmc_rdpmcrng(hndlProduzione, 5, 0, 970, 970, 9, R970);
                if (ret == Focas1.EW_OK)
                {
                    switch (Convert.ToInt32(R970.ldata[0]))
                    {
                        //-- Programma 7 
                        case 128:
                            programmaAttivo = 7;
                            break;
                        case 129:
                            programmaAttivo = 7;
                            break;
                        //-- Programma 6
                        case 65:
                            programmaAttivo = 6;
                            break;
                        case 64:
                            programmaAttivo = 6;
                            break;
                        //-- Programma 5
                        case 33:
                            programmaAttivo = 5;
                            break;
                        case 32:
                            programmaAttivo = 5;
                            break;
                        //-- Programma 4
                        case 17:
                            programmaAttivo = 4;
                            break;
                        case 16:
                            programmaAttivo = 4;
                            break;
                        //-- Programma 3
                        case 9:
                            programmaAttivo = 3;
                            break;
                        case 8:
                            programmaAttivo = 3;
                            break;
                        //-- Programma 2
                        case 5:
                            programmaAttivo = 2;
                            break;
                        case 4:
                            programmaAttivo = 2;
                            break;
                        //-- Programma 1
                        case 3:
                            programmaAttivo = 1;
                            break;
                        case 2:
                            programmaAttivo = 1;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO R970, RET = " + ret.ToString(), MAC_id);
                }
                // Programma 8 - 10
                ret = Focas1.pmc_rdpmcrng(hndlProduzione, 5, 0, 971, 971, 9, R971);
                if (ret == Focas1.EW_OK)
                {
                    switch (Convert.ToInt32(R971.ldata[0]))
                    {
                        //-- Programma 10 
                        case 4:
                            programmaAttivo = 10;
                            break;
                        //-- Programma 9
                        case 2:
                            programmaAttivo = 9;
                            break;
                        //-- Programma 8
                        case 1:
                            programmaAttivo = 8;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO R970, RET = " + ret.ToString(), MAC_id);
                }
                #endregion
                ret = 0;
                //prova per vedere se tutta l'interfaccia ha la commessa
                //StatoLavoriComune();

                //lettura tabella produzione
                CommesseMacchina();

                calcolacomm();

                ModificaInterfaccia();
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        string sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezzifatti = '" + pezziFatti + "' " +
                                   "WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                }
                catch (Exception e)
                {
                    Logs.LogDB(connection_Produzione, "errore aggiornamento pezzi e commessa produzione", MAC_id);
                }

                //scaricaDati();
            }

            if (!connesso && hndlProduzione != 0)
            {
                Focas1.cnc_freelibhndl(hndlProduzione);
                hndlProduzione = 0;
                handle = false;
            }
        }

        //--- Lettura tabella di produzione    
        public void CommesseMacchina()
        {        
            ushort inizio;
            ushort fine;
            switch (MAC_tipoControllo)
            {
                case ("31iB5_old"):
                    inizio = 5500;
                    fine = 5699;
                    break;
                default:
                    inizio = 2500;
                    fine = 2699;
                    break;
            }
            ret = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, inizio, fine, 208, buff_l);
            if (ret == Focas1.EW_OK)
            {
                //ToDo aggiunta lettura fase, articolo e codice UNIQUE della commessa attiva (2780-2816)
                //lettura fase
                conf = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, 2740, 2780, 208, fase_from_mac);
                if (conf == Focas1.EW_OK)
                {
                    for (int i = 0; i < 10; i++)
                        fase_list[i] = fase_from_mac.ldata[i];
                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE LETTURA TABELLA PRODUZIONE-Fase, RET = " + ret.ToString(), MAC_id);
                }

                //lettura articolo
                conf = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, 2940, 2980, 208, art_from_mac);
                if (conf == Focas1.EW_OK)
                {
                    for (int i = 0; i < 10; i++)
                        articolo_list[i] = art_from_mac.ldata[i];

                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE LETTURA TABELLA PRODUZIONE-Articolo, RET = " + ret.ToString(), MAC_id);
                }

                //lettura cprod_IDs 
                conf = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, 2780, 2820, 208, cpids_from_mac);
                if (conf == Focas1.EW_OK)
                {
                    for (int i = 0; i < 10; i++)
                        cpid_list[i] = cpids_from_mac.ldata[i];
                }
                else
                {
                    Logs.LogDB(connection_Produzione, "ERRORE LETTURA TABELLA PRODUZIONE-Id, RET = " + ret.ToString(), MAC_id);
                }

                for (int qq = 0; qq < 50; qq++)
                    tabProduzione[qq] = buff_l.ldata[qq];
            }

            else
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA TABELLA PRODUZIONE, RET = " + ret.ToString(), MAC_id);
            }
        }

        public void calcolacomm()
        {
            //leggere il codice unique (cpid_attivo) 

            switch (programmaAttivo)
            {
                case 1:
                    commessaAttiva = tabProduzione[1].ToString();
                    pezziFatti = tabProduzione[3];
                    break;

                case 2:
                    commessaAttiva = tabProduzione[6].ToString();
                    pezziFatti = tabProduzione[8];
                    break;

                case 3:
                    commessaAttiva = tabProduzione[11].ToString();
                    pezziFatti = tabProduzione[13];
                    break;

                case 4:
                    commessaAttiva = tabProduzione[16].ToString();
                    pezziFatti = tabProduzione[18];
                    break;

                case 5:
                    commessaAttiva = tabProduzione[21].ToString();
                    pezziFatti = tabProduzione[23];
                    break;

                case 6:
                    commessaAttiva = tabProduzione[26].ToString();
                    pezziFatti = tabProduzione[28];
                    break;

                case 7:
                    commessaAttiva = tabProduzione[31].ToString();
                    pezziFatti = tabProduzione[33];
                    break;

                case 8:
                    commessaAttiva = tabProduzione[36].ToString();
                    pezziFatti = tabProduzione[38];
                    break;

                case 9:
                    commessaAttiva = tabProduzione[41].ToString();
                    pezziFatti = tabProduzione[43];
                    break;

                case 10:
                    commessaAttiva = tabProduzione[46].ToString();
                    pezziFatti = tabProduzione[48];
                    break;

                default:
                    break;
            }
        }


        #region calcoloTempi 
        public void calcoloTempi(ushort indicestart, ushort indicestop)
        {
            int tmpStatus = 0;
            string PROD_appTempoLavoro = null;
            string PROD_appTempoPiazzamento = null;
            string PROD_appTempoPausa = null;
            string PROD_efficienza = null;
            
            int secLavoro = 0;
            int secPiazzamento = 0;
            int secPausa = 0;
            int secLavoroDB = 0;
            int secPiazzamentoDB = 0;
            int secPausaDB = 0;

            int EFF_Totale = 0;
            int EFF_Lavoro = 0;

            
            string sqlQuery = "";
            SqlCommand command = null;
            SqlDataReader reader = null;
            //Aggiorno tempi
            // Leggo lo stato della commessa attiva

            StatoLavoriComune();
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //Leggo status commessa attiva
                    sqlQuery = "SELECT (CPROD_status) FROM Cicloproduzione WHERE CPROD_commessa = '" + commessaAttiva + "' AND CPROD_vs_MAC_id= "+MAC_id+" order by cprod_id desc";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    var st =command.ExecuteScalar();
                    if (st != null)
                    {
                        tmpStatus = Convert.ToInt32(st);
                    }
                    //reader = command.ExecuteReader();
                    //if (reader.HasRows)
                    //{
                    //    while (reader.Read())
                    //    {
                    //        tmpStatus = (int)(byte)reader[0];
                    //    }
                    //    reader.Close();
                    //}
                    //reader.Close();
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }


            // leggo i tempi dalla macchina
            ret = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, indicestart, indicestop, 208, buff_l);
            if (ret == Focas1.EW_OK)
            {
                for (int qq = 0; qq < 6; qq++)
                    tmpTempiLetti[qq] = buff_l.ldata[qq];
            }
            else
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO D5252, RET = " + ret.ToString(), MAC_id);
            }

            if (tmpStatus >= 3)
            {
                if (tmpStatus == 4)
                {
                    try
                    {
                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                        {
                            sqlQuery = "SELECT PROD_appTempoLavoro, PROD_appTempoPiazzamento, PROD_appTempoPausa " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = '" + commessaAttiva + "' order by CPROD_id desc";
                            command = new SqlCommand(sqlQuery, connection_Produzione);
                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                            reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(0))
                                    {
                                        PROD_appTempoLavoro = reader.GetString(0);
                                        secLavoroDB = Convert.ToInt32(PROD_appTempoLavoro);
                                    }
                                    else secLavoroDB = 0;
                                    if (!reader.IsDBNull(1))
                                    {
                                        PROD_appTempoPiazzamento = reader.GetString(1);
                                        secPiazzamentoDB = Convert.ToInt32(PROD_appTempoPiazzamento);
                                    }
                                    else secPiazzamentoDB = 0;
                                    if (!reader.IsDBNull(2))
                                    {
                                        PROD_appTempoPausa = reader.GetString(2);
                                        secPausaDB = Convert.ToInt32(PROD_appTempoPausa);
                                    }
                                    else secPausaDB = 0;
                                }
                                reader.Close();
                            }
                            reader.Close();
                            command.Dispose();
                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
                secPiazzamento = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[0], tmpTempiLetti[1])+ secPiazzamentoDB;
                secLavoro = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[2], tmpTempiLetti[3])+ secLavoroDB;
                secPausa = TimeHelper.calcolaSecondiDaRegistro(tmpTempiLetti[4], tmpTempiLetti[5])+ secPausaDB;
                EFF_Totale = secLavoro + secPausa + secPiazzamento;
                EFF_Lavoro = secLavoro;
                if (EFF_Totale != 0)
                    efficienza = (EFF_Lavoro * 100) / EFF_Totale;
                PROD_efficienza = efficienza.ToString();
            }

            // aggiorno i tempi su DB
            
            using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    sqlQuery = "if (ISNULL((SELECT distinct(PROD_tempoLavoro) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa =" + DbHelper.cleanNullValue(commessaAttiva) + "),0)<=" + DbHelper.cleanNullValue(secLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPiazzamento) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazzamento) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPausa) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                            "UPDATE Produzione SET PROD_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + ", PROD_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazzamento) + ",  PROD_tempoPausa= " + DbHelper.cleanNullValue(secPausa) + ", PROD_efficienza= " + DbHelper.cleanNullValue(efficienza) +
                           " FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + " ";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }
                //i pezzi fatti
                //Leggo status commessa attiva
                

                //if (tmpStatus==4)
                //{
                //    sqlQuery = "UPDATE Produzione " +
                //            "SET PROD_appTempoLavoro = PROD_tempoLavoro,  PROD_appTempoPiazzamento = PROD_tempoPiazzamento,  " +
                //            "PROD_appTempoPausa = PROD_tempoPausa " +
                //            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id  WHERE CPROD_commessa = '" + commessaAttiva + "'";
                //    command = new SqlCommand(sqlQuery, connection_Produzione);
                //    command.ExecuteNonQuery();
                //    command.Dispose();
                //}
                //if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
            }

            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //Aggiorno anche tabella output
                    sqlQuery = "if (ISNULL((SELECT distinct(OUT_tempoLavoro) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPiazzamento) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazzamento) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPausa) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                        "UPDATE Output SET OUT_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + " , OUT_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazzamento) + " " +
                    ", OUT_tempoPausa = " + DbHelper.cleanNullValue(secPausa) + " , OUT_efficienza = " + DbHelper.cleanNullValue(efficienza) + " FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva);
                    SqlCommand comando = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    int N = comando.ExecuteNonQuery();
                    sqlQuery = "UPDATE Cicloproduzione SET CPROD_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + " WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva);
                    comando = new SqlCommand(sqlQuery, connection_Produzione);
                    N = comando.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();


                    comando.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
            }

            int originale = 0;
            int rispostaAttivazione = 0;
            //in macchina azzera tempi e attiva commessa
            if (reqAttivazione)
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        sqlQuery = "UPDATE Produzione " +
                                "SET PROD_appTempoLavoro = PROD_tempoLavoro,  PROD_appTempoPiazzamento = PROD_tempoPiazzamento,  " +
                                "PROD_appTempoPausa = PROD_tempoPausa " +
                                "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id  WHERE CPROD_id = '" + commessaAttiva + "'";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();

                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }

                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    try
                    {
                        sqlQuery = "UPDATE Output " +
                                "SET OUT_TempoLavoro = PROD_tempoLavoro,  OUT_TempoPiazzamento = PROD_tempoPiazzamento,  " +
                                "OUT_TempoPausa = PROD_tempoPausa " +
                                "FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id=CPROD_id INNER JOIN Produzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        command.ExecuteNonQuery();
                        command.Dispose();

                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                    }
                }
                originale = R972.ldata[0] - 64;
                rispostaAttivazione = originale + 128;

                //Azzero i tempi su CNC
                short ret = 0;
                buff.ldata = new int[50];
                buff.type_a = 9;
                buff.type_d = 2;
                buff.datano_s = (short)indicestart;
                buff.datano_e = (short)indicestop;
                ushort length = 32;

                for (int bb = 0; bb < 6; bb++)
                    buff.ldata[bb] = 0;

                ret = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE AZZERAMENTO TEMPI, RET = " + ret.ToString(), MAC_id);
                }
                
                buff.type_a = 5;
                buff.type_d = 0;
                buff.ldata = new int[50];
                buff.datano_s = 972;
                buff.datano_e = 972;
                buff.ldata[0] = rispostaAttivazione;


                ret = Focas1.pmc_wrpmcrng(hndlProduzione, 9, buff);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE RISPOSTA PER ATTIVAZIONE COMMESSA, RET = " + ret.ToString(), MAC_id);
                }
                reqAttivazione = false;
                //calcolacomm();
            }
            ret = 0;
        }
    
        public void StatoLavoriComune()
        {
            boolCommessaFinita = false;
            //Attivazione commessa da CN
            ret = Focas1.pmc_rdpmcrng(hndlProduzione, 5, 0, 972, 972, 9, R972);
            if (ret == Focas1.EW_OK)
            {
                if (R972.ldata[0] > 63 && R972.ldata[0] < 128)
                {
                    switch (programmaAttivo)
                    {
                        case 1:
                            if (tabProduzione[3] >= tabProduzione[2])
                            {
                                commessaAttiva = tabProduzione[1].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 2:
                            if (tabProduzione[8] >= tabProduzione[7])
                            {
                                commessaAttiva = tabProduzione[6].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 3:
                            if (tabProduzione[13] >= tabProduzione[12])
                            {
                                commessaAttiva = tabProduzione[11].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 4:
                            if (tabProduzione[18] >= tabProduzione[17])
                            {
                                commessaAttiva = tabProduzione[16].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 5:
                            if (tabProduzione[23] >= tabProduzione[22])
                            {
                                commessaAttiva = tabProduzione[21].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 6:
                            if (tabProduzione[28] >= tabProduzione[27])
                            {
                                commessaAttiva = tabProduzione[26].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 7:
                            if (tabProduzione[33] >= tabProduzione[32])
                            {
                                commessaAttiva = tabProduzione[31].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 8:
                            if (tabProduzione[38] >= tabProduzione[37])
                            {
                                commessaAttiva = tabProduzione[36].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 9:
                            if (tabProduzione[43] >= tabProduzione[42])
                            {
                                commessaAttiva = tabProduzione[41].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        case 10:
                            if (tabProduzione[48] >= tabProduzione[47])
                            {
                                commessaAttiva = tabProduzione[46].ToString();
                                boolCommessaFinita = true;
                            }
                            reqAttivazione = true;
                            break;
                        default:
                            reqAttivazione = true;
                            break;
                    }
                    //Console.WriteLine("La commessa attiva è " + commessaAttiva);
                }
                else if (R972.ldata[0] > 191 && R972.ldata[0] < 256)
                {
                    reqAttivazione = true;
                    switch (programmaAttivo)
                    {
                        case 1:
                            if (tabProduzione[3] >= tabProduzione[2])
                            {
                                commessaAttiva = tabProduzione[1].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 2:
                            if (tabProduzione[8] >= tabProduzione[7])
                            {
                                commessaAttiva = tabProduzione[6].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 3:
                            if (tabProduzione[13] >= tabProduzione[12])
                            {
                                commessaAttiva = tabProduzione[11].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 4:
                            if (tabProduzione[18] >= tabProduzione[17])
                            {
                                commessaAttiva = tabProduzione[16].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 5:
                            if (tabProduzione[23] >= tabProduzione[22])
                            {
                                commessaAttiva = tabProduzione[21].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 6:
                            if (tabProduzione[28] >= tabProduzione[27])
                            {
                                commessaAttiva = tabProduzione[26].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 7:
                            if (tabProduzione[33] >= tabProduzione[32])
                            {
                                commessaAttiva = tabProduzione[31].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 8:
                            if (tabProduzione[38] >= tabProduzione[37])
                            {
                                commessaAttiva = tabProduzione[36].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 9:
                            if (tabProduzione[43] >= tabProduzione[42])
                            {
                                commessaAttiva = tabProduzione[41].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        case 10:
                            if (tabProduzione[48] >= tabProduzione[47])
                            {
                                commessaAttiva = tabProduzione[46].ToString();
                                boolCommessaFinita = true;
                                reqAttivazione = true;
                            }
                            break;
                        default:
                            reqAttivazione = true;
                            break;
                    }
                    //Console.WriteLine("La commessa attiva è " + commessaAttiva);

                }
            }
            else
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO R970, RET = " + ret.ToString(), MAC_id);
            }
            string sqlQuery;
            if (boolCommessaFinita && reqAttivazione && programmaAttivo != 0)
            {
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        boolCommessaFinita = false;
                        sqlQuery = "UPDATE Cicloproduzione SET CPROD_status = '5' WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = command.ExecuteNonQuery();
                        command.Dispose();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
            else if (reqAttivazione && programmaAttivo != 0)
            {
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        sqlQuery = "UPDATE Cicloproduzione " +
                            "SET CPROD_status = '4'" +
                            "WHERE CPROD_commessa = '" + commessaAttiva + "'";
                        SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        int N = command.ExecuteNonQuery();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
        }
        #endregion

        #region Lettura richiesta aggiunta commessa
        public void aggiungiCommessa(ushort indicestart, ushort indicestop) {
            //Richiesta aggiunta commessa
                
            bool reqAggiunta = false;
            string nuovaCommessa = null;
            string nuovaLotto = null;
            string nuovaPriorita = null;
            string nuovaFase = null;
            string nuovaArticolo = null;
            int nuovoid = 0;


            int AGG_originale = 0;
            int AGG_risposta = 0;
            string sqlQuery="";

            ret = Focas1.pmc_rdpmcrng(hndlProduzione, 5, 0, 972, 972, 9, R972);
            if (ret == Focas1.EW_OK)
            {
                if (!(R972.ldata[0] % 2 == 0))
                {
                    reqAggiunta = true;

                    //--- commessa - pezzi - priorita da aggiungere                
                    ret = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, indicestart, indicestop, 208, buff_l);
                    if (ret == Focas1.EW_OK)
                    {
                        nuovaCommessa = buff_l.ldata[0].ToString();
                        nuovaLotto = buff_l.ldata[1].ToString();
                        nuovaPriorita = buff_l.ldata[2].ToString();

                        //lettura FASE
                        conf= Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, 2236, 2240, 208, fase_from_mac);
                        if (conf == Focas1.EW_OK)
                        {
                            nuovaFase = fase_from_mac.ldata[0].ToString();
                        }
                        else
                        {
                            nuovaFase = "";
                            Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO 2236, RET = " + ret.ToString(), MAC_id);
                        }

                        //lettura ARTICOLO  
                        conf = Focas1.pmc_rdpmcrng(hndlProduzione, 9, 2, 2280, 2284, 208, art_from_mac);
                        if (conf == Focas1.EW_OK)
                        {
                            nuovaArticolo = art_from_mac.ldata[0].ToString();
                        }
                        else
                        {
                            nuovaArticolo = "";
                            Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO 2280, RET = " + ret.ToString(), MAC_id);
                        }

                        //Carico i valori nuova commessa - lotto - priorità - ricetta da CNC a Tabella Input Database 
                        carica_da_macchina(nuovaCommessa, nuovaLotto, nuovaPriorita, importFrom: "CNC", fase:nuovaFase, codiceArticolo:nuovaArticolo);
                    }
                    else
                    {
                        Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO "+indicestart+", RET = " + ret.ToString(), MAC_id);
                    }
                }
            }
            else
            {
                Logs.LogDB(connection_Produzione, "ERRORE LETTURA PARAMETRO R970, RET = " + ret.ToString(), MAC_id);
            }

            if (reqAggiunta)
            {
                AGG_originale = R972.ldata[0] - 1;
                AGG_risposta = AGG_originale + 2;

                buff.type_a = 5;
                buff.type_d = 0;
                ushort length = 9;
                buff.ldata = new int[50];
                buff.datano_s = 972;
                buff.datano_e = 972;
                buff.ldata[0] = AGG_risposta;

                ret = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
                if (ret != 0)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE RISPOSTA A CNC PER AGGIUNTA COMMESSA, RET = " + ret.ToString(), MAC_id);
                }
            }
        }
        #endregion

        #region SpostaCommesse
        public void spostaCommesse()
        {
            if (contaCommesseAttive(1) > 0)
            {
                //scrive lavori in macchina
                scriviLavori();
            }
            
            //--- Aggiorno tabella di Output (ToDo solo per commessa attiva?)
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    //--- JOIN TRA TABELLA Produzione e Produzione_Out ---//
                    string sqlQuery = "UPDATE a SET a.OUT_tempoLavoro = b.PROD_tempoLavoro, " +
                        "a.OUT_tempoPiazzamento = b.PROD_tempoPiazzamento, a.OUT_tempoPausa = b.PROD_tempoPausa, a.OUT_efficienza = b.PROD_efficienza " +
                            "FROM Output AS a INNER JOIN Produzione AS b ON a.OUT_vs_CPROD_id = b.PROD_vs_CPROD_id ";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
            
        }
        #endregion

        #region scrivi JOB
        public void aggiornaStatoLavori(short  startIndex , short stopIndex, int  indextt , int status, short fase_code=0, short articolo_code=0, short id_code=0 ) 
        {
            //Marco record in Produzione come inviato a CNC
            //Status = 2 --> Record scritto su CNC
            
            buff.ldata = new int[50];
            buff.type_a = 9;
            buff.type_d = 2;

            buff.datano_s = startIndex;
            buff.datano_e = stopIndex;
            
            ushort length = 28; 
            for (int aa = 1; aa < 6; aa++)
                buff.ldata[aa - 1] = Convert.ToInt32(tmpCommesse[indextt, aa]);

            ret = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
            if (ret != 0)
            {
                Logs.LogDB(connection_Produzione, "ERRORE " + indextt + " RIGA TAB. PROD. CNC, RET = " + ret.ToString(), MAC_id);
            }
            else {
                length=12;
                //ToDo aggiunta fase+articolo+cpid in macchina *Lorenzo*//
                if (fase_code != 0)
                {
                    buff.ldata = new int[50];
                    buff.type_a = 9;
                    buff.type_d = 2;
                    buff.datano_s = fase_code;
                    buff.datano_e =(short) (fase_code+3);
                    buff.ldata[0] = Convert.ToInt32(tmpFase[indextt]);
                }
                conf = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
                if (conf != Focas1.EW_OK)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE SCRITTURA TABELLA PRODUZIONE-Fase, RET = " + ret.ToString(), MAC_id);
                }


                if (articolo_code != 0)
                {
                    buff.ldata = new int[50];
                    buff.type_a = 9;
                    buff.type_d = 2;
                    buff.datano_s = articolo_code;
                    buff.datano_e = (short)(articolo_code + 3);
                    buff.ldata[0] = Convert.ToInt32(tmpArticolo[indextt]);
                }
                conf = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
                if (conf != Focas1.EW_OK)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE SCRITTURA TABELLA PRODUZIONE-Articolo, RET = " + ret.ToString(), MAC_id);
                }

                if (id_code != 0)
                {
                    buff.ldata = new int[50];
                    buff.type_a = 9;
                    buff.type_d = 2;
                    buff.datano_s = id_code;
                    buff.datano_e = (short)(id_code + 3);
                    buff.ldata[0] = Convert.ToInt32(cp_ids[indextt]);

                }
                conf = Focas1.pmc_wrpmcrng(hndlProduzione, length, buff);
                if (conf != Focas1.EW_OK)
                {
                    Logs.LogDB(connection_Produzione, "ERRORE SCRITTURA TABELLA PRODUZIONE-Articolo, RET = " + ret.ToString(), MAC_id);
                }

                try
                {
                    int in_id = Convert.ToInt32(tmpCommesse[indextt, 0]);
                    using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                        SqlCommand command = new SqlCommand("UPDATE Input SET IN_status = '2' WHERE IN_id = '" + in_id + "'", connection_Produzione);
                        int m = command.ExecuteNonQuery();
                        
                        command.Dispose();

                        string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                                " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + in_id + "";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        m = command.ExecuteNonQuery();

                        //cicloproduzione status 2
                         command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '3' " +
                            "FROM Cicloproduzione INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE IN_id = '" + in_id + "' AND CPROD_vs_MAC_id= " + MAC_id, connection_Produzione);
                        m = command.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();

                        //Trasferisco da Produzione a Output
                        sqlQuery = "INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data, OUT_tempoLavoro, OUT_tempoPiazzamento,  OUT_tempoPausa, OUT_efficienza) " +
                                "SELECT PROD_vs_CPROD_id, PROD_priorita , GETDATE(), PROD_tempoLavoro, PROD_tempoPiazzamento, PROD_tempoPausa, PROD_efficienza " +
                                "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE CPROD_vs_MAC_id=" + MAC_id + " AND IN_id= "+in_id;
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        m = command.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                    }
                    
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message)+ ex.StackTrace, MAC_id);
                }
                tmpCommesse[indextt, 0] = null;
                //Console.WriteLine("inserite in macchina");
            }
        }
        
        public void scriviLavori()
        {
            int ww = 0;

            for (int ll = 0; ll < 10; ll++)
            {
                finite[ll] = null;
                vuote[ll] = null;
            }

            //Leggo tabella in macchina per contare Record finiti e vuoti
            for (int qq = 4; qq < 50; qq = qq + 5)
            {
                if (tabProduzione[qq] == 0)
                {
                    commesseVuote = true;
                    vuote[ww] = ww;
                }
                else if (tabProduzione[qq] < 0)
                {
                    commesseFinite = true;
                    finite[ww] = ww;
                }
                ww++;
            }
            //Rendo non disponibile la riga delle commessa attiva
            if (programmaAttivo != 0)
            {
                finite[programmaAttivo - 1] = null;
                vuote[programmaAttivo - 1] = null;
            }

            int rr = 0;

            //Pulisco tmpCommesse
            for (int i = 0; i < 10; i++)
            {
                for (int o = 0; o < 6; o++)
                    tmpCommesse[i, o] = null;
            }
            cp_ids = new List<int>();

            //Controllo se i Record inseriti in tabella Produzione sono maggiori di 10
            //Estraggo solo i primi 10 Record in Produzione CC
            try
            {
                using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command = new SqlCommand("SELECT TOP (10) IN_id, CPROD_programma, CPROD_commessa, CPROD_lotto, CPROD_pezziFatti, IN_priorita, CPROD_id, IN_fase, In_articolo " +
                        "FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id  " +
                        "WHERE CPROD_status = '1' AND CPROD_vs_MAC_id='"+MAC_id+"' ORDER BY IN_priorita ASC", connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (tmpCommesse[rr, 0] == null)
                            {
                                //id
                                if (!reader.IsDBNull(0))
                                    tmpCommesse[rr, 0] = (reader.GetInt32(0)).ToString();
                                else
                                    tmpCommesse[rr, 0] = "0";
                                //programma
                                if (!reader.IsDBNull(1))
                                    tmpCommesse[rr, 1] = reader.GetString(1);
                                else
                                    tmpCommesse[rr, 1] = "0";
                                //commessa
                                if (!reader.IsDBNull(2))
                                    tmpCommesse[rr, 2] = reader.GetString(2);
                                else
                                    tmpCommesse[rr, 2] = "0";
                                //lotto
                                if (!reader.IsDBNull(3))
                                    tmpCommesse[rr, 3] = reader.GetString(3);
                                else
                                    tmpCommesse[rr, 3] = "0";
                                //pezzifatti
                                if (!reader.IsDBNull(4))
                                    tmpCommesse[rr, 4] = (reader.GetInt32(4)).ToString();
                                else
                                    tmpCommesse[rr, 4] = "0";
                                //priorità
                                if (!reader.IsDBNull(5))
                                    tmpCommesse[rr, 5] = (reader.GetInt32(5)).ToString();
                                else
                                    tmpCommesse[rr, 5] = "0";
                                //cprod_id
                                if (!reader.IsDBNull(6))
                                    cp_ids.Add(reader.GetInt32(6));
                                /*else
                                    cp_ids.Add(0);*/
                                //fase
                                if (!reader.IsDBNull(7))
                                    tmpFase[rr] = Convert.ToInt32(reader.GetString(7));
                                else
                                    tmpFase[rr] = 0;
                                //articolo
                                if (!reader.IsDBNull(8))
                                    tmpArticolo[rr] = Convert.ToInt32(reader.GetString(8));
                                else
                                    tmpArticolo[rr] = 0;

                                rr++;
                            }
                        }
                    }
                    reader.Close();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }

            for (int yy = 0; yy < rr; yy++)
            {
                if (commesseVuote == true || commesseFinite == true)
                {
                    for (int uu = 0; uu < 10; uu++)
                    {
                        if (vuote[uu] != null || finite[uu] != null)
                        {
                            int? mycurrentCommessa = null;
                            if (vuote[uu] != null)
                            {
                                mycurrentCommessa = vuote[uu];
                            }
                            else {
                                mycurrentCommessa = finite[uu];
                            }
                            switch (MAC_tipoControllo)
                            {
                                case ("31iB5_old"):
                                    switch (mycurrentCommessa)
                                    {
                                        case (0):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5500, 5519, yy, 2);
                                            break;
                                        case (1):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5520, 5539, yy, 2);
                                            break;
                                        case (2):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5540, 5559, yy, 2);
                                            break;
                                        case (3):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5560, 5579, yy, 2);
                                            break;
                                        case (4):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5580, 5599, yy, 2);
                                            break;
                                        case (5):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5600, 5619, yy, 2);
                                            break;
                                        case (6):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5620, 5639, yy, 2);
                                            break;
                                        case (7):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5640, 5659, yy, 2);
                                            break;
                                        case (8):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5660, 5679, yy, 2);
                                            break;
                                        case (9):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(5680, 5699, yy, 2);
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                default:
                                    switch (mycurrentCommessa)
                                    {
                                        case (0):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2500, 2519, yy, 2, fase_code: 2740, articolo_code: 2940, id_code: 2780);
                                            break;
                                        case (1):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2520, 2539, yy, 2, fase_code: 2744, articolo_code: 2944, id_code: 2784);
                                            break;
                                        case (2):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2540, 2559, yy, 2, fase_code: 2748, articolo_code: 2948, id_code: 2788);
                                            break;
                                        case (3):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2560, 2579, yy, 2, fase_code: 2752, articolo_code: 2952, id_code: 2792);
                                            break;
                                        case (4):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2580, 2599, yy, 2, fase_code: 2756, articolo_code: 2956, id_code: 2796);
                                            break;
                                        case (5):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2600, 2619, yy, 2, fase_code: 2760, articolo_code: 2960, id_code: 2800);
                                            break;
                                        case (6):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2620, 2639, yy, 2, fase_code: 2764, articolo_code: 2964, id_code: 2804);
                                            break;
                                        case (7):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2640, 2659, yy, 2, fase_code: 2768, articolo_code: 2968, id_code: 2808);
                                            break;
                                        case (8):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2660, 2679, yy, 2, fase_code: 2772, articolo_code: 2972, id_code: 2812);
                                            break;
                                        case (9):
                                            //Passso info da locale a CNC
                                            aggiornaStatoLavori(2680, 2699, yy, 2, fase_code: 2776, articolo_code: 2976, id_code: 2816);
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                            }

                            //Ricontrollo su CNC la presenza di righe vuote
                            //Pulisco Array 
                            for (int pp = 0; pp < 10; pp++)
                            {
                                vuote[pp] = null;
                                finite[pp] = null;
                            }
                            ww = 0;

                            CommesseMacchina();

                            for (int qq = 4; qq < 50; qq = qq + 5)
                            {
                                if (tabProduzione[qq] == 0)
                                {
                                    commesseVuote = true;
                                    vuote[ww] = ww;
                                }
                                else if (tabProduzione[qq] < 0)
                                {
                                    commesseFinite = true;
                                    finite[ww] = ww;
                                }
                                ww++;
                            }
                            //Rendo non disponibile la riga delle commessa attiva
                            if (programmaAttivo != 0)
                            {
                                finite[programmaAttivo - 1] = null;
                                vuote[programmaAttivo - 1] = null;
                            }


                            break;
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region Files

        public override bool getPath()
        {
            bool to_filter = false;
            List<string> cartelle = new List<LPWSTR>();
            try
            {
                int ret = 0;
                Focas1.ODBPDFDRV Odb = new Focas1.ODBPDFDRV();
                string str = "";
                CncFileDir CncDir = new CncFileDir();
                CncFileDir CncRoot = new CncFileDir();
                CncDir.name = MAC_nomemacchina;
                ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlSocket);
                ret = Focas1.cnc_rdpdf_drive(hndlSocket, Odb);

                if (ret == Focas1.EW_OK)
                {
                    string[] supporti_disco = {Odb.drive1, Odb.drive2, Odb.drive3, Odb.drive4, Odb.drive5, Odb.drive6,
                                           Odb.drive7,Odb.drive8, Odb.drive9, Odb.drive10, Odb.drive11, Odb.drive12,
                                           Odb.drive13, Odb.drive14, Odb.drive15, Odb.drive16};
                    
                    for (int i = 0; i < Odb.max_num; i++)
                    {
                        str = supporti_disco[i];
                        
                        if (str != "")
                        {
                            CncRoot = new CncFileDir();
                            CncRoot.name = "//" + str + "/";
                            CncDir.cl.Add(Guid.NewGuid().ToString(), CncRoot);
                        }
                    }

                    JSON_path = "{";
                    JSON_path += root("CNC_MEM", "//CNC_MEM/");
                    foreach (CncFileDir CncRoot2 in CncDir.cl.Values)
                    {
                        CncRoot = CncRoot2;
                        //filtering system folder
                        if (MAC_tipoControllo == "0i-MD" || MAC_tipoControllo == "0i-TD")
                        {
                            to_filter = true;
                            CncRoot.name = "//CNC_MEM/USER/";
                        }
                        cartelle =ExploreCncFolder(CncRoot, to_filter);
                        CncRoot = null;
                    }
                    if (to_filter)
                    {
                        JSON_path = JSON_path.Remove(JSON_path.LastIndexOf(","), 1);
                    }
                    else
                    {
                        ReadFiles("//" + str + "/", cartelle, to_filter);
                        JSON_path += "]}";
                    }
                    

                    Focas1.cnc_freelibhndl(hndlSocket);
                    return true;
                }
                else
                {
                    Focas1.cnc_freelibhndl(hndlSocket);
                    return false;
                }


            }
            catch(Exception e)
            {
                Focas1.cnc_freelibhndl(hndlSocket);
                return false;
            }
        }

        //funzione ricorsiva
        public List<string> ExploreCncFolder(CncFileDir Parent_Renamed, bool to_filter)
        {
            int ret = 0;
            short Index = 0;
            string str = "";
            int NumFolder = 0;
            int TempNumFolder = 0;
            string tempRefParam = "";
            string st = "";
            List<string> cartelle = new List<string>();

            CncFileDir CncChild = null;
            Focas1.ODBPDFNFIL Odbpdf = new Focas1.ODBPDFNFIL();
            Focas1.IDBPDFSDIR Idb = new Focas1.IDBPDFSDIR();
            Focas1.ODBPDFSDIR Odb = new Focas1.ODBPDFSDIR();

            tempRefParam = Parent_Renamed.name;
            Console.WriteLine("cartella: "+tempRefParam);

            //mi leggo le dir dentro la cartella in cui mi trovo
            ret = Focas1.cnc_rdpdf_subdirn(hndlSocket, tempRefParam, Odbpdf);
            if (ret == Focas1.EW_OK)
            {
                //numero di cartelle
                NumFolder = Odbpdf.dir_num;
                st = "";
                if (NumFolder != 0)
                {
                    st = "";
                    do
                    {
                        List<string> sottocartelle = new List<string>();
                        Idb.req_num = Index;
                        // ci concateno  la stringa 0
                        Idb.path = Parent_Renamed.name + Strings.Chr(0).ToString();
                        //TempNumFolder = NumFolder;
                        short tempRefParam2 = 1;
                        //if (tempRefParam2 >= 8) tempRefParam2 = 7;

                        try
                        {
                            ret = Focas1.cnc_rdpdf_subdir(hndlSocket, ref tempRefParam2, Idb, Odb);
                        }
                        catch
                        {
                            Console.WriteLine("catch");
                        }

                        //TempNumFolder = tempRefParam2;

                        if (ret != Focas1.EW_OK) {
                            //aggiungo un log dell'errore
                            break;
                        }

                    

                        str = CutStr(Odb.d_f, Strings.Chr(0).ToString());
                        
                        //filtro cartelle di sistema su vecchio cn
                        if(to_filter){
                            if (Parent_Renamed.name == "//CNC_MEM/" && str!="USER")
                            {
                                str= "";
                            }
                            if (Parent_Renamed.name == "//CNC_MEM/USER/" && str != "PATH1")
                            {
                                str = "";
                            }
                        }

                        if (str != "")
                        {
                            CncChild = new CncFileDir();
                            st = ExtractRoot(Parent_Renamed.name);
                            st = Parent_Renamed.name + str + "/";
                            CncChild.name = st;
                            if (!to_filter|| st != "//CNC_MEM/USER/PATH1/")
                            {
                                JSON_path += dir(str, st + "/", to_filter);
                            }
                            
                            //indica se il figlio ha anche lui una cartella all'interno

                            if ((Odb.sub_exist & 1) != 0)
                            {
                                CncChild.SubFolder = true;
                                sottocartelle = ExploreCncFolder(CncChild, to_filter);
                            }
                            else
                            {
                                CncChild.SubFolder = false;
                            }
                            cartelle.Add(str);
                            Parent_Renamed.cl.Add(Guid.NewGuid().ToString(), CncChild);
                            ReadFiles(st + "/", sottocartelle, to_filter);
                            JSON_path += "]}, ";
                        }
                        Index++;
                        
                    }
                    while (Index <= (NumFolder - 1));
                    //funzione while.

                    Focas1.ODBPDFNFIL Odb_F = new Focas1.ODBPDFNFIL();
                    ret = Focas1.cnc_rdpdf_subdirn(hndlSocket, tempRefParam, Odb_F);
                    if (ret == Focas1.EW_OK && Odb_F.file_num == 0)
                        JSON_path = JSON_path.Remove(JSON_path.LastIndexOf(","), 1);
                    return cartelle;
                }
            }
            return new List<LPWSTR>();
        }

        public override bool uploadfile(string path, string content)
        {
            short ret;
            int numer = 0;

            //filtering system folder
            if (MAC_tipoControllo == "0i-MD" || MAC_tipoControllo == "0i-TD")
            {
                path = path.Replace("//CNC_MEM/", "//CNC_MEM/USER/PATH1/");
            }

            try
            {
                ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlSocket);
                if (path!="") {
                    //connessione con la macchina nel path (MAC_ipAdress)
                    //path = MAC_ipAdress + path.Substring(1, path.Length - 2);
                    path = "/" + path.Substring(1, path.Length - 2);
                    path = Strings.Replace(path, @"\", "/", 1, -1, CompareMethod.Binary);
                    //Console.WriteLine(path);
                    ret = Focas1.cnc_dwnstart4(hndlSocket, 0, path);
                    //Console.WriteLine("Start download. " + "ret= " + ret.ToString());
                    if (ret != Focas1.EW_OK)
                    {
                        Focas1.cnc_dwnend4(hndlSocket);
                        Logs.LogDB(connection_Fanuc, "Errore 11 " + ret.ToString(), MAC_id);
                        //Console.WriteLine("Errore 11 " + ret.ToString());
                        return false;
                    }
                    else
                    {
                        content = "\n"+content+"%";
                        //Logs.LogDB(new SqlConnection(), " testo:" + content, MAC_id);
                        //content = replaceCR(content);
                        int len = content.Length;
                        //Console.WriteLine(content);
                        while (len > 0)
                        {
                            Thread.Sleep(1);

                            if (len >= 1024) numer = 1024;
                            else numer = len;

                            ret = Focas1.cnc_download4(hndlSocket, ref numer, content);

                            if (ret == (short)Focas1.focas_ret.EW_BUFFER)
                            {
                                Thread.Sleep(100);
                                continue;
                            }
                            else if (ret == (short)Focas1.focas_ret.EW_RESET)
                            {
                                Logs.LogDB(new SqlConnection(), "Errore upload file, Errore 13A " + ret.ToString(), MAC_id);
                                //Console.WriteLine("Errore 13A " + ret.ToString());
                            }

                            if (ret == (short)Focas1.focas_ret.EW_OK)
                            {
                                content = content.Substring(numer);
                                //Logs.LogDB(new SqlConnection(), " testo in upload:" + content, MAC_id);
                                len = len - numer;
                            }
                            else
                            {
                                Logs.LogDB(new SqlConnection(), "Errore upload file, Errore download4 "+path+"\n" + ret.ToString(), MAC_id);
                                Focas1.cnc_freelibhndl(hndlSocket);
                                return false;
                            }
                        }
                        
                        ret = Focas1.cnc_dwnend4(hndlSocket);
                        if (ret == (short)Focas1.focas_ret.EW_OK)
                        {
                            //Console.WriteLine("OK, ret= " + ret.ToString());

                            Focas1.cnc_freelibhndl(hndlSocket);
                            return true;
                        }
                        else
                        {
                            Focas1.ODBERR error = new Focas1.ODBERR();
                            short ret2 = Focas1.cnc_getdtailerr(hndlSocket, error);
                            if (error.err_no == 4)
                            {
                                Logs.LogDB(new SqlConnection(), "Errore 14\nRet = " + ret.ToString() + "\nError = " + error.err_no + "\nNUMERO PROGRAMMA GIA' ESISTENTE\npath: "+path, MAC_id);
                                //Console.WriteLine("Errore 14\nRet= " + ret.ToString() + "\nError= " + error.err_no + "\nNUMERO PROGRAMMA GIA' ESISTENTE");
                            }
                            else
                            {
                                Logs.LogDB(new SqlConnection(), "Errore 14\nRet = " + ret.ToString() + "\nError = " + error.err_no +" testo:"+ content, MAC_id);
                                //Console.WriteLine("Errore 14\nRet= " + ret.ToString() + "\nError= " + error.err_no);
                            }

                            return false;
                        }

                    }
                }
                else
                {
                    Focas1.cnc_freelibhndl(hndlSocket);
                    Logs.LogDB(new SqlConnection(), "Errore upload file \nPath vuoto ", MAC_id);
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }

        }

        //legge i files all'interno della cartella indicata
        private void ReadFiles(string fpath, List<string> cartelle, bool to_filter)
        {
            int Num = 0;
            int NumProg = 0;
            int tnum = 0;
            //foreach(string cartella in cartelle)
            //{
            //    Console.WriteLine(cartella);
            //}
            //Console.WriteLine(fpath);
            //Console.WriteLine("----------");
            string CncPath = "";

            Focas1.IDBPDFADIR Idb = new Focas1.IDBPDFADIR();
            Focas1.ODBPDFNFIL Odb = new Focas1.ODBPDFNFIL();
            Focas1.ODBPDFADIR odbf = new Focas1.ODBPDFADIR();
            fpath = "/" + fpath.Substring(1, fpath.Length - 2);
            CncPath = Strings.Replace(fpath, "\\", "/", 1, -1, CompareMethod.Binary);
            //+ "/" + Strings.Chr(0).ToString();
            string tempRefParam = CutStr(CncPath, Strings.Chr(0).ToString());
            int ret = Focas1.cnc_rdpdf_subdirn(hndlSocket, tempRefParam, Odb);
            if (ret != Focas1.EW_OK) return;

            NumProg = Odb.file_num + Odb.dir_num;
            Num = Odb.dir_num;

            tnum = 1;
            Idb.path = CncPath;
            Idb.size_kind = 1;
            Idb.type = 1;

            while (Num < NumProg)
            {
                Idb.req_num = (short)Num;
                short tempRefParam2 = (short)tnum;
                ret = Focas1.cnc_rdpdf_alldir(hndlSocket, ref tempRefParam2, Idb, odbf);
                tnum = tempRefParam2;
                if (ret != Focas1.EW_OK)
                {
                    //Console.WriteLine("cnc_rdpdf_alldir Error n. " + ret.ToString());
                    Logs.LogDB(new SqlConnection(), "cnc_rdpdf_alldir Error n. " + ret.ToString(), MAC_id);
                    return;
                }

                Num++;

                string nome = CutStr(odbf.d_f, Strings.Chr(0).ToString()) + "";
                if (!cartelle.Contains(nome))
                {
                    string dimensione = odbf.size.ToString();
                    string data_ora = odbf.hour.ToString() + ":" + odbf.min.ToString() + ":" + odbf.sec.ToString() + " " + odbf.day.ToString() + "/" + odbf.mon.ToString() + "/" + odbf.year.ToString();
                    string attributi = odbf.attr.ToString();
                    string[] riga = { nome, dimensione, data_ora, attributi };

                    //aggiungi lista files (sotto forma di lista di "oggetti file JSON" )
                    JSON_path += file(nome, CncPath+nome, to_filter);
                }
            }
            Focas1.ODBPDFNFIL Odb_F = new Focas1.ODBPDFNFIL();
            ret = Focas1.cnc_rdpdf_subdirn(hndlSocket, tempRefParam, Odb_F);
            if (ret == Focas1.EW_OK && Odb_F.file_num != 0)
                JSON_path = JSON_path.Remove(JSON_path.LastIndexOf(","), 1);
            //JSON_path = JSON_path.Remove(JSON_path.LastIndexOf(","), 1);
        }

        #region genera cartella
        public string root(string name, string path)
        {
            return "'name': '"+name+ "', 'type': 'folder', 'key': '" + path + "', 'color': 'transparent', 'tag': 'org', 'icon': 'folder', 'root': true, 'children': [";
        }
        public string dir(string name, string path, bool to_filter)
        {
            if (to_filter) path = path.Replace("USER/PATH1/","");
            return "{'name': '"+name+"', 'type': 'folder', 'key': '"+path+"', 'color': 'transparent', 'tag': 'org', 'icon': 'folder', 'children': [";
        }
        public string file(string name, string path, bool to_filter)
        {
            if (to_filter) path = path.Replace("USER/PATH1/", "");
            return "{'name': '" + name + "', 'type': 'file', 'key': '" + path + "', 'color': '#FFFFFF', 'tag': 'org', 'icon': 'description'}, ";
        }
        #endregion

        //downloadfile
        public override string DownloadFile(string path)
        {
            short ret;
            int numer = 0;
            string content = "";
            int BUFFER_SIZE = 1048576;
            Focas1.ODBERR error = new Focas1.ODBERR();

            if (MAC_tipoControllo == "0i-MD" || MAC_tipoControllo == "0i-TD")
            {
                path = path.Replace("//CNC_MEM/", "//CNC_MEM/USER/PATH1/");
            }

            try
            {
                ret = Focas1.cnc_allclibhndl3(MAC_ipAdress, 8193, 9, out hndlSocket);
                if (path != "")
                {
                    string prg_name1 = "";
                    prg_name1 = Strings.Replace(path, MAC_nomemacchina, "/", 1, -1, CompareMethod.Binary);
                    //Console.WriteLine("Nome Programma Orig = {0}\nNome Programma = {1}\nHandle = {2}"
                    //    , prg_name, prg_name1, hndl);
                    char[] buffer = new char[BUFFER_SIZE + 1];
                    string buf;
                    int len;

                    ret = Focas1.cnc_upstart4(hndlSocket, 0, prg_name1);

                    if (ret != Focas1.EW_OK)
                    {
                        Focas1.cnc_upend4(hndl);
                        //Console.WriteLine(prg_name1);
                        //Console.WriteLine("Errore 9\nRet= " + ret.ToString());
                        Logs.LogDB(new SqlConnection(), "Errore dowload file \nProgramma "+prg_name1+ " \nErrore 9 \nRet= " + ret.ToString(), MAC_id);
                        return "";
                    }

                    do
                    {
                        len = BUFFER_SIZE;
                        ret = Focas1.cnc_upload4(hndlSocket, ref len, buffer);
                        //Console.WriteLine("ret " + ret.ToString());
                        if (ret == (short)Focas1.focas_ret.EW_BUFFER)
                        {
                            Thread.Sleep(Convert.ToInt32(ConfigurationSettings.AppSettings["sleep_fanuc_program"]));
                            continue;
                        }

                        if (ret == Focas1.EW_OK)
                        {
                            buffer[len] = '\0';
                            buf = new string(buffer);
                            //rtbProgram.Text += buf;
                            content += buf.TrimStart(new Char[] { '%', '\n', '\r' });
                            content= content.TrimEnd(new Char[] { '\u0000' });
                            //Console.WriteLine(content);
                        }
                        if (content.Contains('%'))
                        {
                            content = CutStr(content, "%");
                            break;
                        }
                        
                    }
                    while ((ret == Focas1.EW_OK) || (ret == (short)Focas1.focas_ret.EW_BUFFER));

                    ret = Focas1.cnc_upend4(hndlSocket);
                    if (ret == Focas1.EW_OK)
                    {
                        //Console.WriteLine("OK");
                    }else {
                        Logs.LogDB(new SqlConnection(), "Errore dowload file \nErrore 10 \nRet= " + ret.ToString(), MAC_id);

                        Focas1.cnc_freelibhndl(hndlSocket);
                        //Console.WriteLine("Errore 10" + ret.ToString());
                        return "";
                    }

                    Focas1.cnc_freelibhndl(hndlSocket);
                    return content;
                }
                else
                {
                    Logs.LogDB(new SqlConnection(), "Errore dowload file \n path vuoto", MAC_id);

                    Focas1.cnc_freelibhndl(hndlSocket);
                    return "";
                }
            }
            catch (Exception e)
            {
                Focas1.cnc_freelibhndl(hndlSocket);
                Logs.LogDB(new SqlConnection(), "Errore dowload file \n lettura file:"+e.Message+e.StackTrace, MAC_id);
                return "";
            }
        }
        
        //tools
        public string CutStr(string str, string tofind)
        {
            if (Strings.Len(str) == 0) return "";

            int i = Strings.InStr(str, tofind);

            if ((i > 0) && (i < Strings.Len(str))) return Strings.Left(str, i - 1);
            else return str;
        }

        //prende la path del root 
        public string ExtractRoot(string str)
        {
            string result = "";
            string tmpStr = "";

            int val = Strings.Len(str);

            if (val == 0) return "";

            do
            {
                val--;
                tmpStr = str.Substring(val - 1, Math.Min(1, str.Length - (val - 1)));
                if (tmpStr == "/") break;
            }
            while (val >= 1);

            result = str.Substring(Math.Max(str.Length - (Strings.Len(str) - val), 0));
            return result.Substring(0, Math.Min(Strings.Len(result) - 1, result.Length));
        }
        public string replaceCR (string str)
        {
            str= str.Replace("\n","\r");
            return (str);
        }
        
        #endregion
    }
}
