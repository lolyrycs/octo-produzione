﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace cns.macchine.OpcuaCN
{
    class DatoOpc
    {
        public string ChiaveDb { get; set; }
        public string ChiaveMac { get; set; }

        public DatoOpc(string kdb, string kmac)
        {
            ChiaveDb = kdb;
            ChiaveMac = kmac;
        }
    }

    class TabellaData
    {
        public string nome { get; set; }
        public List<DatoOpc> dati { get; set; }

        public TabellaData(string nometabella)
        {
            nome = nometabella;
            dati = new List<DatoOpc>();
        }

        public TabellaData(string nometabella, List<DatoOpc> datitabella)
        {
            nome = nometabella;
            dati = datitabella;
        }

        public void AggiungiDato(DatoOpc dato)
        {
            dati.Add(dato);
        }
    }

    class DataOpc
    {
        public List<TabellaData> Tabelle { get; set; }

        public DataOpc()
        {
            TabellaData interfaccia = new TabellaData("Interfaccia");
            TabellaData diagnostica = new TabellaData("Manutenzione2");
            Tabelle = new List<TabellaData>() { interfaccia, diagnostica};
        }

        public DataOpc(string nomeFileConf, string nomeMacchina)
        {
            string macchina = "";
            string tabella = "";
            TabellaData Tab;
            string[] couple;
            string data;

            using (var reader = new StreamReader(@""+nomeFileConf))
            {
                while (!reader.EndOfStream)
                {
                    //leggiamo una riga del file di configurazione
                    data = reader.ReadLine();
                    if (data.Length > 0)
                    {
                        if (data[data.Length - 1] == ';') data = data.Substring(0, data.Length - 1);

                        //riga di impostazione valori globali
                        if (data[0] == '[' && data[data.Length - 1] == ']')
                        {
                            data = data.Substring(1, data.Length - 2);
                            couple = data.Split(' ');
                            //lettura macchina
                            if (couple.Length == 2 && couple[0] == "MACCHINA")
                            {
                                macchina = couple[1];
                            }
                            //lettura tabella di riferimento della lettura
                            else if (couple.Length == 1)
                            {
                                switch (couple[0])
                                {
                                    case "INTERFACCIA":
                                        tabella = "Interfaccia";
                                        break;
                                    case "MANUTENZIONE2":
                                        tabella = "Manutenzione2";
                                        break;
                                    default:
                                        tabella = "";
                                        break;
                                }
                            }
                            if (tabella != "")
                            {
                                Tab = new TabellaData(tabella);
                                Tabelle.Add(Tab);
                            }
                        }
                        //lettura delle configurazioni della macchina
                        else if (macchina == nomeMacchina)
                        {
                            couple = data.Split(',');
                            if (couple.Length != 2)
                            {
                                utils.Logs.LogDB(new System.Data.SqlClient.SqlConnection(""), "Lunghezza non adatta, sicuro di non aver dimenticato un , o ; "+ data);
                                Console.WriteLine("Lunghezza non adatta, sicuro di non aver dimenticato un , o ;");
                            }
                            else
                            {
                                if (couple[0] != "Campo")
                                {
                                    AggiungiDato(couple[0], couple[1]);
                                }
                            }
                        }
                    }
                }
            }


        }

        public bool AggiungiDato(string kdb, string kmac)
        {
            if (Tabelle.Count == 0)
            {
                return false;
            }
            else
            {
                Tabelle[Tabelle.Count - 1].AggiungiDato(new DatoOpc(kdb, kmac));
            }
            return true;
        }
    }

}
