﻿using cns.utils;
using System.Threading;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace cns.macchine.OpcuaCN
{
    class OpcUa : OpcCN
    { 

        public OpcUa(int id, string tipoMacchina, string controlloNumerico, string ipAddress, string tipoControllo, int? numeroAssi = null, string nomemacchina = null, string pathshare = null) : base(id, tipoMacchina, controlloNumerico, ipAddress, tipoControllo, numeroAssi, nomemacchina, pathshare)
        {

        }

        public override void gestioneInterfaccia(object myId = null)
        {
            connesso = true;
            while (Service1.alive)
            {
                if (connesso)
                {
                    if (!istanziato)
                    {
                        init();
                    }
                    else
                    {
                        read_totem_status();
                        foreach (ChiaveOpc chiave in configurations.Tabella("Interfaccia").chiaviOpc)
                        {
                            switch (chiave.chiaveDb)
                            {
                                case "INT_feedAssi":
                                    feedAssi = null;
                                    feedAssi = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                                case "INT_utensileAttivo":
                                    utensileAttivo = null;
                                    utensileAttivo = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                                case "INT_programmaAttivo":
                                    string lastProg = execProg;
                                    execProg = ReadFromMac(chiave.chiaveMac);
                                    if (lastProg != execProg)
                                    {
                                        cambioprogramma();
                                    }
                                    break;
                                case "INT_pezzifatti":
                                    pezziFatti = null;
                                    pezziFatti = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    /*
                                    if (!configurations.calcola_pezzi)
                                    {
                                        if (pezziFatti == 0)
                                        {
                                            pezziFatti = pezziStart;
                                        }
                                        pezziStart = (int)pezziFatti;
                                    }*/
                                    break;
                                case "INT_feedMandrino":
                                    feedMandrino = null;
                                    feedMandrino = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                            }
                        }
                        ReadPieces();
                        InterfacciDB(connectionOpc);
                    }
                }
                else
                {
                    //chiudi connessione
                    if (istanziato)
                    {
                        istanziato = false;
                        Logs.LogDB(connectionOpc, "PLC non connesso", MAC_id);
                    }
                }
                Thread.Sleep(3000);
            }
        }

        public override void gestioneDiagnostica(object myId = null)
        {
            string querydiag = "";
            while (Service1.alive)
            {
                if (false && connesso && istanziato)
                {
                    Letman();
                    querydiag = "INSERT INTO Manutenzione2 (MAN2_chiave, MAN2_valore, MAN2_vs_LMAN_id) VALUES ";
                    foreach (ChiaveOpc chiave in configurations.Tabella("Manutenzione2").chiaviOpc)
                    {
                        querydiag += "('" + chiave.chiaveDb + "'," + ReadFromMac(chiave.chiaveMac) + "," + man_id + "),";
                    }
                    querydiag.Substring(0, querydiag.LastIndexOf(","));
                    DiagnosticaDB(connection_Diagnosi, querydiag);
                }
                Thread.Sleep(2000);
            }
        }

        #region produzione

        public override void gestioneProduzione(object myId = null)
        {
            while (Service1.alive)
            {
                if (connesso && istanziato)
                {
                    if (configurations.calcola_programma)
                    {
                        string lastProg = execProg;
                        ReadPrg();
                        //se cambio programma azzero i tempi - setto i pezzi - ricalcolo la commessa
                        if (lastProg != execProg)
                        {
                            cambioprogramma();
                        }
                    }
                    getcommessa();
                    if (commessaAttiva != "")
                    {
                        //check e aggiornamento dell'avanzamento della produzione
                        foreach (ChiaveOpc chiave in configurations.Tabella("Produzione").chiaviOpc)
                        {
                            switch (chiave.chiaveDb)
                            {
                                case "PROD_TempoLavoro":
                                    secLavoro = 0;
                                    secLavoro = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                                case "PROD_TempoPiazzamento":
                                    secPiazz = 0;
                                    secPiazz = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                                case "PROD_TempoPausa":
                                    secPausa = 0;
                                    secPausa = DbHelper.toInt(ReadFromMac(chiave.chiaveMac));
                                    break;
                            }
                        }

                        aggiornapezzicommessa();
                        letturatempi();
                        efficienza = 0;
                        if (secLavoro + secPiazz + secPausa > 0)
                        {
                            efficienza = secLavoro * 100 / (secLavoro + secPiazz + secPausa);
                        }

                        aggiornaTempi();

                        //consiglio un nuovo programma 
                        if(configurations.cambio_programma && configurations.ConsigliaPrgAddress != "" && ctrlPrg 
                            && pezziFatti>lotto && INT_totem != "verde")
                        {
                            ctrlPrg = false;
                            WriteInMac(configurations.ConsigliaPrgAddress, getNuovoProgramma());
                        }
                    }
                }
                Thread.Sleep(5000);
            }
        }

        //se cambio programma 
        // aggiorno lo stato della vecchia commessa in db
        // azzero i tempi - setto i pezzi - ricalcolo la commessa
        public void cambioprogramma()
        {
            aggiornaVecchiaCommessa();

            if (!configurations.calcola_pezzi) { pezziStart = DbHelper.toInt(ReadFromMac(pezzicode)); }
            else ReadPieces();
            getcommessa();
            secLavoro = secLavoroApp;
            secPausa = secPausaApp;
            secPiazz = secPiazzApp;

            if (configurations.calcola_tempi == "Hms")
            {
                ReadSeconds();
                secondiStart = secLavoro;
                ReadSeconds();
            }

            shiftpezzi = ReadPiecesDb();
            //posso di nuovo consigliare un programma all'operatore
            ctrlPrg = true;
        }

        //da inserire in produzione solo se non l'ho già fatto
        // per ora mi fido che ci sia
        public void insertProd()
        {
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                    string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() " +
                            "FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_ID = CPROD_ID WHERE CPROD_ID = " + commessaAttiva + "";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    command.ExecuteNonQuery();

                    //cicloproduzione status 2
                    command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '3' " +
                       "FROM Cicloproduzione CPROD_ID WHERE CPROD_ID = " + commessaAttiva + " AND CPROD_vs_MAC_id= " + MAC_id, connection_Produzione);
                    command.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    command.Dispose();

                    //Trasferisco da Produzione a Output
                    sqlQuery = "INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data, OUT_tempoLavoro, OUT_tempoPiazzamento,  OUT_tempoPausa, OUT_efficienza) " +
                            "SELECT PROD_vs_CPROD_id, PROD_priorita , GETDATE(), PROD_tempoLavoro, PROD_tempoPiazzamento, PROD_tempoPausa, PROD_efficienza " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_vs_MAC_id=" + MAC_id + " AND CPROD_ID= " + commessaAttiva;
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
        }

        //aggiorna tempi produzione e output
        public void aggiornaTempi()
        {
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command;
                    string sqlQuery = "if (ISNULL((SELECT distinct(PROD_tempoLavoro) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_commessa =" + DbHelper.cleanNullValue(commessaAttiva) + "),0)<=" + DbHelper.cleanNullValue(secLavoro) + ")" +
                           "AND(ISNULL((SELECT distinct(PROD_tempoPiazzamento) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazz) + ")" +
                           "AND(ISNULL((SELECT distinct(PROD_tempoPausa) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                           "UPDATE Produzione SET PROD_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + ", PROD_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazz) + ",  PROD_tempoPausa= " + DbHelper.cleanNullValue(secPausa) + ", " +
                           "PROD_appTempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + ", PROD_appTempoPiazzamento = " + DbHelper.cleanNullValue(secPiazz) + ", PROD_appTempoPausa = " + DbHelper.cleanNullValue(secPausa) + ", PROD_efficienza= " + DbHelper.cleanNullValue(efficienza) +
                           " FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(commessaAttiva) + " ";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();

                    sqlQuery = "if (ISNULL((SELECT distinct(OUT_tempoLavoro) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPiazzamento) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <= " + DbHelper.cleanNullValue(secPiazz) + ")" +
                            "AND(ISNULL((SELECT distinct(OUT_tempoPausa) FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = " + DbHelper.cleanNullValue(commessaAttiva) + "),0) <=" + DbHelper.cleanNullValue(secPausa) + ")" +
                            "UPDATE Output SET OUT_tempoLavoro = " + DbHelper.cleanNullValue(secLavoro) + " , OUT_tempoPiazzamento = " + DbHelper.cleanNullValue(secPiazz) + " " +
                            ", OUT_tempoPausa = " + DbHelper.cleanNullValue(secPausa) + " , OUT_efficienza = " + DbHelper.cleanNullValue(efficienza) + " FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(commessaAttiva);
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();

                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
        }

        //leggi la commessa dal programma attivo
        public void getcommessa()
        {
            SqlDataReader reader;
            int status = 0;
            commessaAttiva = "";
            if (execProg != null && execProg != "")
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        string sqlQuery = "SELECT TOP(1) CPROD_id, CPROD_status, CPROD_lotto FROM Cicloproduzione WHERE CPROD_programma = '" + execProg +
                                "' AND CPROD_vs_MAC_id = " + MAC_id + " ORDER BY CPROD_id DESC ";
                        SqlCommand command = new SqlCommand(sqlQuery, connection);
                        if (connection.State != ConnectionState.Open) connection.Open();
                        reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                commessaAttiva = reader.GetInt32(0).ToString();
                            }
                            if (!reader.IsDBNull(1))
                            {
                                status = reader.GetInt32(1);
                            }
                            if (!reader.IsDBNull(2))
                            {
                                lotto = DbHelper.toInt(reader.GetString(2));
                            }
                            if (commessaAttiva != "")
                            {
                                if (status < 3)
                                {
                                    insertProd();
                                }
                                else
                                {
                                    //leggo i tempi in appoggio
                                    try
                                    {
                                        using (connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                                        {
                                            SqlCommand comando = new SqlCommand("SELECT PROD_appTempoLavoro, PROD_appTempoPiazzamento, PROD_appTempoPausa FROM Produzione INNNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = '" + commessaAttiva + "'", connection_Produzione);
                                            if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                                            SqlDataReader dataReader = comando.ExecuteReader();
                                            while (dataReader.HasRows)
                                            {
                                                while (dataReader.Read())
                                                {
                                                    if (!dataReader.IsDBNull(0))
                                                        secLavoroApp = DbHelper.toInt(dataReader.GetString(0));
                                                    else
                                                        secLavoroApp = 0;
                                                    if (!dataReader.IsDBNull(1))
                                                        secPiazzApp = DbHelper.toInt(dataReader.GetString(1));
                                                    else
                                                        secPiazzApp = 0;
                                                    if (!dataReader.IsDBNull(2))
                                                        secPausaApp = DbHelper.toInt(dataReader.GetString(2));
                                                    else
                                                        secPausaApp = 0;
                                                }
                                                dataReader.NextResult();
                                            }
                                            dataReader.Close();

                                            if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                                            comando.Dispose();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                                    }
                                }
                            }
                        }

                        if (connection.State != ConnectionState.Closed) connection.Close();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }


            }
            return;
        }

        void letturatempi()
        {
            switch (configurations.calcola_tempi)
            {
                case "Hms":
                    ReadSeconds();
                    secLavoro += secLavoroApp;
                    secPausa += secPausaApp;
                    secPiazz += secPiazzApp;
                    break;
                case "C":
                    countSeconds();
                    break;
            }

        }

        void aggiornapezzicommessa()
        {
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command;
                    string sqlQuery = "UPDATE Cicloproduzione SET CPROD_PezziFatti = " + DbHelper.cleanNullValue(pezziFatti) +
                        "WHERE CPROD_id = " + DbHelper.cleanNullValue(commessaAttiva);
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();

                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
        }

        void aggiornaVecchiaCommessa(){
            if (commessaAttiva != "")
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                    {
                        string sqlQuery = "if(exists(SELECT CPROD_lotto, CPROD_pezziFatti FROM  Cicloproduzione WHERE CPROD_id = "+commessaAttiva+" AND CPROD_lotto<=CPROD_pezziFatti))" +
                        "UPDATE Cicloproduzione SET CPROD_status = 5 WHERE CPROD_id = " + commessaAttiva +
                        " else BEGIN " +
                        "UPDATE Cicloproduzione SET CPROD_status = 4 WHERE CPROD_id = CPROD_id = " + commessaAttiva + ";" +
                        "UPDATE Produzione SET PROD_appTempoLavoro = PROD_tempoLavoro, PROD_appTempoPiazzamento = PROD_tempoPiazzamento," +
                        "PROD_appTempoPausa = PROD_tempoPausa FROM Produzione WHERE PROD_vs_CPROD_id = " + commessaAttiva + "; END";
                        SqlCommand command = new SqlCommand(sqlQuery, connection);
                        if (connection.State != ConnectionState.Open) connection.Open();
                        command.ExecuteNonQuery();
                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        command.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
        }
        
        string getNuovoProgramma()
        {
            string nextPrg = "";
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                    string sqlQuery = "SELECT TOP(1) CPROD_programma FROM CICLOPRODUZIONE INNER JOIN INPUT ON IN_vs_CPROD_id=CPROD_id "+
                        "WHERE CPROD_status=1 AND CPROD_vs_MAC_id="+MAC_id+" ORDER BY IN_priorita desc";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    nextPrg = command.ExecuteScalar().ToString();
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "Programmi completati, " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
            return nextPrg;
        }
        
        #endregion

        #region letture custom

        //interpreta i dati per scrivere totem e status
        void read_totem_status()
        {
            INT_totem = "grigio";
            status = "";
            switch (MAC_tipoMacchina)
            {
                case "Rettifica":
                    // MDI
                    if (ReadFromMac("ModeMDI") == "True")
                    {
                        INT_totem = "verde";
                        status = "MDI RUN *** *** ***";
                    }

                    // MEM
                    if (ReadFromMac("ModeMEM") == "True")
                    {
                        string programName = ReadFromMac("ProgramState");
                        INT_totem = "giallo";
                        switch (programName)
                        {
                            case "3":
                                INT_totem = "verde";
                                status = "MEM RUN *** *** ***";
                                break;
                            case "2":
                                status = "MEM HLD *** *** ***";
                                break;
                            case "1":
                                status = "MEM STP *** *** ***";
                                break;
                            default:
                                status = "MEM *** *** *** ***";
                                break;
                        }
                    }

                    // EDI
                    if (ReadFromMac("ModeEdit") == "True")
                    {
                        INT_totem = "giallo";
                        status = "EDI *** *** *** ***";
                    }

                    // SET
                    if (ReadFromMac("ModeSET") == "True")
                    {
                        INT_totem = "giallo";
                        status = "*** *** SET *** ***";
                    }

                    // JOG
                    if (ReadFromMac("ModeJOG") == "True")
                    {
                        INT_totem = "giallo";
                        status = "JOG *** *** *** ***";
                    }

                    // ALM
                    if (ReadFromMac("MachineAlarm") == "True")
                    {
                        INT_totem = "rosso";
                        status = "*** *** *** *** ALM";
                    }
                    

                    // EMG
                    if (ReadFromMac("EStop") == "True")
                    {
                        INT_totem = "viola";
                        status = "*** *** *** EMG ***";
                    }
                    break;


                case "Dentatrice":
                    // Error ALM
                    if (ReadFromMac("DB140.DBX0.2") == "True")
                    {
                        INT_totem = "rosso";
                        status = "*** *** *** *** ALM";
                    }

                    // E-stop EMG
                    if (ReadFromMac("DB140.DBX0.3") == "True")
                    {
                        INT_totem = "viola";
                        status = "*** *** *** EMG ***";
                    }

                    // JOG
                    if (ReadFromMac("DB140.DBX0.4") == "True")
                    {
                        INT_totem = "giallo";
                        status = "JOG *** *** *** ***";
                    }

                    // MEM
                    if (ReadFromMac("DB140.DBX0.5") == "True")
                    {
                        INT_totem = "verde";
                        status = "MEM STP *** *** ***";
                    }

                    if (ReadFromMac("DB140.DBX0.6") == "True")
                    {
                        INT_totem = "verde";
                        status = "MEM RUN *** *** ***";
                    }

                    break;
            }
        }

        // aggiunge i tempi dall'ultima lettura allo stato attuale della commessa (Lavoro-Pausa-Piazzamento)
        public void countSeconds()
        {
            now = DateTime.Now;
            //Logs.LogDB(connectionOpc, "Lettura tempi da octo: ora :" + now.ToString() + " ultima lettura tempi: " + last.ToString());
            //commessa in lavoro
            if(status == "MEM RUN *** *** ***")
            {
                secLavoro += (now - last).Seconds;
            }
            //commessa in Pausa
            else if (INT_totem == "viola")
            {
                secPausa += (now - last).Seconds;
            }
            //commessa in piazzamento
            else
            {
                secPiazz += (now - last).Seconds;
            }
            last = now;
        }

        //Somma OreMinSec
        public void ReadSeconds()
        {
            int ore, minuti, secondi = 0;
            ore = DbHelper.toInt(ReadFromMac("DB140.DBW210"));
            minuti = DbHelper.toInt(ReadFromMac("DB140.DBW214"));
            secondi = DbHelper.toInt(ReadFromMac("DB140.DBW216"));
            secLavoro = TimeHelper.calcolaSecondiDaRegistro(ore, minuti, secondi) - secondiStart;
        }

        //compone il programma attivo della Gleason trashormando in lettere i dati letti in macchina e concatenandoli
        public void ReadPrg()
        {
            string code = "DB140.DBB";
            execProg = "";
            int asciicode;
            if (configurations.calcola_programma)
            {
                for(int i=12; i<=83; i++)
                {
                    asciicode = DbHelper.toInt(ReadFromMac(code + i.ToString()));
                    execProg += ((char)asciicode).ToString();
                }
                execProg.Trim();
            }
        }

        public void ReadPieces()
        {
            if (configurations.calcola_pezzi)
            {
                if (pezziStart < 0)
                {
                    pezziStart = DbHelper.toInt(ReadFromMac(pezzicode));
                    pezziFatti = shiftpezzi;
                }
                else
                {
                    int pezzi = DbHelper.toInt(ReadFromMac(pezzicode)) - pezziStart + shiftpezzi;
                    if (pezzi>=0) pezziFatti = pezzi;
                }
                

            }

        }

        public int ReadPiecesDb()
        {
            int pezziDb = 0;
            try
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "Programma attivo = "+ execProg, MAC_id);
                if (execProg!=null && execProg != "") { 
                    using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                    {
                        SqlCommand command;
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                        string sqlQuery = "SELECT TOP(1) CPROD_pezzifatti FROM Cicloproduzione, Interfaccia " +
                            "WHERE INT_vs_MAC_id=" + MAC_id + " AND INT_programmaAttivo=CPROD_programma AND INT_programmaAttivo = '"+ execProg +
                            "' ORDER BY INT_id desc, CPROD_id desc";
                        command = new SqlCommand(sqlQuery, connection_Produzione);
                        pezziDb = DbHelper.toInt(command.ExecuteScalar().ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""),  DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
            return pezziDb;

        }

        #endregion

    }
}
