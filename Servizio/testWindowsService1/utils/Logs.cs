﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace cns.utils
{
    class Logs
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // funzione per loggare sul DB Tabella di LOG 
        public static void LogDB(SqlConnection mysqlCon, string message, int? macId = null)
        {
            string mex = DbHelper.cleanMessSql(DbHelper.cleanNullValue(message));
            //Console.WriteLine(mex);
            try
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Octo errore, macchina " +DbHelper.cleanNullValue(macId) + " \n messaggio: " + DbHelper.cleanNullValue(DbHelper.cleanMessSql(message)));
                }
                using (mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    string dummyId = "";
                    if (macId == null)
                        dummyId = "NULL";
                    else dummyId = macId.ToString();
                    string sqlQuery = "INSERT INTO Logs (LOG_fk_MAC_id, LOG_description, LOG_date) " +
                                                "VALUES (" + dummyId + ", " + DbHelper.cleanNullValue(DbHelper.cleanMessSql(message)) + ", GETDATE())";
                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open) mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed) mysqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("Octo errore, macchina " + DbHelper.cleanNullValue(macId) + " \n messaggio: " + DbHelper.cleanNullValue(DbHelper.cleanMessSql(ex.Message + " " + ex.StackTrace)));
                }
            }
        }

        public static string clearLogs(string message)
        {
            message = message.Replace((char)39, (char)0);
            return message;
        }
    }
}
