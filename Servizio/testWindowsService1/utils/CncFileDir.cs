﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Specialized;

namespace cns.utils
{
    public class CncFileDir
    {
        //local variable(s) to hold property value(s)
        private string mvarname = ""; //local copy
        private OrderedDictionary mvarcl = null; //local copy
                                                 //local variable(s) to hold property value(s)
        private bool mvarSubFolder = false; //local copy
        public bool SubFolder
        {
            get
            {
                //used when retrieving value of a property, on the right side of an assignment.
                //Syntax: Debug.Print X.SubFolder
                if (Information.IsReference(mvarSubFolder))
                {
                    return mvarSubFolder;
                }
                else
                {
                    return mvarSubFolder;
                }
            }
            set
            {
                if (Microsoft.VisualBasic.Information.IsReference(value) && !(value is string))
                {
                    //used when assigning an Object to the property, on the left side of a Set statement.
                    //Syntax: Set x.SubFolder = Form1
                    mvarSubFolder = value;
                }
                else
                {
                    //used when assigning a value to the property, on the left side of an assignment.
                    //Syntax: X.SubFolder = 5
                    mvarSubFolder = value;
                }
            }
        }






        public OrderedDictionary cl
        {
            get
            {
                //used when retrieving value of a property, on the right side of an assignment.
                //Syntax: Debug.Print X.cl
                return mvarcl;
            }
            set
            {
                //used when assigning an Object to the property, on the left side of a Set statement.
                //Syntax: Set x.cl = Form1
                mvarcl = value;
            }
        }






        public string name
        {
            get
            {
                //used when retrieving value of a property, on the right side of an assignment.
                //Syntax: Debug.Print X.name
                return mvarname;
            }
            set
            {
                //used when assigning a value to the property, on the left side of an assignment.
                //Syntax: X.name = 5
                mvarname = value;
            }
        }




        public CncFileDir()
        {
            mvarname = "";
            mvarSubFolder = false;
            mvarcl = new OrderedDictionary(System.StringComparer.OrdinalIgnoreCase);
        }
    }
}