﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;


namespace cns.utils
{
    public class Commessa
    {
        //dati commessa
        public int commessa = 0, lotto = 0, priorita = 0, fase = 0, articolo = 0, programma = 0, pezzifatti = 0;

        //dati database
        public int cprod_id, in_id,  MAC_id, status;

        public Commessa()
        { }

        //costruttore validatore
        public Commessa(int commessa, int lotto, int priorita, int fase = 0, int articolo = 0, int cprod_id=0)
        {
            if (commessa <= 99999999) this.commessa = commessa;
            if (lotto <= 9999) this.lotto = lotto;
            if (priorita > 0 & priorita < 100) this.priorita = priorita;

            if (fase > 0 & fase < 100) this.fase = fase;
            if (articolo <= 99999999) this.articolo = articolo;
            if (cprod_id > 0) this.cprod_id = cprod_id;
        }


        //inserisce la commessa in cicloproduzione, set cprod_id e in_id 
        public void input_db()
        {
            try
            {
                using (SqlConnection mysqlCon = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    string query = "INSERT INTO Cicloproduzione (CPROD_vs_MAC_id, CPROD_commessa, CPROD_importFrom, CPROD_status, CPROD_lotto, CPROD_pezziFatti, CPROD_fase, CPROD_articolo) VALUES " +
                            "(" + MAC_id + ", " + DbHelper.cleanNullValue(commessa) + ", cnc, '1', " + DbHelper.cleanNullValue(lotto) + ", 0, " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(articolo) + " ); " +
                            "SELECT CAST(SCOPE_IDENTITY() AS int)";
                    SqlCommand command = new SqlCommand();
                    command.Connection = mysqlCon;
                    command.CommandText = query;
                    cprod_id = (Int32)command.ExecuteScalar();

                    if (cprod_id != 0)
                    {
                        //Update tabella Input
                        query = "INSERT INTO Input (IN_commessa, IN_lotto, IN_importFrom,  IN_status,  IN_vs_CPROD_id, IN_priorita, IN_data, IN_fase, IN_articolo) " +
                            "SELECT CPROD_commessa, CPROD_lotto, CPROD_importFrom, '1' , " + cprod_id + ", " + DbHelper.cleanNullValue(priorita) + ", GETDATE(), " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(articolo) + " " +
                            "FROM Cicloproduzione WHERE CPROD_id=" + cprod_id + ";" +
                            "SELECT CAST(SCOPE_IDENTITY() AS int)";
                        command.Connection = mysqlCon;
                        command.CommandText = query;
                        in_id = Convert.ToInt32(command.ExecuteScalar());
                    }

                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
            return;
        }

        //inserimento commessa in produzione
        public void prod_db()
        {
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();

                    SqlCommand command = new SqlCommand("UPDATE Input SET IN_status = '2' WHERE IN_id = '" + in_id + "'", connection_Produzione);
                    int m = command.ExecuteNonQuery();

                    command.Dispose();

                    string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_id= " + in_id + "";
                    command = new SqlCommand(sqlQuery, connection_Produzione);
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    m = command.ExecuteNonQuery();

                    //cicloproduzione status 2
                    command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '3' " +
                       "FROM Cicloproduzione INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE IN_id = '" + in_id + "' AND CPROD_vs_MAC_id= " + MAC_id, connection_Produzione);
                    m = command.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    command.Dispose();

                    //Trasferisco da Produzione a Output
                    sqlQuery = "INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data, OUT_tempoLavoro, OUT_tempoPiazzamento,  OUT_tempoPausa, OUT_efficienza) " +
                            "SELECT PROD_vs_CPROD_id, PROD_priorita , GETDATE(), PROD_tempoLavoro, PROD_tempoPiazzamento, PROD_tempoPausa, PROD_efficienza " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id INNER JOIN Input ON IN_vs_CPROD_id=CPROD_id WHERE CPROD_vs_MAC_id=" + MAC_id + " AND IN_id= " + in_id;
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    m = command.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();

                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
        }

        //camibio lo status della commessa
        public static void changestatus_db(int cprod_id, int status, int pezziFatti)
        {
            if (status > 1 & status <= 5)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                    {
                        string SqlQuery = "UPDATE Cicloproduzione SET CPROD_pezziFatti = " + DbHelper.cleanNullValue(pezziFatti) + ", CPROD_status=" + status + " WHERE CPROD_id=" + cprod_id;
                        SqlCommand comando = new SqlCommand(SqlQuery, connection);
                        if (connection.State != ConnectionState.Open) connection.Open();
                        comando.ExecuteNonQuery();
                        if (connection.State != ConnectionState.Closed) connection.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace);
                }
            }
        }
    }

    public class CommessaMacchina : Commessa
    {
        public bool finita = false, vuota = false;
        public int indice = 0;

        //indica se il led è acceso e la commessa è attiva
        public bool attiva = false;
        
        public CommessaMacchina()
        {
            vuota = true;
        }

        public CommessaMacchina(int MAC_id, int cprod_id, int indice, int commessa, int lotto, int priorita, int fase = 0, int articolo = 0, bool attiva=false, int pezzifatti=0)
        {
            if (commessa <= 99999999) this.commessa = commessa;
            if (lotto <= 9999) this.lotto = lotto;
            if (priorita > 0)
            {
                if (priorita < 100) this.priorita = priorita;
            }
            else
            {
                if (priorita == 0)
                {
                    this.priorita = priorita;
                    vuota = true;
                }
                if (priorita == -1)
                {
                    this.priorita = priorita;
                    finita = true;
                }
            }
            if (fase > 0 & fase < 100) this.fase = fase;
            if (articolo <= 99999999) this.articolo = articolo;

            this.MAC_id = MAC_id;
            this.indice = indice;
            this.attiva = attiva;
            this.pezzifatti = pezzifatti;
            this.cprod_id = cprod_id;

            status = Queries.LeggiStatusDb(cprod_id, MAC_id);
            if (status>0 && status<=4 && finita)
            {
                status = 5;
            }
        }

        //inserisce la commessa in Input/Cicloproduzione
        public void carica_da_macchina()
        {
            using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
            {
                if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                {
                    try
                    {
                        //crea il record in cicloproduzione, salva l'id 
                        string sqlQuery = "INSERT INTO Cicloproduzione (CPROD_vs_MAC_id, CPROD_commessa, CPROD_importFrom, CPROD_status, CPROD_lotto, CPROD_pezziFatti, CPROD_fase, CPROD_articolo) VALUES " +
                            "(" + MAC_id + ", " + DbHelper.cleanNullValue(commessa) + ", CNC, '1', " + DbHelper.cleanNullValue(lotto) + ", 0, " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(articolo) + " ); " +
                            "SELECT CAST(SCOPE_IDENTITY() AS int)";
                        SqlCommand command = new SqlCommand();
                        command.Connection = connection_Produzione;
                        command.CommandText = sqlQuery;
                        cprod_id = (Int32)command.ExecuteScalar();
                        int N;
                        if (cprod_id != 0)
                        {
                            //Update tabella Input
                            sqlQuery = "INSERT INTO Input (IN_commessa, IN_lotto, IN_importFrom,  IN_status,  IN_vs_CPROD_id, IN_priorita, IN_data, IN_programma, IN_fase, IN_articolo) " +
                                "SELECT CPROD_commessa, CPROD_lotto, CPROD_importFrom, '1' , " + cprod_id + ", " + DbHelper.cleanNullValue(priorita) + ", GETDATE(), " + DbHelper.cleanNullValue(programma) + ", " + DbHelper.cleanNullValue(fase) + ", " + DbHelper.cleanNullValue(articolo) + " " +
                                "FROM Cicloproduzione WHERE CPROD_id=" + cprod_id + "";
                            command.Connection = connection_Produzione;
                            command.CommandText = sqlQuery;
                            N = command.ExecuteNonQuery();
                        }

                        //ToDo inserire il valore cp_id in macchina come codice commessa *Lorenzo*//
                    }
                    catch (Exception ex)
                    {
                        Logs.LogDB(connection_Produzione, "errore caricamento in produzione " + ex.Message + MAC_id);
                    }
                }
            }
        }
    }

    //produzione Database
    public class Produzione
    {
        public List<Commessa> commesse = new List<Commessa>();
        public int MAC_id;

        #region costruttori
        public Produzione(int MAC_id=0)
        {
            this.MAC_id = MAC_id;
        }

        public Produzione(int MAC_id, Commessa commessa)
        {
            commesse.Add(commessa);
            this.MAC_id = MAC_id;
        }

        public Produzione(int MAC_id, List<Commessa> commesse)
        {
            this.commesse=commesse;
            this.MAC_id = MAC_id;
        }
        #endregion

        //Estraggo i primi top Record di Cicloproduzione (Input)
        public bool LeggiDb(int top = 10)
        {
            commesse = new List<Commessa>();
            int cpid, commessa, lotto, priorita, fase, articolo;
            Commessa letta;
            bool ret = false;
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                {
                    SqlCommand command = new SqlCommand("SELECT TOP (" + top + ") CPROD_id, CPROD_commessa, CPROD_lotto, IN_priorita, CPROD_id, IN_fase, IN_articolo, CPROD_pezziFatti " +
                       "FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id=CPROD_id  " +
                       "WHERE CPROD_status = '1' AND CPROD_vs_MAC_id='" + MAC_id + "' ORDER BY IN_priorita ASC", connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            cpid = DbHelper.toInt(DbHelper.NotNull(reader, 0, "int"));
                            commessa = DbHelper.toInt(DbHelper.NotNull(reader, 1));
                            lotto = DbHelper.toInt(DbHelper.NotNull(reader, 2));
                            priorita = DbHelper.toInt(DbHelper.NotNull(reader, 3, "int"));
                            fase = DbHelper.toInt(DbHelper.NotNull(reader, 5, "string"));
                            articolo = DbHelper.toInt(DbHelper.NotNull(reader, 6, "string"));
                            letta = new Commessa(commessa, lotto, priorita, fase, articolo, cprod_id:cpid);
                            commesse.Add(letta);
                        }
                    }
                    reader.Close();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    ret = true;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
            return ret;
        }

    }

    //Produzione Macchina
    public class ProduzioneMacchina : Produzione
    {
        #region variabili
        public Commessa Attiva= new CommessaMacchina();
        public new List<CommessaMacchina> commesse = new List<CommessaMacchina>();
        public bool commesseVuote = false, commesseFinite = false;
        public int SecondiLavoro = 0, SecondiPiazzamento = 0, SecondiPausa = 0, efficienza = 0;
        #endregion

        #region costruttori
        public ProduzioneMacchina(int MAC_id = 0)
        {
            this.MAC_id = MAC_id;
        }

        public ProduzioneMacchina(int MAC_id, CommessaMacchina commessa)
        {
            commesse.RemoveAt(commessa.indice);
            commesse.Insert(commessa.indice, commessa);
            if (commessa.finita) commesseFinite = true;
            if (commessa.vuota) commesseVuote = true;

            this.MAC_id = MAC_id;
        }

        public ProduzioneMacchina(int MAC_id, List<CommessaMacchina> commesse)
        {
            this.commesse = commesse;
            this.MAC_id = MAC_id;
        }

        #endregion

        #region funzioni Produzione

        //dall'id in database restituisce la posizione in macchina (NON RESTITUISCE LA POSIZIONE DELLA COMMESSA ATTIVA)
        public int get_index_by_id(int id)
        {
            int index = -1;
            foreach (CommessaMacchina cm in commesse)
            {
                if (cm.cprod_id == id && cm.attiva == false) return cm.indice;
            }
            return index;
        }

        //controllo se sono commesse finite o vuote
        public bool checkProd()
        {
            foreach(CommessaMacchina commessa in commesse)
            {
                if (commessa.attiva) { Attiva = commessa; }
                else
                {
                    if (commessa.finita) commesseFinite = true;
                    if (commessa.vuota) commesseVuote = true;
                }
            }
            return (commesseFinite || commesseVuote);
        }
        
        //gestione Tempi, aggiunti tempi di appoggio, calcolo efficienza
        public int Tempi()
        {
            SqlConnection connection_Produzione= new SqlConnection();
            int secLavoroDB = 0, secPiazzamentoDB = 0, secPausaDB = 0;

            if (Attiva.status > 3)
            {
                //lettura tempi appoggio in database
                try
                {
                    using (connection_Produzione = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
                    {
                        SqlCommand comando = new SqlCommand("SELECT PROD_appTempoLavoro, PROD_appTempoPiazzamento, PROD_appTempoPausa FROM Produzione INNNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = '" + Attiva.cprod_id + "'", connection_Produzione);
                        if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                        SqlDataReader dataReader = comando.ExecuteReader();
                        while (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                if (!dataReader.IsDBNull(0)) secLavoroDB = Convert.ToInt32(dataReader.GetString(0));
                                if (!dataReader.IsDBNull(1)) secPiazzamentoDB = Convert.ToInt32(dataReader.GetString(1));
                                if (!dataReader.IsDBNull(2)) secPausaDB = Convert.ToInt32(dataReader.GetString(2));
                            }
                            dataReader.NextResult();
                        }
                        dataReader.Close();

                        if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                        comando.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }

            SecondiLavoro = SecondiLavoro + secLavoroDB;
            SecondiPausa = SecondiPausa + secPausaDB;
            SecondiPiazzamento = SecondiPiazzamento + secPiazzamentoDB;
            if(SecondiPiazzamento + SecondiPausa + SecondiLavoro > 0)
                efficienza = SecondiLavoro * 100 / (SecondiPiazzamento + SecondiPausa + SecondiLavoro);
            return efficienza;
        }

        //salvo i tempi in appoggio se crescono
        public void AggiornamentoTempi()
        {
            //aggiornamento tempi Produzione
            try
            {
                using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    string sqlQuery = "if (ISNULL((SELECT distinct(PROD_tempoLavoro) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(Attiva.cprod_id) + "),0)<=" + DbHelper.cleanNullValue(SecondiLavoro) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPiazzamento) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(Attiva.cprod_id) + "),0) <= " + DbHelper.cleanNullValue(SecondiPiazzamento) + ")" +
                            "AND(ISNULL((SELECT distinct(PROD_tempoPausa) FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(Attiva.cprod_id) + "),0) <=" + DbHelper.cleanNullValue(SecondiPausa) + ")" +
                            "UPDATE Produzione SET PROD_tempoLavoro = " + DbHelper.cleanNullValue(SecondiLavoro) + ", PROD_tempoPiazzamento = " + DbHelper.cleanNullValue(SecondiPiazzamento) + ",  PROD_tempoPausa= " + DbHelper.cleanNullValue(SecondiPausa) + ", PROD_efficienza= " + DbHelper.cleanNullValue(efficienza) +
                           " FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = " + DbHelper.cleanNullValue(Attiva.cprod_id) + " ";
                    SqlCommand command = new SqlCommand(sqlQuery, connection_Produzione);
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                    command.Dispose();
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace, MAC_id);
            }
        }

        //eliminazione commesse
        public List<int> LeggiIndexElimizione()
        {
            List<int> ids_to_remove = Queries.leggi_canc_comm(MAC_id);
            List<int> ret=new List<int>();
            foreach (int id in ids_to_remove)
            {
                // get indice della commessa in macchina, ignora la commessa attiva
                int index = get_index_by_id(id);
                if (index >= 0)
                {
                    ret.Add(index);
                }
                else
                {
                    Logs.LogDB(new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""), "commessa " + id + " inesistente o attiva, impossibile eliminare");
                    Queries.cambia_status_comm(id, 8, MAC_id);
                }
            }
            return ret;

        }

        #endregion
    }

}
