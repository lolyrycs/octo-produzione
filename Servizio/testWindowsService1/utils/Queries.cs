﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;


namespace cns.utils
{
    class Queries
    {
        #region definizioni
        static SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + "");
        static string SqlQuery { get; set; }
        static SqlCommand command = new SqlCommand();
        #endregion

        #region Interfaccia

        #endregion

        #region Produzione
        //salvo tempi in appoggio per pausa commessa
        public static void SalvataggioAppoggio(int MAC_id, int cprod_id)
        {
            using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    SqlQuery = "UPDATE Produzione " +
                            "SET PROD_appTempoLavoro = PROD_tempoLavoro,  PROD_appTempoPiazzamento = PROD_tempoPiazzamento, PROD_appTempoPausa = PROD_tempoPausa " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id  WHERE CPROD_id = '" + cprod_id + "'";
                    command = new SqlCommand(SqlQuery, connection);
                    if (connection.State != ConnectionState.Open) connection.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();

                    if (connection.State != ConnectionState.Closed) connection.Close();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection, ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }

        }

        //inserimento della Commessa in Produzione e Output
        public static void InserimentoProduzione(int cprod_id, int MAC_id)
        {
            try
            {
                using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    if (connection.State != ConnectionState.Open) connection.Open();

                    SqlCommand command = new SqlCommand("UPDATE Input SET IN_status = '2' WHERE IN_vs_CPROD_id = '" + cprod_id + "'", connection);
                    int m = command.ExecuteNonQuery();

                    command.Dispose();

                    string sqlQuery = "INSERT INTO Produzione (PROD_vs_CPROD_id, PROD_lotto, PROD_priorita, PROD_data) " +
                            " SELECT IN_vs_CPROD_ID, IN_lotto, IN_priorita, GETDATE() FROM Input WHERE IN_vs_CPROD_id = '" + cprod_id + "'";
                    command = new SqlCommand(sqlQuery, connection);
                    command.Connection = connection;
                    command.CommandText = sqlQuery;
                    m = command.ExecuteNonQuery();

                    //cicloproduzione status 2
                    command = new SqlCommand("UPDATE Cicloproduzione SET CPROD_status = '3' " +
                       "FROM Cicloproduzione  WHERE CPROD_id = '" + cprod_id + "' AND CPROD_vs_MAC_id= " + MAC_id, connection);
                    m = command.ExecuteNonQuery();
                    if (connection.State != ConnectionState.Closed) connection.Close();
                    command.Dispose();

                    //Trasferisco da Produzione a Output
                    sqlQuery = "INSERT INTO Output (OUT_vs_CPROD_id, OUT_priorita, OUT_data, OUT_tempoLavoro, OUT_tempoPiazzamento,  OUT_tempoPausa, OUT_efficienza) " +
                            "SELECT PROD_vs_CPROD_id, PROD_priorita , GETDATE(), PROD_tempoLavoro, PROD_tempoPiazzamento, PROD_tempoPausa, PROD_efficienza " +
                            "FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_vs_MAC_id=" + MAC_id + " AND CPROD_id = " + cprod_id;
                    command.Connection = connection;
                    command.CommandText = sqlQuery;
                    if (connection.State != ConnectionState.Open) connection.Open();
                    m = command.ExecuteNonQuery();
                    if (connection.State != ConnectionState.Closed) connection.Close();

                }

            }
            catch (Exception ex)
            {
                Logs.LogDB(connection, "errore caricamento in tabella produzione " + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
            }
        }

        //aggiornamento dati di produzione: tempi output, status e pezzi 
        public static void AggiornaDati(int cprod_id, int status, int pezzifatti, int MAC_id)
        {
            using (connection = new SqlConnection(ConfigurationManager.AppSettings["msqkconnectionstring"] + ""))
            {
                if (connection.State != ConnectionState.Open) connection.Open();
                SqlCommand command;
                //Tempi Output
                try
                {
                    string sqlQuery = "UPDATE Output SET OUT_priorita = PROD_priorita, " +
                        "OUT_TempoLavoro = PROD_tempoLavoro,  OUT_TempoPiazzamento = PROD_tempoPiazzamento, OUT_TempoPausa = PROD_tempoPausa, OUT_efficienza = PROD_efficienza " +
                        "FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id=CPROD_id INNER JOIN Produzione ON PROD_vs_CPROD_id=CPROD_id WHERE CPROD_id = '" + cprod_id + "'";
                    command = new SqlCommand(sqlQuery, connection);
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection, "errore aggiornamento tempi output: " + ex.Message + " " + ex.StackTrace, MAC_id);
                }

                //Status Cicloproduzione
                try
                {
                    string sqlQuery = "UPDATE Cicloproduzione SET CPROD_status = '" + status + "', CPROD_pezziFatti= "+pezzifatti+" WHERE CPROD_id = '" + cprod_id + "'";
                    command = new SqlCommand(sqlQuery, connection);
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection, "errore aggiornamento status cicloproduzione: " + ex.Message + " " + ex.StackTrace, MAC_id);
                }

                if (connection.State != ConnectionState.Closed) connection.Close();
            }
        }

        //dopo aver letto la commessa da macchina leggiamo lo status in database
        public static int LeggiStatusDb(int cprod_id, int MAC_id)
        {
            int status = 0;
            using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    string SqlQuery = "SELECT (CPROD_status) FROM Cicloproduzione WHERE CPROD_id =" + cprod_id + " AND CPROD_vs_MAC_id= " + MAC_id + " order by cprod_id desc";
                    command = new SqlCommand(SqlQuery, connection);
                    if (connection.State != ConnectionState.Open) connection.Open();
                    var st = command.ExecuteScalar();
                    if (st != null)
                    {
                        status = Convert.ToInt32(st);
                    }
                    command.Dispose();
                    if (connection.State != ConnectionState.Closed) connection.Close();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection, ex.Message + " " + ex.StackTrace, MAC_id);
                }
            }
            return status;
        }

        //lettura lista commesse da eliminare
        public static List<int> leggi_canc_comm(int MAC_id)
        {
            List<int> cprod_to_del = new List<int>();
            using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    string sqlQuery = "select top(10) cprod_id from Cicloproduzione where cprod_vs_mac_id=" + MAC_id + "and cprod_status=" +6;
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                cprod_to_del.Add(reader.GetInt32(0));
                            }
                        }
                        reader.Close();
                    }
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, "errore lettura cancellazione commessa" + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
                }
            }
            return cprod_to_del;
        }

        //cambia status commessa
        public static void cambia_status_comm(int id_commessa, int status, int MAC_id)
        {
            using (SqlConnection connection_Produzione = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    string sqlQuery = "update Cicloproduzione set cprod_status=" + status + " where cprod_vs_mac_id=" + MAC_id + "and cprod_id=" + id_commessa;
                    command.Connection = connection_Produzione;
                    command.CommandText = sqlQuery;
                    if (connection_Produzione.State != ConnectionState.Open) connection_Produzione.Open();
                    command.ExecuteNonQuery();
                    command.Dispose();
                    if (connection_Produzione.State != ConnectionState.Closed) connection_Produzione.Close();
                }
                catch (Exception ex)
                {
                    Logs.LogDB(connection_Produzione, "errore lettura cancellazione commessa" + DbHelper.cleanMessSql(ex.Message) + ex.StackTrace, MAC_id);
                }
            }
        }
        #endregion
    }
}
