﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace cns.utils
{
    /* singola lettura in macchina*/
    class ChiaveOpc
    {
        public string chiaveDb { get; set; }
        public string chiaveMac { get; set; }

        public ChiaveOpc(string chiaveDatabase, string chiaveMacchina)
        {
            chiaveDb = chiaveDatabase;
            chiaveMac = chiaveMacchina;
        }
    }

    /* raccolta delle letture per tabelle */
    class ChiaviTabellaOpc
    {
        public List<ChiaveOpc> chiaviOpc { get; set; }
        public string nomeTab { get; set; }

        public ChiaviTabellaOpc(string nomeTabella)
        {
            nomeTab = nomeTabella;
            chiaviOpc = new List<ChiaveOpc>();
        }

        public void Aggiungichiave(ChiaveOpc chiave)
        {
            chiaviOpc.Add(chiave);
        }

        public void Aggiungichiave(string chiaveDatabase, string chiaveMacchina)
        {
            ChiaveOpc chiave = new ChiaveOpc(chiaveDatabase, chiaveMacchina);
            Aggiungichiave(chiave);
        }
    }

    /* Instanziamo la lista delle tabelle */
    class ConfigOpc
    {
        public List<ChiaviTabellaOpc> Tabelle { get; set; }
        public string calcola_tempi = "";
        public bool calcola_pezzi = false;
        public bool calcola_programma = false;
        public bool cambio_programma = false;

        public string ConsigliaPrgAddress = "";

        public ConfigOpc()
        {
            Tabelle = new List<ChiaviTabellaOpc>();
            ChiaviTabellaOpc interfaccia = new ChiaviTabellaOpc("Interfaccia");
            Tabelle.Add(interfaccia);
            ChiaviTabellaOpc diagnostica = new ChiaviTabellaOpc("Manutenzione2");
            Tabelle.Add(diagnostica);
            ChiaviTabellaOpc produzione = new ChiaviTabellaOpc("Produzione");
            Tabelle.Add(produzione);
        }

        //aggiunge una configurazione ad una Tabella se questa esiste
        public bool AddConfToTable(ChiaveOpc chiave, string nomeTabella)
        {
            bool ret = false;
            foreach (ChiaviTabellaOpc tab in Tabelle)
            {
                if (tab.nomeTab == nomeTabella)
                {
                    tab.Aggiungichiave(chiave);
                    ret = true;
                }
            }
            return ret;
        }

        public bool AddConfToTable(string chiaveDatabase, string chiaveMacchina, string nomeTabella)
        {
            bool ret = false;
            foreach (ChiaviTabellaOpc tab in Tabelle)
            {
                if (tab.nomeTab == nomeTabella)
                {
                    tab.Aggiungichiave(chiaveDatabase, chiaveMacchina);
                    ret = true;
                }
            }
            return ret;
        }

        //lettura del file di configurazione e popolamento delle chiavi di configurazione
        public void LetturaFileConfig(string nomemacchina)
        {
            string data;
            string macchina = "";
            string tabella = "";
            string[] couple;
            using (var reader = new StreamReader(@"C:\Program Files (x86)\CNS\ServiceI\con_opc.txt"))
            {
                while (!reader.EndOfStream)
                {
                    //leggiamo una riga del file di configurazione
                    data = reader.ReadLine();
                    if (data.Length > 0)
                    {
                        if (data[data.Length - 1] == ';') data = data.Substring(0, data.Length - 1);

                        //riga di impostazione valori globali
                        if (data[0] == '[' && data[data.Length - 1] == ']')
                        {
                            data = data.Substring(1, data.Length - 2);
                            couple = data.Split(' ');
                            //lettura macchina
                            if (couple.Length == 2 && couple[0] == "MACCHINA")
                            {
                                macchina = couple[1];
                            }
                            //lettura tabella di riferimento della lettura
                            else if (couple.Length == 1)
                            {
                                switch (couple[0])
                                {
                                    case "INTERFACCIA":
                                        tabella = "Interfaccia";
                                        break;
                                    case "DIAGNOSTICA":
                                        tabella = "Manutenzione2";
                                        break;
                                    case "PRODUZIONE":
                                        tabella = "Produzione";
                                        break;
                                    default:
                                        tabella = "";
                                        break;
                                }
                            }
                        }

                        //lettura delle configurazioni della macchina
                        else if (macchina == nomemacchina)
                        {
                            couple = data.Split(',');
                            if (couple.Length != 2)
                            {
                                Console.WriteLine("Lunghezza non adatta, sicuro di non aver dimenticato un , o ;");
                            }
                            else
                            {
                                if (couple[0][0] != '#')
                                {
                                    //lettura configurazioni paricolari, variabili da customizzare
                                    if (couple[0] == "Tempi")
                                    {
                                        calcola_tempi = couple[1].Trim();
                                    }
                                    else if (couple[0] == "Pezzi")
                                    {
                                        calcola_pezzi = couple[1].Trim() == "C";
                                    }
                                    else if (couple[0] == "Programma")
                                    {
                                        calcola_programma = couple[1].Trim() == "C";
                                    }
                                    else if (couple[0] == "ProgrammaAuto")
                                    {
                                        cambio_programma = couple[1].Trim() == "S";
                                    }
                                    else if (couple[0] == "ProgrammaAutoAddr")
                                    {
                                        ConsigliaPrgAddress = couple[1].Trim();
                                    }

                                    

                                    else if (!AddConfToTable(couple[0], couple[1], tabella))
                                    {
                                        Logs.LogDB(new SqlConnection(), "macchina : "+nomemacchina+"; mancato inserimento in "+ tabella + " :" + " Tabella Inesistente, controllare di aver scritto correttamente il file di configurazione");
                                    }
                                }
                            }
                        }
                    }
                }
                Console.WriteLine("file letto correttamente");
                Console.ReadLine();
            }
        }

        public ChiaviTabellaOpc Tabella(string nometabella)
        {
            foreach (ChiaviTabellaOpc tab in Tabelle)
            {
                if (tab.nomeTab == nometabella)
                {
                    return tab;
                }
            }
            return new ChiaviTabellaOpc(nometabella);
        }
    }

}
