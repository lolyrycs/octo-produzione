﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace cns.utils
{
    class DbHelper
    {
        public static string cleanNullValue(object value)
        {
            if (value == null)
            {
                return "NULL";
            }
            else if (value.GetType().ToString() == "System.String")
            {
                return "'" + cleanMessSql((string)value) + "'";
            }
            else
            {
                return value + "";
            }
        }

        public static string cleanMessSql(string toremove)
        {
            toremove = toremove.Replace("'", " ");
            toremove = toremove.Replace("\"", " ");
            return toremove;
        }

        public static string NotNull(SqlDataReader reader, int indice, string type="string")
        {
            string result = "";
            if (!reader.IsDBNull(indice))
            {
                switch (type)
                {
                    case "string":
                        result = reader.GetString(indice);
                        break;
                    case "int":
                        result = reader.GetInt32(indice).ToString();
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public static int toInt(string toconvert)
        {
            int ret = 0;
            try
            {
                ret = Convert.ToInt32(toconvert);
            }
            finally
            {
               //Console.Writeline(ret);
            }
            return ret;
        }

    }
}
