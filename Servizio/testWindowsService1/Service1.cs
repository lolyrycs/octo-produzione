using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
using System.Configuration;
using cns.macchine.Fagor;
using cns.macchine.Fanuc;
using cns.macchine;
using cns.utils;


namespace cns
{
    #region object return of socket
    public class return_Socket
    {
        bool isValid = false; 
        string Value = "";
        int len = 0;
        public byte[] MsgBytes = new Byte[1024];
        public byte[] LenBytes = new Byte[512];
        public byte[] BoolBytes = new Byte[32];


        public return_Socket()
        {
            isValid = false;
            Value = "";
        }

        public void setmessage(string message, bool isv=false)
        {
            Value = message;
            isValid = isv;
            MsgBytes = Encoding.ASCII.GetBytes(message);
            len = MsgBytes.Length;
            LenBytes = Encoding.ASCII.GetBytes(len+"");
            BoolBytes = Encoding.ASCII.GetBytes(isv ?"0":"1");
        }

        public void setValid(bool isv=true)
        {
            isValid = isv;
            BoolBytes = Encoding.ASCII.GetBytes(isv ? "0" : "1");
        }
        
    }
    #endregion

    public partial class Service1 : ServiceBase
    {
        public bool debug=false;
        public static int num_Mac = 0;
        List<Macchina> allMachines = new List<Macchina>();
        //List<Socket> socketFagor = new List<Socket>();
        
        public static int?[] MAC_id = null;
        public static string[] MAC_tipoMacchina = null;
        public static string[] MAC_controlloNumerico = null;
        public static string[] MAC_ipAdress = null;
        public static volatile bool alive = true;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ArrayList threadList = new ArrayList();

        public volatile bool sock_istanziato = false;
        public IPEndPoint localEndPoint = null;
        public Socket listener = null;
        public Socket handler = null;
        public Thread threadSocket;


        public Service1()
        {
            InitializeComponent();
        }
        
        public void onDebug()
        {
            OnStart(null);
            debug = true;
        }
        
        protected override void OnStart(string[] args)
        {
            SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
           
            Logs.LogDB(connection, "ON START");
            //log.Info("ON START");

            string sqlQuery = null;
            SqlCommand command;
            SqlDataReader reader;
            try
            {
                //--- Conto il numero di Record in tabella Macchine DB
                using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    sqlQuery = "SELECT COUNT (MAC_id) FROM Macchine";
                    if (connection.State != ConnectionState.Open) connection.Open();
                    command = new SqlCommand(sqlQuery, connection);
                    num_Mac = Convert.ToInt32(command.ExecuteScalar());
                    command.Dispose();
                    if (connection.State != ConnectionState.Closed) connection.Close();
                }
                ControlloNumerico currentMacchinaF;
                //--- Se ho Record in DB ne leggo i valori e li salvo in un array
                using (connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""))
                {
                    if (num_Mac != 0)
                    {
                        sqlQuery = "SELECT MAC_id, MAC_tipoMacchina, MAC_controlloNumerico, MAC_ipAdress, MAC_tipoControllo , MAC_numeroAssi, MAC_nomeMacch, MAC_pathshare FROM Macchine WHERE MAC_attivo=1";
                        if (connection.State != ConnectionState.Open) connection.Open();
                        command = new SqlCommand(sqlQuery, connection);
                        reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                List<object> lista = new List<object>();
                                lista.Add(reader.GetInt32(0));
                                for (int i = 1; i < 8; i++)
                                {
                                    if (i != 5)
                                    {
                                        if (reader.IsDBNull(i))
                                        {
                                            lista.Add(null);
                                        }
                                        else
                                        {
                                            lista.Add(reader.GetString(i));
                                        }
                                    }
                                    else
                                    {
                                        if (reader.IsDBNull(5))
                                        {
                                            lista.Add(null);
                                        }
                                        else
                                        {
                                            lista.Add(reader.GetInt32(5));
                                        }
                                    }
                                }
                                
                                switch (reader.GetString(2) + "") {
                                    case "Fanuc":
                                        switch (reader.GetString(1) + "") {
                                            case "Robot":
                                                Robot currentMacchinaR = new Robot((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                allMachines.Add(currentMacchinaR);
                                                break;
                                            case "Tornio":
                                                Tornio currentMacchinaT = new Tornio((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                allMachines.Add(currentMacchinaT);
                                                break;
                                            case "Fresa":
                                                cns.macchine.Fanuc.Fresa  currentMacchinaFr = new cns.macchine.Fanuc.Fresa((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                allMachines.Add(currentMacchinaFr);
                                                break;
                                        }
                                        break;
                                    case "Fagor":
                                        switch (reader.GetString(1) + "")
                                        {
                                            case "Fresa":
                                                cns.macchine.Fagor.Fresa currentMacchinaFg = new cns.macchine.Fagor.Fresa((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                allMachines.Add(currentMacchinaFg);
                                                break;
                                        }
                                        break;
                                    case "Mitsubishi":
                                        switch (reader.GetString(1) + "")
                                        {
                                            case "Rettifica":
                                                cns.macchine.Mitsubishi currentMacchinaM = new cns.macchine.Mitsubishi((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                allMachines.Add(currentMacchinaM);
                                                break;
                                        }
                                        break;
                                    case "Heidenhain":
                                        switch (reader.GetString(1) + "")
                                        {
                                            case "Fresa":
                                                if (ConfigurationSettings.AppSettings["Heiden_test"] == "True") {
                                                    cns.macchine.Heidenhain.Fresa currentMacchinaH = new cns.macchine.Heidenhain.Fresa((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                                    allMachines.Add(currentMacchinaH);
                                                }
                                                break;
                                        }
                                        break;
                                    case "Opc":
                                        macchine.OpcuaCN.OpcUa currentMacchinaO = new macchine.OpcuaCN.OpcUa((int)lista[0], (string)lista[1], (string)lista[2], (string)lista[3], (string)lista[4], (int?)lista[5], (string)lista[6], (string)lista[7]);
                                        allMachines.Add(currentMacchinaO);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            reader.NextResult();
                        }
                        reader.Close();
                        command.Dispose();
                        if (connection.State != ConnectionState.Closed) connection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + ""), ex.Message + " " + ex.StackTrace);
            }
          
            Thread newThread = new Thread(Main);
            newThread.Start();
            threadList.Add(newThread);

            Logs.LogDB(connection, "END START");
        }

        protected override void OnStop()
        {
            alive = false;

            /*foreach(Thread thread in threadList)
            {
                thread.Join();
            }*/

            //handler.Shutdown(SocketShutdown.Both);
            //handler.Close();


            SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");



            /*
            foreach (Thread threadrunning in threadList)
            {
                if (threadrunning.IsAlive)
                {
                    threadrunning.Abort();
                }
            }
            */
            
            Logs.LogDB(connection, "ON STOP");
        }

        //--- Controllo che tipo di CN è montato in macchina poi richiamo i processi per leggere i dati
        private void Main()
        {
            threadSocket = new Thread(Socket);
            threadSocket.Start();
            threadList.Add(threadSocket);

            int aa = 0;
            foreach (Macchina ccMachine in allMachines) {
                
                
                Thread threadPing = new Thread(ccMachine.ping);
                threadPing.Start();
                threadList.Add(threadPing);
                

                

                if (ccMachine.MAC_controlloNumerico == "Fagor"||ccMachine.MAC_controlloNumerico=="Mitsubishi")
                {
                    Thread threadInterfaccia = new Thread(ccMachine.gestioneInterfaccia);
                    threadInterfaccia.Start(aa);
                    threadList.Add(threadInterfaccia);

                    Thread threadProduzione = new Thread(ccMachine.gestioneProduzione);
                    threadProduzione.Start(aa);
                    threadList.Add(threadProduzione);
                }
                else
                {
                    Thread threadInterfaccia = new Thread(ccMachine.gestioneInterfaccia);
                    threadInterfaccia.Start();
                    threadList.Add(threadInterfaccia);

                    Thread threadDiagnostica = new Thread(ccMachine.gestioneDiagnostica);
                    threadDiagnostica.Start();
                    threadList.Add(threadDiagnostica);

                    Thread threadProduzione = new Thread(ccMachine.gestioneProduzione);
                    threadProduzione.Start();
                    threadList.Add(threadProduzione);
                }
                aa++;
            }
		}

        
        //funzione per il collegamento in Socket con il gest web
        public void Socket()
        {
            short ret;
            bool valid = false;
            bool connesso = false;
            string data;
            bool res = false;
            int id = -1;
            int len_recived = 0;
            byte[] bytes = new Byte[2048];
            Macchina mac_corrente = new Macchina();

            return_Socket retSocket = new return_Socket();

            while (alive)
            {
                sock_istanziato = false;
                try {

                    // Create a new TCP/IP socket.
                    try
                    {
                        if (localEndPoint == null)
                            localEndPoint = new IPEndPoint(IPAddress.Any, 50007);
                        if (listener == null)
                        {
                            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            // Bind the socket to the local endpoint and listen for incoming connections.
                            listener.Bind(localEndPoint);
                            listener.Listen(10);
                        }
                        if (handler == null)
                            handler = new Socket(SocketType.Stream, ProtocolType.Tcp);
                        sock_istanziato = true;

                        retSocket = new return_Socket();
                    }
                    catch (Exception e)
                    {
                        Logs.LogDB(new SqlConnection(), e.Message + " " + e.StackTrace);
                        sock_istanziato = false;
                        listener = null;
                        handler = null;
                        localEndPoint = null;
                    }
                 

                    // Start listening for connections.
                    while (sock_istanziato)
                    {
                        if (!connesso)
                        {
                            //Console.WriteLine("Waiting for a connection...");
                            handler = listener.Accept();
                            connesso = true;
                        }
                        data = null;
                        //Console.WriteLine("Connected");
                        //Logs.LogDB(new SqlConnection(), "connesso per invio file");
                        bytes = new byte[1024];
                        int bytesRec = handler.Receive(bytes);
                        string prova = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                        //var details = JObject.Parse(prova);
                        int protocol = Convert.ToInt32(prova);

                        switch (protocol)
                        {
                            case 1:
                                //ricezione id macchina, invio path delle cartelle
                                try
                                {

                                    //lettura id e selezione macchina
                                    bytes = new byte[2048];

                                    //lettura lunghezza messaggio 
                                    bytesRec = handler.Receive(bytes);
                                    len_recived = Convert.ToInt32(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                                    Console.WriteLine(len_recived);
                                    bytes = new byte[len_recived];
                                   
                                    bytesRec = handler.Receive(bytes);
                                    id = Convert.ToInt32(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                                    Console.WriteLine(id);
                                    foreach (Macchina mac in allMachines)
                                    {
                                        if (mac.MAC_id == id)
                                        {
                                            valid = true;
                                            mac_corrente = mac;
                                        }
                                    }
                                    //invio path della macchina
                                    if (valid)
                                    {
                                        valid = false;
                                        if (mac_corrente.MAC_controlloNumerico == "Fanuc")
                                        {

                                            mac_corrente.getPath();
                                            //Console.WriteLine("Text received : {0}", id);
                                            if (mac_corrente.JSON_path != "")
                                            {
                                                retSocket.setmessage(mac_corrente.JSON_path, true);
                                                valid = true;
                                            }
                                            else retSocket.setmessage("Lettura path macchina non riuscito");
                                        }
                                        else retSocket.setmessage("Attenzione, il tipo della Macchina deve essere Fanuc");
                                    }
                                    else retSocket.setmessage("Macchina non connessa, riprovare o riavviare il servizio");
                                }
                                catch (Exception e)
                                {
                                    retSocket.setmessage("Errore ricezione dati per gestione directory, riprovare");
                                    valid = false;
                                }

                                if (!valid) Logs.LogDB(new SqlConnection(), "Errore protocollo get_path in socket programmi Fanuc", mac_corrente.MAC_id);

                                //invio risposta protocollo gestione directory
                                try
                                {
                                    handler.Send(retSocket.BoolBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.LenBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.MsgBytes);
                                    valid = true;
                                }
                                catch (Exception e)
                                {
                                    Logs.LogDB(new SqlConnection(), "Errore invio risposta gestione directory a django", mac_corrente.MAC_id);
                                    valid = false;
                                }
                                break;
                            
                            case 2:
                                //ricezione del path di destinazione, ricezione programma
                                try
                                {
                                    if (valid)
                                    {
                                        string path, text_file;
                                        bytes = new byte[2048];
                                        retSocket.setmessage("Errore ricezione dati per upload, riprovare");
                                        //lettura lunghezza path 
                                        bytesRec = handler.Receive(bytes);
                                        len_recived = Convert.ToInt32(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                                        bytes = new byte[len_recived];
                                        //lettura path
                                        bytesRec = handler.Receive(bytes);
                                        path = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                                        //lettura lunghezza file
                                        bytesRec = handler.Receive(bytes);
                                        len_recived = Convert.ToInt32(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                                        bytes = new byte[len_recived];
                                        //lettura file 
                                        bytesRec = handler.Receive(bytes);
                                        text_file = Encoding.ASCII.GetString(bytes, 0, bytesRec);

                                        valid = false;
                                        retSocket.setmessage("Lettura macchina non corretta, riprovare");
                                        if (mac_corrente.MAC_controlloNumerico == "Fanuc")
                                        {
                                            valid = mac_corrente.uploadfile(path, text_file);
                                            //validazione riuscita upload
                                            if (!valid) retSocket.setmessage("Upload file non riuscito, controllare che il file non sia presente in macchina");
                                            else retSocket.setmessage("Upload avvenuto con successo", true);
                                        }
                                        else retSocket.setmessage("Attenzione, il tipo della Macchina deve essere Fanuc");
                                    }
                                    else retSocket.setmessage("Lettura macchina non corretta, riprovare");
                                }
                                catch (Exception e)
                                {
                                    valid = false;
                                }

                                if (!valid) Logs.LogDB(new SqlConnection(), "Errore protocollo upload in socket programmi Fanuc", mac_corrente.MAC_id);

                                //invio risposta protocollo gestione directory
                                try
                                {
                                    handler.Send(retSocket.BoolBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.LenBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.MsgBytes);
                                    valid = true;
                                }
                                catch (Exception e)
                                {
                                    Logs.LogDB(new SqlConnection(), "Errore invio risposta gestione directory a django", mac_corrente.MAC_id);
                                    valid = false;
                                }
                                break;
                            //ricezione path, invio del file
                            case 3:
                                try
                                {
                                    //if pass:
                                    if (valid)
                                    {
                                        string path, text_file;
                                        text_file = "";
                                        bytes = new byte[1024];
                                        retSocket.setmessage("Errore ricezione dati per download, riprovare");
                                        //lettura lunghezza messaggio 
                                        bytesRec = handler.Receive(bytes);
                                        len_recived = Convert.ToInt32(Encoding.ASCII.GetString(bytes, 0, bytesRec));
                                        bytes = new byte[len_recived];

                                        //lettura path
                                        bytesRec = handler.Receive(bytes);
                                        path = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                                        //lettura file da macchina Fanuc
                                        valid = false;
                                        retSocket.setmessage("Lettura macchina non corretta, riprovare");
                                        if (mac_corrente.MAC_controlloNumerico == "Fanuc") text_file = mac_corrente.DownloadFile(path);
                                        else retSocket.setmessage("Attenzione, il tipo della Macchina deve essere Fanuc");
                                        valid = !(text_file == "");

                                        if (!valid) retSocket.setmessage("Download file non riuscito, controllare il percorso del file");
                                        else
                                        {
                                            retSocket.setmessage(text_file, true);
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    valid = false;
                                }

                                if (!valid) Logs.LogDB(new SqlConnection(), "Errore protocollo download in socket programmi Fanuc", mac_corrente.MAC_id);

                                //invio risposta protocollo gestione directory
                                try
                                {
                                    handler.Send(retSocket.BoolBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.LenBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.MsgBytes);
                                    valid = true;
                                }
                                catch (Exception e)
                                {
                                    Logs.LogDB(new SqlConnection(), "Errore invio risposta gestione directory a django", mac_corrente.MAC_id);
                                    valid = false;
                                }
                                break;
                            default:
                                retSocket.setmessage("Errore ricezione protocollo, riprovare");
                                Logs.LogDB(new SqlConnection(), "Protocollo non valido", mac_corrente.MAC_id);
                                try
                                {
                                    handler.Send(retSocket.BoolBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.LenBytes);
                                    Thread.Sleep(500);
                                    handler.Send(retSocket.MsgBytes);
                                    valid = true;
                                }
                                catch (Exception e)
                                {
                                    Logs.LogDB(new SqlConnection(), "Errore invio risposta gestione directory a django", mac_corrente.MAC_id);
                                    valid = false;
                                }
                                valid = false;
                                break;
                        }
                        //close socket
                        try
                        {
                            if (handler != null && sock_istanziato)
                            {
                                handler.Shutdown(SocketShutdown.Both);
                                handler.Close();
                                handler = null;
                            }
                            if (listener != null && sock_istanziato)
                            {
                                //listener.Shutdown(SocketShutdown.Both);//errore ricezione path
                                listener.Close();
                                listener = null;
                                localEndPoint = null;
                            }
                            sock_istanziato = false;

                        }
                        catch (Exception ex)
                        {
                            sock_istanziato = false;
                            if (mac_corrente.MAC_id == -1)
                                Logs.LogDB(new SqlConnection(), ex.ToString());
                            else
                                Logs.LogDB(new SqlConnection(), ex.ToString(), mac_corrente.MAC_id);
                        }   
                        connesso = false;
                    }
                }
                catch (Exception e)
                {
                    connesso = false;
                    
                    Logs.LogDB(new SqlConnection(), e.ToString());
                    //close the socket if a problem is detected
                    try
                    {
                        if (handler != null && sock_istanziato)
                        {
                            handler.Shutdown(SocketShutdown.Both);
                            handler.Close();
                            handler = null;
                        }

                        if (listener != null && sock_istanziato)
                        {
                            listener.Close();
                            listener = null;
                            localEndPoint = null;
                        }
                        sock_istanziato = false;
                    }
                    catch (Exception ex)
                    {
                        sock_istanziato = false;
                        if (mac_corrente.MAC_id == -1)
                            Logs.LogDB(new SqlConnection(), ex.ToString());
                        else
                            Logs.LogDB(new SqlConnection(), ex.ToString(), mac_corrente.MAC_id);
                    }
                }
                
                Thread.Sleep(2000);
            }
            
            
        }

        

        // funzione per la gestione degli allarmi macchina
        public static int AlarmDB(SqlConnection mysqlCon, string numeroallarme = null, string messaggioallarme = null, string numerooperatore = null, string messaggiooperatore = null, int? macId = null)
        {
            try
            {
                mysqlCon = new SqlConnection(ConfigurationSettings.AppSettings["msqkconnectionstring"] + "");
                using (mysqlCon)
                {
                    if (macId == null)
                    {
                        Logs.LogDB(mysqlCon, "Funzione Alarm: macId non nullabile");
                        return -1;
                    }
                    /* modifica di lorenzo, cleanNullValue gestisce anche il null
                    if (messaggioallarme != null)
                        messaggioallarme = "'" + messaggioallarme + "'";
                    if (numeroallarme != null)
                        numeroallarme = "'" + numeroallarme + "'";
                    if (numerooperatore != null)
                        numerooperatore = "'" + numerooperatore + "'";
                    if (messaggiooperatore != null)
                        messaggiooperatore = "'" + messaggiooperatore + "'";*/

                    string sqlQuery = "INSERT INTO Allarmi (ALL_date, ALL_numeroAllarme, ALL_messaggioAllarme, ALL_numeroWarning, ALL_messaggioWarning, ALL_vs_MAC_id)" +
                                                " values (GETDATE(), " + DbHelper.cleanNullValue(numeroallarme) + ", " + DbHelper.cleanNullValue(messaggioallarme) + "," + DbHelper.cleanNullValue(numerooperatore) + "," + DbHelper.cleanNullValue(messaggiooperatore) + ", " + macId + ")";
                    SqlCommand command = new SqlCommand(sqlQuery, mysqlCon);
                    if (mysqlCon.State != ConnectionState.Open)
                        mysqlCon.Open();
                    int N = command.ExecuteNonQuery();
                    command.Dispose();
                    if (mysqlCon.State != ConnectionState.Closed)
                        mysqlCon.Close();
                    return N;
                }
            }
            catch (Exception ex)
            {
                Logs.LogDB(mysqlCon, "errore nella scrittura in allarmi"+ex.Message+" "+ex.StackTrace, macId);
                return -1;
            }
        }
    }
}