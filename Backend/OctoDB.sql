USE [octoClienti]
GO
/****** Object:  Table [dbo].[Allarmi]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Allarmi](
	[ALL_date] [datetime] NULL,
	[ALL_numeroAllarme] [varchar](1000) NULL,
	[ALL_messaggioAllarme] [varchar](1000) NULL,
	[ALL_numeroWarning] [varchar](1000) NULL,
	[ALL_messaggioWarning] [varchar](1000) NULL,
	[ALL_vs_MAC_id] [int] NOT NULL,
	[ALL_id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Allarmi] PRIMARY KEY CLUSTERED 
(
	[ALL_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group_permissions]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_group_permissions_group_id_permission_id_0cd325b0_uniq] UNIQUE NONCLUSTERED 
(
	[group_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_permission]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_permission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[codename] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_permission_content_type_id_codename_01ab375a_uniq] UNIQUE NONCLUSTERED 
(
	[content_type_id] ASC,
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[last_login] [datetime] NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](30) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_username_6821ab7c_uniq] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user_groups]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user_groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_groups_user_id_group_id_94350c0c_uniq] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user_user_permissions]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user_user_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_user_permissions_user_id_permission_id_14a6b632_uniq] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[authtoken_token]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[authtoken_token](
	[key] [nvarchar](40) NOT NULL,
	[created] [datetime] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cicloproduzione]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cicloproduzione](
	[CPROD_id] [int] NOT NULL,
	[CPROD_vs_MAC_id] [int] NOT NULL,
	[CPROD_commessa] [nvarchar](50) NOT NULL,
	[CPROD_importFrom] [nvarchar](50) NOT NULL,
	[CPROD_status] [int] NOT NULL,
	[CPROD_pezziFatti] [int] NOT NULL,
	[CPROD_lotto] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Cicloproduzione] PRIMARY KEY CLUSTERED 
(
	[CPROD_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Componenti]    Script Date: 09/05/2018 15:51:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Componenti](
	[COM_id] [int] IDENTITY(1,1) NOT NULL,
	[COM_nome] [nchar](10) NOT NULL,
	[COM_chiave] [nchar](10) NULL,
	[COM_vita] [nchar](10) NULL,
	[COM_codice] [nchar](10) NULL,
 CONSTRAINT [PK_Componenti] PRIMARY KEY CLUSTERED 
(
	[COM_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_admin_log]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_admin_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_time] [datetime] NOT NULL,
	[object_id] [nvarchar](max) NULL,
	[object_repr] [nvarchar](200) NOT NULL,
	[action_flag] [smallint] NOT NULL,
	[change_message] [nvarchar](max) NOT NULL,
	[content_type_id] [int] NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_content_type]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_content_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app_label] [nvarchar](100) NOT NULL,
	[model] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [django_content_type_app_label_model_76bd3d3b_uniq] UNIQUE NONCLUSTERED 
(
	[app_label] ASC,
	[model] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_migrations]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[applied] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_session]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_session](
	[session_key] [nvarchar](40) NOT NULL,
	[session_data] [nvarchar](max) NOT NULL,
	[expire_date] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[session_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[guardian_groupobjectpermission]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[guardian_groupobjectpermission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[object_pk] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [guardian_groupobjectpermission_group_id_permission_id_object_pk_3f189f7c_uniq] UNIQUE NONCLUSTERED 
(
	[group_id] ASC,
	[permission_id] ASC,
	[object_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[guardian_userobjectpermission]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[guardian_userobjectpermission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[object_pk] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [guardian_userobjectpermission_user_id_permission_id_object_pk_b0b3d2fc_uniq] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_id] ASC,
	[object_pk] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Input]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input](
	[IN_id] [int] IDENTITY(1,1) NOT NULL,
	[IN_commessa] [nchar](50) NULL,
	[IN_lotto] [nchar](10) NULL,
	[IN_priorita] [int] NULL,
	[IN_importFrom] [nchar](10) NULL,
	[IN_status] [tinyint] NULL,
	[IN_data] [datetime] NULL,
	[IN_vs_USER_id] [int] NULL,
	[IN_programma] [nchar](10) NULL,
	[IN_vs_CPROD_id] [int] NULL,
 CONSTRAINT [PK_Produzione_In] PRIMARY KEY CLUSTERED 
(
	[IN_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Interfaccia]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Interfaccia](
	[INT_id] [int] IDENTITY(1,1) NOT NULL,
	[INT_ipAdress] [varchar](20) NULL,
	[INT_status] [varchar](50) NULL,
	[INT_programmaAttivo] [varchar](50) NULL,
	[INT_feedAssi] [int] NULL,
	[INT_feedMandrino] [int] NULL,
	[INT_utensileAttivo] [varbinary](20) NULL,
	[INT_pezziFatti] [int] NULL,
	[INT_vs_MAC_id] [int] NULL,
	[INT_path] [int] NULL,
	[INT_date] [datetime] NULL,
	[INT_Totem] [nvarchar](10) NULL,
 CONSTRAINT [PK_Intefaccia] PRIMARY KEY CLUSTERED 
(
	[INT_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Letmanutenzione]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Letmanutenzione](
	[LMAN_Id] [int] NOT NULL,
	[LMAN_data] [datetime] NOT NULL,
	[LMAN_vs_MAC_id] [int] NOT NULL,
 CONSTRAINT [PK_Letmanutenzione] PRIMARY KEY CLUSTERED 
(
	[LMAN_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Logs]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Logs](
	[LOG_id] [int] IDENTITY(1,1) NOT NULL,
	[LOG_fk_USER_id] [int] NULL,
	[LOG_fk_MAC_id] [int] NULL,
	[LOG_description] [varchar](max) NULL,
	[LOG_status] [int] NULL,
	[LOG_date] [datetime] NULL,
 CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED 
(
	[LOG_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Macchine]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Macchine](
	[MAC_id] [int] IDENTITY(1,1) NOT NULL,
	[MAC_ipAdress] [varchar](20) NULL,
	[MAC_ipAdress2] [varchar](20) NULL,
	[MAC_nomeMacch] [varchar](20) NULL,
	[MAC_soprannomeMacch] [varchar](20) NULL,
	[MAC_tipoMacchina] [varchar](20) NULL,
	[MAC_controlloNumerico] [varchar](20) NULL,
	[MAC_tipoControllo] [varchar](5) NULL,
	[MAC_packegTipo] [varchar](5) NULL,
	[MAC_reparto] [int] NULL,
	[MAC_imagePath] [varchar](250) NULL,
	[MAC_image] [image] NULL,
	[MAC_serialNumber] [varchar](20) NULL,
	[MAC_boolCamera] [varchar](20) NULL,
	[MAC_ipAdressCamera] [varchar](20) NULL,
	[MAC_portaCamera] [varchar](20) NULL,
	[MAC_comandoCamera] [varchar](20) NULL,
	[MAC_modeCamera] [varchar](20) NULL,
	[MAC_userCamera] [varchar](20) NULL,
	[MAC_passCamera] [varchar](20) NULL,
	[MAC_userUsername] [varchar](50) NULL,
	[MAC_userPassword] [varchar](50) NULL,
	[MAC_vs_SER_id] [int] NULL,
	[MAC_numeroAssi] [int] NULL,
	[MAC_statoConnessione] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MAC_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manutenzione]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manutenzione](
	[MAN_tempMandrino] [int] NULL,
	[MAN_tempAsseX] [int] NULL,
	[MAN_tempAsseY] [int] NULL,
	[MAN_tempAsseZ] [int] NULL,
	[MAN_tempAsseB] [int] NULL,
	[MAN_tempAsseC] [int] NULL,
	[MAN_tempEncX] [int] NULL,
	[MAN_tempEncY] [int] NULL,
	[MAN_tempEncZ] [int] NULL,
	[MAN_tempEncB] [int] NULL,
	[MAN_tempEncC] [int] NULL,
	[MAN_loadMandrino] [int] NULL,
	[MAN_loadAsseX] [int] NULL,
	[MAN_loadAsseY] [int] NULL,
	[MAN_loadAsseZ] [int] NULL,
	[MAN_loadAsseB] [int] NULL,
	[MAN_loadAsseC] [int] NULL,
	[MAN_rmpMandrino] [int] NULL,
	[MAN_data] [datetime] NULL,
	[MAN_metriLavoroX] [int] NULL,
	[MAN_metriRapidoX] [int] NULL,
	[MAN_metriLavoroY] [int] NULL,
	[MAN_metriRapidoY] [int] NULL,
	[MAN_metriLavoroZ] [int] NULL,
	[MAN_metriRapidoZ] [int] NULL,
	[MAN_metriLavoroB] [int] NULL,
	[MAN_metriRapidoB] [int] NULL,
	[MAN_metriLavoroC] [int] NULL,
	[MAN_metriRapidoC] [int] NULL,
	[MAN_metriX] [int] NULL,
	[MAN_metriY] [int] NULL,
	[MAN_metriZ] [int] NULL,
	[MAN_metriB] [int] NULL,
	[MAN_metriC] [int] NULL,
	[MAN_sbloccaggiCU] [int] NULL,
	[MAN_cambiEseguiti] [int] NULL,
	[MAN_oreLavoro] [int] NULL,
	[MAN_oreAccensione] [int] NULL,
	[MAN_rpm1] [int] NULL,
	[MAN_rpm2] [int] NULL,
	[MAN_rpm3] [int] NULL,
	[MAN_rpm4] [int] NULL,
	[MAN_rpm5] [int] NULL,
	[MAN_timeOverLimit] [int] NULL,
	[MAN_limitX] [int] NULL,
	[MAN_limitY] [int] NULL,
	[MAN_limitZ] [int] NULL,
	[MAN_limitB] [int] NULL,
	[MAN_limitC] [int] NULL,
	[MAN_id] [int] IDENTITY(1,1) NOT NULL,
	[MAN_vs_MAC_id] [int] NOT NULL,
	[MAN_opPinza1Grezzo] [int] NULL,
	[MAN_clPinza1Grezzo] [int] NULL,
	[MAN_opPinza1Finiti] [int] NULL,
	[MAN_clPinza1Finiti] [int] NULL,
	[MAN_opPinza2Grezzo] [int] NULL,
	[MAN_clPinza2Grezzo] [int] NULL,
	[MAN_opPinza2Finiti] [int] NULL,
	[MAN_clPinza2Finiti] [int] NULL,
	[MAN_violazioniGialla] [int] NULL,
	[MAN_violazioniRossa] [int] NULL,
	[MAN_opPinzaPallet] [int] NULL,
	[MAN_clPinzaPallet] [int] NULL,
	[MAN_cestelliCaricati] [int] NULL,
	[MAN_pzPrelevati] [int] NULL,
	[MAN_pzDepositati] [int] NULL,
	[MAN_fineLotto] [int] NULL,
	[MAN_accInterventoTermicaNastro] [int] NULL,
	[MAN_accTeleruttoreNastro] [int] NULL,
	[MAN_accLetturaProxy] [int] NULL,
 CONSTRAINT [PK_Manutenzione] PRIMARY KEY CLUSTERED 
(
	[MAN_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manutenzione2]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manutenzione2](
	[MAN2_id] [int] NOT NULL,
	[MAN2_chiave] [nvarchar](50) NOT NULL,
	[MAN2_valore] [nvarchar](50) NOT NULL,
	[MAN2_vs_LMAN_id] [int] NOT NULL,
 CONSTRAINT [PK_Manutezione2] PRIMARY KEY CLUSTERED 
(
	[MAN2_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Marcature]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Marcature](
	[MAR_id] [int] NOT NULL,
	[MAR_vs_OPE_id] [int] NOT NULL,
	[MAR_vs_MAC_id] [int] NOT NULL,
	[MAR_vs_OUT_id] [int] NULL,
	[MAR_date] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Operatori]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operatori](
	[OPE_id] [int] NOT NULL,
	[OPE_badge] [nchar](10) NULL,
	[OPE_date] [datetime] NULL,
	[OPE_custom1] [nchar](10) NULL,
	[OPE_custom2] [nchar](10) NULL,
 CONSTRAINT [PK_Operatori] PRIMARY KEY CLUSTERED 
(
	[OPE_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Output]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Output](
	[OUT_id] [int] IDENTITY(1,1) NOT NULL,
	[OUT_tempoLavoro] [nchar](10) NULL,
	[OUT_tempoPiazzamento] [nchar](10) NULL,
	[OUT_tempoPausa] [nchar](10) NULL,
	[OUT_efficienza] [nchar](10) NULL,
	[OUT_priorita] [nchar](10) NULL,
	[OUT_vs_CPROD_id] [int] NOT NULL,
	[OUT_data] [datetime] NULL,
 CONSTRAINT [PK_Produzione_Out] PRIMARY KEY CLUSTERED 
(
	[OUT_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produzione]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produzione](
	[PROD_id] [int] IDENTITY(1,1) NOT NULL,
	[PROD_lotto] [nchar](10) NULL,
	[PROD_tempoLavoro] [nchar](10) NULL,
	[PROD_tempoPiazzamento] [nchar](10) NULL,
	[PROD_tempoPausa] [nchar](10) NULL,
	[PROD_efficienza] [nchar](10) NULL,
	[PROD_priorita] [int] NULL,
	[PROD_data] [datetime] NULL,
	[PROD_vs_CPROD_id] [int] NOT NULL,
 CONSTRAINT [PK_Produzione] PRIMARY KEY CLUSTERED 
(
	[PROD_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reparti]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reparti](
	[REP_id] [int] IDENTITY(1,1) NOT NULL,
	[REP_descr] [nvarchar](50) NULL,
	[REP_name] [nvarchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[REP_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Serie]    Script Date: 09/05/2018 15:51:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Serie](
	[SER_id] [int] IDENTITY(1,1) NOT NULL,
	[SER_nome] [nchar](10) NULL,
	[SER_costruttore] [nchar](10) NULL,
 CONSTRAINT [PK_Serie] PRIMARY KEY CLUSTERED 
(
	[SER_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SerieMacchine_vs_Componenti]    Script Date: 09/05/2018 15:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SerieMacchine_vs_Componenti](
	[SERMAC_id] [int] NOT NULL,
	[SERMAC_vs_COM_id] [int] NOT NULL,
	[SERMAC_vs_MAC_id] [int] NULL,
	[SERMAC_vs_SER_id] [int] NULL,
 CONSTRAINT [PK_SerieMacchine_vs_Componenti] PRIMARY KEY CLUSTERED 
(
	[SERMAC_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 09/05/2018 15:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[SET_id] [int] IDENTITY(1,1) NOT NULL,
	[SET_descr] [nvarchar](50) NULL,
	[SET_name] [nvarchar](20) NULL,
	[SET_email] [nvarchar](50) NULL,
	[SET_email_port] [nvarchar](50) NULL,
	[SET_email_ip] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[SET_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/05/2018 15:51:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[USER_id] [int] IDENTITY(1,1) NOT NULL,
	[USER_username] [nvarchar](50) NOT NULL,
	[USER_pwd] [nvarchar](50) NOT NULL,
	[USER_mail] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[USER_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [auth_group_permissions_group_id_b120cbf9]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_group_permissions_group_id_b120cbf9] ON [dbo].[auth_group_permissions]
(
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_group_permissions_permission_id_84c5c92e]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_group_permissions_permission_id_84c5c92e] ON [dbo].[auth_group_permissions]
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_permission_content_type_id_2f476e4b]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_permission_content_type_id_2f476e4b] ON [dbo].[auth_permission]
(
	[content_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_user_groups_group_id_97559544]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_user_groups_group_id_97559544] ON [dbo].[auth_user_groups]
(
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_user_groups_user_id_6a12ed8b]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_user_groups_user_id_6a12ed8b] ON [dbo].[auth_user_groups]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_user_user_permissions_permission_id_1fbb5f2c]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_user_user_permissions_permission_id_1fbb5f2c] ON [dbo].[auth_user_user_permissions]
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [auth_user_user_permissions_user_id_a95ead1b]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [auth_user_user_permissions_user_id_a95ead1b] ON [dbo].[auth_user_user_permissions]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [django_admin_log_content_type_id_c4bce8eb]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [django_admin_log_content_type_id_c4bce8eb] ON [dbo].[django_admin_log]
(
	[content_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [django_admin_log_user_id_c564eba6]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [django_admin_log_user_id_c564eba6] ON [dbo].[django_admin_log]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [django_session_expire_date_a5c62663]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [django_session_expire_date_a5c62663] ON [dbo].[django_session]
(
	[expire_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_groupobjectpermission_content_type_id_7ade36b8]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_groupobjectpermission_content_type_id_7ade36b8] ON [dbo].[guardian_groupobjectpermission]
(
	[content_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_groupobjectpermission_group_id_4bbbfb62]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_groupobjectpermission_group_id_4bbbfb62] ON [dbo].[guardian_groupobjectpermission]
(
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_groupobjectpermission_permission_id_36572738]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_groupobjectpermission_permission_id_36572738] ON [dbo].[guardian_groupobjectpermission]
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_userobjectpermission_content_type_id_2e892405]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_userobjectpermission_content_type_id_2e892405] ON [dbo].[guardian_userobjectpermission]
(
	[content_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_userobjectpermission_permission_id_71807bfc]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_userobjectpermission_permission_id_71807bfc] ON [dbo].[guardian_userobjectpermission]
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [guardian_userobjectpermission_user_id_d5c1e964]    Script Date: 09/05/2018 15:51:53 ******/
CREATE NONCLUSTERED INDEX [guardian_userobjectpermission_user_id_d5c1e964] ON [dbo].[guardian_userobjectpermission]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Allarmi]  WITH CHECK ADD  CONSTRAINT [FK_Allarmi_Macchine] FOREIGN KEY([ALL_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Allarmi] CHECK CONSTRAINT [FK_Allarmi_Macchine]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_permission]  WITH CHECK ADD  CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[auth_permission] CHECK CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[auth_user_groups]  WITH CHECK ADD  CONSTRAINT [auth_user_groups_group_id_97559544_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_user_groups] CHECK CONSTRAINT [auth_user_groups_group_id_97559544_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_user_groups]  WITH CHECK ADD  CONSTRAINT [auth_user_groups_user_id_6a12ed8b_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[auth_user_groups] CHECK CONSTRAINT [auth_user_groups_user_id_6a12ed8b_fk_auth_user_id]
GO
ALTER TABLE [dbo].[auth_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [auth_user_user_permissions_permission_id_1fbb5f2c_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_user_user_permissions] CHECK CONSTRAINT [auth_user_user_permissions_permission_id_1fbb5f2c_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[auth_user_user_permissions] CHECK CONSTRAINT [auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id]
GO
ALTER TABLE [dbo].[authtoken_token]  WITH CHECK ADD  CONSTRAINT [authtoken_token_user_id_35299eff_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[authtoken_token] CHECK CONSTRAINT [authtoken_token_user_id_35299eff_fk_auth_user_id]
GO
ALTER TABLE [dbo].[Cicloproduzione]  WITH CHECK ADD  CONSTRAINT [FK_Cicloproduzione_Macchine] FOREIGN KEY([CPROD_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Cicloproduzione] CHECK CONSTRAINT [FK_Cicloproduzione_Macchine]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_user_id_c564eba6_fk] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_user_id_c564eba6_fk]
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_groupobjectpermission_content_type_id_7ade36b8_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission] CHECK CONSTRAINT [guardian_groupobjectpermission_content_type_id_7ade36b8_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_groupobjectpermission_group_id_4bbbfb62_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission] CHECK CONSTRAINT [guardian_groupobjectpermission_group_id_4bbbfb62_fk_auth_group_id]
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_groupobjectpermission_permission_id_36572738_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[guardian_groupobjectpermission] CHECK CONSTRAINT [guardian_groupobjectpermission_permission_id_36572738_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[guardian_userobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_userobjectpermission_content_type_id_2e892405_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[guardian_userobjectpermission] CHECK CONSTRAINT [guardian_userobjectpermission_content_type_id_2e892405_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[guardian_userobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_userobjectpermission_permission_id_71807bfc_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[guardian_userobjectpermission] CHECK CONSTRAINT [guardian_userobjectpermission_permission_id_71807bfc_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[guardian_userobjectpermission]  WITH CHECK ADD  CONSTRAINT [guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[guardian_userobjectpermission] CHECK CONSTRAINT [guardian_userobjectpermission_user_id_d5c1e964_fk_auth_user_id]
GO
ALTER TABLE [dbo].[Input]  WITH CHECK ADD  CONSTRAINT [FK_Input_Cicloproduzione] FOREIGN KEY([IN_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Input] CHECK CONSTRAINT [FK_Input_Cicloproduzione]
GO
ALTER TABLE [dbo].[Input]  WITH CHECK ADD  CONSTRAINT [FK_Input_Users] FOREIGN KEY([IN_vs_USER_id])
REFERENCES [dbo].[Users] ([USER_id])
GO
ALTER TABLE [dbo].[Input] CHECK CONSTRAINT [FK_Input_Users]
GO
ALTER TABLE [dbo].[Interfaccia]  WITH CHECK ADD  CONSTRAINT [FK_Interfaccia_Macchine] FOREIGN KEY([INT_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Interfaccia] CHECK CONSTRAINT [FK_Interfaccia_Macchine]
GO
ALTER TABLE [dbo].[Letmanutenzione]  WITH CHECK ADD  CONSTRAINT [FK_Letmanutenzione_Macchine] FOREIGN KEY([LMAN_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Letmanutenzione] CHECK CONSTRAINT [FK_Letmanutenzione_Macchine]
GO
ALTER TABLE [dbo].[Logs]  WITH CHECK ADD  CONSTRAINT [FK_Logs_Macchine] FOREIGN KEY([LOG_fk_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Logs] CHECK CONSTRAINT [FK_Logs_Macchine]
GO
ALTER TABLE [dbo].[Macchine]  WITH CHECK ADD  CONSTRAINT [FK_Macchine_Reparti] FOREIGN KEY([MAC_reparto])
REFERENCES [dbo].[Reparti] ([REP_id])
GO
ALTER TABLE [dbo].[Macchine] CHECK CONSTRAINT [FK_Macchine_Reparti]
GO
ALTER TABLE [dbo].[Macchine]  WITH CHECK ADD  CONSTRAINT [FK_Macchine_Serie] FOREIGN KEY([MAC_vs_SER_id])
REFERENCES [dbo].[Serie] ([SER_id])
GO
ALTER TABLE [dbo].[Macchine] CHECK CONSTRAINT [FK_Macchine_Serie]
GO
ALTER TABLE [dbo].[Manutenzione]  WITH CHECK ADD  CONSTRAINT [FK_Manutenzione_Macchine] FOREIGN KEY([MAN_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Manutenzione] CHECK CONSTRAINT [FK_Manutenzione_Macchine]
GO
ALTER TABLE [dbo].[Manutenzione2]  WITH CHECK ADD  CONSTRAINT [FK_Manutezione2_Letmanutenzione] FOREIGN KEY([MAN2_vs_LMAN_id])
REFERENCES [dbo].[Letmanutenzione] ([LMAN_Id])
GO
ALTER TABLE [dbo].[Manutenzione2] CHECK CONSTRAINT [FK_Manutezione2_Letmanutenzione]
GO
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Macchine] FOREIGN KEY([MAR_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Macchine]
GO
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Operatori] FOREIGN KEY([MAR_vs_OPE_id])
REFERENCES [dbo].[Operatori] ([OPE_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Operatori]
GO
ALTER TABLE [dbo].[Marcature]  WITH CHECK ADD  CONSTRAINT [FK_Marcature_Output] FOREIGN KEY([MAR_vs_OUT_id])
REFERENCES [dbo].[Output] ([OUT_id])
GO
ALTER TABLE [dbo].[Marcature] CHECK CONSTRAINT [FK_Marcature_Output]
GO
ALTER TABLE [dbo].[Output]  WITH CHECK ADD  CONSTRAINT [FK_Cicloproduzione_Produzione] FOREIGN KEY([OUT_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Output] CHECK CONSTRAINT [FK_Cicloproduzione_Produzione]
GO
ALTER TABLE [dbo].[Produzione]  WITH CHECK ADD  CONSTRAINT [FK_CPrdouzione_id] FOREIGN KEY([PROD_vs_CPROD_id])
REFERENCES [dbo].[Cicloproduzione] ([CPROD_id])
GO
ALTER TABLE [dbo].[Produzione] CHECK CONSTRAINT [FK_CPrdouzione_id]
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Componenti] FOREIGN KEY([SERMAC_vs_COM_id])
REFERENCES [dbo].[Componenti] ([COM_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Componenti]
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Macchine] FOREIGN KEY([SERMAC_vs_MAC_id])
REFERENCES [dbo].[Macchine] ([MAC_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Macchine]
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti]  WITH CHECK ADD  CONSTRAINT [FK_SerieMacchine_vs_Componenti_Serie] FOREIGN KEY([SERMAC_vs_SER_id])
REFERENCES [dbo].[Serie] ([SER_id])
GO
ALTER TABLE [dbo].[SerieMacchine_vs_Componenti] CHECK CONSTRAINT [FK_SerieMacchine_vs_Componenti_Serie]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Logs] FOREIGN KEY([USER_id])
REFERENCES [dbo].[Logs] ([LOG_id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Logs]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_action_flag_a8637d59_check] CHECK  (([action_flag]>=(0)))
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_action_flag_a8637d59_check]
GO
