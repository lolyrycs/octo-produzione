from django.core.management.base import BaseCommand
from Octo.models import *
from Octo.serialziers import ManChiaviSerializer


def addcomlet(componentemacchina, chiave, gruppo):
    clet = Componentilettura.objects.create(
        comlet_vs_com=componentemacchina,
        # comlet_desc=chiave.key_value,
        comlet_key=chiave,
        comlet_unita=chiave.key_unita if chiave.key_unita is not None else "ND",
        comlet_vs_gl=gruppo
    )


def removeFromQset(item, lista):
    for el in lista:
        if el['key_id'] == item.key_id:
            lista.remove(el)
            return True
    return False


def removeFromList(item, lista):
    for el in lista:
        if el['key_id'] == item['key_id']:
            lista.remove(el)
            return True
    return False


class Command(BaseCommand):
    def handle(self, *args, **options):
        macs = Macchine.objects.all()
        gruppostandard = Gruppilettura.objects.get(pk=2)
        for mac in macs:
            # print("macchina: " + mac.mac_nomemacch)
            allkeys = ManChiavi.objects.all()
            serialized = ManChiaviSerializer(allkeys, many=True).data
            compsmacs = Componentimac.objects.filter(cm_mac=mac)
            for compmac in compsmacs:
                nomecomponente = compmac.cm_com.com_nome.strip()
                # print("componente: " + nomecomponente)
                if nomecomponente != "Generale":
                    if nomecomponente[:-1] == "Asse":
                        asse = nomecomponente[-1]
                        keys = [el for el in serialized if el['key_value'][-1] == asse]
                        for key in keys:
                            # print ("chiave: "+key['key_value']+" keyser: "+str(ManChiaviSerializer(key).data))
                            addcomlet(compmac, ManChiavi.objects.get(pk=key['key_id']), gruppostandard)
                            removeFromList(key, serialized)
                    else:
                        keys = allkeys.filter(key_value__contains=nomecomponente)
                        for key in keys:
                            # print ("chiave: "+key.key_value+" keyser: "+str(ManChiaviSerializer(key).data))
                            addcomlet(compmac, key, gruppostandard)
                            removeFromQset(key, serialized)
                else:
                    for key in serialized:
                        # print ("chiave: " + key['key_value'] + " keyser: " + str(ManChiaviSerializer(key).data))
                        addcomlet(compmac, ManChiavi.objects.get(pk=key['key_id']), gruppostandard)
                    # print(len(serialized))
