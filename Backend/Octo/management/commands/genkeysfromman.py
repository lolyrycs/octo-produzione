from django.core.management.base import BaseCommand
from Octo.models import Manutenzione2, Macchine, ManChiavi

keys = ManChiavi.objects.all()


def is_in_keys(chiave):
    global keys
    for key in keys:
        if chiave == key.key_value:
            return True
    return False


class Command(BaseCommand):

    def handle(self, *args, **options):
        machine = Macchine.objects.all()
        global keys
        absents = []
        for mac in machine:
            # select a lecture linked to data
            man = Manutenzione2.objects.filter(man2_vs_lman__lman_vs_mac=mac).values('man2_chiave').distinct()
            if len(man) > 0:
                # man = Manutenzione2.objects.filter(man2_vs_lman=man[0].man2_vs_lman).values('man2_chiave')
                for key in man:
                    if not is_in_keys(key['man2_chiave']):
                        if key['man2_chiave'] not in absents:
                            absents.append(key['man2_chiave'])
        for ab in absents:
            print(str(ab))
            ManChiavi.objects.create(key_value=str(ab))