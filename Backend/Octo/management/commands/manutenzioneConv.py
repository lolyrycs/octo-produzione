from django.core.management.base import BaseCommand
from django.db import utils
from Octo.models import Macchine, Manutenzione2,  \
    ManChiavi, Componentilettura, Logsconversione, Letturamanutenzione

keys = ManChiavi.objects.all()

#region util


# lectures have same value and are consecutive (timestamp diff < seconds)
def is_same_man(let1, let2, seconds):
    if let1.man2_valore != let2.man2_valore:
        return False
    else:
        diff = abs(let1.man2_vs_lman.lman_data - let2.man2_vs_lman.lman_data).total_seconds()
        return diff < seconds

def is_same(let1, let2, seconds):
    diff = abs(let1.lman_data - let2.lman_data).total_seconds()
    return diff < seconds


def is_in_keys(chiave):
    global keys
    for key in keys:
        if chiave == key.key_value:
            return True
    return False

def get_key_by_name(chiave):
    global keys
    for key in keys:
        if chiave == key.key_value:
            return key
    raise ValueError("chiave non trovata: " + chiave)

# diamo per assunto che ogni macchina abbia letture fisse --- DEPRECATED ?
def keys_to_read(machine):
    # select a lecture linked to data
    man = Manutenzione2.objects.filter(man2_vs_lman__lman_vs_mac=machine)[:1]
    if len(man) > 0:
        lman = man[0].man2_vs_lman
        # choose keys to read
        keysman = [mank.man2_chiave for mank in Manutenzione2.objects.filter(man2_vs_lman=lman)]
        for kman in keysman:
            keys.append(ManChiavi.objects.filter(key_value=str(kman))[0])
    return keys

# get componentiLettura
def get_com_lettura(mac, key):
    ret = Componentilettura.objects.filter(comlet_vs_com__cm_mac=mac, comlet_key=key)
    if len(ret) > 0:
        return ret[0]
    else:
        raise ValueError("componente non trovato per valori di macchina: "+str(mac.mac_id)+" e chiave: "+key.key_value)

#endregion


def main_by_key(seconds, test):
    error = {'is_error': False, 'err_msg': ''}

    machines = Macchine.objects.all()[:1] if test else Macchine.objects.all()
    for mac in machines:
        let_open = {'mac': mac.mac_id}
        print mac.mac_nomemacch
        # seleziono le chiavi per macchina
        data_frommac = Manutenzione2.objects.filter(man2_vs_lman__lman_vs_mac=mac)
        key_names = data_frommac.values("man2_chiave").distinct()[:1] if test \
                        else data_frommac.values("man2_chiave").distinct()
        for name in key_names:
            count_newrec = 0
            count_rec = 0
            try:
                key = get_key_by_name(name['man2_chiave'])
                let_open['clet'] = get_com_lettura(mac, key)
                print(key.key_id, name['man2_chiave'])
                data_from_man = \
                    data_frommac.filter(man2_chiave=name['man2_chiave'])[:1000] if test \
                        else data_frommac.filter(man2_chiave=name['man2_chiave'])
                for i in range(len(data_from_man)):
                    if i == 0:
                        let_open['value'] = data_from_man[i].man2_valore
                        let_open['date_start'] = data_from_man[i].man2_vs_lman.lman_data
                    elif not is_same_man(data_from_man[i-1], data_from_man[i], seconds):
                        let_open['date_stop'] = data_from_man[i-1].man2_vs_lman.lman_data
                        try:  # this is the record to insert in LETTURAMANUTENZIONE
                            Letturamanutenzione.objects.create(
                                letman_datastato=let_open['date_start'],
                                letman_datastop=let_open['date_stop'],
                                letman_vs_comlet=let_open['clet'],
                                letman_valore=let_open['value']
                            )
                            count_newrec += 1
                            # data_from_man[:i-1].delete()
                        except utils.IntegrityError as e:
                            error['is_error'] = True
                            error['err_msg'] = e.message
                        let_open['value'] = data_from_man[i].man2_valore
                        let_open['date_start'] = data_from_man[i].man2_vs_lman.lman_data
                    elif i == len(data_from_man)-1:
                        let_open['date_stop'] = data_from_man[i].man2_vs_lman.lman_data
                        try:  # this is the record to insert in LETTURAMANUTENZIONE
                            Letturamanutenzione.objects.create(
                                letman_datastato=let_open['date_start'],
                                letman_datastop=let_open['date_stop'],
                                letman_vs_comlet=let_open['clet'],
                                letman_valore=let_open['value']
                            )
                            count_newrec += 1
                            # data_from_man[:i].delete()
                        except utils.IntegrityError as e:
                            error['is_error'] = True
                            error['err_msg'] = e.message
                    count_rec += 1
                # data_from_man.delete()
            except ValueError as e:
                error['is_error'] = True
                error['err_msg'] = e.message
            if error['is_error']:
                Logsconversione.objects.create(logc_vs_comlet=let_open['clet'],
                                              logc_numletturetot=len(data_from_man),
                                              logc_letturenuove=count_newrec,
                                              logc_numletture=count_rec,
                                              logc_message=error['err_msg'])
            else:
                Logsconversione.objects.create(logc_vs_comlet=let_open['clet'],
                                              logc_numletturetot=len(data_from_man),
                                              logc_letturenuove=count_newrec,
                                              logc_numletture=count_rec)
            error['is_error'] = False
            error['err_msg'] = ''
        print("-------------------------------------")
    print('finished')


class Command(BaseCommand):
    help = "Comando per convertire la tabella di Manutezione"

    def add_arguments(self, parser):
        parser.add_argument('-s', type=int)

        parser.add_argument('--simple', default=False, help='Prova della conversione per una macchina e una chiave')

    def handle(self, *args, **options):
        seconds = 15
        test = False
        if options['s']:
            seconds = options['s']
        if options['simple']:
            test = True
        main_by_key(seconds, test)