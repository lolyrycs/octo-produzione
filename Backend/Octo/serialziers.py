from models import *
from rest_framework import serializers
from django.core.serializers.json import Serializer
from django.contrib.auth.models import User, Group
from guardian.shortcuts import assign_perm ,remove_perm, get_groups_with_perms, get_objects_for_group, get_objects_for_user


def format_time(seconds):
    time = ""
    hours = seconds/3600
    seconds  =seconds%3600

    if hours>0:
        time+=str(hours)+" h:"

    minutes=seconds/60
    seconds=seconds%60
    time+=str(minutes)+" m"

    if seconds>0:
        time+=":"+str(seconds)+" s"

    return time

class AllarmiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Allarmi
        fields = ('all_date', 'all_numeroallarme', 'all_messaggioallarme', 'all_numerowarning', 'all_messaggiowarning', 'all_vs_mac', 'all_id' )

class CicloproduzioneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cicloproduzione
        fields = ('cprod_id', 'cprod_vs_mac', 'cprod_commessa', 'cprod_importfrom', 'cprod_status', 'cprod_pezzifatti', 'cprod_lotto', 'cprod_fase', 'cprod_articolo', 'cprod_programma')

class ComponentiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Componenti
        fields = ('com_id', 'com_nome', 'com_chiave', 'com_vita', 'com_codice', 'com_unita')

class ChiaviManSerializer(serializers.ModelSerializer):
    class Meta:
        model = ManChiavi
        fields = ('key_id', 'key_value', 'key_unita')

class InputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Input
        fields = ('in_id', 'in_commessa', 'in_lotto', 'in_priorita', 'in_importfrom', 'in_status', 'in_data', 'in_vs_user', 'in_vs_cprod', 'in_fase', 'in_articolo', 'in_programma')

class InterfacciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interfaccia
        fields = ('int_id', 'int_ipadress', 'int_status', 'int_programmaattivo', 'int_feedassi', 'int_feedmandrino', 'int_utensileattivo' , 'int_pezzifatti', 'int_vs_mac', 'int_path', 'int_date' , 'int_totem', 'int_commessa' )

class LetmanutenzioneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Letmanutenzione
        fields = ('lman_id', 'lman_data', 'lman_vs_mac')

class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logs
        fields = ('log_id', 'log_fk_user_id', 'log_fk_mac', 'log_description', 'log_status', 'log_date')

class RepartiSerializer(serializers.ModelSerializer):
    group_ref = serializers.SerializerMethodField('get_ref_auth')
    def get_ref_auth(self, obj):
        currentgroup =  get_groups_with_perms(obj)
        if(len(currentgroup)>0):
            return currentgroup[0].name
        else:
            return ""
    class Meta:
        model = Reparti
        fields = ('rep_id', 'rep_descr', 'rep_name', 'group_ref')

class ComponetMacchinaSerialeSerializer(serializers.ModelSerializer):
    compont_ref = serializers.SerializerMethodField('get_ref_components')
    machine_ref = serializers.SerializerMethodField('get_machine_components')
    series_ref = serializers.SerializerMethodField('get_machine_components')

    def get_ref_series(self, obj):
        try:
            series = Serie.objects.get(pk=obj.sermac_vs_ser)
            return {"ser_id": series.ser_id, "ser_nome": series.ser_nome, "ser_costruttore": series.ser_costruttore}
        except:
            return None

    def get_ref_components(self, obj):
        try:
            currentcoompos = Componenti.objects.get(pk=obj.sermac_vs_com)
            return {"com_id": currentcoompos.com_id, "com_nome": currentcoompos.com_nome, "com_chiave": currentcoompos.com_nome, "com_vita": currentcoompos.com_vita, "com_codice" :currentcoompos.com_codice  }
        except:
            return None

    def get_ref_machine(self, obj):
        try:
            currentmac = Macchine.objects.get(pk=obj.sermac_vs_mac)
            return {"mac_id": currentmac.mac_id, "mac_ipadress": currentmac.mac_ipadress, "mac_ipadress2": currentmac.mac_ipadress2, "mac_nomemacch": currentmac.mac_nomemacch, "mac_soprannomemacch" :currentmac.mac_soprannomemacch,
                    "mac_tipomacchina": currentmac.mac_tipomacchina, "mac_controllonumerico": currentmac.mac_controllonumerico, "mac_tipocontrollo": currentmac.mac_tipocontrollo, "mac_packegtipo":currentmac.mac_packegtipo,
                    "mac_imagepath": currentmac.mac_imagepath, "mac_image": currentmac.mac_image,
                    "mac_serialnumber": currentmac.mac_serialnumber, "mac_boolcamera": currentmac.mac_boolcamera, "mac_ipadresscamera": currentmac.mac_ipadresscamera,"mac_portacamera": currentmac.mac_portacamera,
                    "mac_comandocamera": currentmac.mac_comandocamera, "mac_modecamera": currentmac.mac_modecamera, "mac_usercamera": currentmac.mac_usercamera,"mac_portacamera": currentmac.mac_portacamera,
                    "mac_passcamera": currentmac.mac_passcamera, "mac_numeroassi": currentmac.mac_numeroassi, "mac_userusername": currentmac.mac_userusername, "mac_userpassword": currentmac.mac_userpassword, "mac_pathshare": currentmac.mac_pathshare,
                    }
        except:
            return None

    class Meta:
        model = Reparti
        fields = ('sermac_id', 'compont_ref', 'machine_ref', 'series_ref')

class MachineHome(serializers.ModelSerializer):
    interf = serializers.SerializerMethodField('get_interfaccia')

    def get_interfaccia(self, obj):
        machine = Macchine.objects.get(pk=obj.mac_id)
        ret={}
        try:
            myInterfaccia = Interfaccia.objects.filter(int_vs_mac=machine, int_attivo=1)
            if myInterfaccia:
                ret ={"int_programmaattivo":myInterfaccia[0].int_programmaattivo,
                      "int_totem":myInterfaccia[0].int_totem,
                      "int_commessa":myInterfaccia[0].int_commessa,
                      "int_pezzifatti":myInterfaccia[0].int_pezzifatti}
                commessa=myInterfaccia[0].int_commessa
                if commessa:
                    try:
                        if machine.mac_controllonumerico == "Fagor":
                            myCommessa = Produzione.objects.get(prod_vs_cprod__cprod_vs_mac=machine, prod_vs_cprod__cprod_id=commessa)
                            ret["int_commessa"] = myCommessa.prod_vs_cprod.cprod_id
                        else:
                            myCommessa = Produzione.objects.get(prod_vs_cprod__cprod_vs_mac=machine, prod_vs_cprod__cprod_commessa=commessa)
                        efficienza = myCommessa.prod_efficienza
                    except:
                        efficienza = "N.d."
                    ret["prod_efficienza"] = efficienza
        finally:
            return ret

    class Meta:
        model = Macchine
        fields = ('mac_id', 'mac_ipadress', 'mac_ipadress2', 'mac_nomemacch', 'mac_reparto', 'mac_soprannomemacch',
                  'mac_tipomacchina',
                  'mac_controllonumerico', 'mac_tipocontrollo', 'mac_packegtipo', 'mac_imagepath', 'mac_image',
                  'mac_pathshare',
                  'mac_serialnumber', 'mac_boolcamera', 'mac_ipadresscamera', 'mac_portacamera', 'mac_comandocamera',
                  'mac_modecamera', 'mac_usercamera',
                  'mac_passcamera', 'mac_userusername', 'mac_userpassword', 'mac_vs_ser', 'mac_numeroassi',
                  'mac_statoconnessione', 'interf',
                  'mac_multipallet', 'mac_attivo', 'mac_assi')


class MachineSerializer(serializers.ModelSerializer):
    mac_reparto = RepartiSerializer(read_only=True)
    comps = serializers.SerializerMethodField('get_components')
    interf = serializers.SerializerMethodField('get_interfaccia')

    def get_components(self, obj):
        machine = Macchine.objects.get(pk=obj.mac_id)
        myComponents = SeriemacchineVsComponenti.objects.filter(sermac_vs_mac=machine)

        if (len(myComponents) > 0):
            myComponentsOut = []
            for comp in myComponents:
                ## fields = ('com_id', 'com_nome', 'com_chiave', 'com_vita', 'com_codice', 'com_vs_ser', 'com_vs_mac')
                compo = {"com_id":comp.sermac_vs_com.com_id, "com_nome":comp.sermac_vs_com.com_nome, "com_vita":comp.sermac_vs_com.com_vita, "com_codice":comp.sermac_vs_com.com_codice, "com_chiave":comp.sermac_vs_com.com_chiave }
                myComponentsOut.append(compo)
            return myComponentsOut
        else:
            return list()

    def get_interfaccia(self, obj):
        machine = Macchine.objects.get(pk=obj.mac_id)
        ret={}
        try:
            myInterfaccia = Interfaccia.objects.filter(int_vs_mac=machine, int_attivo=1)
            if myInterfaccia:
                ret ={"int_programmaattivo":myInterfaccia[0].int_programmaattivo,
                      "int_totem":myInterfaccia[0].int_totem,
                      "int_commessa":myInterfaccia[0].int_commessa,
                      "int_pezzifatti":myInterfaccia[0].int_pezzifatti}
                commessa=myInterfaccia[0].int_commessa
                if commessa:
                    try:
                        if machine.mac_controllonumerico == "Fagor" or machine.mac_controllonumerico == "Heidenhain":
                            myCommessa = Produzione.objects.get(prod_vs_cprod__cprod_vs_mac=machine, prod_vs_cprod__cprod_id=commessa)
                            ret["int_commessa"] = myCommessa.prod_vs_cprod.cprod_commessa
                        else:
                            myCommessa = Produzione.objects.get(prod_vs_cprod__cprod_vs_mac=machine, prod_vs_cprod__cprod_commessa=commessa)
                        efficienza = myCommessa.prod_efficienza
                    except:
                        efficienza = "N.d."
                    ret["prod_efficienza"] = efficienza
        finally:
            return ret

    class Meta:
        model = Macchine
        fields = ('mac_id', 'mac_ipadress', 'mac_ipadress2', 'mac_nomemacch', 'mac_reparto', 'mac_soprannomemacch', 'mac_tipomacchina',
                  'mac_controllonumerico', 'mac_tipocontrollo', 'mac_packegtipo', 'mac_imagepath', 'mac_image', 'mac_pathshare',
                  'mac_serialnumber', 'mac_boolcamera', 'mac_ipadresscamera', 'mac_portacamera', 'mac_comandocamera', 'mac_modecamera', 'mac_usercamera' ,
                  'mac_passcamera', 'mac_userusername', 'mac_userpassword' , 'mac_vs_ser',  'mac_numeroassi', 'mac_statoconnessione', 'comps', 'interf',
                  'mac_multipallet', 'mac_attivo', 'mac_assi')

class MacchinaListaSerializer(serializers.ModelSerializer):
    mac_reparto = RepartiSerializer(read_only=True)
    class Meta:
        model = Macchine
        fields = ('mac_id', 'mac_nomemacch', 'mac_tipomacchina', 'mac_controllonumerico', 'mac_soprannomemacch', 'mac_reparto',  'mac_attivo')

class SettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Setting
        fields = ('set_id','set_descr', 'set_name', 'set_email','set_email_port', 'set_email_ip' )

'''
deprecated
class ManutenzioneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manutenzione
        fields = ( 'man_tempmandrino', 'man_tempassex', 'man_tempassey', 'man_tempassez', 'man_tempasseb', 'man_tempassec', 'man_tempencx', 'man_tempency'
                   'man_tempencz', 'man_tempencb', 'man_tempencc', 'man_loadmandrino', 'man_loadassex', 'man_loadassey', 'man_loadassez', 'man_loadasseb', 'man_loadassec',
                   'man_rmpmandrino', 'man_data', 'man_metrilavorox', 'man_metrirapidox', 'man_metrilavoroy', 'man_metrirapidoy', 'man_metrilavoroz', 'man_metrirapidoz',
                   'man_metrilavorob', 'man_metrirapidob', 'man_metrilavoroc', 'man_metrirapidoc', 'man_metrix', 'man_metriy', 'man_metriz', 'man_metrib', 'man_metric'
                   'man_sbloccaggicu', 'man_cambieseguiti', 'man_orelavoro', 'man_oreaccensione', 'man_rpm1', 'man_rpm2', 'man_rpm3', 'man_rpm4', 'man_rpm5', 'man_timeoverlimit',
                   'man_limitx', 'man_limity', 'man_limitz', 'man_limitb', 'man_limitc', 'man_id', 'man_vs_mac' )
############
'''

class SerieSerializer(serializers.ModelSerializer):

    comps = serializers.SerializerMethodField('get_components')

    def get_components(self, obj):
        serie = Serie.objects.get(pk=obj.ser_id)
        myComponents = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=serie)

        if (len(myComponents) > 0):
            myComponentsOut = []
            for comp in myComponents:
                ## fields = ('com_id', 'com_nome', 'com_chiave', 'com_vita', 'com_codice', 'com_vs_ser', 'com_vs_mac')
                compo = {"com_id":comp.sermac_vs_com.com_id, "com_nome":comp.sermac_vs_com.com_nome, "com_vita":comp.sermac_vs_com.com_vita, "com_codice":comp.sermac_vs_com.com_codice, "com_chiave":comp.sermac_vs_com.com_chiave }
                myComponentsOut.append(compo)
            return myComponentsOut
        else:
            return list()

    class Meta:
        model = Serie
        fields = ('ser_id','ser_nome', 'ser_costruttore', 'comps' )

class Manutenzione2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Manutenzione2
        fields = ('man2_id','man2_chiave', 'man2_valore','man2_vs_lman')

class MarcatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marcature
        fields = ('mar_id','mar_vs_ope', 'mar_vs_mac','mar_vs_out', 'mar_date')

class OperatoriSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operatori
        fields = ('ope_id','ope_badge', 'ope_date','ope_custom1', 'ope_custom2')

class OutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Output
        fields = ( 'out_id', 'out_tempolavoro', 'out_tempopiazzamento', 'out_tempopausa', 'out_efficienza', 'out_priorita', 'out_vs_cprod', 'out_data' )

class OutputComplSerializer(serializers.ModelSerializer):
    mac = serializers.SerializerMethodField('get_machine')
    time = serializers.SerializerMethodField('timer')

    def timer(self,obj):
        ret={}
        ret['pausa']='N.d.' if obj.out_tempopausa is None else format_time(int(obj.out_tempopausa))
        ret['setup']='N.d.' if obj.out_tempopiazzamento is None else  format_time(int(obj.out_tempopiazzamento))
        ret['lavoro']='N.d.' if obj.out_tempolavoro is None else  format_time(int(obj.out_tempolavoro))
        return ret

    def get_machine(self,obj):
        cprod=obj.out_vs_cprod
        mac=cprod.cprod_vs_mac
        ret={"mac_nomemacch":mac.mac_nomemacch, "cprod_commessa":cprod.cprod_commessa, "cprod_lotto":cprod.cprod_lotto, "cprod_fase":cprod.cprod_fase, "cprod_articolo":cprod.cprod_articolo }
        return ret

    class Meta:
        model = Output
        fields = ( 'out_id', 'out_tempolavoro', 'out_tempopiazzamento', 'out_tempopausa', 'out_efficienza', 'out_priorita', 'out_vs_cprod', 'out_data', 'mac', 'time' )


class ProduzioneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produzione
        field = ( 'prod_id', 'prod_lotto', 'prod_tempolavoro', 'prod_tempopiazzamento', 'prod_tempopausa', 'prod_efficienza', 'prod_priorita', 'prod_data',  'prod_vs_cprod' )


class ManChiaviSerializer(serializers.ModelSerializer):
    class Meta:
        model = ManChiavi
        fields = ('key_id','key_value')

'''
Gestione dei serializer nativi Django auth
'''

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=False, read_only=True)
    ##groups = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    '''def create(self, validated_data):
        user = User.objects.create(validated_data)
        if 'password' in validated_data:
            user.set_password(validated_data['password'])
            user.save()
        user.save()
        return user'''

    def create(self, validated_data):
        user, created = User.objects.get_or_create(username=validated_data["username"], email='',
                                                   first_name=validated_data["first_name"], last_name=validated_data["last_name"])
        if created:
            user.set_password(validated_data["password"])  # This line will hash the password
            user.save()  # DO NOT FORGET THIS LINE
        return user

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'is_staff','groups' )

class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Typemachine
        fields = ('typ_id', 'typ_tipomacchina', 'typ_controllonumerico', 'typ_tipocontrollo' )

class GroupMemberSerializer(serializers.BaseSerializer):
    groups = GroupSerializer()
    user = UserSerializer()

class RepartoOwnerSerializer(serializers.BaseSerializer):
    reparto = RepartiSerializer()
    group = GroupSerializer()





