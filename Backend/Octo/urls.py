"""Octo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
#from Octo.views import Homepage
from django.conf.urls.static import static

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import verify_jwt_token, obtain_jwt_token
from Octo.views import *


router = DefaultRouter()

#region ALARM
router.register(r'allarmi', AlarmViewSet, base_name='allarmi')
#endregion

#region INTERFACCIA
router.register(r'interfacce', InterfacciaViewSet, base_name='interfacce')
#endregion

#region INPUT
router.register(r'input', InputViewSet, base_name='input')
#endregion

#region OUTPUT
router.register(r'output', OutputViewSet, base_name='input')
#endregion

#region PRODUZIONE
router.register(r'produzione', ProduzioneViewSet, base_name='input')
#endregion


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^hearthbeat/$', myFirstJsonHearthBeat, name='hearthbeat'),

    url(r'^ctrlPermessi/', ctrlPermessi.as_view(), name='permessi'),
    url(r'^ctrlPermessiStd/', ctrlPermessiStd.as_view(), name='ctrlPermessiStd'),

    #region JWT
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^registrazione/', creaUtente),
    #endregion

    # region ALLARMI
    url(r'^allarmi-by-machine/(?P<pk>\d+)/', all_by_mac, name='allarmi mac'),
    url(r'^allarmi-attivi-by-machine/(?P<pk>\d+)/', all_att_by_mac, name='allarmi mac'),
    url(r'^allarmi-ricerca/(?P<query>\w+)/', allarms_collection, name='allarmi'),
    # endregion

    #region REPARTO
    url(r'^reparto/(?P<pk>\d+)/', RepartiDetail.as_view(), name='reparto'),
    url(r'^searchreparti/(?P<query>\w+)/', reparti_collection, name='reparto'),
    url(r'^reparti/$', reparti_collection, name='reparti'),
    #endregion

    #region SETTINGS
    url(r'^setting/(?P<pk>\d+)/', SettingDetail.as_view(), name='setting'),
    url(r'^settings/$', setting_collection, name='settings'),
    #endregion

    #region MACCHINA
    url(r'^macchina/(?P<pk>\d+)/', MacchineDetail.as_view(), name='macchina'),
    url(r'^macchine/$', macchine_collection, name='macchine'),
    url(r'^macchine_list/$', macchine_list, name='macchine'),
    url(r'^searchmacchine/(?P<query>\w+)/$', macchine_collection, name='macchine'),
    url(r'^macchineinterfaccia/$', macchine_interfaccia_collection, name='macchine-interfacce'),
    url(r'^changeactive/(?P<pk>\d+)/$', macchine_activation, name='macchine'),
    url(r'^type_machine/$', get_types, name='macchine'),
    #endregion

    # region SERIES
    url(r'^serie/(?P<pk>\d+)/', SeriesDetail.as_view(), name='reparto'),
    url(r'^series/$', series_collection, name='reparti'),
    url(r'^searchseries/(?P<query>\w+)/$', series_collection, name='reparti'),
    # endregion

    # region COMPONENTS
    url(r'^component/(?P<pk>\d+)/', ComponentDetail.as_view(), name='componente'),
    url(r'^components/$', componenti_collection, name='componenti'),
    url(r'^searchcomponents/(?P<query>\w+)/', componenti_collection, name='searchcomponents'),
    url(r'^componentsavailabale/$', componenti_collection, name='searchcomponentavailabale'),
    url(r'^allcomponentbyMachine/(?P<pk>\d+)/', componenti_bymachine, name='searchcomponentnymachine'),
    url(r'^components/(?P<stato>\d+)/(?P<associazione>\w+)/$', get_componenti_bystatus, name='filtrocomponentivsstatovsassociazione'),
    # endregion

    #region GRUPPO
    url(r'^gruppo/(?P<pk>\d+)/', GruppiDetail.as_view(), name='gruppo'),
    url(r'^gruppo/(?P<name>\w+)/', GruppiDetail.as_view(), name='gruppon'),
    url(r'^gruppi/$', gruppi_collection, name='gruppi'),
    url(r'^groupmembership/$', get_allGroupByUser, name='gruppimembership'),
    url(r'^groupsearch/(?P<name>\w+)/$',searchGroupByName, name='gruppi'),
    #endregion

    #region USERS
    url(r'^user/(?P<pk>\d+)/', searchUserById, name='userbyid'),
    url(r'^users/$', macchine_collection, name='gruppi'),
    url(r'^usersearch/(?P<name>\w+)/$',searchUserByName, name='userbyname'),
    #endregion

    #region UPLOAD
    url(r'^upload/', MultiFileUploadView.as_view()),
    #endregion

    # region MANUTENZIONE
    url(r'^searchman/(?P<name>\w+)/$', ManKeyByName, name='key_by_name'),
    url(r'^mankey/', ManKey, name='key_by_name'),
    # endregion

    #region INTERFACCIABIS
    url(r'^interfacce-macchine/$',searchInterfacciaAndMachine, name='interfacce-machine'),
    url(r'^interfacce-by-macchine/(?P<id>\d+)/',searchInterfacciaByMachine, name='input-by-machine'),
    #endregion

    #region INPUTBIS
    url(r'^input-macchine/$',searchInputAndMachine, name='input-machine'),
    url(r'^input-by-macchine/(?P<id>\d+)/',searchInputByMachine, name='input-by-machine'),
    #endregion

    #region OUTPUTBIS
    url(r'^output-machine/$',searchOutputAndMachine, name='output-machine'),
    url(r'^output-by-macchine/(?P<id>\d+)/', searchOutputByMachine, name='output-by-machine'),
    url(r'^searchoutput/(?P<name>\w+)/', searchOutputByName, name='output-by-machine name'),
    #endregion

    #region PRODUZIONEBIS
    url(r'^produzione-machine/(?P<id>\d+)/',searchProduzioneByMachine, name='produzione-machine'),
    url(r'^produzione-commessa/(?P<id>\d+)/(?P<com>\d+)/',searchProduzioneByCommessa, name='produzione-comm'),
    url(r'^macchine-attive/$',MacchineAttive, name='macchine-attive'),
    #endregion

    url(r'^smb-connection/$', comnnect_machinesmb, name=''),
    url(r'^download/$', downloadFromMac, name=''),

    url(r'^smb-upload/$', SmbFileUploadView.as_view(), name=''),
    url(r'^$', Homepage.as_view()),
]

urlpatterns += router.urls

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
