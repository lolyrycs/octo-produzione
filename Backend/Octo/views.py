from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.db import transaction
import os
from django.http import JsonResponse , HttpResponse
from Octo.serialziers import *
from Octo.models import *
from Octo.util import *
import Octo.settings as DjangoSettings
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User, Group
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import FileUploadParser, MultiPartParser
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework import generics, viewsets, views
from django.forms.models import model_to_dict
from django.core.serializers.json import DjangoJSONEncoder
from guardian.shortcuts import assign_perm ,remove_perm, get_groups_with_perms, get_objects_for_group, get_objects_for_user
from django.db import connection
from django.db.models import Q

from django.core.files.storage import FileSystemStorage
import json
import os
from base64 import b64encode

sec_allarmi= 10

class Homepage(TemplateView):
    template_name = "homepage.html"

def  myFirstJsonHearthBeat(request):
    if request.method == "GET":
        return JsonResponse({'message': 'beat', 'status': 'ok'})

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def creaUtente(request):
    if request.method == "GET":
        return Response("no", status=status.HTTP_400_BAD_REQUEST)
    elif request.method == "POST":
        data = {'username':request.data.get("username"), 'password': request.data.get("password"), 'first_name': request.data.get("first_name"),
                'last_name':request.data.get("last_name"), 'groups': list()}
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#region Alarm
'''
TODO refactoring oggetto Allarm, distinct su numero e messaggio
'''

@api_view(['GET', 'POST'])
def allarms_collection(request, query=None):
    if request.method == 'GET':
        if(query is None):
            allarms = Allarmi.objects.all()[:1000]
            serializer = AllarmiSerializer(allarms, many=True)
            return Response(serializer.data)
        else:
            #filtra prima per data poi per nome
            allarms = Allarmi.objects.filter(Q(all_vs_mac__mac_nomemacch__icontains=query)|Q(all_numeroallarme__icontains=query)|Q(all_numerowarning__icontains=query))[:100]
            serializer = AllarmiSerializer(allarms, many=True)
            return Response(serializer.data)

@api_view(['GET'])
def all_by_mac( request, pk=None):
    queryset = Allarmi.objects.filter(all_vs_mac=pk)
    serializer = AllarmiSerializer(queryset, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def all_att_by_mac( request, pk=None):
    if request.method == 'GET':
        if( pk is not None  ):
            with connection.cursor() as cursor:
                #ragruppo per allarme(messaggio e macchina), prendo la
                cursor.execute(
                    "select * from (SELECT ALL_numeroAllarme, ALL_numeroWarning, ALL_vs_MAC_id, "+
                        "MAX(ALL_date) as ALL_date, MIN(ALL_messaggioAllarme) as ALL_messaggioAllarme, MIN(ALL_messaggioWarning)as ALL_messaggioWarning "+
                        "FROM allarmi GROUP BY ALL_numeroAllarme, ALL_numeroWarning, ALL_vs_MAC_id)" +
                    "as X where DATEDIFF(second,  ALL_date, getdate())<"+str(sec_allarmi)+"and ALL_vs_MAC_id="+str(pk)+" order by ALL_date desc;") #seconds
                rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')
        else:
            Response('macchina non presente', status=status.HTTP_400_BAD_REQUEST)

class AlarmViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Allarmi.objects.all()[:100]
        serializer = AllarmiSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Allarmi.objects.all()
        allarme = get_object_or_404(queryset, pk=pk)
        serializer = AllarmiSerializer(allarme)
        return Response(serializer.data)
#endregion

#region reparti
'''
REPARTI
Funzione di creazione e listing di tutti gli oggetti 
'''
@api_view(['GET', 'POST'])
def reparti_collection(request, query=None):
    if request.method == 'GET':
        if(query is None):
            reparti = Reparti.objects.all()
            serializer = RepartiSerializer(reparti, many=True)
            return Response(serializer.data)
        else:
            reparti = Reparti.objects.filter(rep_name__icontains=query)
            serializer = RepartiSerializer(reparti, many=True)
            return Response(serializer.data)

    elif request.method == 'POST':
        mycurrentGroup = ""
        if (len(request.data.get("group_ref")) > 0):
            mycurrentGroup = request.data.get("group_ref")
        data = {'rep_descr': request.data.get("rep_descr"), 'rep_name': request.data.get("rep_name"),
                'my_current_group': mycurrentGroup}
        serializer = RepartiSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            reparto = Reparti.objects.get(rep_id= serializer.data["rep_id"])
            if (mycurrentGroup != ""):
                groupAdmin = Group.objects.get(name=data['my_current_group'])
                assign_perm('view_reparto', groupAdmin, reparto)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



'''
REPARTI
Funzione di update delete e get di un singolo oggetto 
'''
class RepartiDetail(APIView):
    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return Reparti.objects.get(rep_descr=pk)
        except Reparti.DoesNotExist:
            return None

    def get_object_pk(self, pk):
        try:
            return Reparti.objects.get(rep_id=pk)
        except Reparti.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        reparti = self.get_object(pk)
        if(reparti is not None):
            serializer = RepartiSerializer(reparti)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        reparto = self.get_object_pk(pk)

        mycurrentGroup = ""
        if( len(request.data.get("group_ref"))>0):
            mycurrentGroup = request.data.get("group_ref")
            print mycurrentGroup

        data = {'rep_descr': request.data.get("rep_descr"), 'rep_name': request.data.get("rep_name"), 'my_current_group': mycurrentGroup }

        if (reparto is not None):
            reparto.rep_descr = data['rep_descr']
            reparto.rep_name = data['rep_name']
            reparto.save()

            if(mycurrentGroup != ""):
                groupAdmin = Group.objects.get(name=data['my_current_group'])
                print groupAdmin
                assign_perm('view_reparto', groupAdmin, reparto)

            serializer = RepartiSerializer(reparto)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        reparto = self.get_object_pk(pk)
        if (reparto is not None):
            serializer = RepartiSerializer(reparto)
            mydata = serializer.data
            reparto.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
#endregion

#region Setting
'''
SETTING
Funzione di creazione e listing di tutti gli oggetti 
'''
@api_view(['GET'])
def setting_collection(request):
    if request.method == 'GET':
        setting = Setting.objects.all()
        serializer = SettingSerializer(setting, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = {'sett_id': request.data.get("sett_id"), 'sett_descr': request.data.get("sett_descr"),
                'sett_name': request.data.get("sett_name"), 'sett_email': request.data.get("sett_email"),
                'sett_port': request.data.get("sett_port"), 'sett_email_ip':request.data.get("sett_email_ip")}
        serializer = SettingSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''
SETTING
Funzione di update delete e get di un singolo oggetto 
'''
class SettingDetail(APIView):

    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return Setting.objects.get(sett_id=pk)
        except Setting.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        setting = self.get_object(pk)
        if(setting is not None):
            serializer = SettingSerializer(setting)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        setting = self.get_object(pk)
        data = {'sett_id': request.data.get("sett_id"), 'sett_descr': request.data.get("sett_descr"),
                'sett_name': request.data.get("sett_name"), 'sett_email': request.data.get("sett_email"),
                'sett_email_port': request.data.get("sett_email_port"), 'sett_email_ip': request.data.get("sett_email_ip")}
        if (setting is not None):
            setting.sett_id = data['sett_id']
            setting.sett_descr = data['sett_descr']
            setting.sett_email = data['sett_email']
            setting.sett_email_ip = data['sett_email_ip']
            setting.sett_email_port = data['sett_email_port']
            setting.save()
            serializer = SettingSerializer(setting)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        setting = self.get_object(pk)
        if (setting is not None):
            serializer = SettingSerializer(setting)
            setting.delete()
            mydata = serializer.data
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
#endregion

def cleanString(myval):
    if(myval is not None):
        return str(myval).strip()
    else:
        return ""


#region macchine
'''
MACCHINE
Funzione di creazione e listing di tutti gli oggetti 
'''
@api_view(['GET', 'POST'])
def macchine_list(request, query=None):
    if request.method == 'GET':
        if (query is None):
            macchine = Macchine.objects.all()
            serializer = MacchinaListaSerializer(macchine, many=True)
            return Response(serializer.data)
        else:
            macchine = Macchine.objects.filter(mac_nomemacch__icontains=query)
            serializer = MacchinaListaSerializer(macchine, many=True)
            return Response(serializer.data)


@api_view(['GET', 'POST'])
def macchine_collection(request, query=None):
    if request.method == 'GET':
        if (query is None):
            macchine = Macchine.objects.all()
            serializer = MachineSerializer(macchine, many=True)
            macs = serializer.data
            return Response(macs)
        else:
            macchine = Macchine.objects.filter(mac_nomemacch__icontains=query)
            serializer = MachineSerializer(macchine, many=True)
            return Response(serializer.data)

    elif request.method == 'POST':

        proxyboolcamer = "0"
        if(request.data.get("MAC_boolCamera") == True):
            proxyboolcamer = "1"

        data = {
            'mac_attivo':1,
            'mac_ipadress': cleanString(request.data.get("MAC_ipAdress")),
            'mac_ipadress2': cleanString(request.data.get("MAC_ipAdress2")),
            'mac_nomemacch': cleanString(request.data.get("MAC_nomeMacch")),
            'mac_soprannomemacch': cleanString(request.data.get("MAC_soprannomeMacch")),
            'mac_tipomacchina': cleanString(request.data.get("MAC_tipoMacchina")),
            'mac_controllonumerico': cleanString(request.data.get("MAC_controlloNumerico")),
            'mac_tipocontrollo':cleanString(request.data.get("MAC_tipoControllo")),
            'mac_packegtipo': cleanString(request.data.get("MAC_packegtipo")),
            'mac_reparto':cleanString(request.data.get("MAC_reparto")),
            'mac_imagepath': cleanString(request.data.get("MAC_imagepath")),
            'mac_image':cleanString(request.data.get("MAC_image")),
            'mac_serialnumber':cleanString(request.data.get("MAC_serialNumber")),
            'mac_boolcamera': proxyboolcamer,
            'mac_ipadresscamera': cleanString(request.data.get("MAC_ipAdressCamera")),
            'mac_portacamera':cleanString(request.data.get("MAC_portaCamera")),
            'mac_comandocamera': cleanString(request.data.get("MAC_comandoCamera")),
            'mac_modecamera':cleanString(request.data.get("MAC_modeCamera")),
            'mac_usercamera': cleanString(request.data.get("MAC_userCamera")),
            'mac_passcamera': cleanString(request.data.get("MAC_passCamera")),
            'mac_userusername':cleanString(request.data.get("MAC_userUsername")),
            'mac_userpassword': cleanString(request.data.get("MAC_userPassword")),
            'mac_pathshare': cleanString(request.data.get("MAC_pathshare")),
            'mac_numeroassi': cleanString(request.data.get("MAC_numeroAssi")),
            'mac_assi': cleanString(request.data.get("MAC_assi")),
            'mac_vs_ser': request.data.get("MAC_vs_ser"),
        }
        if(data["mac_vs_ser"]==0):
            data["mac_vs_ser"] = None

        serializer = MachineSerializer(data=data)
        if serializer.is_valid():
            macchina = serializer.save()

            if (data['mac_reparto'] is not None and data['mac_reparto'] != 0):
                myReparto = Reparti.objects.get(rep_id=data['mac_reparto'])
                macchina.mac_reparto = myReparto
                macchina.save()

            for element in request.data["componenti"]:
                mycomps = Componenti.objects.get(pk=int(element["com_id"]))
                mycompsup = SeriemacchineVsComponenti()
                mycompsup.sermac_vs_com=mycomps
                mycompsup.sermac_vs_mac=macchina
                mycompsup.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print serializer.errors
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def macchine_interfaccia_collection(request, query=None):
    if request.method == 'GET':
        if(query is None):
            macchine = Macchine.objects.all()
            serializer = MachineSerializer(macchine, many=True)
            return Response(serializer.data)
        else:
            macchine = Macchine.objects.filter(mac_nomemacch__icontains=query)
            serializer = MachineSerializer(macchine, many=True)
            return Response(serializer.data)

@api_view(['POST'])
def macchine_activation(request, pk):
    try:
        if pk>0 :
            currentmachine=Macchine.objects.get(mac_id=pk)
            currentmachine.mac_attivo=not currentmachine.mac_attivo
            currentmachine.save()
            serializer = MacchinaListaSerializer(currentmachine)
            return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response({"no"},status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_types(request):
    try:
        types=Typemachine.objects.all().order_by('typ_controllonumerico')
        serializer=TypeSerializer(types , many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except:
        return Response({"no"},status=status.HTTP_400_BAD_REQUEST)



'''
MACCHINE
Funzione di update delete e get di un singolo oggetto 
'''
class MacchineDetail(APIView):

    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return Macchine.objects.get(mac_id=pk)
        except Macchine.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        macchina = self.get_object(pk)
        if(macchina is not None):
            data = model_to_dict(macchina)
            if(macchina.mac_reparto is not None):
                data["mac_reparto_desc"]=macchina.mac_reparto.rep_name
                data["mac_reparto_id"]=macchina.mac_reparto.rep_id

            myComponents = SeriemacchineVsComponenti.objects.filter(sermac_vs_mac=macchina)

            if (len(myComponents) > 0):
                myComponentsOut = []
                for comp in myComponents:
                    ## fields = ('com_id', 'com_nome', 'com_chiave', 'com_vita', 'com_codice', 'com_vs_ser', 'com_vs_mac')
                    compo = {"com_id": comp.sermac_vs_com.com_id, "com_nome": comp.sermac_vs_com.com_nome,
                             "com_vita": comp.sermac_vs_com.com_vita, "com_codice": comp.sermac_vs_com.com_codice,
                             "com_chiave": comp.sermac_vs_com.com_chiave}
                    myComponentsOut.append(compo)
                if(len(myComponentsOut)>0):
                    data["components"]= myComponentsOut
            data["mac_imagepath"]=getImgByName(macchina.mac_imagepath)
            ##serializer = MachineSerializer(instance=macchina)
            ##print serializer.validated_data
            return Response(data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        macchina = self.get_object(pk)

        data = {
                'mac_ipadress': cleanString(request.data["MAC_ipAdress"]),
                'mac_ipadress2':  cleanString(request.data["MAC_ipAdress2"]),
                'mac_nomemacch': cleanString( request.data["MAC_nomeMacch"]),
                'mac_soprannomemacch':  cleanString(request.data["MAC_soprannomeMacch"]),
                'mac_tipomacchina': request.data["MAC_tipoMacchina"],
                'mac_controllonumerico': cleanString( request.data["MAC_controlloNumerico"]),
                'mac_tipocontrollo': cleanString( request.data["MAC_tipoControllo"]),
                #'mac_packegtipo': request.data["MAC_packegtipo"],
                'mac_reparto':  request.data["MAC_reparto"],
                #'mac_image': request.data["MAC_image"],
                'mac_serialnumber':  cleanString(request.data["MAC_serialNumber"]),
                'mac_boolcamera':  cleanString(request.data["MAC_boolCamera"]),
                'mac_ipadresscamera':  cleanString(request.data["MAC_ipAdressCamera"]),
                'mac_portacamera': cleanString( request.data["MAC_portaCamera"]),
                'mac_comandocamera':  cleanString(request.data["MAC_comandoCamera"]),
                'mac_modecamera': cleanString( request.data["MAC_modeCamera"]),
                'mac_imagepath': cleanString( request.data.get("MAC_imagepath")),
                'mac_vs_ser':  request.data.get("MAC_vs_ser"),
                #'mac_usercamera': request.data["MAC_userCamera"],
                #'mac_passcamera': request.data["MAC_passCamera"],
                'mac_userusername': cleanString(request.data["MAC_userUsername"]),
                'mac_userpassword': cleanString(request.data["MAC_userPassword"]),
                'mac_pathshare': cleanString(request.data["MAC_pathshare"]),
                'mac_numeroassi': cleanString(request.data["MAC_numeroAssi"]),
                'mac_assi': cleanString(request.data["MAC_assi"]),


        }

        if (data["mac_vs_ser"] == 0):
            data["mac_vs_ser"] = None

        if (macchina is not None):
            macchina.mac_ipadress = data['mac_ipadress']
            macchina.mac_ipadress2 = data['mac_ipadress2']
            macchina.mac_tipomacchina = data['mac_tipomacchina']
            macchina.mac_controllonumerico = data['mac_controllonumerico']
            macchina.mac_soprannomemacch = data['mac_soprannomemacch']
            macchina.mac_nomemacch = data['mac_nomemacch']
            macchina.mac_tipocontrollo= data['mac_tipocontrollo']
            macchina.mac_numeroassi= data['mac_numeroassi']

            #macchina.mac_packegtipo = data['mac_packegtipo']
            macchina.mac_imagepath = data['mac_imagepath']
            macchina.mac_serialnumber = data['mac_serialnumber']
            macchina.mac_ipadresscamera = data['mac_ipadresscamera']
            macchina.mac_portacamera = data['mac_portacamera']
            macchina.mac_comandocamera = data['mac_comandocamera']
            #macchina.mac_usercamera = data['mac_usercamera']
            #macchina.mac_passcamera = data['mac_passcamera']
            macchina.mac_userpassword = data['mac_userpassword']
            macchina.mac_userusername = data['mac_userusername']
            macchina.mac_pathshare = data['mac_pathshare']
            macchina.mac_assi = data['mac_assi']


            if (data['mac_reparto'] is not None and data['mac_reparto'] != 0):
                myReparto = Reparti.objects.get(rep_id=data['mac_reparto'])
                macchina.mac_reparto = myReparto


            if(data["mac_vs_ser"] is not None and data["mac_vs_ser"] != 0  ):
                mySerie = Serie.objects.get(pk=data["mac_vs_ser"])
                macchina.mac_vs_ser = mySerie


            #macchina.mac_image = data['mac_image']
            macchina.mac_boolcamera = data['mac_boolcamera']
            macchina.mac_modecamera = data['mac_modecamera']
            #macchina.mac_usercamera = data['mac_usercamera']
            #macchina.mac_username = data['mac_userusername']
            macchina.save()
            serializer = MachineSerializer(macchina)

            myseriecomponent = SeriemacchineVsComponenti.objects.filter(sermac_vs_mac=macchina)
            for comp in myseriecomponent:
                comp.delete()

            for element in request.data["componenti"]:
                mycomps = Componenti.objects.get(pk=int(element["com_id"]))
                mycompsup = SeriemacchineVsComponenti()
                mycompsup.sermac_vs_com = mycomps
                mycompsup.sermac_vs_mac = macchina
                mycompsup.save()

            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        macchina = self.get_object(pk)
        if (macchina is not None):

            ## check se ci sono serie collegate

            allmySeries = SeriemacchineVsComponenti.objects.filter(sermac_vs_mac=macchina)
            for lilserie in allmySeries:
                lilserie.delete()

            ## check se ce sono cicloprod collegato
            allmyCicl = Cicloproduzione.objects.filter(cprod_vs_mac=macchina)
            for lilciclo in allmyCicl:
                lilciclo.delete()

            serializer = MachineSerializer(macchina)
            mydata = serializer.data
            macchina.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
#endregion

#region upload
'''
Classi per la gestione degli upload
'''
class FileUploadView(views.APIView):
    parser_classes = (FileUploadParser,)
    def put(self, request, filename=None, format=None):
        file_obj = request.data['file']
        #### caricato tutto
        print "caricato tutto"
        return Response({'aaarghhh':'bohohohoh'},status=200)

def getImgByName(name):
    try:
        with open(DjangoSettings.MEDIA_ROOT + name, "rb") as image_file:
            encoded_string = "data:image/png;base64," + b64encode(image_file.read())
            return encoded_string
    except:
        return ''


class MultiFileUploadView(views.APIView):
    parser_classes = (MultiPartParser,)
    def put(self, request, filename=None, format=None):
        file_obj = request.data['file']
        #### caricato tutto
        print "caricato tutto"
        fs = FileSystemStorage()
        filename = fs.save(request.data['file'].name, file_obj)

        uploaded_file_url = fs.url(filename)
        encoded_string=getImgByName(uploaded_file_url)
        # myResponse = {'file':uploaded_file_url}
        myResponse = {'name': uploaded_file_url, 'file':encoded_string}

        return Response(myResponse,status=200)
#endregion

#region gruppi
'''
Gestione Memebrship e diritti
'''
@api_view(['GET', 'POST', 'DELETE'])
def gruppi_collection(request):
    if request.method == 'GET':
        gruppi = Group.objects.all()
        serializer = GroupSerializer(gruppi, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = {'name': request.data.get("name")}
        serializer = GroupSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

        if (len(request.data["members"]) > 0):
            mygruppo = Group.objects.get(name=serializer.data["name"])
            for userId in request.data["members"]:
                try:
                    currentUser = None
                    if (type(userId) is int):
                        currentUser = User.objects.get(pk=userId)
                    else:
                        currentUser = User.objects.get(pk=userId["pk"])
                    currentUser.groups.add(mygruppo)
                except User.DoesNotExist:
                    print "no valid user"
            allUser = User.objects.filter(groups__name=mygruppo.name).values('pk', 'username')
            myGroupReport = {}
            myGroupReport["name"] = mygruppo.name
            myGroupReport["members"] = list(allUser)
            return HttpResponse(json.dumps(myGroupReport, cls=DjangoJSONEncoder), content_type='application/json')

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''
TODO refactoring oggetto Interfaccia
'''
class InterfacciaViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Interfaccia.objects.all()
        serializer = InterfacciaSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Interfaccia.objects.all()
        input = get_object_or_404(queryset, int_id=pk)
        serializer = InterfacciaSerializer(input)
        return Response(serializer.data)

'''
GRUPPI
Funzione di update delete e get di un singolo oggetto 
'''
class GruppiDetail(APIView):

    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, name):
        try:
            return Group.objects.get(name=name)
        except Group.DoesNotExist:
            return None
    def get(self, request, name, format=None):
        gruppo = self.get_object(name)
        if(gruppo is not None):
            data = model_to_dict(gruppo)
            return Response(data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    def post(self, request, name, format=None):
        gruppo = self.get_object(name)
        if (gruppo is not None):
            gruppo.name = request.data["name"]
            gruppo.save()
            if( len( request.data["members"] ) > 0 ):
                gruppo.user_set.clear()
                for userId in request.data["members"]:
                    ###
                    ### walkaround per la gestione anomala del componente vue
                    try:
                        currentUser = None
                        if(type(userId) is int):
                            currentUser = User.objects.get(pk=userId)
                        else:
                            currentUser = User.objects.get(pk=userId["pk"])
                        currentUser.groups.add(gruppo)
                    except User.DoesNotExist:
                        print "no valid user"

            allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            myGroupReport = {}
            myGroupReport["name"] = gruppo.name
            myGroupReport["members"] = list(allUser)
            return HttpResponse(json.dumps(myGroupReport, cls=DjangoJSONEncoder), content_type='application/json')
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    def delete(self, request, name, format=None):
        gruppo = self.get_object(name)
        if (gruppo is not None):
            serializer = GroupSerializer(gruppo)
            mydata = serializer.data
            gruppo.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

'''
Gestione Memebrship e diritti
'''
@api_view(['GET'])
def get_allGroupByUser(request):
    if request.method == 'GET':
        allGroups = list()
        gruppi = Group.objects.all()
        for gruppo in gruppi:
            allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            myGroupReport={}
            myGroupReport["name"] = gruppo.name
            myGroupReport["members"] = list(allUser)
            allGroups.append(myGroupReport)

        return HttpResponse(json.dumps(allGroups, cls=DjangoJSONEncoder), content_type='application/json')
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)

#endregion

#region user
'''
Gestione Memebrship e diritti
'''
@api_view(['GET', 'POST'])
def user_collection(request):
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = {'url': request.data.get("url"),'username': request.data.get("username"),'email': request.data.get("email"), 'is_staff': request.data.get("is_staff"), 'groups': request.data.get("groups")}
        serializer = UserSerializer(id=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def searchUserByName(request, name):
    if request.method == 'GET':
        if( name is not None  ):
            users = User.objects.filter(username__icontains=name).values('pk', 'username')
            # allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(list(users), cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def searchUserById(request, pk):
    if request.method == 'GET':
        if( pk is not None  ):
            userMap = {}
            users = User.objects.get(pk=pk)
            groups = list(users.groups.values())
            userMap = { "username":users.username , "first_name":users.first_name , "last_name":users.last_name , "groups":groups , 'admin':users.is_staff  }
            # allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(userMap, cls=DjangoJSONEncoder), content_type='application/json')

'''
USERS
Funzione di update delete e get di un singolo oggetto 
'''
class UserDetail(APIView):

    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            return None
    def get(self, request, pk, format=None):
        user = self.get_object(pk=pk)
        if(user is not None):
            data = model_to_dict(user)
            return Response(data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    def post(self, request, pk, format=None):
        user = self.get_object(pk=pk)
        if (user is not None):
            user.url = request.data["url"]
            user.username = request.data["username"]
            user.email = request.data["email"]
            user.is_staff = request.data["is_staff"]
            user.save()
            serializer = UserSerializer(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        user = self.get_object(pk=pk)
        if (user is not None):
            serializer = UserSerializer(user)
            mydata = serializer.data
            user.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
'''
Gestione dei gruppi
'''
@api_view(['GET', 'POST', 'DELETE'])
def user_attributes(request):
    if request.method == 'GET':
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        currentGroup = Group.objects.get(name=request.data.get("group_name"))
        user = User.objects.get(pk=request.data.get("id"))
        data = {'group': currentGroup,'user': user}
        serializer= GroupMemberSerializer(data)
        if serializer.is_valid():
            user.groups.add(currentGroup)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        currentGroup = Group.objects.get(name=request.data.get("group_name"))
        user = User.objects.get(pk=request.data.get("id"))
        data = {'group': currentGroup, 'user': user}
        serializer = GroupMemberSerializer(data)
        if serializer.is_valid():
            user.groups.remove(currentGroup)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#endregion

#region diritti
'''
Gestione Authorities
'''
@api_view(['POST', 'DELETE'])
def reparto_authentication(request):

    if request.method == 'POST':
        reparto = Reparti.objects.get(request.data.get("reparto"))
        gruppo = Group.objects.get(name=request.data.get("group_name"))

        data = {'reparto': reparto,'group': gruppo}
        serializer = RepartoOwnerSerializer(data)
        if serializer.is_valid():
            #serializer.save()
            assign_perm('view_reparto', gruppo, reparto)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        reparto = Reparti.objects.get(request.data.get("reparto"))
        gruppo = Group.objects.get(name=request.data.get("group_name"))
        data = {'reparto': reparto, 'group': gruppo}
        serializer = RepartoOwnerSerializer(data)
        if serializer.is_valid():
            # serializer.save()
            remove_perm('view_reparto', gruppo, reparto)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def searchGroupByName(request, name):
    if request.method == 'GET':
        if( name is not None  ):
            group = Group.objects.filter(name__icontains=name).values('name')
            # allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(list(group), cls=DjangoJSONEncoder), content_type='application/json')

#endregion

#region Input
'''
TODO refactoring oggetto Input
'''
class InputViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Input.objects.all()
        serializer = InputSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        if request.data['in_fase'] == '': request.data['in_fase'] = None
        if request.data['in_articolo'] == '': request.data['in_articolo'] = None
        myKeys = request.data.keys()
        if u'in_status' not in myKeys:
            request.data['in_status'] = 1
        serializers = InputSerializer(data=request.data)
        if(serializers.is_valid()):
            input = serializers.save()
            if (input.in_vs_cprod is None and u'in_vs_mac_id' in myKeys):
                cicloprod = Cicloproduzione()
                cicloprod.cprod_commessa = input.in_commessa
                cicloprod.cprod_lotto = input.in_lotto
                mymachine = Macchine.objects.get(pk=request.data["in_vs_mac_id"])
                cicloprod.cprod_vs_mac = mymachine
                cicloprod.cprod_status = 1
                cicloprod.cprod_pezzifatti = 0
                cicloprod.cprod_fase = input.in_fase
                cicloprod.cprod_articolo = input.in_articolo
                cicloprod.cprod_importfrom = input.in_importfrom
                print(input.in_programma)
                cicloprod.cprod_programma = input.in_programma
                cicloprod.save()
                input.in_vs_cprod = cicloprod
                input.in_status=1
        return Response(serializers.data)

    def retrieve(self, request, pk=None):
        queryset = Input.objects.all()
        input = get_object_or_404(queryset, in_id=pk)
        serializer = InputSerializer(input)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        #queryset = Input.objects.all()

        input = Input.objects.get(in_id=pk)
        cprod=input.in_vs_cprod
        myKeys = request.data.keys()
        for valueK in myKeys:
            if(hasattr(input, valueK)):
                setattr(input, valueK,request.data[valueK])
        if(input.in_vs_cprod is None and u'in_vs_mac_id' in myKeys ):
            cicloprod = Cicloproduzione()
            cicloprod.cprod_commessa = input.in_commessa
            cicloprod.cprod_lotto = input.in_lotto
            mymachine = Macchine.objects.get(pk=request.data["in_vs_mac_id"])
            cicloprod.cprod_vs_mac = mymachine
            cicloprod.cprod_fase = input.in_fase
            cicloprod.cprod_articolo = input.in_articolo
            cicloprod.cprod_importfrom = input.in_importfrom
            cicloprod.cprod_status = 1
            cicloprod.cprod_pezzifatti = 0
            cicloprod.cprod_programma = input.in_programma
            if (input.in_articolo is not None) and (input.in_articolo!=''):
                cicloprod.cprod_articolo=input.in_articolo
            if input.in_fase is not None and input.in_fase!='':
                cicloprod.cprod_fase=input.in_fase
            cicloprod.save()
            input.in_vs_cprod = cicloprod
        input.save()

        if (input.in_vs_cprod is None and cprod is not None ):
            try:
                cprod.delete()
            except:
                input.in_vs_cprod=cprod
                input.save()
                return Response(status=status.HTTP_400_BAD_REQUEST)
        serializer = InputSerializer(input)
        return Response(serializer.data)

    def delete(self, request, pk=None):
        queryset = Input.objects.all()
        input = get_object_or_404(queryset, pk=pk)
        serializer = InputSerializer(input)
        mydata = serializer.data
        input.delete()
        return Response(mydata, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        queryset = Input.objects.all()
        input = get_object_or_404(queryset, pk=pk)
        myKeys = request.data.keys()
        for valueK in myKeys:
            if (hasattr(input, valueK)):
                setattr(input, valueK, request.data[valueK])

        input.save()
        serializer = InputSerializer(input)
        return Response(serializer.data)


@api_view(['GET'])
def searchInputAndMachine(request):
    if request.method == 'GET':
        if( id is not None  ):
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM Macchine INNER JOIN Reparti ON MAC_reparto = REP_id LEFT OUTER JOIN Interfaccia ON MAC_Id = INT_vs_MAC_id")
                rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')


@api_view(['GET'])
def searchInputByMachine(request, id):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            if(int(id)!=0):
                cursor.execute(
                    "SELECT * FROM Input INNER JOIN Cicloproduzione ON IN_vs_CPROD_id = CPROD_id WHERE Cprod_status<>5 and CPROD_vs_MAC_id = " + id)
            else:
                cursor.execute(
                    "SELECT * FROM Input WHERE IN_vs_CPROD_id  IS NULL")
            rows = dictfetchall(cursor)
        # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
        return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')



#endregion

#region Interfaccia
'''
TODO refactoring oggetto Interfaccia
'''
class InterfacciaViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Interfaccia.objects.all()
        serializer = InterfacciaSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Interfaccia.objects.all()
        input = get_object_or_404(queryset, int_id=pk)
        serializer = InterfacciaSerializer(input)
        return Response(serializer.data)

@api_view(['GET'])
def searchInterfacciaByMachine(request, id):
    if request.method == 'GET':
        if( id is not None  ):

            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM Macchine INNER JOIN Reparti ON MAC_reparto = REP_id LEFT OUTER JOIN Interfaccia ON MAC_Id = INT_vs_MAC_id WHERE MAC_id = "+id+" AND INT_Attivo = 1 ORDER BY INT_date DESC ")
                rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def searchInterfacciaAndMachine(request):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM Macchine INNER JOIN Reparti ON MAC_reparto = REP_id LEFT OUTER JOIN Interfaccia ON MAC_Id = INT_vs_MAC_id WHERE INT_Attivo = 1 ")
            rows=dictfetchall(cursor)
        # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
        return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')
#endregion

#region Output
'''
TODO refactoring oggetto Output
'''
class OutputViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Output.objects.all()
        serializer = OutputSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Output.objects.all()
        input = get_object_or_404(queryset, out_id=pk)
        serializer = OutputSerializer(input)
        return Response(serializer.data)


@api_view(['GET'])
def searchOutputByMachine(request, id):
    if request.method == 'GET':
        if( id is not None  ):
            with connection.cursor() as cursor:
                if (int (id) != 0):
                    commesse = Output.objects.filter(out_vs_cprod__cprod_vs_mac_id=id).filter(out_vs_cprod__cprod_status=5).order_by('-out_data')
                    # cursor.execute("SELECT * FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id WHERE CPROD_status=5 AND CPROD_vs_MAC_id = "+id+" ORDER BY OUT_data desc")
                else:
                    commesse = Output.objects.filter(out_vs_cprod__cprod_status=5).order_by('-out_data')
                    #cursor.execute("SELECT * FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id  INNER JOIN Macchine ON CPROD_vs_MAC_id=MAC_id WHERE CPROD_status=5  ORDER BY OUT_data desc")
                serializer = OutputComplSerializer(commesse, many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
            # rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            # return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def searchOutputByName(request, name):
    if request.method == 'GET':
        if( name is not None  ):
            #commesse=Output.objects.all()

            commesse = Output.objects.filter(out_vs_cprod__cprod_status=5, out_vs_cprod__cprod_vs_mac__mac_nomemacch__icontains=name).order_by('-out_data')
            serializer=OutputComplSerializer(commesse, many=True)
            # allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
def searchOutputAndMachine(request):
    if request.method == 'GET':
        commesse = Output.objects.filter(out_vs_cprod__cprod_status=5)
        serializer = OutputComplSerializer(commesse, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM Output INNER JOIN Cicloproduzione ON OUT_vs_CPROD_id = CPROD_id INNER JOIN Macchine ON CPROD_vs_MAC_id = MAC_id")
            rows=dictfetchall(cursor)
        # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
        return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')
#endregion

#region Produzione
'''
TODO refactoring oggetto Produzione
'''

@api_view(['GET'])
def MacchineAttive(request):
    if request.method == 'GET':
        macchine = Macchine.objects.filter(mac_attivo=True)
        serializer = MachineSerializer(macchine, many=True)
        return Response(serializer.data)


class ProduzioneViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = Produzione.objects.all()
        serializer = ProduzioneSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Produzione.objects.all()
        prod = get_object_or_404(queryset, pk=pk)
        serializer = ProduzioneSerializer(prod)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        queryset = Produzione.objects.all()
        prod = get_object_or_404(queryset, pk=pk)
        serializer = ProduzioneSerializer(prod)
        print("PARTIAL", request.data)
        return Response(serializer.data)

    def update(self, request, pk=None):
        queryset = Produzione.objects.all()
        prod = get_object_or_404(queryset, pk=pk)
        print("UPDATE", request.data)
        serializer = ProduzioneSerializer(prod)
        return Response(serializer.data)

@api_view(['GET'])
def searchProduzioneByMachine(request, id):
    if request.method == 'GET':
        if( id is not None  ):
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT * FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id INNER JOIN Macchine ON CPROD_vs_MAC_id = MAC_id WHERE CPROD_vs_MAC_id = "+id)
                rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def searchProduzioneByCommessa(request,id, com):
    if request.method == 'GET':
        if( id is not None):
            mymac=Macchine.objects.get(pk=id)
            if mymac.mac_controllonumerico!="Fagor" and mymac.mac_controllonumerico!="Heidenhain":
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT * FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_commessa = "+com+" AND CPROD_vs_MAC_id ="+id)
                    rows = dictfetchall(cursor)
            else:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT * FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id WHERE CPROD_id = "+com+" AND CPROD_vs_MAC_id ="+id)
                    rows = dictfetchall(cursor)
                # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def searchProduzioneAndMachine(request):
    if request.method == 'GET':
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM Produzione INNER JOIN Cicloproduzione ON PROD_vs_CPROD_id = CPROD_id INNER JOIN Macchine ON CPROD_vs_MAC_id = MAC_id")
            rows=dictfetchall(cursor)
        # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
        return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

#endregion

#region MANUTENZIONE
@api_view(['GET'])
def ManKeyByName(request, name):
    if request.method == 'GET':
        if (name is not None):
            chiavi = ManChiavi.objects.filter(name__icontains=name).values('name')
            # allUser = User.objects.filter(groups__name=gruppo.name).values('pk', 'username')
            return HttpResponse(json.dumps(list(chiavi), cls=DjangoJSONEncoder), content_type='application/json')

@api_view(['GET'])
def ManKey(request):
    if request.method == 'GET':
        chiavi = ManChiavi.objects.all()
        serializer = ManChiaviSerializer(chiavi, many=True)
        return Response(serializer.data)
#endregion


#region Componenti
@api_view(['GET', 'POST'])
def componenti_collection(request, query=None):
    if request.method == 'GET':
        if(query is None):
            components = Componenti.objects.all()
        else:
            components = Componenti.objects.filter(com_nome__icontains=query)
        serializer = ComponentiSerializer(components, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        data = {'com_nome': str(request.data.get("com_nome")).strip(), 'com_chiave': str(request.data.get("com_chiave")).strip(),
                'com_vita': str(request.data.get("com_vita")).strip(), 'com_codice': str(request.data.get("com_codice")).strip(),
                'com_unita':str(request.data.get("com_unita")).strip()}
        mykey = ManChiavi.objects.get(pk=data['com_chiave'])
        data['com_unita']= str(mykey.key_unita)
        data['com_chiave'] = str(mykey.key_value)
        data['com_unita'] = str(mykey.key_unita)
        serializer = ComponentiSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

### associazione identifica macchina o serial
### id dell'associazione da cui si parte ovvero queli sono i componenti con codice che si disponibili
@api_view(['GET'])
def search_free_components(request,query,fromid=None,fromassociazione=None):
    if request.method == 'GET':
        if(query != ""):
            if(fromid is None and fromassociazione is None):
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_com__com_nome__icontains=query,sermac_vs_ser=None,sermac_vs_mac_id=None)
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)

                return Response(serializer.data)
            elif(fromassociazione == 'macchina' and fromid is not None ):
                searchMachine = Macchine.objects.get(pk=fromid)
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_com__com_nome__icontains=query,
                                                                          sermac_vs_ser=None, sermac_vs_mac_id=searchMachine)
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)
                return Response(serializer.data)
            elif fromid is not None :
                searchSerie = Serie.objects.get(pk=fromid)
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_com__com_nome__icontains=query,
                                                                          sermac_vs_ser=searchSerie, sermac_vs_mac_id=None)
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)
                return Response(serializer.data)

@api_view(['GET'])
def componenti_bymachine(request, pk):
        if(pk>0):
            with connection.cursor() as cursor:
                cursor.execute("select COM_id as com_id, COM_nome as com_nome , COM_chiave as com_chiave, COM_vita as com_vita , COM_codice as com_codice, COM_unita as com_unita  FROM Macchine LEFT OUTER JOIN SerieMacchine_vs_Componenti ON ( SERMAC_vs_MAC_id = MAC_id OR MAC_vs_SER_id = SERMAC_vs_SER_id  ) INNER JOIN Componenti ON ( SERMAC_vs_COM_id = COM_id ) WHERE MAC_id = "+pk)
                rows = dictfetchall(cursor)
            # allUser = User.object = Macchine.objects.rs.filter(groups__name=gruppo.name).values('pk', 'username')
        return HttpResponse(json.dumps(rows, cls=DjangoJSONEncoder), content_type='application/json')

##@api_view(['GET'])
##def componenti_bymachine(request, idmachine):
##    if request.method == 'GET':
##        if(idmachine>0)


#########################################
## ottieni tutti i componenti disponibili
## o i componenti associati
## - request
## - stato
#       0, non associato
#       >0 id
#  - associazione FK colonna
#########################################
@api_view(['GET'])
def get_componenti_bystatus(request,stato=None,associazione=None):
    if request.method == 'GET':
        if(stato is None or stato == '0' ):
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=None,sermac_vs_mac_id=None )
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)
        else:
            if(associazione == 'macchina'):
                mymacchine = Macchine.objects.get(pk=stato)
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=None, sermac_vs_mac_id=mymacchine)
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)
            else:
                myserie = Serie.objects.get(pk=stato)
                componentsFree = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=myserie, sermac_vs_mac_id=None)
                serializer = ComponetMacchinaSerialeSerializer(componentsFree, many=True)
        return Response(serializer.data)

class ComponentDetail(APIView):
    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return Componenti.objects.get(com_id=pk)
        except Componenti.DoesNotExist:
            return None

    def get_object_pk(self, pk):
        try:
            return Componenti.objects.get(com_id=pk)
        except Componenti.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        compoonente = self.get_object(pk)
        if(compoonente is not None):
            serializer = ComponentiSerializer(compoonente)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        componente = self.get_object_pk(pk)
        data = {'com_nome': str(request.data.get("com_nome")).strip(), 'com_chiave': int(str(request.data.get("com_chiave")).strip()),
                'com_vita': str(request.data.get("com_vita")).strip(), 'com_codice': str(request.data.get("com_codice")).strip(),
                'com_vs_ser': request.data.get("com_vs_ser"), 'com_vs_mac': request.data.get("com_vs_mac"), 'com_unita': str(request.data.get("com_unita")).strip()}
        mykey = ManChiavi.objects.get(pk=data['com_chiave'])
        print(mykey.key_unita)
        data['com_unita'] = mykey.key_unita
        data['com_chiave'] = mykey.key_value
        if (componente is not None):
            componente.com_nome = data['com_nome']
            componente.com_vita = data['com_vita']
            componente.com_chiave = data['com_chiave']
            componente.com_codice = data['com_codice']
            #componente.com_unita = data['com_unita']
            componente.save()
            serializer = ComponentiSerializer(componente)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        componente = self.get_object_pk(pk)

        myseriecomponent = SeriemacchineVsComponenti.objects.filter(sermac_vs_com=componente)
        myseriecomponent.delete()

        if (componente is not None):
            serializer = ComponentiSerializer(componente)
            mydata = serializer.data
            componente.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

#endregion

#region Series
@api_view(['GET', 'POST'])
def series_collection(request, query=None):
    if request.method == 'GET':
        if(query is None):
            series = Serie.objects.all()
            serializer = SerieSerializer(series, many=True)
            return Response(serializer.data)
        else:
            series = Serie.objects.filter(ser_nome__icontains=query)
            serializer = SerieSerializer(series, many=True)
            return Response(serializer.data)

    elif request.method == 'POST':
        data = {'ser_nome': request.data.get("ser_nome"), 'ser_costruttore': request.data.get("ser_costruttore")}
        serializer = SerieSerializer(data=data)
        if serializer.is_valid():
            serie = serializer.save()
            ##print(request.data["ser_components"])
            ##elimino tutti i componenti associati alla macchina sbagliata

            for element in request.data["ser_components"]:
                mycomps = Componenti.objects.get(pk=int(element["com_id"]))
                mycompsup = SeriemacchineVsComponenti()
                mycompsup.sermac_vs_com=mycomps
                mycompsup.sermac_vs_ser=serie
                mycompsup.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

'''
SERIES
Funzione di update delete e get di un singolo oggetto 
'''
class SeriesDetail(APIView):
    """
    Retrieve, update or delete a event instance.
    """
    def get_object(self, pk):
        try:
            return Serie.objects.get(ser_id=pk)
        except Serie.DoesNotExist:
            return None

    def get_object_pk(self, pk):
        try:
            return Serie.objects.get(ser_id=pk)
        except Serie.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        serie = self.get_object(pk)
        if(serie is not None):
            serializer = SerieSerializer(serie)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        serie = self.get_object_pk(pk)
        data = {'ser_nome': request.data.get("ser_nome"), 'ser_costruttore': request.data.get("ser_costruttore")}
        if (serie is not None):
            serie.ser_nome = data['ser_nome']
            serie.ser_costruttore = data['ser_costruttore']
            serie.save()

            myseriecomponent = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=serie)
            for comp in myseriecomponent:
                comp.delete()

            for element in request.data["ser_components"]:
                mycomps = Componenti.objects.get(pk=int(element["com_id"]))
                mycompsup = SeriemacchineVsComponenti()
                mycompsup.sermac_vs_com = mycomps
                mycompsup.sermac_vs_ser = serie
                mycompsup.save()

            serializer = SerieSerializer(serie)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        serie = self.get_object_pk(pk)
        if (serie is not None):
            serializer = SerieSerializer(serie)
            mydata=serializer.data
            myseriecomponent = SeriemacchineVsComponenti.objects.filter(sermac_vs_ser=serie)
            for comp in myseriecomponent:
                comp.delete()
            serie.delete()
            return Response(mydata,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
#endregion

#region FS METHOD
''' 
Methodo per interrocare il FS remoto
'''

@api_view(['GET'])
def comnnect_machinesmb(request,stato=None):
    if request.method == 'GET':
        data = {}
        data["status"] = "ko"
        data["message"] = "Servizio non disponibile"
        try:
            myidmacchina = request.GET.get('idmacchina', '')
            if(myidmacchina != ""):
                holamachine = Macchine.objects.get(pk=myidmacchina)
                if holamachine.mac_controllonumerico=='Fanuc':
                    data["message"] = "Impossibile contattare il servizio, riprovare"
                    #socket con il servizio
                    bool,a=data_from_mac(myidmacchina,'1')
                    print bool
                    a=a.replace("'","\"")
                    if bool:
                        tree=json.loads(a)
                        return HttpResponse(json.dumps(tree, cls=DjangoJSONEncoder), content_type='application/json')
                    else:
                        data['message'] = a
                        return Response(data, status=503)
                else:
                    fsExplorer = FilesSystemhelper()
                    ### attingere dall'id Macchina passato in get
                    fsExplorer.connect(holamachine.mac_ipadress, holamachine.mac_userusername, holamachine.mac_userpassword, holamachine.mac_pathshare)
                    tree=fsExplorer.getFsMyTree()
                    return HttpResponse(json.dumps(tree, cls=DjangoJSONEncoder), content_type='application/json')
            data["message"] = "Macchina non riconosciuta, riprovare"
            return HttpResponse(json.dumps(data, cls=DjangoJSONEncoder), content_type='application/json')
        except:
            return Response(data, status=400)

from django.http import HttpResponse
@api_view(['GET'])
def downloadFromMac(request):
    if request.method == 'GET':
        data = {'status':'ko', 'message': 'Errore di comunicazione, riprovare'}
        try:
            filepath = request.GET.get('path', '')
            filename = request.GET.get('name', '')
            myidmacchina = request.GET.get('myidmacchina', '')
            if (myidmacchina != ""):
                holamachine = Macchine.objects.get(pk=myidmacchina)
                if holamachine.mac_controllonumerico == 'Fanuc':
                    try:
                        if(filepath != ""):
                            bool,testo=data_from_mac(filepath,'3')
                            if bool:
                                ret={'file':testo, 'name':filename}
                                return Response(ret, status=status.HTTP_200_OK)
                            else:
                                data["message"]= testo
                                return Response(data, status=400)
                    except:
                        return Response(data, status=400)
                else:
                    fsExplorer = FilesSystemhelper()
                    fsExplorer.connect(holamachine.mac_ipadress, holamachine.mac_userusername, holamachine.mac_userpassword, holamachine.mac_pathshare)
                    fs = FileSystemStorage()
                    dirpath=fsExplorer.get_dir(filepath)
                    content=fsExplorer.readfile(dirpath, filename)
                    ret={'file':content, 'name':filename}
                    return Response(ret, status=status.HTTP_200_OK)
            data["message"]='Macchina non riconosciuta'
            return Response(data, status=400)
        except:
            return Response(data, status=400)


'''
collegamento al servizio via socket. Protocol
1: lettura path
2: upload file
3: download file
'''
from django.core.files import File
from struct import calcsize
def data_from_mac(input, protocol, add_input=""):
    try:
        import time
        import socket

        HOST = 'localhost'  # The remote host
        PORT = 50007  # The same port as used by the server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST, PORT))
        s.sendall(protocol)
        time.sleep(.5)
        lun = str(len(bytearray(input, 'utf8')))
        s.sendall(lun)
        time.sleep(.5)
        s.sendall(input)
        if add_input!="":
            lun = str(len(bytearray(add_input, 'utf8')))
            s.sendall(lun)
            time.sleep(.5)
            s.sendall(add_input)
        bool=s.recv(64)
        lenr=s.recv(128)
        #print("lunghezza ricevuta "+len)
        data=s.recv(int(lenr))
        if protocol=='1':
            ''.join(data.splitlines())
        #print(data)
        s.close()
        return (bool=='0',data)
    except:
        return (False, "impossibile contattare il servizio, provare a riavviare")


class SmbFileUploadView(views.APIView):
    #parser_classes = (MultiPartParser,)
    def put(self, request, filename=None, format=None):
        data = {}
        data ["status"]= "ko"

        try:
            file_obj = request.data['file']
            if(file_obj.name.split('.')[-1] in ["exe","msi","bat"]):
                data["status"] = "ko"
                data["message"] = "Estensione non valida (exe, msi, bat)"
                return Response(json.dumps(data, cls=DjangoJSONEncoder), status=status.HTTP_204_NO_CONTENT)
            dirtoupload = request.data['dirtoupload']
            myidmacchina = request.data['idmachine']
            if (myidmacchina != ""):
                files = FileSystemStorage()
                filename = files.save(request.data['file'].name, file_obj)
                uploaded_file_url = files.path(filename)
                filename = uploaded_file_url.split(os.sep)[-1]
                holamachine = Macchine.objects.get(pk=myidmacchina)
                if holamachine.mac_controllonumerico == 'Fanuc':
                    data["message"] = "Errore di comunicazione con il servizio, riprovare"
                    with open(files.base_location+"\\"+filename) as f:
                        content = f.read()
                        bool,response=data_from_mac(dirtoupload, '2', content)
                        data["message"] = response
                        if bool:
                            data["status"] = "ok"
                            return Response(json.dumps(data, cls=DjangoJSONEncoder), status=200)
                        else:
                            return Response(json.dumps(data, cls=DjangoJSONEncoder), status=400)
                else:
                    fsExplorer = FilesSystemhelper()
                    fsExplorer.connect(holamachine.mac_ipadress, holamachine.mac_userusername, holamachine.mac_userpassword, holamachine.mac_pathshare)
                    fsExplorer.copyfiles(files.base_location, filename, dirtoupload)
                    myResponse = {'file':uploaded_file_url}
                    data["status"] = "ok"
                    data["message"] = "file caricato correttamente"
                    return Response(json.dumps(data, cls=DjangoJSONEncoder),status=200)
            data["message"] = 'Macchina non riconosciuta'
            return Response(json.dumps(data, cls=DjangoJSONEncoder), status=400)
        except:
            return Response(json.dumps(data, cls=DjangoJSONEncoder), status=400)

class ctrlPermessi(APIView):
    def post(self, request, format=None):
        ritorno = {}
        ritorno['esito'] = 1
        codicepermesso = request.POST['codpermesso']
        ritorno['autorizzazione'] = request.user.has_perm(codicepermesso)
        return Response(ritorno)

class ctrlPermessiStd(APIView):
    def post(self, request, format=None):
        ritorno = {}
        ritorno['esito'] = 1
        nomeModello = request.POST['nomeModello']
        ritorno['perm'] = {};
        ritorno['perm']['view'] = request.user.has_perm('view_'+str(nomeModello))
        ritorno['perm']['change'] = request.user.has_perm('change_' + str(nomeModello))
        ritorno['perm']['add'] = request.user.has_perm('add_' + nomeModello)
        ritorno['perm']['delete'] = request.user.has_perm('delete_' + nomeModello)
        return Response(ritorno)

#endregion

