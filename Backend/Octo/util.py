import fs
import fs.smbfs
import fs.osfs
import fs.copy
import fs.path
from fs.walk import Walker
from os import path
import fs.smbfs
import os
import json

#region DB util
def dictfetchall(cursor):
    #"Return all rows from a cursor as a dict"
    columns = [("".join(col[0])).lower() for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
#endregion

class FilesSystemhelper:

    _smb_fs = None
    _user = None
    _password = None
    _starthost = None

    def file_to_dict(self, fpath):
        return {
            'name': fpath.split("/")[-1],
            'type': 'file',
            'key': fpath,
            'tag': 'org',
            'icon': 'description',
            'color': '#FFFFFF',
        }

    def folder_to_dict(self,rootpath,root=None):
        if(root is None):
            return {
                'name': rootpath.split("/")[-1],
                'type': 'folder',
                'key': rootpath,
                'tag': 'org',
                'icon': 'folder',
                'color': 'transparent',
                'children': []
            }
        else:
            return {
                'name': "Radice",
                'type': 'folder',
                'key': "/",
                'color': 'transparent',
                'tag': 'org',
                'icon': 'folder',
                'children': [],
                'root': True
            }

    def tree_to_dict(self, rootpath):
        #print self._starthost
        #print rootpath
        #currenfs = fs.open_fs('smb://'+self._user+':'+self._password+'@'+self._starthost+rootpath+'?direct_tcp=False&name_port=137')
        root_dict = FilesSystemhelper.folder_to_dict(self, rootpath)
        root_dict['children'] = [FilesSystemhelper.file_to_dict(self,fpath) for fpath in self._smb_fs.walk.files( rootpath,max_depth=1)]
        root_dict['children'] += [FilesSystemhelper.tree_to_dict(self,folder) for folder in self._smb_fs.walk.dirs( rootpath,max_depth=1)]
        return root_dict

    def tree_to_json(self, rootdir, pretty_print=True):
        ### this elemnt
        root_dict = self.folder_to_dict("", True)
        root_dict['children'] = [FilesSystemhelper.tree_to_dict(self,folder) for folder in self._smb_fs.walk.dirs( rootdir,max_depth=1)]
        root_dict['children'] += [FilesSystemhelper.file_to_dict(self,fpath) for fpath in self._smb_fs.walk.files( rootdir,max_depth=1)]
        return root_dict

    def connect(self, host, username, password,path):
        self._smb_fs = fs.open_fs('smb://'+username+':'+password+'@'+host+':139/'+path+'?direct_tcp=False&name_port=137')
        self._user = username
        self._password = password
        self._starthost = host+':139/'+path
        self._startdir = '//'+host+'/'+path

    def copyfiles(self,startpath,file, to):
        home_fs = fs.osfs.OSFS(startpath)
        fs.copy.copy_file(home_fs,file,self._smb_fs,to+"/"+file)

    def get_dir(self, startpath):
        return fs.path.dirname(startpath)

    def readfile(self,startpath, file):
        home_fs = fs.osfs.OSFS(self._startdir+startpath)
        path=fs.path.abspath(file)
        with home_fs.open(path, 'r') as f:
            return f.read()
            

    def getFsMyTree(self):
        if (self._smb_fs is not None):
            return self.tree_to_json("/", False)

    def connectionstatus(self):
        return self._smb_fs is not None
