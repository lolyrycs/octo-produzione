import Vue from 'vue'
import Router from 'vue-router'
import store  from '../store'
import globalAxios from 'axios'

const routerOptions = [
  { path: '/', component: 'Landing' },
  { path: '/myhome', component: 'Home', beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/signup', component: 'Signup' },
  { path: '/signin', component: 'Signin' },

  { path: '/createmachine', component: 'CreateMachine' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/machine/:idtoedit', component: 'Machine' , props: true, beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},

  { path: '/usergroup', component: 'RepartiMembership' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/reparti', component: 'Reparti' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/statisticheproduzione', component: 'NotFound' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/statistichemonitoring', component: 'SMonitoring' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/produzione', component: 'Produzione' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/produzionemachine/:idmmachine', component: 'MachineDetails' , props: true, beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/signin', component: 'Signin' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/allmachine', component: 'Allmachine' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/setting', component: 'Setting' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated){
          next()
    }else{
          next('/signup')
    }
  }},
  { path: '/series', component: 'Series' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated ){
      globalAxios.defaults.headers.common["Authorization"] = 'JWT '+store.getters.getMyToken
      let bodyFormData_view = new FormData();
      bodyFormData_view.append('codpermesso', 'Octo.view_serie');
      globalAxios.post('/ctrlPermessi/', bodyFormData_view)
        .then((res)=>{
          if(res.data.autorizzazione) next()
          else next('/404')
        }).catch(error=>{
          console.log(error)
          next('/404')
        })
    }else{next('/signup')}
  }},
  { path: '/components', component: 'Components' , beforeEnter(to, from, next){
    if(store.getters.isAuthenticated ){
      globalAxios.defaults.headers.common["Authorization"] = 'JWT '+store.getters.getMyToken
      let bodyFormData_view = new FormData();
      bodyFormData_view.append('codpermesso', 'Octo.view_componenti');
      globalAxios.post('/ctrlPermessi/', bodyFormData_view)
        .then((res)=>{
          if(res.data.autorizzazione) next()
          else next('/404')
        }).catch(error=>{
          console.log(error)
          next('/404')
        })
    }else{next('/signup')}
  }},
  { path: '*', component: 'NotFound' , name:"404" }
]

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`)
  }
})
Vue.use(Router)
export default new Router({
  routes,
  mode: 'history'
})
