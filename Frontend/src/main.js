import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import Vuetify from 'vuetify'
import axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
import Bars from 'vuebars'

import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.css'


axios.defaults.baseURL = 'http://'+location.host.split(':')[0]+':8000';
axios.defaults.headers.get['Accept'] = 'application/json';

Vue.use(Vuetify);
Vue.use(vue2Dropzone);
Vue.use(Bars);

Vue.config.productionTip = false;
/* eslint-disable no-new */

/* conviene gestire l'aoutlogin prima della creazione dell'istanza di VUE facendo così
  * lo stato rimane persistente
   * **/
store.dispatch('tryAutoLogin').then(() => {
  new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
});
