import axios from 'axios'
import store from './store'
const instance = axios.create({
    baseURL : 'http://localhost:8000/'
})

instance.interceptors.response.use((response) => {
    return response;
}, function (error) {
    // Do something with response error

    if (error.response.status === 400) {
        console.log('unauthorized, logging out ...');
        store.dispatch('storeErrors', 'Wrong authentication')
    }
    return Promise.reject(error.response);
});

//instance.defaults.headers.common['SOMETHING'] = 'something'
export default instance
