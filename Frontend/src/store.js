import Vue from 'vue'
import Vuex from 'vuex'

import axios from "./axios-auth"
import globalAxios from 'axios'
import router  from './router'

import * as moment from 'moment'
import {fetch as fetchPolyfill} from 'whatwg-fetch'
import it from 'moment/locale/it'

moment.locale('it')

Vue.use(Vuex)

function parseJwt (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
}

export default new Vuex.Store({
  state: {
    idtoken:null,
    userId:null,
    user:null,
    errorMsg:'',
    mediapath:'',
    ip:"",
    port:"",
    myurl:'',
    myport:'',
    app_version:"",
    service_version:"",
    web_version:""
  },
  mutations: {
    authUser(state, userData){
      state.idtoken= userData.token
      state.userId = userData.userId
    },
    storeUser(state, user){
      state.user = user
    },
    storeError(state, errorMsg){
      state.errorMsg = errorMsg
    },
    clearAuthdata(state){
      state.idtoken= null
      state.userId = null
      state.user = null
      errorMsg:''
    },
    storeMediaPath(state, mediapath, ip , port, app_version, service_version, web_version){
      state.mediapath = mediapath
      state.port = port
      state.ip = ip
      state.app_version = app_version
      state.service_version = service_version
      state.web_version = web_version
    }
  },
  actions: {
    tryAutoLogin({commit, dispatch}){
        const  token = localStorage.getItem('token')
        if(!token){
          return
        }
        const expirationDate = new Date(localStorage.getItem('expirationDate'))
        const now = new Date()
        if(now >= expirationDate){
          return
        }

        const userId = localStorage.getItem('userId')
        const user = localStorage.getItem('user')
        commit('authUser', { token: token, userId:userId })
        try {
          commit('storeUser', JSON.parse(user))
        }
        catch (Exception)  {
          dispatch("logout")
        }
        dispatch('setLogoutTimer',expirationDate.getTime()-now.getTime())

    },
    setLogoutTimer({commit}, expirationTime){
      /* Faccio scadere la sessione con un timeout */
      setTimeout(()=>{
        commit('clearAuthdata')
        router.replace('/signin')
      }, expirationTime)
    },
    async signup({commit, dispatch}, authData){
      await axios.post('/registrazione/', { username:authData.username, password:authData.password, first_name:authData.first_name, last_name:authData.last_name  })
        .then(res=> {
          axios.post('/api-token-auth/', { username:authData.username, password:authData.password }).then(res=> {
            const jwt =  parseJwt(res.data.token)
            commit('authUser', { token:res.data.token, userId:jwt.user_id  })
            const nowOld = new Date("01/01/1970")
            const expirationDate = new Date(nowOld.getTime()+jwt.exp*1000+60*60*1000)
            localStorage.setItem('token', res.data.token)
            localStorage.setItem('expirationDate', expirationDate)
            localStorage.setItem('userId', jwt.user_id)

            dispatch('fetchUser')

            const now = new Date()
            const dateGap = expirationDate.getTime() - now.getTime();
            dispatch('setLogoutTimer', dateGap)
            router.replace('/myhome')
          }).catch( error=> {
            commit('storeError', 'Something  is going  wrong')}
          )
        }).catch(error=> {commit('storeError', 'Something  is going  wrong')});
    },
    async login({commit, dispatch}, authData){
      await axios.post('/api-token-auth/', { username:authData.email, password:authData.password }).then(res=> {
        const jwt =  parseJwt(res.data.token)
        //console.log(jwt.user_id)
        commit('authUser', { token:res.data.token, userId:jwt.user_id  })
        // mi imposto la data di scadenza
        const nowOld = new Date("01/01/1970")
        const expirationDate = new Date(nowOld.getTime()+jwt.exp*1000+60*60*1000)
        //console.log(expirationDate);
        localStorage.setItem('token', res.data.token)
        localStorage.setItem('expirationDate', expirationDate)
        localStorage.setItem('userId', jwt.user_id)

        const now = new Date()
        const dateGap = expirationDate.getTime() - now.getTime();
        //console.log(dateGap/1000);
        //differenza di data

        // imposto la richiesta dello user

        dispatch('fetchUser')
        // imposto l'outologout
        dispatch('setLogoutTimer', dateGap)
      })
        .catch( error=> {
          //console.log("CATCHHHHHH!!!!!")
          commit('storeError', 'Something  is going  wrong')}
        )
    },
    storeErrors({commit},myerror){
      commit('storeError', myerror)
    },
    fetchUser({commit, state}){
      if(!state.idtoken || !state.userId ){
        return
      }
      globalAxios.defaults.headers.common["Authorization"] = 'JWT '+state.idtoken
      globalAxios.get('/user/'+state.userId+"/")
        .then(res => {console.log(res)
          const data = res.data
          localStorage.setItem('user', JSON.stringify(data))
          commit('storeUser', data)
          router.replace('/myhome')
        }).catch(error=>{console.log(error)})

    },
    logout({commit}){
      commit("clearAuthdata")
      router.replace('/signin')
      localStorage.removeItem('expirationDate')
      localStorage.removeItem('token')
      localStorage.removeItem('userId')
      localStorage.removeItem('user')
    }
  },
  getters: {
    user(state){
      return state.user
    },
    isAuthenticated (state){
      return state.idtoken !== null && state.user !== null
    },
    getMyToken(state)
    {
      return state.idtoken
    },
    errors(state){
      return state.errorMsg
    },
    mediapath(state){
      return state.mediapath
    },
    ip(state){
      return state.ip
    },
    port(state){
      return state.port
    },
    // app_version, service_version, web_version
    app_version(state){
      return state.app_version
    },
    service_version(state){
      return state.service_version
    },
    web_version(state){
      return state.web_version
    }
  }
})

